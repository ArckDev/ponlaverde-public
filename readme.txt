#######################################################################################################################
#################################################  PRIMER DESPLIEGUE  #################################################
#######################################################################################################################

Al tratarse del primer despliegue tenemos que construir la base de datos así como realizar toda la configuración.


PASOS PARA EL DESPLIEGUE

Para construir la base de datos lo mejor es utilizar la autoconfiguración de Spring-boot. Para ello, tenemos que seguir 
los siguientes pasos:

	1- Comprobar que la base de datos es MySQL (si no lo fuera, habría que actualizar las dependencias del POM y añadir el conector a la BD apropiada).
	
	2- Colocar el WAR en la capeta webapps. Esto iniciará automáticamente la descompactación del archivo.
	
	3- Entrar en la carpeta /ponlaverde/WEB-INF/classes.
	
	4- Comprobar que se encuentran los siguientes archivos:
		a. application.properties
		b. application-prod.properties
		c. data.sql
		d. logback-spring.xml
	Si alguno de estos archivos no se encuentra, no se puede realizar el despliegue.
	
	5- En el archivo logback-spring.xml, realizar la configuración hacia la carpeta dónde queremos guardar los logs:
		<springProfile name="prod">
		        <property name="DIR_LOGS" value="{{RUTA_ABSOLUTA_AL_DIRECTORIO_DE_LOGS}}" />
		</springProfile>
	
	6- En el archivo application.properties, la configuración debe apuntar al perfil de producción:
		spring.profiles.active=prod
	
	7- En el archivo application-prod.properties, hay que actualizar los parámetros existentes para apuntar hacia la base de datos correcta:
		#DB SETTINGS MYSQL
		spring.datasource.url=jdbc:mysql://{{URL_BD}}/PONLAVERDE
		spring.datasource.username={{USER}}
		spring.datasource.password={{PASS}}
		spring.datasource.driverClassName=com.mysql.jdbc.Driver
	
	8- En el archivo application-prod.properties, colocar la siguiente línea:
		spring.jpa.hibernate.ddl-auto=create
		BORRA LA BD SI EXISTE y creará todas las tablas, relaciones e insertará los datos del archivo data.sql en la BD.
	
	9- En el archivo application-prod.properties, configuración de la ruta donde se almacenarán los archivos de las cubiertas. Se debe colocar la ruta absoluta dónde se van a almacenar los ficheros de las cubiertas.
		#PATHS APP
		root.file-path = {{RUTA_ABSOLUTA_AL_DIRECTORIO_DE_RESOURCES}}
	
	10- En el archivo server.xml (normalmente se encuentra en la carpeta /conf del Tomcat) y añadir la siguiente línea:
		<Context docBase="{{RUTA_ABSOLUTA_AL_DIRECTORIO_DE_RESOURCES}}" path="/ponlaverde/roofs-resources"/> 
		 
	11- En el archivo server.xml, configurar el certificado SSL si es necesario:
		<Connector
		  protocol="org.apache.coyote.http11.Http11NioProtocol"
		  port="443" maxThreads="200"
		  scheme="https" secure="true" SSLEnabled="true"
		  keystoreFile="{{RUTA_ABSOLUTA_AL_KEYSTORE}}" keystorePass="{{PASSWORD_KEYSTORE}}"
		  clientAuth="false" sslProtocol="TLS" keystoreType="PKCS12"/>

	11- Arrancar Tomcat
	
	12- Si el arranque ha ido correctamente, volver al archivo application-prod.properties y eliminar esta línea:
		spring.jpa.hibernate.ddl-auto=create
		Esto hará que la próxima vez que arranque Tomcat no borre la BD y la vuelva a crear.
		
		

