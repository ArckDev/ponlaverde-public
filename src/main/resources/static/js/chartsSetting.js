// ARRAY WITH BAR COLORS
var opacity = '0.8';
var opacityBkgn = '0.2';
var opacityBorder = '1';

var prettyColors = [
	'rgb(27, 188, 155,'+opacity+')',
    'rgb(232, 76, 61,'+opacity+')',
	'rgb(53, 152, 219,'+opacity+')',
	'rgb(155, 88, 181,'+opacity+')',
	'rgb(255, 255, 255,'+opacity+')',
	'rgb(45, 204, 112,'+opacity+')',
	'rgb(241, 196, 15,'+opacity+')',
	'rgb(231, 126, 35,'+opacity+')',
	'rgb(149, 165, 165,'+opacity+')',
	'rgb(52, 73, 94,'+opacity+')'
	];


var colors = new Object();

colors[0] = '75, 192, 192';   //Cyan
colors[1] = '153, 102, 255';  //Shine purple
colors[2] = '54, 162, 235';   //Blue
colors[3] = '255, 121, 77';   //Orange
colors[4] = '102, 102, 255';  //Purple
colors[5] = '46, 134, 125';   //Dark sea Blue 
colors[6] = '255,218,185';    //Peach
colors[7] = '255, 255, 255';  //White
colors[8] = '255, 196, 77';   //Yellow-Orange
colors[9] = '255, 153, 255';  //Palo Fuchsia 



function colorBuilder(color, opacity){
	return 'rgba('+colors[color]+', '+opacity+')';
}


function colorListBuilder(size, opacity){
	
    var colorListBuilder = new Array();
	for(var i=0; i < size; i++){
		colorListBuilder.push(colorBuilder(i, opacity));
	}

	return colorListBuilder;
}


//LINE CHART
function createLineChart(canvas, color, labels, count) {

	return new Chart($(canvas), {
		type : 'line',
		data : {
			labels : labels,
			datasets : [ {
				data : count,
				backgroundColor : colorBuilder(color, opacityBkgn),
				borderColor : colorBuilder(color, opacityBorder),
				borderWidth : 1
			} ]
		},
		options : {
			legend : {
				display : false
			},
			responsive : true,
			maintainAspectRatio : false,
			scales : {
				yAxes : [ {
					ticks : {
						callback : function(value) {
							if (value % 1 === 0) {
								return value;
							}
						},
						maxTicksLimit : 6,
						beginAtZero : true
					}
				} ]
			}
		}
	});

}

//MULTIPLE LINE CHART
function createMultipleLineChart(canvas, dateLabels, 
        catLabel, catCount, 
        valLabel, valCount,
        errLabel, errCount,
        recLabel, recCount,
        savLabel, savCount,
        setLabel, setCount,
        dasLabel, dasCount) {

    
    return new Chart($(canvas), {
        type : 'line',
        data : {
            labels : dateLabels,
            datasets : [ 
                {
                    label : catLabel,
                    data : catCount,
                    backgroundColor : colorBuilder(0, opacityBkgn),
                    borderColor : colorBuilder(0, opacityBorder),
                    borderWidth : 1
                },
                {
                    label : valLabel,
                    data : valCount,
                    backgroundColor : colorBuilder(1, opacityBkgn),
                    borderColor : colorBuilder(1, opacityBorder),
                    borderWidth : 1
                },
                {
                    label : errLabel,
                    data : errCount,
                    backgroundColor : colorBuilder(2, opacityBkgn),
                    borderColor : colorBuilder(2, opacityBorder),
                    borderWidth : 1
                },
                {
                    label : recLabel,
                    data : recCount,
                    backgroundColor : colorBuilder(3, opacityBkgn),
                    borderColor : colorBuilder(3, opacityBorder),
                    borderWidth : 1
                },
                {
                    label : savLabel,
                    data : savCount,
                    backgroundColor : colorBuilder(4, opacityBkgn),
                    borderColor : colorBuilder(4, opacityBorder),
                    borderWidth : 1
                },
                {
                    label : setLabel,
                    data : setCount,
                    backgroundColor : colorBuilder(5, opacityBkgn),
                    borderColor : colorBuilder(5, opacityBorder),
                    borderWidth : 1
                },
                {
                    label : dasLabel,
                    data : dasCount,
                    backgroundColor : colorBuilder(6, opacityBkgn),
                    borderColor : colorBuilder(6, opacityBorder),
                    borderWidth : 1
                }
           ]
        },
        options : {
            legend : {
                display : true,
                position: 'top',
            },
            responsive : true,
            maintainAspectRatio : false,
            scales : {
                yAxes : [ {
                    ticks : {
                        callback : function(value) {
                            if (value % 1 === 0) {
                                return value;
                            }
                        },
                        maxTicksLimit : 6,
                        beginAtZero : true
                    }
                } ]
            }
        }
    });

}


//DONUT CHART
function createDonutChart(canvas, size, labels, count, showLegend, truncate) {
    
    var legend;
    
    if(size < 4 && showLegend == true){
        displayLegend = true;

    } else {
        displayLegend = false;
    }
    
    if(truncate){
        labels = truncateText(labels);
    }

    return new Chart($(canvas), {
        type : 'doughnut',
        data : {
            labels : labels,
            datasets : [ {
                data : count,
                backgroundColor : colorListBuilder(size, opacityBkgn),
                borderColor : colorListBuilder(size, opacityBorder),
                borderWidth : 1
            } ]
        },
        options : {
            responsive : true,
            maintainAspectRatio : false,
            legend : {
                display: displayLegend,
                position : 'top',
                fullWidth: false
            },
            animation : {
                animateScale : true,
                animateRotate : true
            }
        }
    });
	

}

//BAR HOR CHART
function createBarHorChart(canvas, color, size, labels, count) {

    return new Chart($(canvas), {
        type : 'horizontalBar',
        data : {
            labels : labels,
            datasets : [{
                data : count,
                backgroundColor : colorBuilder(color, opacityBkgn),
                borderColor : colorBuilder(color, opacityBorder),
                borderWidth : 0.5
            }]
        },
        options : {
            responsive : true,
            maintainAspectRatio : true,
            legend : false,
            animation : {
                animateScale : true,
                animateRotate : true
            },
            scales : {
				xAxes : [ {
					ticks : {
						min: 0,
						max: 10
					}
				} ]
			}
        }
    });

}



function truncateText(list){
    
    var response = [];
    
    for(var i = 0; i < labels.length; i++){
        response.push(list[i].substring(0,3).toUpperCase());
    }
    
    return response; 
}
