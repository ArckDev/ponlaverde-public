function sendAsyncRq(rqUrl, rqData, feedback, rqModal, rsModal, processingModal){
    
$(processingModal).modal('toggle');

$.ajax({
	        type: 'POST',
	        contentType: 'application/json',
	        url: rqUrl,
	        data: JSON.stringify(rqData),
	        dataType: 'json',
	        cache: false,
	        timeout: 600000,
	        success: function (data) {

	            if(data.code == 'OK') {
	                $(feedback).css('display','none');
	                $(feedback).empty();
	                $(rqModal).modal('toggle');
	                $(processingModal).modal('toggle');
	                $(rsModal).modal('toggle');
	                
	            } else {
	                $(feedback).html(printErrorList(data.result));
		            $(feedback).css('display','block');
		            $(processingModal).modal('toggle');
	            }

	        },
	        
	        error: function (e) {
	            $(processingModal).modal('toggle');
	        	$('#feedback').html(e);
	            $('#feedback').css('display','block');
	        }

});

}