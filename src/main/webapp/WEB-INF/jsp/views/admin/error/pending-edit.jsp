<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>

<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<spring:url value="/admin/error/update" var="update" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="validate"/>

	   <!-- MAIN CONTENT -->
		<div class="container">
		
			<form:form class="form-horizontal" method="post" enctype="multipart/form-data"
						modelAttribute="registerRoofForm" action="${update}" >
			
				<view:roofsEditViewer disabled="true"/>
			
				<!-- BUTTONS -->
				<div class="row justify-content-center">
					<div class="col-12 text-center">
						<div class="square">
							<div class="col-12 text-center">
								<button type="button" name="register" class="btn btn-outline-success margin-all"
									onclick="submitFormAndImages(this.form)"> 
									<i class="w3-medium fa fa-save"></i> <spring:message code="button.save"/>
								</button>
							</div>
						</div>
					</div>
				</div>
								
			</form:form>

		</div>

</body>

<!-- MODALS -->
<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />