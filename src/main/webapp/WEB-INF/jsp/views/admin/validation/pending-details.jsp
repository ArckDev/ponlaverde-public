<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal" %>

<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<!-- PATHS -->
<spring:url value="/admin/validation/validate" var="validate" />
<spring:url value="/admin/validation/reject" var="reject" />


<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="validate"/>

	   <!-- MAIN CONTENT -->
		<div class="container">
		
			<!-- TITLE AND STATUS -->
			<util:titleStatus title="cat.details"/>
			
			<!-- REJECTION REASON -->
			<util:rejectionReason/>

			<form:form class="form-horizontal" method="post">
			
				<!-- ROOF DETAILS -->
				<view:roofsDetailsViewer/>
			
				<div class="row">

					<!-- BUTTONS -->
					<div class="col-12 text-center">
						<div class="square">
							<button type="button" id="button-reject" class="btn btn-outline-danger margin-all" data-toggle="modal"
							data-target="#rejectModal"><spring:message code="button.reject"/></button>
			
							<button type="button" id="button-validate" class="btn btn-outline-success margin-all" data-toggle="modal"
							data-target="#validationModal"><spring:message code="button.validate"/></button>
						</div>
					</div>
				</div>
				
				<!-- MODAL -->
				<modal:validationModal type="question" message="question.validation" submit="${validate}" submitButton="button.validate"/>
				<modal:rejectModal type="question"  message="question.rejection" submit="${reject}" submitButton="button.reject"/>

			</form:form>

		</div>

</body>

<!-- MODALS -->
<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />

