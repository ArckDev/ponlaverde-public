<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="cat" tagdir="/WEB-INF/tags/catalogue" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />


<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />

<body class="bg2">

	<!-- NAVBAR -->
	<util:navbarLogged activeElement="dashboard" />

	<!-- MAIN CONTENT -->
	<div class="container">
	
		<!-- TITLE -->
		<util:title title="app.dashboard"/>

		<div id="accordion">

			<div class="card">

				<div class="card-header square no-bottom bg1 text-right" id="headingOne">
					<button class="btn btn-outline-info btn-sm fixed-width"
						data-toggle="collapse" data-target="#collapseOne"
						aria-expanded="true" aria-controls="collapseOne">
						<i class="w3-medium fa fa-bar-chart"></i> <spring:message code="dash.application" />
					</button>
				</div>

				<div id="collapseOne" class="collape show"
					aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">

						<div class="row ">

							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp"
												data-count="${indUserRegisterTotal}">0</h3>
											<div>
												<small><spring:message code="dash.individual" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="indUserRegister"></canvas>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp"
												data-count="${supUserRegisterTotal}">0</h3>
											<div>
												<small><spring:message code="dash.supplier" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="supUserRegister"></canvas>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp"
												data-count="${connections}">0</h3>
											<div>
												<small><spring:message code="dash.visits" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="connections"></canvas>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp" data-count="${access}">0</h3>
											<div>
												<small><spring:message code="dash.access" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="access"></canvas>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="row ">

							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp"
												data-count="${connectedUsers}">0</h3>
											<div>
												<small><spring:message code="dash.real-time.users" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="connectedUsers" style="height: 180px;"></canvas>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp" data-count="${deviceOS}">0</h3>
											<div>
												<small><spring:message code="dash.access.os" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="deviceOS" style="height: 180px;"></canvas>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp"
												data-count="${deviceType}">0</h3>
											<div>
												<small><spring:message code="dash.access.type" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="deviceType" style="height: 180px;"></canvas>
										</div>
									</div>
								</div>
							</div>


							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<h3 class="counter font-green-sharp"
												data-count="${systemTotalRoofsByStatus}">0</h3>
											<div>
												<small><spring:message code="dash.roofs.state" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="systemTotalRoofsByStatus" style="height: 180px;"></canvas>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="row ">
							<div class="col-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div>
											<small><spring:message code="dash.features.use" /></small>
										</div>
									</div>
									<div class="charts">
										<canvas id="useOfFeatures" style="height: 250px;"></canvas>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>

			</div>

			<div class="card">
	
				<div class="card-header square no-bottom bg1 text-right" id="headingTwo">
					<button class="btn btn-outline-info btn-sm fixed-width"
						data-toggle="collapse" data-target="#collapseTwo"
						aria-expanded="true" aria-controls="collapseTwo">
						<i class="w3-medium fa fa-bar-chart"></i> <spring:message code="dash.recommender" />
					</button>
				</div>
	
				<div id="collapseTwo" class="collapsing" aria-labelledby="headingTwo"
					data-parent="#accordion">
					<div class="card-body">
	
						<div class="row ">
							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square small bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<div>
												<label class="counter font-green-sharp h3"
													data-count="${successRate}"></label> <label
													class="font-green-sharp h3">%</label>
											</div>
											<small><spring:message code="dash.rec.success-rate" /></small>
										</div>
									</div>
								</div>
							</div>
						</div>
	
	
						<div class="row ">
	
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-small">
											<div>
												<small><spring:message code="dash.rec.scored-params" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="mostValuedVariablesInRecommender"></canvas>
										</div>
									</div>
								</div>
							</div>
	
	
							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<div>
												<small><spring:message code="dash.rec.climate" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="roofClimateSelectedInRecommender" style="height: 180px;"></canvas>
										</div>
									</div>
								</div>
							</div>
	
	
							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display-number">
											<div>
												<small><spring:message code="dash.rec.slope" /></small>
											</div>
										</div>
										<div class="charts">
											<canvas id="roofSlopeSelectedInRecommender" style="height: 180px;"></canvas>
										</div>
									</div>
								</div>
							</div>
	
						</div>
	
						<div class="row ">
	
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display">
											<div>
												<small><spring:message code="dash.rec.roofs.chosen" /></small>
											</div>
										</div>
										<div class="scrollable">
											<table
												class="table table-sm table-hover table-striped table-dark table-dark-stat">
												<thead>
													<tr>
														<th scope="col">#</th>
														<th scope="col"><spring:message code="dash.rec.model" /></th>
														<th scope="col" style="text-align: center;"><spring:message code="dash.rec.type" /></th>
														<th scope="col" style="text-align: center;"><spring:message code="dash.rec.chosen" /></th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${popularChosenRoofs}" var="roofs"
														varStatus="roofsLoop">
														<tr>
															<th scope="row">${roofsLoop.index+1}</th>
															<td>
																<div>${fn:substring(roofs.roofName, 0, 45)}</div>
																<div class="font-small italic">${roofs.roofMaker}</div>
															</td>
															<td style="text-align: center;">
																<spring:message code="${roofs.roofType}" />
															</td>
															<td style="text-align: center;">${roofs.count}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
	
	
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display">
											<div>
												<small><spring:message code="dash.rec.roofs.suggested" /></small>
											</div>
										</div>
										<div class="scrollable">
											<table
												class="table table-sm table-hover table-striped table-dark table-dark-stat">
												<thead>
													<tr>
														<th scope="col">#</th>
														<th scope="col"><spring:message code="dash.rec.model" /></th>
														<th scope="col" style="text-align: center;"><spring:message code="dash.rec.type" /></th>
														<th scope="col" style="text-align: center;"><spring:message code="dash.rec.suggested" /></th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${suggestedRoofs}" var="roofs" varStatus="roofsLoop">
														<tr>
															<th scope="row">${roofsLoop.index+1}</th>
															<td><div>${fn:substring(roofs.roofName, 0, 45)}</div>
																<div class="font-small italic">${roofs.roofMaker}</div></td>
															<td style="text-align: center;"><spring:message
																	code="${roofs.roofType}" /></td>
															<td style="text-align: center;">${roofs.count}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
	
						</div>
					</div>
				</div>
			</div>

			<div class="card" style="margin-bottom: 50px;">
	
				<div class="card-header square no-bottom bg1 text-right" id="headingFour">
					<button class="btn btn-outline-info btn-sm fixed-width"
						data-toggle="collapse" data-target="#collapseFour"
						aria-expanded="true" aria-controls="collapseFour">
						<i class="w3-medium fa fa-bar-chart"></i> <spring:message code="dash.users-cities" />
					</button>
				</div>
	
				<div id="collapseFour" class="collapsing"
					aria-labelledby="headingFour" data-parent="#accordion">
					<div class="card-body">
						<div class="row">
	
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display">
											<div>
												<small><spring:message code="dash.users-cities.registered-cities" /></small>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="scrollable">
													<table
														class="table table-sm table-hover table-striped table-dark table-dark-stat">
														<thead>
															<tr>
																<th scope="col">#</th>
																<th scope="col"><spring:message code="dash.users-cities.city" /></th>
																<th scope="col" style="text-align: center;"><spring:message code="dash.users-cities.totals" /></th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${usersByCityAndCountry}" var="users"
																varStatus="usersLoop">
																<tr>
																	<th scope="row">${usersLoop.index+1}</th>
																	<td><div>${fn:substring(users.city, 0, 45)}</div>
																		<div class="font-small italic">
																			<spring:message code="${users.country}" />
																		</div></td>
																	<td style="text-align: center;">${users.count}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="charts">
													<canvas id="usersByCityAndCountry"></canvas>
												</div>
											</div>
										</div>
									</div>
	
								</div>
							</div>
	
	
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display">
											<div>
												<small><spring:message code="dash.users-cities.visits.country" /></small>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	
												<div class="scrollable">
													<table
														class="table table-sm table-hover table-striped table-dark table-dark-stat">
														<thead>
															<tr>
																<th scope="col">#</th>
																<th scope="col"><spring:message code="dash.users-cities.country" /></th>
																<th scope="col" style="text-align: center;"><spring:message code="dash.users-cities.totals" /></th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${commonCountriesConnection}"
																var="connections" varStatus="connectionsLoop">
																<tr>
																	<th scope="row">${connectionsLoop.index+1}</th>
																	<td>${connections.field}</td>
																	<td style="text-align: center;">${connections.count}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="charts">
													<canvas id="commonCountriesConnection"></canvas>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
								<div class="square auto bg1">
									<div class="dashboard-stat">
										<div class="display">
											<div>
												<small><spring:message code="dash.users-cities.visits" /></small>
											</div>
										</div>
										<div class="scrollable">
											<table class="table table-sm table-hover table-striped table-dark table-dark-stat">
												<thead>
													<tr>
														<th scope="col">#</th>
														<th scope="col"><spring:message code="dash.users-cities.city" /></th>
														<th scope="col" style="text-align: center;"><spring:message code="dash.users-cities.totals" /></th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${commonCitiesConnection}"
														var="connections" varStatus="connectionsLoop">
														<tr>
															<th scope="row">${connectionsLoop.index+1}</th>
															<td><div>${fn:substring(connections.city, 0, 45)}</div>
																<div class="font-small italic">
																	<spring:message code="${connections.country}" />
																</div></td>
															<td style="text-align: center;">${connections.count}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
	
						</div>
					</div>
	
				</div>
			</div>
		</div>	
	</div>



</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />



<!-- CHARTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

<!-- CHARTS SETTINGS -->
<script src="${context}/js/chartsSetting.js"></script>

<!-- SETTING DATA FOR CHARTS -->
<c:set var="adminDashboardJson" value="${adminDashboardJson}"/>

<!-- CHARTS COMMON DATA -->
<script>
// Data
var adminDashboardJson = ${adminDashboardJson};

// Set data into json object
var adminDashboardJsonArray = {"jsonarray" : adminDashboardJson };
</script>


<!-- CHARTS -->
<script>

// indUserRegister 
var labels = adminDashboardJsonArray.jsonarray.indUserRegister.list.map(function(e) {
	return e.date;
});
var count = adminDashboardJsonArray.jsonarray.indUserRegister.list.map(function(e) {
	return e.count;
});
createLineChart('#indUserRegister', 0, labels, count, false);



// supUserRegister
var labels = adminDashboardJsonArray.jsonarray.supUserRegister.list.map(function(e) {
	return e.date;
});
var count = adminDashboardJsonArray.jsonarray.supUserRegister.list.map(function(e) {
	return e.count;
});
createLineChart('#supUserRegister', 1, labels, count, false);



// connections
var labels = adminDashboardJsonArray.jsonarray.connections.list.map(function(e) {
	return e.date;
});
var count = adminDashboardJsonArray.jsonarray.connections.list.map(function(e) {
	return e.count;
});
createLineChart('#connections', 0, labels, count, false);


//logins
var labels = adminDashboardJsonArray.jsonarray.access.list.map(function(e) {
	return e.date;
});
var count = adminDashboardJsonArray.jsonarray.access.list.map(function(e) {
	return e.count;
});
createLineChart('#access', 1, labels, count, false);



// connectedUsers
var labels = adminDashboardJsonArray.jsonarray.connectedUsers.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.connectedUsers.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#connectedUsers', size, labels, count, true);



// deviceOS
var labels = adminDashboardJsonArray.jsonarray.deviceOS.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.deviceOS.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#deviceOS', size, labels, count, true);



// deviceType
var labels = adminDashboardJsonArray.jsonarray.deviceType.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.deviceType.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#deviceType', size, labels, count, true);



//systemTotalRoofsByStatus
var labels = adminDashboardJsonArray.jsonarray.systemTotalRoofsByStatus.list.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.systemTotalRoofsByStatus.list.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#systemTotalRoofsByStatus', size, labels, count, true);



//mostValuedVariablesInRecommender
var labels = adminDashboardJsonArray.jsonarray.mostValuedVariablesInRecommender.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.mostValuedVariablesInRecommender.map(function(e) {
	return e.countD;
});
var size = Object.keys(labels).length;
createBarHorChart('#mostValuedVariablesInRecommender', 1, size, labels, count);



//roofClimateSelectedInRecommender
var labels = adminDashboardJsonArray.jsonarray.roofClimateSelectedInRecommender.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.roofClimateSelectedInRecommender.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#roofClimateSelectedInRecommender', size, labels, count, true);



//roofSlopeSelectedInRecommender
var labels = adminDashboardJsonArray.jsonarray.roofSlopeSelectedInRecommender.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.roofSlopeSelectedInRecommender.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#roofSlopeSelectedInRecommender', size, labels, count, true);



//usersByCityAndCountry
var labels = adminDashboardJsonArray.jsonarray.usersByCityAndCountry.map(function(e) {
	return e.city;
});
var count = adminDashboardJsonArray.jsonarray.usersByCityAndCountry.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#usersByCityAndCountry', size, labels, count, false);



//commonCitiesConnection
var labels = adminDashboardJsonArray.jsonarray.commonCitiesConnection.map(function(e) {
	return e.city;
});
var count = adminDashboardJsonArray.jsonarray.commonCitiesConnection.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#commonCitiesConnection', size, labels, count, false);



//commonCountriesConnection
var labels = adminDashboardJsonArray.jsonarray.commonCountriesConnection.map(function(e) {
	return e.field;
});
var count = adminDashboardJsonArray.jsonarray.commonCountriesConnection.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#commonCountriesConnection', size, labels, count, false);



//useOfFeatures
var dateLabels = adminDashboardJsonArray.jsonarray.useOfFeatures.cat.map(function(e) {
	return e.date; //They are the same for all the values
});

var countCat = adminDashboardJsonArray.jsonarray.useOfFeatures.cat.map(function(e) {
	return e.count;
});
var countVal = adminDashboardJsonArray.jsonarray.useOfFeatures.val.map(function(e) {
	return e.count;
});
var countErr = adminDashboardJsonArray.jsonarray.useOfFeatures.err.map(function(e) {
	return e.count;
});
var countRec = adminDashboardJsonArray.jsonarray.useOfFeatures.rec.map(function(e) {
	return e.count;
});
var countSav = adminDashboardJsonArray.jsonarray.useOfFeatures.sav.map(function(e) {
	return e.count;
});
var countSet = adminDashboardJsonArray.jsonarray.useOfFeatures.set.map(function(e) {
	return e.count;
});
var countDas = adminDashboardJsonArray.jsonarray.useOfFeatures.das.map(function(e) {
	return e.count;
});


createMultipleLineChart('#useOfFeatures', dateLabels, 
        '<spring:message code="FT.CAT"/>', countCat, 
        '<spring:message code="FT.VAL"/>', countVal,
        '<spring:message code="FT.ERR"/>', countErr,
        '<spring:message code="FT.REC"/>', countRec,
        '<spring:message code="FT.SAV"/>', countSav,
        '<spring:message code="FT.SET"/>', countSet,
        '<spring:message code="FT.DAS"/>', countDas);

</script>

