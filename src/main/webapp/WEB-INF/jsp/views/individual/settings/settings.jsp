<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />


<spring:url value="/individual/settings/update-user" var="updateUser" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="settings"/>
  
   <!-- MAIN CONTENT -->
	<div class="container">

		<!-- TITLE -->
		<util:title title="app.settings"/>

		
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="square">
			
					<!-- USER DATA -->
					<div id="accordionUserData">
						<div class="bg3" style="margin-bottom: 0;">
						
							<div id="userData">
								<div class="row">
									<div class="col-12 text-right">
										<button class="btn btn-outline-info btn-sm fixed-width" data-toggle="collapse" data-target="#collapseUserData" aria-expanded="true" aria-controls="collapseUserData">
											<i class="w3-medium fa fa-user"></i> <spring:message code="set.user-data" /> 
										</button>
									</div>
								</div>
							</div>
							
							<form:form class="form-horizontal" method="post" id="userForm">
								<div id="collapseUserData" class="collapse" aria-labelledby="userData" data-parent="#accordionUserData">
									<div class="card-body">
										<div class="row justify-content-center">
										
											<div class="col-md-4">
												<input name="idUser" type="hidden" value="${userDTO.idUser}">
						
												<label class="label-form-subgroup label-margin"><spring:message code="reg.personal-data"/></label> 	
												<div class="form-subgroup dark-border" style="min-height: 260px;">
													<label class="label-margin"><spring:message code="user.name"/></label> 
													<input type="text" class="form-control form-control-sm dark" name="name" id="user-name-ind" value="${userDTO.name}" required> 
													
													<label class="label-margin"><spring:message code="user.surname"/></label>
													<input type="text" class="form-control form-control-sm dark" name="surnames" id="user-surname-ind" value="${userDTO.surnames}" required>
													
													<label class="label-margin"><spring:message code="user.dob"/></label> 
													<input type="date" class="form-control form-control-sm dark" name="dateOfBirth" id="user-dob-ind" value="${userDTO.dateOfBirth}" required> 
												</div>
												
												<label class="label-form-subgroup label-margin"><spring:message code="reg.profess-data"/></label> 	
												<div class="form-subgroup dark-border">
													<label class="label-margin"><spring:message code="user.company"/></label> 
													<input type="text" class="form-control form-control-sm dark" name="company" id="company-ind" value="${userDTO.company}"> 
													
													<label class="label-margin"><spring:message code="user.occupation"/></label>
													<input type="text" class="form-control form-control-sm dark" name="occupation" id="occupation-ind" value="${userDTO.occupation}">
												</div>
											</div>
											
											<div class="col-md-4" >
												<label class="label-form-subgroup label-margin"><spring:message code="reg.access-data"/></label> 	
												<div class="form-subgroup dark-border" style="min-height: 260px;">
													<label class="label-margin"><spring:message code="user.email"/></label>
													<input type="email" class="form-control form-control-sm dark" name="username" id="user-username-ind" value="${userDTO.username}" disabled>
													
													<label class="label-margin"><spring:message code="user.password"/></label> 
													<input id="repeat-password-ind" type="password" class="form-control form-control-sm dark" name="passwordRepeat" > 
													
													<label class="label-margin"><spring:message code="user.rep.password"/></label>
													<div class="password-box">
												    	<input id="password-ind" type="password" name="password" class="form-control form-control-sm dark">
												  	</div>
												</div>
												
												<label class="label-form-subgroup label-margin"><spring:message code="reg.geo-data"/></label> 	
												<div class="form-subgroup dark-border">
													<label class="label-margin"><spring:message code="user.country"/></label> 
													<select class="form-control form-control-sm dark" name="country" id="country-ind" onchange="selectCitiesInd()">
														<c:forEach items="${countries}" var="country">
															<c:choose>
																<c:when test="${country.key == 'ES'}">
																	<c:set var="countrySel" value="ES"/>
																	<option selected value="${country.key}">${country.key} - ${country.value}</option>
																</c:when>
																<c:otherwise>
																	<c:set var="countrySel" value="${country.key}"/>
																	<option value="${country.key}">${country.key} - ${country.value}</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</select>
													<label class="label-margin"><spring:message code="user.city"/></label> 
													<select class="form-control form-control-sm dark" name="city" id="user-city-ind-sel">
														<c:forEach items="${spanishTowns}" var="towns">
															<c:choose>
																<c:when test="${towns.value == 'Barcelona'}">
																	<option selected value="${towns.value}">${towns.value}</option>
																</c:when>
																<c:otherwise>
																	<option value="${towns.value}">${towns.value}</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</select>
													<input style="display: none;" type="text" class="form-control form-control-sm dark" name="city" id="user-city-ind-inp">
												</div>
											</div>
										</div>
	
										<!-- BUTTONS -->
										<div class="row justify-content-center" style="margin-top: 50px;">
											<div class="col-12 text-center">
												<button type="submit" formaction="${updateUser}"
													name="update" class="btn btn-outline-success margin-all">
													<i class="w3-medium fa fa-edit"></i> <spring:message code="button.update-user" /> 
												</button>
											</div>
										</div>

									</div>
								</div>
							</form:form>

						</div>					
					</div>
		
				</div>
				
			</div>
		</div>

	</div>


<!-- MODALS	 -->
<jsp:include page="/WEB-INF/jsp/modal/modal_success_generic.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_warning_generic.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />


</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />

<!-- SCRIPT FOR SHOWING CITIES -->
<script type="text/javascript">
function selectCitiesInd() {
    
    var obj = $("#country-ind").val();
    
    if(obj != 'ES'){
        $("#user-city-ind-sel").css('display','none');
        $("#user-city-ind-inp").css('display','block'); 
    } else {
        $("#user-city-ind-sel").css('display','block');
        $("#user-city-ind-inp").css('display','none'); 
    }

}
</script>

<!-- SCRIPTS FOT PASSWORD STRENGTH -->
<script src="${context}/js/passwordStrength.js"></script>
<script>
    $(document).ready(function () {
        var obj = $('#password-ind').passwordStrength();
        var obj2 = $('#passwort-ind').passwordStrength();
    });
</script>