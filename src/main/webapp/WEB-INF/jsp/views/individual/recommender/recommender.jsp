<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="cat" tagdir="/WEB-INF/tags/catalogue" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<spring:url value="/individual/recommender/start" var="recommenderStart" />
<spring:url value="/individual/recommender/save" var="recommenderSave" />
<spring:url value="/individual/recommender/details" var="recommenderDetails" />


<spring:message code="rec.aesthetics" var="aestheticsTitle"/>
<spring:message code="info.rec-aesthetics" var="aestheticsMessage" />

<spring:message code="rec.implantation" var="implantationTitle"/>
<spring:message code="info.rec-implantation" var="implantationMessage" />

<spring:message code="rec.maintenance" var="maintenanceTitle"/>
<spring:message code="info.rec-maintenance" var="maintenanceMessage" />

<spring:message code="rec.price" var="priceTitle"/>
<spring:message code="info.rec-price" var="priceMessage" />

<spring:message code="rec.regulation" var="regulationTitle"/>
<spring:message code="info.rec-regulation" var="regulationMessage" />

<spring:message code="rec.storage" var="storageTitle"/>
<spring:message code="info.rec-storage" var="storageMessage" />

<spring:message code="rec.thermal" var="thermalTitle"/>
<spring:message code="info.rec-thermal" var="thermalMessage" />

<spring:message code="rec.traffic" var="trafficTitle"/>
<spring:message code="info.rec-traffic" var="trafficMessage" />

<spring:message code="rec.wall" var="wallTitle"/>
<spring:message code="info.rec-wall" var="wallMessage" />

<spring:message code="rec.weight" var="weightTitle"/>
<spring:message code="info.rec-weight" var="weightMessage" />

<spring:message code="rec.winds" var="windsTitle"/>
<spring:message code="info.rec-winds" var="windsMessage" />



<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />

<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="recommender"/>
  
   <!-- MAIN CONTENT -->
	<div class="container">
	
		<!-- TITLE -->
		<util:title title="app.recommender"/>

		<form:form id="recommenderForm" method="post" action="${recommenderStart}" >
			
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="square">
					<p class="seccion-title"><spring:message code="rec.input-message"/></p>   
						<div class="row justify-content-center">	      	
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<div class="row justify-content-center margin-btm">
									 <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
								     	<util:combo required="true" dto="${userRoofRating.slope}" message="cat.slope" map="${slope}" varName="slope" onchange="checkValues()"/>
								     </div>
								     <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
								     	<util:combo required="true" dto="${userRoofRating.climate}" message="cat.climate.single" map="${climate}" varName="climate" onchange="checkValues()"/>
								     </div>
								     <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
								     	<util:combo required="true" dto="${userRoofRating.results}" message="rec.results" map="${results}" varName="results" onchange="checkValues()"/>
								     </div>
								</div>
							</div>
						</div>
				     </div>	
			    </div>		      	
			</div>
			
			<div class="row justify-content-center" id="range" style="visibility: ${visibility};">	
				<div class="col-12">
					<div class="square">
					<p class="seccion-title st-margin "><spring:message code="rec.range-message"/></p>   
						<div class="row justify-content-center">	      	
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<util:range name="Aesthetics" margin="margin-btm" value="${userRoofRating.aesthetics}" infoTitle="${aestheticsTitle}" infoMessage="${aestheticsMessage}" onchange="hideTable()"/>
								<util:range name="Implantation" margin="margin-btm" value="${userRoofRating.implantation}" infoTitle="${implantationTitle}" infoMessage="${implantationMessage}" onchange="hideTable()"/>
								<util:range name="Maintenance" margin="margin-btm" value="${userRoofRating.maintenance}" infoTitle="${maintenanceTitle}" infoMessage="${maintenanceMessage}" onchange="hideTable()"/>
								<util:range name="Price" margin="margin-btm" value="${userRoofRating.price}" infoTitle="${priceTitle}" infoMessage="${priceMessage}" onchange="hideTable()"/>
								<util:range name="Regulation" margin="margin-btm" value="${userRoofRating.regulation}" infoTitle="${regulationTitle}" infoMessage="${regulationMessage}" onchange="hideTable()"/>
								<util:range name="Storage" margin="margin-btm" value="${userRoofRating.storage}" infoTitle="${storageTitle}" infoMessage="${storageMessage}" onchange="hideTable()"/>
								<util:range name="Thermal" margin="margin-btm" value="${userRoofRating.thermal}" infoTitle="${thermalTitle}" infoMessage="${thermalMessage}" onchange="hideTable()"/>
								<util:range name="Traffic" margin="margin-btm" value="${userRoofRating.traffic}" infoTitle="${trafficTitle}" infoMessage="${trafficMessage}" onchange="hideTable()"/>
								<util:range name="Wall" margin="margin-btm" value="${userRoofRating.wall}" infoTitle="${wallTitle}" infoMessage="${wallMessage}" onchange="hideTable()"/>
								<util:range name="Weight" margin="margin-btm" value="${userRoofRating.weight}" infoTitle="${weightTitle}" infoMessage="${weightMessage}" onchange="hideTable()"/>
								<util:range name="Winds" margin="margin-btm" value="${userRoofRating.winds}" infoTitle="${windsTitle}" infoMessage="${windsMessage}" onchange="hideTable()"/>
							</div>
						</div>		
					
						<!-- BUTTONS -->
						<div class="row justify-content-center" style="margin-top: 50px;">
							<div class="col-12 text-center">
								<button class="btn btn-outline-info margin-all" onclick="sendForm();">
									<i class="w3-medium fa fa-th"></i> <spring:message code="button.get-results" /> 
								</button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</form:form>
		
		<form:form id="recommenderFormSave" method="post">
			<c:if test="${bestRoofs != null}">
				<c:choose>
					<c:when test="${empty bestRoofs}">
						<util:noResults/>
					</c:when>	
					<c:otherwise>
						
						<div class="row justify-content-center">
						
							<!-- TABLE FOR BIG AND MEDIUM SCREENS -->
							<div class="col-12 d-none d-md-block text-center" style="margin-bottom: 30px;">
								<table class="table table-sm table-hover table-striped table-dark">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col"><spring:message code="cat.maker"/></th>
											<th scope="col"><spring:message code="cat.model"/></th>
											<th scope="col"><spring:message code="cat.type"/></th>
											<th scope="col"><spring:message code="rec.final-rate"/></th>
											<th scope="col"></th>
											<th scope="col"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${bestRoofs}" var="bestRoof" varStatus="bestRoofsLoop">
											<tr>
												<th scope="row" onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoofsLoop.index+1}</th>
												<td onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoof.roofDTO.maker}</td>
												<td onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoof.roofDTO.model}</td>
												<td onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();"><spring:message code="${bestRoof.roofDTO.type}"/></td>
												<td onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoof.roofDTO.rate}</td>
												<td><input required type="radio" name="selected" value="${bestRoof.roofDTO.idRoof}"></td>
												<td>
													<form:form id="recoForm${bestRoofsLoop.index}" action="${recommenderDetails}" >
														<input type="hidden" name="idRoof" value="${bestRoof.roofDTO.idRoof}"/>
													</form:form>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							
							<!-- TABLE FOR SMALL SCREENS -->
							<div class="col-12 d-md-none text-center" style="margin-bottom: 30px;">
								<table class="table table-sm table-hover table-striped table-dark">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col"><spring:message code="cat.maker"/></th>
											<th scope="col"><spring:message code="cat.model"/></th>
											<th scope="col"><spring:message code="rec.final-rate"/></th>
											<th scope="col"></th>
											<th scope="col"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${bestRoofs}" var="bestRoof" varStatus="bestRoofsLoop">
											<tr>
												<th scope="row" onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoofsLoop.index+1}</th>
												<td onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoof.roofDTO.maker}</td>
												<td onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoof.roofDTO.model}</td>
												<td onclick="document.forms['recoForm${bestRoofsLoop.index}'].submit();">${bestRoof.roofDTO.rate}</td>
												<td><input required type="radio" name="selected" value="${bestRoof.roofDTO.idRoof}"></td>
												<td>
													<form:form id="recoForm${bestRoofsLoop.index}" action="${recommenderDetails}" >
														<input type="hidden" name="idRoof" value="${bestRoof.roofDTO.idRoof}"/>
													</form:form>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>

						</div>
						
						<input type="hidden" name="climate" value="${userRoofRating.climate}"/>
						<input type="hidden" name="slope" value="${userRoofRating.slope}"/>
						<input type="hidden" name="aesthetics" value="${userRoofRating.aesthetics}"/>
						<input type="hidden" name="implantation" value="${userRoofRating.implantation}"/>
						<input type="hidden" name="maintenance" value="${userRoofRating.maintenance}"/>
						<input type="hidden" name="price" value="${userRoofRating.price}"/>
						<input type="hidden" name="regulation" value="${userRoofRating.regulation}"/>
						<input type="hidden" name="storage" value="${userRoofRating.storage}"/>
						<input type="hidden" name="thermal" value="${userRoofRating.thermal}"/>
						<input type="hidden" name="traffic" value="${userRoofRating.traffic}"/>
						<input type="hidden" name="wall" value="${userRoofRating.wall}"/>
						<input type="hidden" name="weight" value="${userRoofRating.weight}"/>
						<input type="hidden" name="winds" value="${userRoofRating.winds}"/>
						<input type="hidden" name="results" value="${userRoofRating.results}"/>
						
						
						
						<!-- BUTTONS -->
						<div class="row justify-content-center">
							<div class="col-12 text-center">
								<div class="square">
									<div class="col-12 text-center">
									
										<button type="button" class="btn btn-outline-info margin-all"
											data-toggle="modal" data-target="#chartsModal">
											<i class="w3-medium fa fa-bar-chart" ></i> <spring:message code="button.charts" /> 
										</button>
										
										<button type="button" class="btn btn-outline-success margin-all"
											data-toggle="modal" data-target="#inputQuestionModal">
											<i class="w3-medium fa fa-save"></i> <spring:message code="button.save"/>
										</button>

									</div>
								</div>
							</div>
						</div>
		
						<modal:inputQuestionModal type="question" message="rec.save" submit="${recommenderSave}" submitButton="button.save"/>

					</c:otherwise>
				</c:choose>
			</c:if>
		</form:form>
	</div>

<!-- MODALS	 -->
<jsp:include page="/WEB-INF/jsp/modal/modal_success_generic.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_warning_generic.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_charts.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />

</body>

<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />

<script>
function checkValues(){
    
    var slope = document.getElementById("slope");
    var climate = document.getElementById("climate");
    var results = document.getElementById("results");

	if(slope.value.length<4 && climate.value.length<4 && results.value.length<4){
	    document.getElementById("range").style.visibility = "visible"; 
	}
	
	hideTable();
}

function hideTable(){
    document.getElementById("recommenderFormSave").style.visibility = "hidden"; 
}


function sendForm() {
    $('#processingModal').modal('toggle');
    this.form.submit();
}
</script>
