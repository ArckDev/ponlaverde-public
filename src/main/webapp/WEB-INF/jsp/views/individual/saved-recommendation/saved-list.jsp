<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="cat" tagdir="/WEB-INF/tags/catalogue" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<spring:url value="/individual/saved-list/saved-recommendation" var="detailsPath" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />

<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="saved"/>
  
   <!-- MAIN CONTENT -->
	<div class="container">
	
		<!-- TITLE -->
		<util:title title="app.saved.recommendations"/>
		
		<div class="row justify-content-center">	

		<!-- LIST -->
		<c:choose>
		
			<c:when test="${empty savedRoofs}">
				<util:noResults/>
			</c:when>
			
			<c:otherwise>
				
				<!-- TABLE FOR BIG AND MEDIUM SCREENS -->
				<div class="col-12 d-none d-md-block text-center">
					<table class="table table-sm table-hover table-striped table-dark">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col"><spring:message code="rec-saved.name"/></th>
									<th scope="col"><spring:message code="rec-saved.date"/></th>
									<th scope="col"><spring:message code="rec-saved.climate"/></th>
									<th scope="col"><spring:message code="rec.saved-slope"/></th>
								</tr>
							</thead>
						<tbody>
							<c:forEach items="${savedRoofs}" var="saved" varStatus="savedLoop">
								<tr onclick="document.forms['savedForm${savedLoop.index}'].submit();">
									<th scope="row">${savedLoop.index+1}</th>
									<td>${saved.savedName}</td>
									<fmt:formatDate var="date" value="${saved.savedDate}" pattern="dd/MM/yy (HH:mm)" />
									<td>${date}</td>
									<td><spring:message code="${saved.climate}"/></td>
									<td><spring:message code="${saved.slope}"/></td>
									<form:form id="savedForm${savedLoop.index}" method="post" action="${detailsPath}" >
										<input type="hidden" name="idUserSavedRoof" value="${saved.idUserSavedRoof}"/>
									</form:form>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
				<!-- TABLE FOR SMALL SCREENS -->
				<div class="col-12 d-md-none text-center">
					<table class="table table-sm table-hover table-striped table-dark">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col"><spring:message code="rec-saved.name"/></th>
									<th scope="col"><spring:message code="rec-saved.date"/></th>
									<th scope="col"><spring:message code="rec-saved.climate"/></th>
								</tr>
							</thead>
						<tbody>
							<c:forEach items="${savedRoofs}" var="saved" varStatus="savedLoop">
								<tr onclick="document.forms['savedForm${savedLoop.index}'].submit();">
									<th scope="row">${savedLoop.index+1}</th>
									<td>${saved.savedName}</td>
									<fmt:formatDate var="date" value="${saved.savedDate}" pattern="dd/MM/yy (HH:mm)" />
									<td>${date}</td>
									<td><spring:message code="${saved.climate}"/></td>
									<form:form id="savedForm${savedLoop.index}" method="post" action="${detailsPath}" >
										<input type="hidden" name="idSavedRoof" value="${saved.idUserSavedRoof}"/>
									</form:form>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
	
			</c:otherwise>
		
		</c:choose>
	
		</div>
	</div>

<!-- MODALS	 -->
<jsp:include page="/WEB-INF/jsp/modal/modal_success_generic.jsp" />

</body>

<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />

<script>
function checkValues(){
    
    var slope = document.getElementById("slope");
    var climate = document.getElementById("climate");
    var results = document.getElementById("results");

	if(slope.value.length<4 && climate.value.length<4 && results.value.length<4){
	    document.getElementById("range").style.visibility = "visible"; 
	}
    
}
</script>
