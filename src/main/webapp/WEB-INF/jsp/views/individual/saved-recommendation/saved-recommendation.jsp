<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="cat" tagdir="/WEB-INF/tags/catalogue" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<spring:url value="/individual/saved-list/delete-recommendation" var="delete" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />

<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="saved"/>
  
   <!-- MAIN CONTENT -->
	<div class="container">
	
		<!-- TITLE -->
		<util:title titleDirect="${savedRoofDetails.savedName}"/>

		<div class="row justify-content-center">
			<div class="col-12">
				<div class="square">
				<p class="seccion-title"><spring:message code="rec.input-message"/></p>   
					<div class="row justify-content-center">	      	
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="row justify-content-center margin-btm">
								 <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
								 	<util:labelField type="text" dto="${savedRoofDetails.slope}" message="cat.slope" varName="slope" disabled="true" />
							     </div>
							     <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							     	<util:labelField type="text" dto="${savedRoofDetails.climate}" message="cat.climate.single" varName="climate" disabled="true" />
							     </div>
							      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							     	<label class="label-margin"><spring:message code="rec.results"/></label>
							     	<input disabled type="text" class="form-control form-control-sm dark" name="results" id="results" value="${savedRoofDetails.results}"> 
							     </div>
							</div>
						</div>
					</div>
			     </div>	
		    </div>		      	
		</div>
		
		<div class="row justify-content-center" id="range">	
			<div class="col-12">
				<div class="square">
				<p class="seccion-title st-margin "><spring:message code="rec.range-message"/></p>   
					<div class="row justify-content-center">	      	
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<util:range name="Aesthetics" margin="margin-btm" value="${savedRoofDetails.aesthetics}" disabled="disabled" />
							<util:range name="Implantation" margin="margin-btm" value="${savedRoofDetails.implantation}" disabled="disabled" />
							<util:range name="Maintenance" margin="margin-btm" value="${savedRoofDetails.maintenance}" disabled="disabled" />
							<util:range name="Price" margin="margin-btm" value="${savedRoofDetails.price}" disabled="disabled" />
							<util:range name="Regulation" margin="margin-btm" value="${savedRoofDetails.regulation}" disabled="disabled" />
							<util:range name="Storage" margin="margin-btm" value="${savedRoofDetails.storage}" disabled="disabled" />
							<util:range name="Thermal" margin="margin-btm" value="${savedRoofDetails.thermal}" disabled="disabled" />
							<util:range name="Traffic" margin="margin-btm" value="${savedRoofDetails.traffic}" disabled="disabled" />
							<util:range name="Wall" margin="margin-btm" value="${savedRoofDetails.wall}" disabled="disabled" />
							<util:range name="Weight" margin="margin-btm" value="${savedRoofDetails.weight}" disabled="disabled" />
							<util:range name="Winds" margin="margin-btm" value="${savedRoofDetails.winds}" disabled="disabled" />
						</div>
					</div>		

				</div>
			</div>
		</div>

		
		<form:form id="recommenderFormSave" method="post">
			<input type="hidden" name="idUserSavedRoof" value="${savedRoofDetails.idUserSavedRoof}"/>
			<c:if test="${savedRoofDetails.userSavedRoofDetailsDTO != null}">
				<c:choose>
					<c:when test="${empty savedRoofDetails.userSavedRoofDetailsDTO}">
						<util:noResults/>
					</c:when>	
					<c:otherwise>
						
						<div class="row justify-content-center">
						
							<!-- TABLE FOR BIG AND MEDIUM SCREENS -->
							<div class="col-12 d-none d-md-block text-center" style="margin-bottom: 30px;">
								<table class="table table-sm table-hover table-striped table-dark">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col"><spring:message code="cat.maker"/></th>
											<th scope="col"><spring:message code="cat.model"/></th>
											<th scope="col"><spring:message code="cat.type"/></th>
											<th scope="col"><spring:message code="rec.final-rate"/></th>
											<th scope="col"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${savedRoofDetails.userSavedRoofDetailsDTO}" var="bestRoof" varStatus="bestRoofsLoop">
											<tr>
												<th scope="row">${bestRoofsLoop.index+1}</th>
												<td>${bestRoof.roofDTO.maker}</td>
												<td>${bestRoof.roofDTO.model}</td>
												<td><spring:message code="${bestRoof.roofDTO.type}"/></td>
												<td>${bestRoof.finalRate}</td>
												<c:if test="${bestRoof.selected == true}">
													<td><input disabled checked type="radio" name="selected" value="${bestRoof.roofDTO.idRoof}"></td>
												</c:if>
												<c:if test="${bestRoof.selected == false}">
													<td></td>
												</c:if>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							
							<!-- TABLE FOR SMALL SCREENS -->
							<div class="col-12 d-md-none text-center" style="margin-bottom: 30px;">
								<table class="table table-sm table-hover table-striped table-dark">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col"><spring:message code="cat.maker"/></th>
											<th scope="col"><spring:message code="cat.model"/></th>
											<th scope="col"><spring:message code="rec.final-rate"/></th>
											<th scope="col"></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${savedRoofDetails.userSavedRoofDetailsDTO}" var="bestRoof" varStatus="bestRoofsLoop">
											<tr>
												<th scope="row">${bestRoofsLoop.index+1}</th>
												<td>${bestRoof.roofDTO.maker}</td>
												<td>${bestRoof.roofDTO.model}</td>
												<td>${bestRoof.finalRate}</td>
												<c:if test="${bestRoof.selected == true}">
													<td><input disabled checked type="radio" name="selected" value="${bestRoof.roofDTO.idRoof}"></td>
												</c:if>
												<c:if test="${bestRoof.selected == false}">
													<td></td>
												</c:if>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>

						</div>

					</c:otherwise>
				</c:choose>
				
				<!-- BUTTONS -->
				<div class="row justify-content-center">
					<div class="col-12 text-center">
						<div class="square">
							<div class="col-12 text-center">
							
								<button type="button" class="btn btn-outline-info margin-all"
									data-toggle="modal" data-target="#chartsModalSaved">
									<i class="w3-medium fa fa-bar-chart" ></i> <spring:message code="button.charts" /> 
								</button>

								<button type="button" class="btn btn-outline-danger margin-all"
									data-toggle="modal" data-target="#confirmModal">
									<i class="w3-medium fa fa-trash"></i> <spring:message code="button.delete" />
								</button>
								
							</div>
						</div>
					</div>
				</div>
				
			</c:if>
			
			<!-- MODALS	 -->
			<jsp:include page="/WEB-INF/jsp/modal/modal_charts_saved.jsp" />
			<modal:confirmModal type="warning" message="question.delete-rec"
					submit="${delete}" submitButton="button.delete" />

		</form:form>
	</div>


</body>

<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />

