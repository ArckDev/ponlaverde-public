<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged />
  
   <!-- MAIN CONTENT -->
	<div class="container">

		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
				<div class="square">
					<p>Version information</p>
					<img src="${context}/images/ack_question_ico.png" height=200px>
					<table class="table-striped table-bordered" style="margin-top: 40px;margin-bottom: 40px;margin-left: auto;margin-right: auto;width: 100%;">
						<tr>
							<td><h5>Status</h5></td>
							<td><h5>${applicationName}</h5><b>is up and running...</b></td>
						</tr>
						<tr>
							<td><h5>Build Version</h5></td>
							<td><b>${buildVersion}</b></td>
						</tr>
						<tr>
							<td><h5>Build Time</h5></td>
							<td><b>${buildTimestamp}</b></td>
						</tr>
					</table>

					<button class="btn btn-outline-warning margin-all" onclick="goBack()"><spring:message code="button.return"/></button>
				</div>
			</div>
		</div>

	</div>

</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />

<script>
function goBack() {
  window.history.back();
}
</script> 