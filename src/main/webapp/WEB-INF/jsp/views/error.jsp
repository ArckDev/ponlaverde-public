<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged />
  
   <!-- MAIN CONTENT -->
	<div class="container">
	
	<c:set var="jsessionid" value="${pageContext.session.id}"/>
	<jsp:useBean id="now" class="java.util.Date" />

		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
				<div class="square">
					<p><spring:message code="error.generic"/></p>
					<img src="${context}/images/ack_error_ico.png" height=200px>
					<table class="table-striped table-bordered" style="margin-top: 40px;margin-bottom: 40px;margin-left: auto;margin-right: auto;width: 100%;">
						<tr>
							<td><h5><spring:message code="error.generic.session"/></h5></td>
							<td>${jsessionid}</td>
						</tr>
						<tr>
							<td><h5><spring:message code="error.generic.time"/></h5></td>
							<td><fmt:formatDate pattern="dd/MM/yy (HH:mm:ss)" value="${now}" /></td>
						</tr>
						<tr>
							<td><h5><spring:message code="error.generic.path"/></h5></td>
							<td>${pageContext.errorData.requestURI}</td>
						</tr>
						<tr>
							<td><h5><spring:message code="error.generic.code"/></h5></td>
							<td>${pageContext.errorData.statusCode}</td>
						</tr>
						<tr>
							<td><h5><spring:message code="error.generic.message"/></h5></td>
							<td>${pageContext.exception.message}</td>
						</tr>
					</table>

					<button class="btn btn-outline-warning margin-all" onclick="goBack()"><spring:message code="button.return"/></button>
				</div>
			</div>
		</div>

	</div>

</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />

<script>
function goBack() {
  window.history.back();
}
</script> 