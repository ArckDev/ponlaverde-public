<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<spring:url value="/supplier/validation/details" var="details" />


<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="validate"/>
  
   <!-- MAIN CONTENT -->
	<div class="container">
		
		<!-- TITLE -->
		<util:title title="app.validate"/>
		

		<!-- RESULTS -->
		<view:roofsListViewer detailsPath="${details}" validationView="true"/>

	</div>
	
<jsp:include page="/WEB-INF/jsp/modal/modal_success_generic.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_warning_generic.jsp" />

</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />