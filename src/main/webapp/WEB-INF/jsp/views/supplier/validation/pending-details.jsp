<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal" %>

<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<!-- PATHS -->
<spring:url value="/supplier/validation/delete" var="delete" />
<spring:url value="/supplier/validation/edit" var="edit" />


<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="validate"/>

	   <!-- MAIN CONTENT -->
		<div class="container">

			<form:form class="form-horizontal" method="post">
			
				<!-- TITLE AND STATUS -->
				<util:titleStatus title="cat.details"/>
				
				<!-- REJECTION REASON -->
				<util:rejectionReason/>
			
				<!-- ROOF DETAILS -->
				<view:roofsDetailsViewer/>
			
				<div class="row">
					
					<!-- BUTTONS -->
					<div class="col-12 text-center">
						<div class="square">
							<button type="button" class="btn btn-outline-danger margin-all" data-toggle="modal"
							data-target="#confirmModal"><spring:message code="button.delete"/></button>

							<button type="submit" formaction="${edit}" name="edit"
							class="btn btn-outline-success margin-all"><spring:message code="button.update" /></button>

						</div>
					</div>
				</div>
				
				<!-- MODAL -->
				<modal:confirmModal type="warning" message="question.delete-roof" submit="${delete}" submitButton="button.delete"/>

			</form:form>

		</div>

</body>

<!-- MODALS -->
<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />