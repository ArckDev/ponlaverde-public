<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="cat" tagdir="/WEB-INF/tags/catalogue" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />


<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />

<body class="bg2">

	<!-- NAVBAR -->
	<util:navbarLogged activeElement="dashboard" />

	<!-- MAIN CONTENT -->
	<div class="container">
	
		<!-- TITLE -->
		<util:title title="app.dashboard"/>

			<div class="row ">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
					<div class="square auto bg1">
						<div class="dashboard-stat">
							<div class="display-number">
								<h3 class="counter font-green-sharp"
									data-count="${supplierTotalRoofsByStatus}">0</h3>
								<div>
									<small><spring:message code="dash.roofs.state" /></small>
								</div>
							</div>
							<div class="charts">
								<canvas id="supplierTotalRoofsByStatus" style="height: 180px;"></canvas>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="square auto bg1">
						<div class="dashboard-stat">
							<div class="display">
								<div>
									<small><spring:message code="dash.users-cities.residence" /></small>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

									<div class="scrollable">
										<table
											class="table table-sm table-hover table-striped table-dark table-dark-stat">
											<thead>
												<tr>
													<th scope="col">#</th>
													<th scope="col"><spring:message code="dash.users-cities.city" /></th>
													<th scope="col" style="text-align: center;"><spring:message code="dash.users-cities.totals" /></th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${supplierCommonUserCities}"
													var="cities" varStatus="citiesLoop">
													<tr>
														<th scope="row">${citiesLoop.index+1}</th>
														<td><div>${fn:substring(cities.city, 0, 45)}</div>
															<div class="font-small italic">
																<spring:message code="${cities.country}" />
															</div></td>
														<td style="text-align: center;">${cities.count}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
									<div class="charts">
										<canvas id="supplierCommonUserCities"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="row">
	
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="square auto bg1">
						<div class="dashboard-stat">
							<div class="display">
								<div>
									<small><spring:message code="dash.rec.roofs.suggested" /></small>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
									<div class="scrollable">
										<table
											class="table table-sm table-hover table-striped table-dark table-dark-stat">
											<thead>
												<tr>
													<th scope="col">#</th>
													<th scope="col"><spring:message code="dash.rec.model" /></th>
													<th scope="col" style="text-align: center;"><spring:message code="dash.rec.type" /></th>
													<th scope="col" style="text-align: center;"><spring:message code="dash.rec.suggested" /></th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${supplierMostSuggestedRoofs}" var="roofs" varStatus="roofsLoop">
													<tr>
														<th scope="row">${roofsLoop.index+1}</th>
														<td><div>${fn:substring(roofs.roofName, 0, 45)}</div>
															<div class="font-small italic">${roofs.roofMaker}</div></td>
														<td style="text-align: center;"><spring:message
																code="${roofs.roofType}" /></td>
														<td style="text-align: center;">${roofs.count}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
									<div class="charts">
										<canvas id="supplierMostSuggestedRoofs"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="square auto bg1">
						<div class="dashboard-stat">
							<div class="display">
								<div>
									<small><spring:message code="dash.rec.roofs.chosen" /></small>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
									<div class="scrollable">
										<table
											class="table table-sm table-hover table-striped table-dark table-dark-stat">
											<thead>
												<tr>
													<th scope="col">#</th>
													<th scope="col"><spring:message code="dash.rec.model" /></th>
													<th scope="col" style="text-align: center;"><spring:message code="dash.rec.type" /></th>
													<th scope="col" style="text-align: center;"><spring:message code="dash.rec.chosen" /></th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${supplierMostChosenRoofs}" var="roofs" varStatus="roofsLoop">
													<tr>
														<th scope="row">${roofsLoop.index+1}</th>
														<td><div>${fn:substring(roofs.roofName, 0, 45)}</div>
															<div class="font-small italic">${roofs.roofMaker}</div></td>
														<td style="text-align: center;"><spring:message
																code="${roofs.roofType}" /></td>
														<td style="text-align: center;">${roofs.count}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
									<div class="charts">
										<canvas id="supplierMostChosenRoofs"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	

			</div>



	</div>


</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />



<!-- CHARTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

<!-- CHARTS SETTINGS -->
<script src="${context}/js/chartsSetting.js"></script>

<!-- SETTING DATA FOR CHARTS -->
<c:set var="supplierDashboardJson" value="${supplierDashboardJson}"/>

<!-- CHARTS COMMON DATA -->
<script>
// Data
var supplierDashboardJson = ${supplierDashboardJson};

// Set data into json object
var supplierDashboardJsonArray = {"jsonarray" : supplierDashboardJson };
</script>


<!-- CHARTS -->
<script>

//supplierTotalRoofsByStatus
var labels = supplierDashboardJsonArray.jsonarray.supplierTotalRoofsByStatus.list.map(function(e) {
	return e.field;
});
var count = supplierDashboardJsonArray.jsonarray.supplierTotalRoofsByStatus.list.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#supplierTotalRoofsByStatus', size, labels, count, true);



//supplierMostSuggestedRoofs
var labels = supplierDashboardJsonArray.jsonarray.supplierMostSuggestedRoofs.map(function(e) {
	return e.roofName;
});
var count = supplierDashboardJsonArray.jsonarray.supplierMostSuggestedRoofs.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#supplierMostSuggestedRoofs', size, labels, count, false);



//supplierMostChosenRoofs
var labels = supplierDashboardJsonArray.jsonarray.supplierMostChosenRoofs.map(function(e) {
	return e.roofName;
});
var count = supplierDashboardJsonArray.jsonarray.supplierMostChosenRoofs.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#supplierMostChosenRoofs', size, labels, count, false);



//supplierCommonUserCities
var labels = supplierDashboardJsonArray.jsonarray.supplierCommonUserCities.map(function(e) {
	return e.city;
});
var count = supplierDashboardJsonArray.jsonarray.supplierCommonUserCities.map(function(e) {
	return e.count;
});
var size = Object.keys(labels).length;
createDonutChart('#supplierCommonUserCities', size, labels, count, false);



</script>

