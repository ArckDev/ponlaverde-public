<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="cat" tagdir="/WEB-INF/tags/catalogue" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<spring:url value="/supplier/catalogue/details" var="details" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />

<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged activeElement="catalogue"/>
  
   <!-- MAIN CONTENT -->
	<div class="container">
	
		<!-- TITLE -->
		<div class="row">
			<div class="col-12">
				<div class="square">
					<div class="row">
						<div class="col-12">
							<p class="title"><spring:message code="cat.catalogue"/></p>
						</div>
		
					</div>
				</div>
			</div>
		</div>

		<!-- FILTER -->
		<cat:catalogueRoofsFilter />

		<!-- RESULTS -->
		<view:roofsSquaresViewer detailsPath="${details}" />

	</div>

<!-- MODALS	 -->
<jsp:include page="/WEB-INF/jsp/modal/modal_success_generic.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_warning_generic.jsp" />
<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />

</body>



<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />