<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg2">

  <!-- NAVBAR -->
  <util:navbarLogged/>
  
   <!-- MAIN CONTENT -->
	<div class="container menu">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-menu">
				<img class="bg3 round" src="${context}/images/logo_no_bg.png">
			</div>			
		</div>		
	</div>

</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />