<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<!-- HEADER -->
<jsp:include page="/WEB-INF/jsp/default/default_header.jsp" />


<body class="bg1">

	<!-- NAVBAR -->
	<jsp:include page="/WEB-INF/jsp/navbar/navbar_welcome.jsp" />

	<div class="bgimg-1 shape1">
		<div class=" row justify-content-center text-center">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo">
				<img class="bg3 round" src="images/logo_no_bg.png">
			</div>
		</div>
	</div>

	<!-- METRICS -->
  	<div class="metrics bg1" style="text-align:center;padding:50px 80px;">
	    <div class="row justify-content-center text-center">
	    
	      <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	              <span class=""><spring:message code="visits"/></span>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <span class="counter font-green-banner" data-count="${welcomeMetricsDTO.visits}">0</span>
	          </div>
	        </div>
	      </div>
	
	      <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	              <span class=""><spring:message code="catalogue"/></span>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <span class="counter font-green-banner" data-count="${welcomeMetricsDTO.catalogue}">0</span>
	          </div>
	        </div>
	      </div>
	
	      <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <span><spring:message code="users"/></span>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <span class="counter font-green-banner" data-count="${welcomeMetricsDTO.users}">0</span>
	          </div>
	        </div>
	      </div>
	
	      <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <span><spring:message code="suppliers"/></span>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <span class="counter font-green-banner" data-count="${welcomeMetricsDTO.suppliers}">0</span>
	          </div>
	        </div>
	      </div>
	
	    </div>
	</div>

	<div class="bgimg-2 shape2">
		<div class="caption">
			<!-- <span class="border" style="background-color:transparent;font-size:25px;color: #f7f7f7;"></span> -->
		</div>
	</div>

	<div id="project" style="position: relative;">
		<div class="bg1 text-areas">
			
			<h3><spring:message code="lbl.project.description"/></h3>			
			<p><spring:message code="wel.project"/></p>

		</div>
	</div>

	<div class="bgimg-3 shape2">
		<div class="caption"></div>
	</div>

	<div id="application" style="position: relative;">
		<div class="bg1 text-areas">

			<h3><spring:message code="lbl.app.description" /></h3>
			<p><spring:message code="wel.app" /></p>


				
		</div>
	</div>


	<div class="bgimg-4 shape4">
		<div class="caption">
			<button type="button" class="btn-success border" data-toggle="modal"
				data-target="#registerModal">
				<spring:message code="register" />
			</button>
		</div>
	</div>

	<!-- LOGIN MODAL -->
	<jsp:include page="/WEB-INF/jsp/modal/modal_login.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_restore_pass.jsp" />

	<!-- REGISTER MODAL -->
	<jsp:include page="/WEB-INF/jsp/modal/modal_register.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_register_sup.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_register_ind.jsp" />

	<!-- FEEDBACK MODALS  -->
	<jsp:include page="/WEB-INF/jsp/modal/modal_success_email.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_success_generic.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_error_generic.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_processing.jsp" />

	<!-- MORE MODALS -->
	<jsp:include page="/WEB-INF/jsp/modal/modal_credits.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_cookies.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_GDPR.jsp" />
	<jsp:include page="/WEB-INF/jsp/modal/modal_terms.jsp" />



</body>


<!-- FOOTER -->
<jsp:include page="/WEB-INF/jsp/default/default_footer.jsp" />