<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />


<!DOCTYPE html>

<html>
<head>
  <title>Ponla verde</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="${context}/images/logo_banner_no_bg.png">

	<!-- GENERIC STYLESHEET -->	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"> <!-- Latest compiled and minified CSS -->
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet"> 
	
	<!-- TOGGLE STYLESHEET -->
	<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
	
	<!-- ICONS STYLESHEET -->
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" >
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" >
	
	<!-- PONLAVERDE STYLESHEET -->
	<link href="${context}/stylesheet/ponlaverde.css" rel="stylesheet" type="text/css" >
	
	
	<!-- JS SCRIPTS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> <!-- jQuery library -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script> <!-- Popper JS -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script> <!-- Latest compiled JavaScript -->

</head>

<script type="text/javascript">
    $(window).on('load',function(){
        $('${onLoadModal}').modal('show');
    });
    
    $(window).on('load',function(){
        $('${onLoadToggle}').collapse();
    });
</script>