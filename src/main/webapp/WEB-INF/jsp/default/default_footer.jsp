<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page language="java" import="java.util.*" %> 
<%@ page import = "java.util.ResourceBundle" %>
<%@ page import = "org.springframework.context.i18n.LocaleContextHolder" %>
<% Locale locale = LocaleContextHolder.getLocale();
   ResourceBundle messageBundle = ResourceBundle.getBundle("build", locale); 
   String version = messageBundle.getString("build.version");
   String date = messageBundle.getString("build.date");
   String user = messageBundle.getString("build.user");
%>

<c:set var="context" value="${pageContext.request.contextPath}" />
  
  <footer class="text-center">
    <div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <a href="http://www.upc.edu"><img src="${context}/images/logo_UPC.png"></a>
              <a href="http://epseb.upc.edu"><img src="${context}/images/logo_EPSEB.png"></a>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <span class="font-medium footer-text">ponlaverde® developed by <a href="mailto:arck23@gmail.com">Jose RL</a></span><br>
              <jsp:useBean id="now" class="java.util.Date" />
              <fmt:formatDate var="year" value="${now}" pattern="yyyy" />
              <span class="footer-text" style="font-size: 12px;">Made with &hearts; in Barcelona 2018-${year}</span><br>
              <span class="footer-text" style="font-size: 12px;color: white">Version: <%=version%> | Build: <%=date%>.<%=user%></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
</html>

	<!-- JS SCRIPTS -->
	<script src="${context}/js/footer.js"></script>
	<script src="${context}/js/counterUp.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/js/bootstrap4-toggle.min.js"></script>
	