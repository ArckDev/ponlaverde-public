<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />


  <nav class="navbar navbar-expand-lg navbar-dark bg3 fixed-top">
  
    <a class="navbar-brand" href="#">
      <img src="images/logo_banner_no_bg.png" width="30" height="30" class="d-inline-block align-top" alt="">
      <span class="ponla"><spring:message code="ponla"/></span><span class="verde"><spring:message code="verde"/></span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarWelcome">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarWelcome">
    
    	<!-- OPTIONS LINKS -->
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	        <li class="nav-item">
	          	<a class="nav-link" href="#project"><spring:message code="lbl.project.description"/></a>
	        </li>
	        <li class="nav-item">
	        	<a class="nav-link" href="#application"><spring:message code="lbl.app.description"/></a>
	        </li>
	        <li class="nav-item">
	          	<a class="nav-link" data-toggle="modal" data-target="#creditsModal"><spring:message code="lbl.credits"/></a>
	        </li>
		</ul>
		
		<div class="dropdown-divider"></div>
		
		<!-- LANGUAGE LINKS -->
		<ul class="navbar-nav">
			<li class="nav-item dropdown">
	          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	            <spring:message code="language"/>
	          </a>
	          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	            <a class="dropdown-item" href="?lang=ca">CAT</a>
	            <a class="dropdown-item" href="?lang=es">CAS</a>
	            <a class="dropdown-item" href="?lang=en">ENG</a>
	          </div>
	        </li>
		</ul>
		
		<!-- BUTTON TRIGGER MODAL -->
		<button type="button" class="btn btn-outline-success margin" data-toggle="modal"
			data-target="#registerModal"> <i class="w3-medium fa fa-user-plus"></i> <spring:message code="register"/> </button>
		<button type="button" class="btn btn-outline-primary" data-toggle="modal"
			data-target="#loginModal"> <i class="w3-medium fa fa-sign-in"></i> <spring:message code="enter"/> </button>
			
	</div>
	
  </nav>