<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:url value="${contextPath}/login" var="login" />

		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content bg3">
					<div class="modal-body centered" >
						<div class="form-group ${error != null ? 'has-error' : ''}">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<form:form class="form-horizontal" method="post"
								modelAttribute="loginForm" action="${login}" >
								<p class="title"><spring:message code="login.modal.title"/></p>
							
								<div class="row">
									<div class="col-2"></div>
									<div class="col-8" >
										<label class="label-margin"><spring:message code="user"/></label> 
										<input type="email" required class="form-control form-control-sm dark" name="username" placeholder="<spring:message code="user.placeholder"/>"> 
										<label class="label-margin"><spring:message code="password"/></label>
										<input type="password" required class="form-control form-control-sm dark" name="password" placeholder="<spring:message code="pass.placeholder"/>">
										<c:if test="${not empty error}"> 
											<div class="alert alert alert-danger text-center feedback-message" role="alert"> 
		  										<span>${error}</span>
											</div>
										</c:if>
									</div>
								</div>
								<div class="col-12 text-center">
									<button type="submit" class="btn btn-outline-primary login-btn" name="login">
										<i class="w3-medium fa fa-sign-in"></i>
										<spring:message code="enter" />
									</button>
								</div>
								<div class="col-12 text-center">
									<a class="text-info open-info" data-toggle="modal" data-dismiss="modal"
										data-target="#restorePassModal"> <spring:message code="recover.password" />
									</a>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	
	
