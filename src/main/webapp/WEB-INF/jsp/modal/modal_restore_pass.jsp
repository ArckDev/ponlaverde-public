<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:url value="/restore-password" var="restorePassword" />

		<div class="modal fade" id="restorePassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content bg3">
					<div class="modal-body centered" >
						<div class="form-group ${error != null ? 'has-error' : ''}">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<form:form class="form-horizontal" method="post"
								modelAttribute="loginForm" action="${restorePassword}" >
								<p class="title"><spring:message code="restore.modal.title"/></p>
							
								<div class="row">
									<div class="col-2"></div>
									<div class="col-8" >
										<label class="label-margin"><spring:message code="user"/></label> 
										<input type="email" required class="form-control form-control-sm dark" name="username" placeholder="<spring:message code="user.placeholder"/>"> 
										<c:if test="${not empty error}"> 
											<div class="alert alert alert-danger text-center feedback-message" role="alert"> 
		  										<span>${error}</span>
											</div>
										</c:if>
									</div>
								</div>
								<div class="col-12 text-center">
									<button type="submit" class="btn btn-outline-primary login-btn" name="login" onclick="sendForm();">
										<i class="w3-medium fa fa-sign-in"></i>
										<spring:message code="restore.modal.button" />
									</button>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
<script type="text/javascript">
function sendForm() {
    $('#processingModal').modal('toggle');
    this.form.submit();
}
</script>
	
