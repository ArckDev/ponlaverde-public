<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />


<spring:url value="/register" var="register" />

	<form:form id="register-form-ind" class="form-horizontal" method="post"
		modelAttribute="registerForm" action="${register}">

		<div class="modal fade" id="registerModalInd" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content bg3">
					<div class="modal-body centered">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<p class="title"><spring:message code="register.ind"/></p>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
								
									<input name="userType" type="hidden" value="IN" id="userType-ind">

									<label class="label-form-subgroup label-margin"><spring:message code="reg.personal-data"/></label> 	
									<div class="form-subgroup dark-border" style="min-height: 280px;">
										<label class="label-margin"><spring:message code="user.name"/></label> 
										<input type="text" class="form-control form-control-sm dark" name="name" id="user-name-ind" > 
										
										<label class="label-margin"><spring:message code="user.surname"/></label>
										<input type="text" class="form-control form-control-sm dark" name="surnames" id="user-surname-ind">
										
										<label class="label-margin"><spring:message code="user.dob"/></label> 
										<input type="date" class="form-control form-control-sm dark" name="dateOfBirth" id="user-dob-ind"> 
									
										<div style="margin-top: 10px;margin-bottom: -30px;"> 
											<input type="checkbox" id="privacy-ind" name="privacy">
										  	<a href="#gdprModal" data-toggle="modal" data-target="#gdprModal"><spring:message code="gdpr.title-privacy"/>*</a><br>
										  	
										  	<input type="checkbox" id="terms-ind" name="terms">
										  	<a href="#termsModal" data-toggle="modal" data-target="#termsModal"><spring:message code="cond.title-terms"/>*</a>
										</div>

									</div>
									
									<label class="label-form-subgroup label-margin"><spring:message code="reg.profess-data"/></label> 	
									<div class="form-subgroup dark-border">
										<label class="label-margin"><spring:message code="user.company"/></label> 
										<input type="text" class="form-control form-control-sm dark" name="company" id="company-ind"> 
										
										<label class="label-margin"><spring:message code="user.occupation"/></label>
										<input type="text" class="form-control form-control-sm dark" name="occupation" id="occupation-ind">
									</div>
									
								</div>
								
								<div class="col-md-6" >

									<label class="label-form-subgroup label-margin"><spring:message code="reg.access-data"/></label> 	
									<div class="form-subgroup dark-border" style="min-height: 280px;">
										<label class="label-margin"><spring:message code="user.email"/></label>
										<input type="email" class="form-control form-control-sm dark" name="username" id="user-username-ind" onblur="checkEmailExistInd()">
										
										<label class="label-margin"><spring:message code="user.password"/></label> 
										<input id="repeat-password-ind" type="password" class="form-control form-control-sm dark" name="passwordRepeat" > 
										
										<label class="label-margin"><spring:message code="user.rep.password"/></label>
										<div class="password-box">
									    	<input id="password-ind" type="password" name="password" class="form-control form-control-sm dark">
									  	</div>
									</div>
									
									<label class="label-form-subgroup label-margin"><spring:message code="reg.geo-data"/></label> 	
									<div class="form-subgroup dark-border">
										<label class="label-margin"><spring:message code="user.country"/></label> 
										<select class="form-control form-control-sm dark" name="country" id="country-ind" onchange="selectCitiesInd()">
											<c:forEach items="${countries}" var="country">
												<c:choose>
													<c:when test="${country.key == 'ES'}">
														<c:set var="countrySel" value="ES"/>
														<option selected value="${country.key}">${country.key} - ${country.value}</option>
													</c:when>
													<c:otherwise>
														<c:set var="countrySel" value="${country.key}"/>
														<option value="${country.key}">${country.key} - ${country.value}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
										<label class="label-margin"><spring:message code="user.city"/></label> 
										<select class="form-control form-control-sm dark" name="city" id="user-city-ind-sel">
											<c:forEach items="${spanishTowns}" var="towns">
												<c:choose>
													<c:when test="${towns.value == 'Barcelona'}">
														<option selected value="${towns.value}">${towns.value}</option>
													</c:when>
													<c:otherwise>
														<option value="${towns.value}">${towns.value}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
										<input style="display: none;" type="text" class="form-control form-control-sm dark" name="city" id="user-city-ind-inp">
									</div>

								</div>
							</div>
						</div>
						<div class="col-12 text-center">
							<button type="button" id="registerSubmitInd" onclick="submitFormInd()" name="register" class="btn btn-outline-primary btn-margin-top">
								<i class="w3-medium fa fa-user-plus"></i>
								<spring:message code="register.action" />
							</button>
						</div>
					</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<div class="alert alert-danger" role="alert" id="feedback-ind" style="display:none;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</form:form>
	
	
	<!-- SCRIPTS FOT PASSWORD STRENGTH -->
	<script src="js/passwordStrength.js"></script>
	<script>
	    $(document).ready(function () {
	        var obj = $('#password-ind').passwordStrength();
	        var obj2 = $('#passwort-ind').passwordStrength();
	    });
	</script>
	

	<script src="${context}/js/printErrorList.js"></script>
	<script src="${context}/js/sendAsyncRq.js"></script>
	
	<!-- SCRIPT FOR ASYNC RQ TO CHECK EMAIL IN DB -->
	<script type="text/javascript">
	function checkEmailExistInd() {

	    var rqData = {}
	    rqData["username"] = $("#user-username-ind").val();
	    var rqUrl = "${context}/api/search-username";

	    sendAsyncRq(rqUrl, rqData, "#feedback-ind", null, null, null);

	}
	</script>
	
	<!-- SCRIPT FOR SHOWING CITIES -->
	<script type="text/javascript">
	function selectCitiesInd() {
	    
	    var obj = $("#country-ind").val();
	    
	    if(obj != 'ES'){
	        $("#user-city-ind-sel").css('display','none');
	        $("#user-city-ind-inp").css('display','block'); 
	    } else {
	        $("#user-city-ind-sel").css('display','block');
	        $("#user-city-ind-inp").css('display','none'); 
	    }

	}
	</script>
	

	<!-- SCRIPT FOR ASYNC RQ TO VALIDATE AND SUBMIT FORM FIELDS -->
	<script type="text/javascript">
	function submitFormInd() {
	    
	    var city;
	    
	    if($("#user-city-ind-inp").val() != ''){
	        city = $("#user-city-ind-inp").val();
	    } else {
	        city = $("#user-city-ind-sel").val();
	    }

		var rqData = {}
		rqData["name"] = $("#user-name-ind").val();
		rqData["surnames"] = $("#user-surname-ind").val();
		rqData["dateOfBirth"] = $("#user-dob-ind").val();
	    rqData["city"] = city;
	    rqData["username"] = $("#user-username-ind").val();
	    rqData["userType"] = $("#userType-ind").val();
	    rqData["company"] = $("#company-ind").val();
	    rqData["occupation"] = $("#occupation-ind").val();
	    rqData["password"] = $("#password-ind").val();
	    rqData["passwordRepeat"] = $("#repeat-password-ind").val();
	    rqData["country"] = $("#country-ind").val();
	    rqData["privacy"] = $("#privacy-ind").prop("checked");
	    rqData["terms"] = $("#terms-ind").prop("checked");

	    var rqUrl = "${context}/api/validate-form-ind";
	    
	    sendAsyncRq(rqUrl, rqData, "#feedback-ind", "#registerModalInd", "#successEmailModal", "#processingModal");
	}
	</script>


