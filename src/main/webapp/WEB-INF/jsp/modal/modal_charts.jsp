<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="modal fade" id="chartsModal" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
	<view:recommenderCharts/>
</div>

<!-- SETTING DATA FOR CHARTS -->
<c:set var="chartData" value="${chartData}"/>

<!-- CHARTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>

<!-- CHARTS SETTINGS -->
<script src="${context}/js/chartsSetting.js"></script>

<!-- CHARTS DATA -->
<script>

	//DATA
	var chartData = ${chartData};
	
	//SET DATA INTO JSON OBJECT
	var jsonfile = {
		"jsonarray" : chartData
	};

	// GET DATA FROM JSON
	var labels = jsonfile.jsonarray.map(function(e) {
		return e.roofDTO.modelTruncated;
	});
	var dataPriceR = jsonfile.jsonarray.map(function(e) {
		return e.roofDTO.price;
	});
	var dataWeightR = jsonfile.jsonarray.map(function(e) {
		return e.roofDTO.weight;
	});
	var dataWallR = jsonfile.jsonarray.map(function(e) {
		return e.roofDTO.wall;
	});
	var dataPrice = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.price;
	});
	var dataWeight = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.weight;
	});
	var dataWall = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.wall;
	});
	var dataStorage = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.storage;
	});
	var dataImplantation = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.implantation;
	});
	var dataMaintenance = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.maintenance;
	});
	var dataWinds = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.winds;
	});
	var dataThermal = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.thermal;
	});
	var dataRegulation = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.regulation;
	});
	var dataTraffic = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.traffic;
	});
	var dataAesthetics = jsonfile.jsonarray.map(function(e) {
		return e.roofRatingDTO.aesthetics;
	});

	
	// COMBINED VERT CHART

	var ctxComb = document.getElementById("combChart")

	var configComb = {
			type : 'bar',
			data : {
				labels : labels,
				datasets : [ 
				{
					label : "<spring:message code='rec.price'/>",
					data : dataPrice,
					backgroundColor : prettyColors[0]
				} , {
					label : "<spring:message code='rec.weight'/>",
					data : dataWeight,
					backgroundColor : prettyColors[1]
				} , {
					label : "<spring:message code='rec.wall'/>",
					data : dataWall,
					backgroundColor : prettyColors[2]
				}, {
					label : "<spring:message code='rec.storage'/>",
					data : dataStorage,
					backgroundColor : prettyColors[3]
				}, {
					label : "<spring:message code='rec.implantation'/>",
					data : dataImplantation,
					backgroundColor : prettyColors[4]
				}, {
					label : "<spring:message code='rec.maintenance'/>",
					data : dataMaintenance,
					backgroundColor : prettyColors[5]
				}, {
					label : "<spring:message code='rec.thermal'/>",
					data : dataThermal,
					backgroundColor : prettyColors[6]
				}, {
					label : "<spring:message code='rec.regulation'/>",
					data : dataRegulation,
					backgroundColor : prettyColors[7]
				}, {
					label : "<spring:message code='rec.traffic'/>",
					data : dataTraffic,
					backgroundColor : prettyColors[8]
				}, {
					label : "<spring:message code='rec.aesthetics'/>",
					data : dataAesthetics,
					backgroundColor : prettyColors[9]
				}
				]
			},
			options: {
		        scales: {
		            xAxes: [{
// 		                barPercentage: 0.4,
// 		                categoryPercentage: 0.5
						display: true,
						truncate: true
		              }],
		            yAxes: [{
		                ticks: {
		                    suggestedMin: 0,
		                    suggestedMax: 100
		                }
		            }]
	        	},
	        	legend: {
        	      	display: true
        	    }
		    }
		};

		var combChart = new Chart(ctxComb, configComb);
	
		
// COMBINED VERT CHART MOBILE

		var ctxComb = document.getElementById("combChartM")

		var configComb = {
				type : 'bar',
				data : {
					labels : labels,
					datasets : [ 
						{
							label : "<spring:message code='rec.price'/>",
							data : dataPrice,
							backgroundColor : prettyColors[0]
						} , {
							label : "<spring:message code='rec.weight'/>",
							data : dataWeight,
							backgroundColor : prettyColors[1]
						} , {
							label : "<spring:message code='rec.wall'/>",
							data : dataWall,
							backgroundColor : prettyColors[2]
						}, {
							label : "<spring:message code='rec.storage'/>",
							data : dataStorage,
							backgroundColor : prettyColors[3]
						}, {
							label : "<spring:message code='rec.implantation'/>",
							data : dataImplantation,
							backgroundColor : prettyColors[4]
						}, {
							label : "<spring:message code='rec.maintenance'/>",
							data : dataMaintenance,
							backgroundColor : prettyColors[5]
						}, {
							label : "<spring:message code='rec.thermal'/>",
							data : dataThermal,
							backgroundColor : prettyColors[6]
						}, {
							label : "<spring:message code='rec.regulation'/>",
							data : dataRegulation,
							backgroundColor : prettyColors[7]
						}, {
							label : "<spring:message code='rec.traffic'/>",
							data : dataTraffic,
							backgroundColor : prettyColors[8]
						}, {
							label : "<spring:message code='rec.aesthetics'/>",
							data : dataAesthetics,
							backgroundColor : prettyColors[9]
						}
						]
				},
				options: {
			        scales: {
			            xAxes: [{
			                display: false
			              }],
			            yAxes: [{
			                ticks: {
			                    suggestedMin: 0,
			                    suggestedMax: 100
			                }
			            }]
		        	},
		        	legend: {
	        	      	display: false
	        	    }
			    }
			};

			var combChart = new Chart(ctxComb, configComb);



// PRICE CHART

	var ctxPrice = document.getElementById("priceChart");

	var configPrice = {
		type : 'bar',
		data : {
			labels : labels,
			datasets : [ {
				label : "<spring:message code='rec.price'/> (€/m2)",
				data : dataPriceR,
				backgroundColor : prettyColors[0]
			} ]
		},
		options: {
	        scales: {
	            xAxes: [{
	                display: true
	              }],
	            yAxes: [{
	                ticks: {
	                    suggestedMin: 0
	                }
	            }]
        	},
        	legend: {
    	      	display: false
    	    }
	    }
	};


	var priceChart = new Chart(ctxPrice, configPrice);
	
// PRICE CHART MOBILE

	var ctxPrice = document.getElementById("priceChartM");

	var configPrice = {
		type : 'bar',
		data : {
			labels : labels,
			datasets : [ {
				label : "<spring:message code='rec.price'/> (€/m2)",
				data : dataPriceR,
				backgroundColor : prettyColors[0]
			} ]
		},
		options: {
	        scales: {
	            xAxes: [{
	                display: false
	              }],
	            yAxes: [{
	                ticks: {
	                    suggestedMin: 0
	                }
	            }]
        	},
        	legend: {
    	      	display: false
    	    }
	    }
	};


	var priceChart = new Chart(ctxPrice, configPrice);

// WEIGHT CHART
	
	var ctxWeight = document.getElementById("weightChart");

	var configWeight = {
		type : 'bar',
		data : {
			labels : labels,
			datasets : [ {
				label : "<spring:message code='rec.weight'/> (kg/m3)",
				data : dataWeightR,
				backgroundColor : prettyColors[1]
			} ]
		},
		options: {
	        scales: {
	            xAxes: [{
	                display: true
	              }],
	            yAxes: [{
	                ticks: {
	                    suggestedMin: 0
	                }
	            }]
        	},
        	legend: {
    	      	display: false
    	    }
	    }
	};

	var weightChart = new Chart(ctxWeight, configWeight);
	
	
// WEIGHT CHART MOBILE
	
	var ctxWeight = document.getElementById("weightChartM");

	var configWeight = {
		type : 'bar',
		data : {
			labels : labels,
			datasets : [ {
				label : "<spring:message code='rec.weight'/> (kg/m3)",
				data : dataWeightR,
				backgroundColor : prettyColors[1]
			} ]
		},
		options: {
	        scales: {
	            xAxes: [{
	                display: false
	              }],
	            yAxes: [{
	                ticks: {
	                    suggestedMin: 0
	                }
	            }]
        	},
        	legend: {
    	      	display: false
    	    }
	    }
	};

	var weightChart = new Chart(ctxWeight, configWeight);

// WALL CHART
	
	var ctxWall = document.getElementById("wallChart");

	var configWall = {
		type : 'bar',
		data : {
			labels : labels,
			datasets : [ {
				label : "<spring:message code='rec.wall'/> (cm)",
				data : dataWallR,
				backgroundColor : prettyColors[2]
			} ]
		},
		options: {
	        scales: {
	            xAxes: [{
	                display: true
	              }],
	            yAxes: [{
	                ticks: {
	                    suggestedMin: 0
	                }
	            }]
        	},
        	legend: {
    	      	display: false
    	    }
	    }
	};

	var wallChart = new Chart(ctxWall, configWall);
	
// WALL CHART MOBILE
	
	var ctxWall = document.getElementById("wallChartM");

	var configWall = {
		type : 'bar',
		data : {
			labels : labels,
			datasets : [ {
				label : "<spring:message code='rec.wall'/> (cm)",
				data : dataWallR,
				backgroundColor : prettyColors[2]
			} ]
		},
		options: {
	        scales: {
	            xAxes: [{
	                display: false
	              }],
	            yAxes: [{
	                ticks: {
	                    suggestedMin: 0
	                }
	            }]
        	},
        	legend: {
    	      	display: false
    	    }
	    }
	};

	var wallChart = new Chart(ctxWall, configWall);

</script>


	