<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html; charset=UTF-8" %>
	
<c:set var="context" value="${pageContext.request.contextPath}" />

	<div class="modal fade" id="errorGenericModal" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
		<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
			<div class="modal-content bg3">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body centered">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 success-logo">
						<img src="${context}/images/ack_error_ico.png">
					</div>
					<div class="col-12 text-center">
						<div class="alert alert-danger" role="alert" style="margin-top:10%;"><spring:message code="${key}" text="Error occurred"/></div>
					</div>
				</div>
			</div>
		</div>
	</div>