<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html; charset=UTF-8" %>
	
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="modal fade" id="creditsModal" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content bg1">
			<div class="modal-body centered">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
				<div class="bg1 text-areas">
					<h3 class="text-areas-title"><spring:message code="lbl.credits" /></h3>
	
					<p><spring:message code="wel.credits"/></p>

				</div>
			</div>
		</div>
	</div>
</div>