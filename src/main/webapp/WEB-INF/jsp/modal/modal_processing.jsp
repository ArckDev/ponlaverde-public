<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html; charset=UTF-8" %>
	
<c:set var="context" value="${pageContext.request.contextPath}" />

	<div class="modal" id="processingModal" tabindex="-1"  aria-hidden="true" data-backdrop="static" data-keyboard="false" style="overflow-y: auto;">
		<div class="modal-dialog modal-dialog-centered modal-sm" >
			<div class="modal-content">
				<div class="modal-body centered">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 success-logo">
						<img src="${context}/images/processing.gif">
					</div>
					<div class="col-12 text-center">
					</div>
				</div>
			</div>
		</div>
	</div>