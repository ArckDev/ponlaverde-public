<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<c:set var="context" value="${pageContext.request.contextPath}" />



<spring:url value="/register" var="register" />

	<form:form id="register-form-sup" class="form-horizontal" method="post"
		modelAttribute="registerForm" action="${register}">

		<div class="modal fade" id="registerModalSup" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content bg3">
					<div class="modal-body centered">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<p class="title"><spring:message code="register.sup"/></p>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
								
									<input name="userType" type="hidden" value="SU" id="userType-sup">
	
									<label class="label-form-subgroup label-margin"><spring:message code="reg.profess-data"/></label> 	
									<div class="form-subgroup dark-border">
										<label class="label-margin"><spring:message code="user.supplier"/></label> 
										<input type="text" class="form-control form-control-sm dark" name="company" id="company-sup"> 
									</div>

									<label class="label-form-subgroup label-margin"><spring:message code="reg.geo-data"/></label> 	
									<div class="form-subgroup dark-border">
										<label class="label-margin"><spring:message code="user.country"/></label> 
										<select class="form-control form-control-sm dark" name="country" id="country-sup" onchange="selectCitiesSup()">
											<c:forEach items="${countries}" var="country">
												<c:choose>
													<c:when test="${country.key == 'ES'}">
														<option selected value="${country.key}">${country.key} - ${country.value}</option>
													</c:when>
													<c:otherwise>
														<option value="${country.key}">${country.key} - ${country.value}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
										<label class="label-margin"><spring:message code="user.city"/></label> 
										<select class="form-control form-control-sm dark" name="city" id="user-city-sup-sel">
											<c:forEach items="${spanishTowns}" var="towns">
												<c:choose>
													<c:when test="${towns.value == 'Barcelona'}">
														<option selected value="${towns.value}">${towns.value}</option>
													</c:when>
													<c:otherwise>
														<option value="${towns.value}">${towns.value}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
										<input style="display: none;" type="text" class="form-control form-control-sm dark" name="city" id="user-city-sup-inp"> 
									</div>
									
								</div>
								
								<div class="col-md-6" >

									<label class="label-form-subgroup label-margin"><spring:message code="reg.access-data"/></label> 	
									<div class="form-subgroup dark-border" style="min-height: 310px;">
										<label class="label-margin"><spring:message code="user.email"/></label>
										<input type="email" class="form-control form-control-sm dark" name="email" id="user-username-sup" onblur="checkEmailExistSup()">
										
										<label class="label-margin"><spring:message code="user.password"/></label> 
										<input id="repeat-password-sup" type="password" class="form-control form-control-sm dark" name="passwordRepeat" > 
										
										<label class="label-margin"><spring:message code="user.rep.password"/></label>
										<div class="password-box">
									    	<input id="password-sup" type="password" name="password" class="form-control form-control-sm dark">
									  	</div>
									  	
									  	<div style="margin-top: 20px;margin-bottom: -15px;">
										  	<input type="checkbox" id="privacy-sup" name="privacy">
										  	<a href="#gdprModal" data-toggle="modal" data-target="#gdprModal"><spring:message code="gdpr.title-privacy"/>*</a><br> 
										  	
										  	<input type="checkbox" id="terms-sup" name="terms">
										  	<a href="#termsModal" data-toggle="modal" data-target="#termsModal"><spring:message code="cond.title-terms"/>*</a>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="col-12 text-center">
							<button type="button" id="registerSubmitSup" onclick="submitFormSup()" name="register" class="btn btn-outline-success btn-margin-top">
								<i class="w3-medium fa fa-user-plus"></i>
								<spring:message code="register.action" />
							</button>
						</div>
					</div>
					<div class="modal-footer">
						<div class="col-12 text-center">
							<div class="alert alert-danger" role="alert" id="feedback-sup" style="display:none;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</form:form>
	
	
	<!-- SCRIPTS FOT PASSWORD STRENGTH -->
	<script src="js/passwordStrength.js"></script>
	<script>
	    $(document).ready(function () {
	        var obj = $('#password-sup').passwordStrength();
	        var obj2 = $('#passwort-sup').passwordStrength();
	    });
	</script>
	

	<script src="${context}/js/printErrorList.js"></script>
	<script src="${context}/js/sendAsyncRq.js"></script>
	
	<!-- SCRIPT FOR ASYNC RQ TO CHECK EMAIL IN DB -->
	<script type="text/javascript">
	function checkEmailExistSup() {

	    var rqData = {}
	    rqData["username"] = $("#user-username-sup").val();
	    
	    var rqUrl = "${context}/api/search-username";

	    sendAsyncRq(rqUrl, rqData, "#feedback-sup", null, null, null);

	}
	</script>
	
	<!-- SCRIPT FOR SHOWING CITIES -->
	<script type="text/javascript">
	function selectCitiesSup() {
	    
	    var obj = $("#country-sup").val();
	    
	    if(obj != 'ES'){
	        $("#user-city-sup-sel").css('display','none');
	        $("#user-city-sup-inp").css('display','block'); 
	    } else {
	        $("#user-city-sup-sel").css('display','block');
	        $("#user-city-sup-inp").css('display','none'); 
	    }

	}
	</script>

	<!-- SCRIPT FOR ASYNC RQ TO VALIDATE AND SUBMIT FORM FIELDS -->
	<script type="text/javascript">
	function submitFormSup() {
	    
		var city;
	    
	    if($("#user-city-sup-inp").val() != ''){
	        city = $("#user-city-sup-inp").val();
	    } else {
	        city = $("#user-city-sup-sel").val();
	    }

		var rqData = {}
	    rqData["city"] = city;
	    rqData["username"] = $("#user-username-sup").val();
	    rqData["userType"] = $("#userType-sup").val();
	    rqData["company"] = $("#company-sup").val();
	    rqData["password"] = $("#password-sup").val();
	    rqData["passwordRepeat"] = $("#repeat-password-sup").val();
	    rqData["country"] = $("#country-sup").val();
	    rqData["privacy"] = $("#privacy-sup").prop("checked");
	    rqData["terms"] = $("#terms-sup").prop("checked");
	    

	    var rqUrl = "${context}/api/validate-form-sup";
	    
	    sendAsyncRq(rqUrl, rqData, "#feedback-sup", "#registerModalSup", "#successEmailModal", "#processingModal");

	}
	</script>