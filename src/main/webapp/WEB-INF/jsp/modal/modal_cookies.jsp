<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html; charset=UTF-8" %>
	
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="modal fade" id="cookiesModal" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body centered">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="cookies-areas">
					<h3><spring:message code="cookies.title" /></h3>
					
					<div class="col-12 success-logo">
						<img src="${context}/images/ack_cookies_ico.png">
					</div>

					<p><spring:message code="wel.cookies" /></p>
				</div>
			</div>
		</div>
	</div>
</div>