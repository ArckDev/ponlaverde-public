<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html; charset=UTF-8" %>
	
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="modal fade" id="gdprModal" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body centered">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="cookies-areas">
					<h3><spring:message code="gdpr.title-privacy" /></h3>
					
					<div class="col-12 success-logo header-logo">
						<img src="${context}/images/ack_privacy_ico.png">
					</div>

					<p><spring:message code="gdpr.text-main" /></p>
					<h4><spring:message code="gdpr.title-1" /></h4>
					<p><spring:message code="gdpr.text-1" /></p>
					<h4><spring:message code="gdpr.title-2" /></h4>
					<p><spring:message code="gdpr.text-2" /></p>
					<h4><spring:message code="gdpr.title-2.1" /></h4>
					<p><spring:message code="gdpr.text-2.1" /></p>
					<h4><spring:message code="gdpr.title-3" /></h4>
					<p><spring:message code="gdpr.text-3" /></p>
					<h4><spring:message code="gdpr.title-4" /></h4>
					<p><spring:message code="gdpr.text-4" /></p>
					<h4><spring:message code="gdpr.title-5" /></h4>
					<p><spring:message code="gdpr.text-5" /></p>
				</div>
			</div>
		</div>
	</div>
</div>