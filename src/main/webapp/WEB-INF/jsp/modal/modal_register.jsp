<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html; charset=UTF-8" %>


<div class="modal fade bd-example-modal-sm" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content bg3">
     	<div class="modal-body centered">
     		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
     		<p class="title"><spring:message code="register"/></p>
     		
   			<div class="row justify-content-center">			
   				<div class="col-12 text-center">
   					<spring:message code="register.usertype"/>
   				</div>			
   				<div class="col-6 text-center">
					<button type="button"
						class="btn btn-outline-primary btn-lg btn-margin-top"
						data-toggle="modal" data-target="#registerModalInd"
						data-dismiss="modal" onclick="resetModalInd()">
						<i class="w3-medium fa fa-user"></i>
						<spring:message code="user.type.ind" />
					</button>
				</div>
	     		<div class="col-6 text-center">
					<button type="button"
						class="btn btn-outline-success btn-lg btn-margin-top"
						data-toggle="modal" data-target="#registerModalSup"
						data-dismiss="modal" onclick="resetModalSup()">
						<i class="w3-medium fa fa-users"></i>
						<spring:message code="user.type.sup" />
					</button>
				</div>
   			</div>
     	</div>
    </div>
  </div>
</div>


<script type="text/javascript">
	function resetModalInd() {

		$("#user-name-ind").val('');
		$("#user-surname-ind").val('');
		$("#user-dob-ind").val('');
	    $("#user-city-ind").val('');
	    $("#user-email-ind").val('');
	    $("#company-ind").val('');
	    $("#occupation-ind").val('');
	    $("#password-ind").val('');
	    $("#repeat-password-ind").val('');
	    $("#country-ind").val('ES');
	    $("#userType-ind").val('IN');
	}
	
	function resetModalSup() {

	    $("#user-city-sup").val('');
	    $("#user-email-sup").val('');
	    $("#company-sup").val('');
	    $("#password-sup").val('');
	    $("#repeat-password-sup").val('');
	    $("#country").val('ES');
	    $("#userType-sup").val('SU');

	}
	</script>