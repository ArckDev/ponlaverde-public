<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="disabled" required="false" %>

<!-- PATHS -->
<security:authorize access="hasRole('ROLE_AD')"> 
	<spring:url value="/admin/catalogue/details" var="details" />
	<spring:url value="/admin/catalogue/filter/1" var="filter" />
	<spring:url value="/admin/catalogue/filter-reset" var="reset" />
	<spring:url value="/admin/catalogue/new-roof" var="createRoof" />
</security:authorize>
<security:authorize access="hasRole('ROLE_IN')"> 
	<spring:url value="/individual/catalogue/details" var="details" />
	<spring:url value="/individual/catalogue/filter/1" var="filter" />
	<spring:url value="/individual/catalogue/filter-reset" var="reset" />
	<spring:url value="/individual/catalogue/new-roof" var="createRoof" />
</security:authorize>
<security:authorize access="hasRole('ROLE_SU')"> 
	<spring:url value="/supplier/catalogue/details" var="details" />
	<spring:url value="/supplier/catalogue/filter/1" var="filter" />
	<spring:url value="/supplier/catalogue/filter-reset" var="reset" />
	<spring:url value="/supplier/catalogue/new-roof" var="createRoof" />
</security:authorize>


<spring:message code="cat.climate.single" var="climateTitle"/>
<spring:message code="cat.type" var="typeTitle"/>
<spring:message code="info.roof-type" var="typeMessage" />
<spring:message code="cat.regulation" var="regulationTitle"/>
<spring:message code="info.roof-regulation" var="regulationMessage" />
<spring:message code="cat.storage" var="storageTitle"/>
<spring:message code="info.roof-storage" var="storageMessage" />
<spring:message code="cat.implantation" var="implantationTitle"/>
<spring:message code="info.roof-implantation" var="implantationMessage" />
<spring:message code="cat.maintenance" var="maintenanceTitle"/>
<spring:message code="info.roof-maintenance" var="maintenanceMessage" />
<spring:message code="cat.winds" var="windsTitle"/>
<spring:message code="info.roof-winds" var="windsMessage" />
<spring:message code="cat.aesthetics" var="aestheticsTitle"/>
<spring:message code="info.roof-aesthetics" var="aestheticsMessage" />
<spring:message code="cat.thermal" var="thermalTitle"/>
<spring:message code="info.roof-thermal" var="thermalMessage" />
<spring:message code="cat.traffic" var="trafficTitle"/>
<spring:message code="info.roof-traffic" var="trafficMessage" />





<div class="row">
	<div class="col-12">
		<div id="accordion">
			<div class="square bg3">
	
				<div id="headingOne">
					<div class="row">
						<div class="col-6 text-left">
							<a href="${createRoof}" class="btn btn-outline-success btn-sm" role="button">
								<i class="w3-medium fa fa-plus-circle"></i> <spring:message code="cat.new" /> 
							</a>
						</div>
						<div class="col-6 text-right">
							<button class="btn btn-outline-info btn-sm" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								<i class="w3-medium fa fa-filter" ></i> <spring:message code="cat.filter" /> 
							</button>
						</div>
					</div>
				</div>
	
				<form:form class="form-horizontal" method="post" modelAttribute="filterRoofForm" action="${filter}">
					<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">

							<p class="seccion-title"><spring:message code="cat.filter-message"/></p>
							
							<div class="row">   
							    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							  		<p><label class="label-margin font-medium"><spring:message code="cat.price"/> <spring:message code="cat.max"/></label></p>
							  		<div class="slidecontainer">
							  			<c:choose>
								  			<c:when test="${roofDTO.price != null}">
								  				<input type="range" min="${roofLimitValuesDTO.priceMin}" max="${roofLimitValuesDTO.priceMax}" value="${roofDTO.price}" class="slider" id="rangePrice" name="price">
								  			</c:when>
								  			<c:otherwise>
								  				<input type="range" min="${roofLimitValuesDTO.priceMin}" max="${roofLimitValuesDTO.priceMax}" value="${roofLimitValuesDTO.priceMax}" class="slider" id="rangePrice" name="price">
								  			</c:otherwise>
							  			</c:choose>
							  			<p class="italic-small text-center"><span id="valuePrice"></span> €/m²</p>
									</div>
							  	</div>	
							    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							    	<p><label class="label-margin font-medium"><spring:message code="cat.weight"/> <spring:message code="cat.max"/></label></p>
							    	<div class="slidecontainer">
									  	<c:choose>
								  			<c:when test="${roofDTO.weight != null}">
												<input type="range" min="${roofLimitValuesDTO.weightMin}" max="${roofLimitValuesDTO.weightMax}" value="${roofDTO.weight}" class="slider" id="rangeWeight" name="weight">
								  			</c:when>
								  			<c:otherwise>
												<input type="range" min="${roofLimitValuesDTO.weightMin}" max="${roofLimitValuesDTO.weightMax}" value="${roofLimitValuesDTO.weightMax}" class="slider" id="rangeWeight" name="weight">
								  			</c:otherwise>
							  			</c:choose>
									  	<p class="italic-small text-center"><span id="valueWeight"></span> kg/m²</p>
									</div>
							 	</div>
							  	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							    	<p><label class="label-margin font-medium"><spring:message code="cat.wall"/> <spring:message code="cat.max"/></label></p>
									<div class="slidecontainer">
									  	<c:choose>
								  			<c:when test="${roofDTO.wall != null}">
									  			<input type="range" min="${roofLimitValuesDTO.wallMin}" max="${roofLimitValuesDTO.wallMax}" value="${roofDTO.wall}" class="slider" id="rangeWall" name="wall">
								  			</c:when>
								  			<c:otherwise>
									  			<input type="range" min="${roofLimitValuesDTO.wallMin}" max="${roofLimitValuesDTO.wallMax}" value="${roofLimitValuesDTO.wallMax}" class="slider" id="rangeWall" name="wall">
								  			</c:otherwise>
							  			</c:choose>
									  	<p class="italic-small text-center"><span id="valueWall"></span> cm</p>
									</div>      		
								</div>
							</div>
							
							<div class="row">	
								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
									<c:if test="${disabled == true}">
										<label class="label-margin font-medium"><spring:message code="cat.maker"/></label>
										<input type="text" class="form-control form-control-sm dark" name="makerLbl" id="makerLbl" value="${userDTO.name}" disabled> 
									</c:if>
									<c:if test="${disabled == false || disabled == null}">
							     		<util:combo dto="${roofDTO.maker}" message="cat.maker" list="${roofLimitValuesDTO.makerList}" varName="maker" />	
									</c:if>
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
									<util:combo dto="${roofDTO.slope}" message="cat.slope" map="${slope}" varName="slope" />
							    </div>		      	
								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
									<util:info title="${climateTitle}" imagePath="${context}/images/spain_climate.jpg"/>
							    	<util:combo dto="${roofDTO.climate[0]}" message="cat.climate.single" map="${climate}" varName="climate" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">							    	
									<util:info title="${typeTitle}" message="${typeMessage}"/>
							     	<util:combo dto="${roofDTO.type}" message="cat.type" map="${type}" varName="type" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							    	<util:info title="${thermalTitle}" message="${thermalMessage}"/>
							     	<util:combo dto="${roofDTO.thermal}" message="cat.thermal" map="${rating}" varName="thermal" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							     	<util:info title="${regulationTitle}" message="${regulationMessage}"/>
							     	<util:combo dto="${roofDTO.regulation}" message="cat.regulation" map="${regulation}" varName="regulation" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							    	<util:info title="${storageTitle}" message="${storageMessage}"/>
							     	<util:combo dto="${roofDTO.storage}" message="cat.storage" map="${storage}" varName="storage" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							    	<util:info title="${implantationTitle}" message="${implantationMessage}"/>
							     	<util:combo dto="${roofDTO.implantation}" message="cat.implantation" map="${rating}" varName="implantation" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							    	<util:info title="${maintenanceTitle}" message="${maintenanceMessage}"/>
							     	<util:combo dto="${roofDTO.maintenance}" message="cat.maintenance" map="${maintenance}" varName="maintenance" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							    	<util:info title="${windsTitle}" message="${windsMessage}"/>
							     	<util:combo dto="${roofDTO.winds}" message="cat.winds" map="${rating}" varName="winds" />
							    </div>
							     <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							     	<util:info title="${aestheticsTitle}" message="${aestheticsMessage}"/>
							     	<util:combo dto="${roofDTO.aesthetics}" message="cat.aesthetics" map="${rating}" varName="aesthetics" />
							    </div>
							    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 overflow">
							    	<util:info title="${trafficTitle}" message="${trafficMessage}"/>
							     	<util:combo dto="${roofDTO.traffic}" message="cat.traffic" map="${traffic}" varName="traffic" />
							    </div> 
							</div>
							
							<div class="row">	
								<div class="col-12 text-right row-padding">
									<button class="btn btn-outline-warning btn-sm margin" formaction="${reset}" >
								    	<i class="w3-medium fa fa-eraser" ></i> <spring:message code="reset" /> 
								    </button>
								    <button class="btn btn-outline-info btn-sm">
								    	<i class="w3-medium fa fa-filter" ></i> <spring:message code="cat.filtering" /> 
								    </button>
								</div>
							</div>
							
						</div>
					</div>
				</form:form>

			</div>
		</div>
	</div>
</div>



<script>
var sliderWall = document.getElementById("rangeWall");
var outputWall = document.getElementById("valueWall");
outputWall.innerHTML = sliderWall.value;

sliderWall.oninput = function() {
    outputWall.innerHTML = this.value;
}

var sliderWeight = document.getElementById("rangeWeight");
var outputWeight = document.getElementById("valueWeight");
outputWeight.innerHTML = sliderWeight.value;

sliderWeight.oninput = function() {
    outputWeight.innerHTML = this.value;
}

var sliderPrice = document.getElementById("rangePrice");
var outputPrice = document.getElementById("valuePrice");
outputPrice.innerHTML = sliderPrice.value;

sliderPrice.oninput = function() {
    outputPrice.innerHTML = this.value;
}
</script>


<script>
//Open a modal with the message as parameter
// $(document).on("click", ".open-info", function () {
//     var title = $(this).data('title');
//     $(".modal-body #title").text(title);
    
//     var message = $(this).data('id');
//     $(".modal-body #message").text(message);
    
//     var image = $(this).data('image');
//     $(".modal-body #image").attr("src", image);
// });
</script>

