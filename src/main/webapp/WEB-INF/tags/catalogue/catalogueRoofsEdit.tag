<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="disabled" required="false" %>

<security:authorize access="hasRole('ROLE_AD')"> 
	<spring:url value="/admin/catalogue/new-roof/register" var="register" />
</security:authorize>
<security:authorize access="hasRole('ROLE_IN')"> 
	<spring:url value="/individual/catalogue/new-roof/register" var="register" />
</security:authorize>
<security:authorize access="hasRole('ROLE_SU')"> 
	<spring:url value="/supplier/catalogue/new-roof/register" var="register" />
</security:authorize>


<form:form class="form-horizontal" method="post" enctype="multipart/form-data"
			modelAttribute="registerRoofForm" action="${register}" >

	<view:roofsEditViewer disabled="${disabled}"/>

	<!-- BUTTONS -->
	<div class="row justify-content-center">
		<div class="col-12 text-center">
			<div class="square">
				<div class="col-12 text-center">
					<button type="button" name="register" class="btn btn-outline-success margin-all"
						onclick="submitFormAndImages(this.form)"> 
						<i class="w3-medium fa fa-save"></i> <spring:message code="button.save"/>
					</button>
				</div>
			</div>
		</div>
	</div>

		
</form:form>