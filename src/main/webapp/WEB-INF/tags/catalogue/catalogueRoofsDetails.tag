<%@tag pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils"%>
<%@ taglib prefix="view" tagdir="/WEB-INF/tags/viewer"%>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal"%>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="disabled" required="false"%>


<!-- PATHS -->
<security:authorize access="hasRole('ROLE_AD')">
	<spring:url value="/admin/catalogue/disable-enable"
		var="disable_enable" />
	<spring:url value="/admin/catalogue/delete" var="delete" />
	<spring:url value="/admin/catalogue/move-global" var="move" />
	<spring:url value="/admin/catalogue/restore" var="restore" />
	<spring:url value="/admin/catalogue/edit-roof" var="edit" />
</security:authorize>
<security:authorize access="hasRole('ROLE_IN')">
	<spring:url value="/individual/error/report-error" var="reportError" />
	<spring:url value="/individual/catalogue/disable-enable"
		var="disable_enable" />
	<spring:url value="/individual/catalogue/delete" var="delete" />
	<spring:url value="/individual/catalogue/edit-roof" var="edit" />
</security:authorize>
<security:authorize access="hasRole('ROLE_SU')">
	<spring:url value="/supplier/catalogue/disable-enable"
		var="disable_enable" />
	<spring:url value="/supplier/catalogue/delete" var="delete" />
	<spring:url value="/supplier/catalogue/edit-roof" var="edit" />
</security:authorize>

<c:if test="${returnPath != null}">
	<spring:url value="${returnPath}" var="back" />
</c:if>

<form:form class="form-horizontal" method="post">

	<!-- TITLE AND STATUS -->
	<util:titleStatus title="cat.details" />

	<!-- ROOF DETAILS -->
	<view:roofsDetailsViewer />

	<!-- BUTTONS -->
	<security:authorize access="hasRole('ROLE_AD')">
		<div class="row justify-content-center">
			<div class="col-12 text-center">
				<div class="square">

					<c:if test="${back != null}">
						<button type="submit" class="btn btn-outline-light margin-all" formaction="${back}">
							<i class="w3-medium fa fa-chevron-left"></i> <spring:message code="button.return" />
						</button>
					</c:if>

					<c:if test="${roofDTO.status eq 'IND'}">
						<button type="submit" formaction="${move}" name="move"
							class="btn btn-outline-light margin-all">
							<i class="w3-medium fa fa-share-square"></i> <spring:message code="button.move" />							
						</button>
					</c:if>

					<c:if
						test="${roofDTO.status eq 'VAL' && roofDTO.userDTO.roleDTO.name eq 'ROLE_IN'}">
						<button type="submit" formaction="${restore}" name="restore"
							class="btn btn-outline-light margin-all">
							<i class="w3-medium fa fa-undo"></i> <spring:message code="button.restore" />						
						</button>
					</c:if>

					<a
						href="${context}/roofs-resources/download/Roof-${roofDTO.idRoof}.zip"
						download="${roofDTO.maker}-${roofDTO.model}.zip">
						<button type="button" name="download"
							class="btn btn-outline-info margin-all">
							<i class="w3-medium fa fa-download"></i> <spring:message code="button.download" />
						</button>
					</a>

					<c:if test="${roofDTO.status eq 'VAL'}">
						<c:if test="${roofDTO.enabled == true}">
							<button type="submit" formaction="${disable_enable}"
								name="disable" class="btn btn-outline-warning margin-all">
								<i class="w3-medium fa fa-eye-slash"></i> <spring:message code="button.disable" />
							</button>
						</c:if>
						<c:if test="${roofDTO.enabled == false}">
							<button type="submit" formaction="${disable_enable}"
								name="disable" class="btn btn-outline-warning margin-all">
								<i class="w3-medium fa fa-eye"></i> <spring:message code="button.enable" />
							</button>
						</c:if>
					</c:if>
					
					<c:if test="${roofDTO.userDTO.idUser eq userDTO.idUser}">
						<button type="submit" formaction="${edit}" name="edit"
							class="btn btn-outline-success margin-all">
							<i class="w3-medium fa fa-edit"></i> <spring:message code="button.update" />	
						</button>
						<button type="button" class="btn btn-outline-danger margin-all"
							data-toggle="modal" data-target="#confirmModal">
							<i class="w3-medium fa fa-trash"></i> <spring:message code="button.delete" />
						</button>
					</c:if>
	
				</div>
			</div>
		</div>
	</security:authorize>

	<security:authorize access="hasRole('ROLE_IN')">
		<div class="row justify-content-center">
			<div class="col-12 text-center">
				<div class="square">
				
					<c:if test="${back != null}">
						<button type="submit" class="btn btn-outline-light margin-all" formaction="${back}">
							<i class="w3-medium fa fa-chevron-left"></i> <spring:message code="button.return" />
						</button>
					</c:if>

					<c:if test="${roofDTO.status == 'VAL'}">
						<button type="button" id="button-report"
							class="btn btn-outline-warning margin-all" data-toggle="modal"
							data-target="#reportModal">
							<i class="w3-medium fa fa-exclamation-triangle"></i> <spring:message code="button.report-error" />
						</button>
					</c:if>

					<a
						href="${context}/roofs-resources/download/Roof-${roofDTO.idRoof}.zip"
						download="${roofDTO.maker}-${roofDTO.model}.zip">
						<button type="button" name="download"
							class="btn btn-outline-info margin-all">
							<i class="w3-medium fa fa-download"></i> <spring:message code="button.download" />
						</button>
					</a>

					<c:if test="${roofDTO.status == 'IND'}">
						<button type="submit" formaction="${edit}" name="edit"
							class="btn btn-outline-success margin-all">
							<i class="w3-medium fa fa-edit"></i> <spring:message code="button.update" />
						</button>
					</c:if>

					<c:if test="${roofDTO.status == 'IND'}">
						<c:if test="${roofDTO.enabled == true}">
							<button type="submit" formaction="${disable_enable}"
								name="disable" class="btn btn-outline-warning margin-all">
								<i class="w3-medium fa fa-eye-slash"></i> <spring:message code="button.disable" />
							</button>
						</c:if>
						<c:if test="${roofDTO.enabled == false}">
							<button type="submit" formaction="${disable_enable}"
								name="disable" class="btn btn-outline-warning margin-all">
								<i class="w3-medium fa fa-eye"></i> <spring:message code="button.enable" />
							</button>
						</c:if>
						<button type="button" class="btn btn-outline-danger margin-all"
							data-toggle="modal" data-target="#confirmModal">
							<i class="w3-medium fa fa-trash"></i> <spring:message code="button.delete" />
						</button>
					</c:if>

				</div>
			</div>
		</div>
	</security:authorize>

	<security:authorize access="hasRole('ROLE_SU')">
		<div class="row justify-content-center">
			<div class="col-12 text-center">
				<div class="square">
				
					<c:if test="${back != null}">
						<button type="submit" class="btn btn-outline-light margin-all" formaction="${back}">
							<i class="w3-medium fa fa-chevron-left"></i> <spring:message code="button.return" />
						</button>
					</c:if>

					<a
						href="${context}/roofs-resources/download/Roof-${roofDTO.idRoof}.zip"
						download="${roofDTO.maker}-${roofDTO.model}.zip">
						<button type="button" name="download"
							class="btn btn-outline-info margin-all">
							<i class="w3-medium fa fa-download"></i> <spring:message code="button.download" />
						</button>
					</a>

					<button type="submit" formaction="${edit}" name="edit"
						class="btn btn-outline-success margin-all">
						<i class="w3-medium fa fa-edit"></i> <spring:message code="button.update" />
					</button>

					<c:if test="${roofDTO.enabled == true}">
						<button type="submit" formaction="${disable_enable}"
							name="disable" class="btn btn-outline-warning margin-all">
							<i class="w3-medium fa fa-eye-slash"></i> <spring:message code="button.disable" />
						</button>
					</c:if>
					<c:if test="${roofDTO.enabled == false}">
						<button type="submit" formaction="${disable_enable}"
							name="disable" class="btn btn-outline-warning margin-all">
							<i class="w3-medium fa fa-eye"></i> <spring:message code="button.enable" />
						</button>
					</c:if>

					<button type="button" class="btn btn-outline-danger margin-all"
						data-toggle="modal" data-target="#confirmModal">
						<i class="w3-medium fa fa-trash"></i> <spring:message code="button.delete" />
					</button>

				</div>
			</div>
		</div>
	</security:authorize>

	<!-- MODAL -->
	<modal:reportModal type="question" message="question.reporting"
		submit="${reportError}" submitButton="button.report" />

	<modal:confirmModal type="warning" message="question.delete-roof"
		submit="${delete}" submitButton="button.delete" />



</form:form>
