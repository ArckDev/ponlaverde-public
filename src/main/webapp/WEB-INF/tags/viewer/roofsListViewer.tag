<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="detailsPath" required="true" %>
<%@ attribute name="errorView" required="false" %>
<%@ attribute name="validationView" required="false" %>

<div class="row justify-content-center">	

	<c:choose>
	
		<c:when test="${roofList.getTotalElements() == 0}">
			<util:noResults/>
		</c:when>
		
		<c:otherwise>
			
			<!-- TABLE FOR BIG AND MEDIUM SCREENS -->
			<div class="col-12 d-none d-md-block text-center">
				<table class="table table-sm table-hover table-striped table-dark">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col"><spring:message code="cat.maker"/></th>
								<th scope="col"><spring:message code="cat.model"/></th>
								<th scope="col"><spring:message code="cat.slope"/></th>
								<th scope="col"><spring:message code="cat.type"/></th>
								<th scope="col"><spring:message code="cat.price"/></th>
								<th scope="col"><spring:message code="cat.weight"/></th>
								<c:if test="${errorView == true}">
									<th scope="col"><spring:message code="err.reported"/></th>
								</c:if>
								<c:if test="${validationView == true}">
									<th scope="col"><spring:message code="cat.status"/></th>
								</c:if>
							</tr>
						</thead>
					<tbody>
						<c:forEach items="${roofList.getElements()}" var="roofDTO" varStatus="roofLoop">
							<tr onclick="document.forms['roofForm${roofLoop.index}'].submit();">
								<th scope="row">${roofLoop.index+1}</th>
								<td>${roofDTO.maker}</td>
								<td>${fn:toUpperCase(roofDTO.model)}</td>
								<td><spring:message code="${roofDTO.slope}"/></td>
								<td><spring:message code="${roofDTO.type}"/></td>
								<td>${fn:replace(roofDTO.price, '.', ',')} €/m²</td>
								<td>${roofDTO.weight} kg/m²</td>
								<c:if test="${errorView == true}">
									<c:choose>
										<c:when test="${roofDTO.countErrorReported() > 1}">
											<td class="label-danger">${roofDTO.countErrorReported()}</td>
										</c:when>
										<c:otherwise>
											<td>${roofDTO.countErrorReported()}</td>
										</c:otherwise>
									</c:choose>
								</c:if>
								<c:if test="${validationView == true}">
									<c:if test="${roofDTO.status eq 'REJ'}">
										<td class="label-danger"><spring:message code="STATUS.${roofDTO.status}"/></td>
									</c:if>
									<c:if test="${roofDTO.status eq 'PEN'}">
										<td class="label-warning"><spring:message code="STATUS.${roofDTO.status}"/></td>
									</c:if>
								</c:if>
								<form:form id="roofForm${roofLoop.index}" method="post" action="${detailsPath}" >
									<input type="hidden" name="idRoof" value="${roofDTO.idRoof}"/>
								</form:form>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			
			<!-- TABLE FOR SMALL SCREENS -->
			<div class="col-12 d-md-none text-center">
				<table class="table table-sm table-hover table-striped table-dark">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col"><spring:message code="cat.model"/></th>
								<c:if test="${errorView == true}">
									<th scope="col"><spring:message code="err.reported"/></th>
								</c:if>
								<c:if test="${validationView == true}">
									<th scope="col"><spring:message code="cat.status"/></th>
								</c:if>
							</tr>
						</thead>
					<tbody>
						<c:forEach items="${roofList.getElements()}" var="roofDTO" varStatus="roofLoop">
							<tr onclick="document.forms['roofForm${roofLoop.index}'].submit();">
								<th scope="row">${roofLoop.index+1}</th>
								<td>${fn:toUpperCase(roofDTO.model)}</td>
								<c:if test="${errorView == true}">
									<td>${roofDTO.countErrorReported()}</td>
								</c:if>
								<c:if test="${validationView == true}">
									<c:if test="${roofDTO.status eq 'REJ'}">
										<td class="label-danger"><spring:message code="STATUS.${roofDTO.status}"/></td>
									</c:if>
									<c:if test="${roofDTO.status eq 'PEN'}">
										<td class="label-warning"><spring:message code="STATUS.${roofDTO.status}"/></td>
									</c:if>
								</c:if>
								<form:form id="roofForm${roofLoop.index}" method="post" action="${detailsPath}" >
									<input type="hidden" name="idRoof" value="${roofDTO.idRoof}"/>
								</form:form>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</c:otherwise>
	
	</c:choose>

</div>

<!-- PAGINATION -->
<util:pagination/>

