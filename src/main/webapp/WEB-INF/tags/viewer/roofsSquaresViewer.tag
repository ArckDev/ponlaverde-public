<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="detailsPath" required="true" %>

<!-- USER ROLE -->
<security:authorize access="hasRole('ROLE_AD')"> 
	<c:set value = "ROLE_AD" var = "userRole" />
</security:authorize>
<security:authorize access="hasRole('ROLE_IN')"> 
	<c:set value = "ROLE_IN" var = "userRole" />
</security:authorize>
<security:authorize access="hasRole('ROLE_SU')"> 
	<c:set value = "ROLE_SU" var = "userRole" />
</security:authorize>

<!-- RESULTS -->
<div class="row display-flex">	

	<c:choose>
	
		<c:when test="${roofList.getTotalElements() == 0}">
			<div class="col-12 text-center">
				<div class="square">
					<p><spring:message code="fil.no-results" /></p>
					<div class="wb" >
						<img src="${context}/images/ack_notfound_ico.png" style="max-width: 15%;">
					</div>
				</div>
			</div>
		</c:when>
		
		<c:otherwise>
				<c:forEach items="${roofList.getElements()}" var="roof" varStatus="roofLoop">
				
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
						<div class="square normal">
						
							<c:choose>
						
							<c:when test="${not empty roof.uploadedImagesList}">
	
								<div id="roofImages${roofLoop.index}" class="carousel slide" data-ride="carousel" data-interval="false">
								
								  <ol class="carousel-indicators">
									  <c:forEach items="${roof.uploadedImagesList}" var="roofImages" varStatus="roofImagesLoop">
									  	<c:choose>
									  		<c:when test="${roofImagesLoop.index == '0'}">
										  		<li data-target="#roofImages${roofLoop.index}" data-slide-to="${roofImagesLoop.index}" class="active"></li>
										    </c:when>
										    <c:otherwise>
										    	<li data-target="#roofImages${roofLoop.index}" data-slide-to="${roofImagesLoop.index}"></li>
										    </c:otherwise>
									    </c:choose>
									  </c:forEach>
								  </ol>
								  
								  <div class="carousel-inner">
								  	<c:forEach items="${roof.uploadedImagesList}" var="roofImagesPath" varStatus="roofImagesLoop">
									  	<c:choose>
									  		<c:when test="${roofImagesLoop.index == '0'}">
										  		<div class="carousel-item active">
										  			<c:choose>
										  				<c:when test="${roof.enabled == false}">
											      			<img class="d-block w-100 wb" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
											    		</c:when>
											    		<c:when test="${roof.status eq 'PEN' && userRole eq 'ROLE_SU'}">
											      			<img class="d-block w-100 blur" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
											    		</c:when>
											    		<c:otherwise>
											    			<img class="d-block w-100" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
											    		</c:otherwise>
										  			</c:choose>
											    </div>
										    </c:when>
										    <c:otherwise>
												<div class="carousel-item">
													<c:choose>
										  				<c:when test="${roof.enabled == false}">
											     	 		<img class="d-block w-100 wb" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
											    		</c:when>
											    		<c:when test="${roof.status eq 'PEN' && user eq 'ROLE_SU'}">
											      			<img class="d-block w-100 blur" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
											    		</c:when>
												    	<c:otherwise>
												    		<img class="d-block w-100" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
												    	</c:otherwise>
												    </c:choose>
											    </div>							    
											</c:otherwise>
									    </c:choose>
									</c:forEach>
								  </div>
								  
								  <a class="carousel-control-prev" href="#roofImages${roofLoop.index}" role="button" data-slide="prev">
								    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
								    <span class="sr-only">Previous</span>
								  </a>
								  
								  <a class="carousel-control-next" href="#roofImages${roofLoop.index}" role="button" data-slide="next">
								    <span class="carousel-control-next-icon" aria-hidden="true"></span>
								    <span class="sr-only">Next</span>
								  </a>
								  
								</div>
							
							</c:when>
							<c:otherwise>
								<div id="roofImages${roofLoop.index}" class="carousel slide" data-ride="carousel" data-interval="false">
								  <div class="carousel-inner">
								  	<div class="carousel-item active">
								  		<img class="d-block w-100" src="${context}/images/default_roof.png" alt="Default roof image">
								    </div>								  	
								  </div>
								</div>
							</c:otherwise>
							
						</c:choose>
			
						<form:form id="roofForm${roofLoop.index}" method="post" action="${detailsPath}" >
							<div class="square-details" onclick="document.forms['roofForm${roofLoop.index}'].submit();">

								<div class="row square-detail-model">
									<div class="col-12 h-100">
										<p ><small>${roof.model}</small></p>
										<input type="hidden" name="idRoof" value="${roof.idRoof}"/>
									</div>			
								</div>
								<div class="row first">
									<div class="col-8" style="white-space: nowrap;overflow: hidden;">
										<p style="overflow: hidden;text-overflow: ellipsis;"><strong>${roof.maker}</strong></p>
									</div>
									<div class="col-4 text-right">
										<p class="italic"><spring:message code="${roof.slope}"/></p>
									</div>					
								</div>
							
								<div class="row h-40p">
									<div class="col-4">	
										<c:forEach items="${roof.climate}" var="climate" begin="0" end="1" >
											<p class="font-small"><spring:message code="${climate}"/></p>
										</c:forEach>	
									</div>	
									<div class="col-4">	
										<c:forEach items="${roof.climate}" var="climate" begin="2" end="3" >
											<p class="font-small"><spring:message code="${climate}"/></p>
										</c:forEach>	
									</div>	
									<div class="col-4">	
										<c:forEach items="${roof.climate}" var="climate" begin="4" end="5" >
											<p class="font-small"><spring:message code="${climate}"/></p>
										</c:forEach>	
									</div>			
								</div>
	
							
								<div class="row border-top">
									<div class="col-5 text-center no-pad">
										<c:set var = "price" value = "${fn:replace(roof.price, '.', ',')}" />
										<p>${price}<span> €/m²</span></p>
									</div>
									<div class="col-5 text-center no-pad">
										<p>${roof.weight}<span> kg/m²</span></p>
									</div>
									<div class="col-2 text-center">
										<i class="w3-medium fa fa-info text-info" ></i>
									</div>
								</div>

						</div>
					</form:form>
					</div>	
						
				</div>
			</c:forEach>

		</c:otherwise>
	
	</c:choose>

</div>

<!-- PAGINATION -->
<util:pagination showDetails="Y"/>


