<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content bg2">
			<div class="modal-header">	
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body centered">	
				
				<div class="row justify-content-center">

					<div class="col-12 d-none d-md-block text-center">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-all"/></h4>
							<canvas id="combChart"></canvas>
						</div>
					</div>
					<div class="col-12 d-md-none text-center">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-all"/></h4>
							<canvas id="combChartM"></canvas>
						</div>
					</div>
					
					<div class="col-12 d-none d-md-block text-center" style="margin-top: 30px;">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-price"/> (<spring:message code="eur.m2"/>)</h4>
							<canvas id="priceChart"></canvas>
						</div>
					</div>
					<div class="col-12 d-md-none text-center" style="margin-top: 30px;">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-price"/> (<spring:message code="eur.m2"/>)</h4>
							<canvas id="priceChartM"></canvas>
						</div>
					</div>
					
					<div class="col-12 d-none d-md-block text-center" style="margin-top: 30px;">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-weight"/> (<spring:message code="kg.m2"/>)</h4>
							<canvas id="weightChart"></canvas>
						</div>
					</div>
					<div class="col-12 d-md-none text-center" style="margin-top: 30px;">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-weight"/> (<spring:message code="kg.m2"/>)</h4>
							<canvas id="weightChartM"></canvas>
						</div>
					</div>
					
					<div class="col-12 d-none d-md-block text-center" style="margin-top: 30px;">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-wall"/> (<spring:message code="cm"/>)</h4>
							<canvas id="wallChart"></canvas>
						</div>
					</div>
					<div class="col-12 d-md-none text-center" style="margin-top: 30px;">
						<div class="square">
							<h4 style="margin-bottom: 30px;"><spring:message code="rec.compare-wall"/> (<spring:message code="cm"/>)</h4>
							<canvas id="wallChartM"></canvas>
						</div>
					</div>
				</div>
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="storageChart"></canvas> --%>
<!-- 				</div> -->
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="implantationChart"></canvas> --%>
<!-- 				</div> -->
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="maintenanceChart"></canvas> --%>
<!-- 				</div> -->
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="windsChart"></canvas> --%>
<!-- 				</div> -->
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="thermalChart"></canvas> --%>
<!-- 				</div> -->
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="regulationChart"></canvas> --%>
<!-- 				</div> -->
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="trafficChart"></canvas> --%>
<!-- 				</div> -->
<!-- 				<div style="height: 500px;"> -->
<%-- 					<canvas id="aestheticsChart"></canvas> --%>
<!-- 				</div> -->
				
			</div>

		</div>
	</div>