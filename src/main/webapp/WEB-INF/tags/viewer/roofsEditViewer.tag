<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="disabled" required="false" %>

<spring:message code="cat.climate.single" var="climateTitle"/>
<spring:message code="cat.type" var="typeTitle"/>
<spring:message code="info.roof-type" var="typeMessage" />
<spring:message code="cat.regulation" var="regulationTitle"/>
<spring:message code="info.roof-regulation" var="regulationMessage" />
<spring:message code="cat.storage" var="storageTitle"/>
<spring:message code="info.roof-storage" var="storageMessage" />
<spring:message code="cat.implantation" var="implantationTitle"/>
<spring:message code="info.roof-implantation" var="implantationMessage" />
<spring:message code="cat.maintenance" var="maintenanceTitle"/>
<spring:message code="info.roof-maintenance" var="maintenanceMessage" />
<spring:message code="cat.winds" var="windsTitle"/>
<spring:message code="info.roof-winds" var="windsMessage" />
<spring:message code="cat.aesthetics" var="aestheticsTitle"/>
<spring:message code="info.roof-aesthetics" var="aestheticsMessage" />
<spring:message code="cat.thermal" var="thermalTitle"/>
<spring:message code="info.roof-thermal" var="thermalMessage" />
<spring:message code="cat.traffic" var="trafficTitle"/>
<spring:message code="info.roof-traffic" var="trafficMessage" />


<!-- TITLE AND STATUS -->
<util:titleStatus title="cat.edit"/>

<!-- INFO -->
<div class="row">	
		      	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
			<input type="hidden" name="idRoof" value="${roofDTO.idRoof}"/>
		
			<label class="label-margin"><spring:message code="cat.maker"/></label>
			<c:if test="${disabled == true}">
	    		<input required type="text" class="form-control form-control-sm dark" name="makerLbl" id="makerLbl" value="${userDTO.company}" disabled> 
			</c:if>
			<c:if test="${disabled == false || disabled == null}">
				<input required type="text" class="form-control form-control-sm dark" name="maker" id="maker" maxlength="20" placeholder="<spring:message code="cat.maker.plh"/>" value="${roofDTO.maker}"> 
			</c:if>
	    	
	    	<label class="label-margin"><spring:message code="cat.model"/></label>
	    	<input required type="text" class="form-control form-control-sm dark" name="model" id="model" maxlength="45" placeholder="<spring:message code="cat.model.plh"/>" value="${roofDTO.model}" onblur="checkModelExist()"> 
			<div class="col-12 text-center">
				<div class="form-error-feedback" id="feedback-model" style="display: block;margin-top: 10px;"></div>
			</div>
			
			<div class="row">
				<div class="col-4">
					<label class="label-margin"><spring:message code="cat.price"/></label>
					<input required type="number" min="0" max="9999.99" class="form-control form-control-sm dark" name="price" id="price" placeholder="<spring:message code="cat.price.plh"/>" value="${roofDTO.price}"> 
				</div>
				<div class="col-4">
					<label class="label-margin"><spring:message code="cat.weight"/></label>
					<input required type="number" min="0" max="5000" class="form-control form-control-sm dark" name="weight" id="weight" placeholder="<spring:message code="cat.weight.plh"/>" value="${roofDTO.weight}"> 
				</div>
				<div class="col-4">
					<label class="label-margin"><spring:message code="cat.wall"/></label>
					<input required type="number" min="0" max="5000" class="form-control form-control-sm dark" name="wall" id="wall" placeholder="<spring:message code="cat.wall.plh"/>" value="${roofDTO.wall}"> 
				</div>
			</div>
			<util:info title="${climateTitle}" imagePath="${context}/images/spain_climate.jpg"/>
			<label class="label-margin"><spring:message code="cat.climate"/></label><br>
			<div class="row">
				<c:choose>
				<c:when test="${fn:length(climate) % 2 == 0}">
					 <c:set var="begin" value="${(fn:length(climate)/2)}" />
					 <c:set var="end" value="${(fn:length(climate)/2)-1}" />
				</c:when>
				<c:otherwise>
					<c:set var="begin" value="${(fn:length(climate)/2)+1}" />
					<c:set var="end" value="${(fn:length(climate)/2)}" />
				</c:otherwise>
				</c:choose> 
				<div class="col-6">
					<div class="form-check">
						<c:set var="roofClimate" value="${roofDTO.climate}" /> 
						<c:forEach items="${climate}" var="climate" varStatus="loop" begin="0" end="${end}">
							<c:choose>
								<c:when test='${fn:contains(roofClimate, climate.key)}'>
	        						<input class="form-check-input" type="checkbox" id="inlineCheckbox${loop.index}" value="${climate.key}" name="climate" required checked>
					 				<label class="form-check-label" for="inlineCheckbox${loop.index}">${climate.value}</label><br>
								</c:when>
								<c:otherwise>
	        						<input class="form-check-input" type="checkbox" id="inlineCheckbox${loop.index}" value="${climate.key}" name="climate" required>
					 				<label class="form-check-label" for="inlineCheckbox${loop.index}">${climate.value}</label><br>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</div>
				</div>
				<div class="col-6">
					<div class="form-check">
						<c:forEach items="${climate}" var="climate" varStatus="loop" begin="${begin}" end="${fn:length(climate)}">
							<c:choose>
								<c:when test='${fn:contains(roofClimate, climate.key)}'>
	        						<input class="form-check-input" type="checkbox" id="inlineCheckbox${loop.index}" value="${climate.key}" name="climate" required checked>
					 				<label class="form-check-label" for="inlineCheckbox${loop.index}">${climate.value}</label><br>
								</c:when>
								<c:otherwise>
	        						<input class="form-check-input" type="checkbox" id="inlineCheckbox${loop.index}" value="${climate.key}" name="climate" required>
					 				<label class="form-check-label" for="inlineCheckbox${loop.index}">${climate.value}</label><br>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>	
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
	    <div class="square big">
	    	<util:info title="${typeTitle}" message="${typeMessage}"/>
			<util:combo dto="${roofDTO.type}" message="cat.type" map="${type}" varName="type" required="true"/>
			<util:combo dto="${roofDTO.slope}" message="cat.slope" map="${slope}" varName="slope" required="true"/>
			<util:info title="${storageTitle}" message="${storageMessage}"/>
			<util:combo dto="${roofDTO.storage}" message="cat.storage" map="${storage}" varName="storage" required="true"/>
			<util:info title="${implantationTitle}" message="${implantationMessage}"/>
			<util:combo dto="${roofDTO.implantation}" message="cat.implantation" map="${rating}" varName="implantation" required="true"/>
			<util:info title="${maintenanceTitle}" message="${maintenanceMessage}"/>
			<util:combo dto="${roofDTO.maintenance}" message="cat.maintenance" map="${maintenance}" varName="maintenance" required="true"/>
		 </div> 
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
	    <div class="square big">
	    	<util:info title="${windsTitle}" message="${windsMessage}"/>
	    	<util:combo dto="${roofDTO.winds}" message="cat.winds" map="${rating}" varName="winds" required="true"/>
			<util:info title="${thermalTitle}" message="${thermalMessage}"/>
			<util:combo dto="${roofDTO.thermal}" message="cat.thermal" map="${rating}" varName="thermal" required="true"/>
			<util:info title="${regulationTitle}" message="${regulationMessage}"/>
			<util:combo dto="${roofDTO.regulation}" message="cat.regulation" map="${regulation}" varName="regulation" required="true"/>
			<util:info title="${trafficTitle}" message="${trafficMessage}"/>
			<util:combo dto="${roofDTO.traffic}" message="cat.traffic" map="${traffic}" varName="traffic" required="true"/>
			
			<div class="col-12 text-center">
				<label class="form-error-feedback" id="feedback" style="display:none; margin-top: 20px;"><spring:message code="error.required.attached"/></label>
			</div>
		</div>
    </div>
 
</div>


<div class="row">

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
		
		<label class="label-margin"><spring:message code="cat.photos"/></label><br>
		
			<c:if test="${not empty roofDTO.uploadedImagesList}">
				<div id="holder-images" class="no-bg">
					<label for="images"><strong><spring:message code="cat.file"/></strong> <span><spring:message code="cat.drag"/></span></label>
					<input multiple="multiple" accept="image/*" name="images" id="images" type="file" style="visibility: hidden;">
					
					<c:forEach items="${roofDTO.uploadedImagesList}" var="roofImagesPath" varStatus="roofImagesLoop">
						<img src="${context}${roofImagesPath}" width="70">
					</c:forEach>
				</div>
			</c:if>	
			<c:if test="${empty roofDTO.uploadedImagesList}">
				<div id="holder-images" >
					<label for="images"><strong><spring:message code="cat.file"/></strong> <span><spring:message code="cat.drag"/></span></label>
					<input multiple="multiple" accept="image/*" name="images" id="images" type="file" style="visibility: hidden;">
				</div>
			</c:if>	

			<div class="col-12 text-center">			
				<button type="button" name="removeDetails" class="btn btn-outline-danger" onclick="deleteAppend('holder-images', 'images')"> 
					<i class="w3-medium fa fa-undo"></i>
				</button>			
			</div>
			
			<p id="filereader">File API and FileReader API not supported</p>
			<p id="formdata">XHR2's FormData is not supported</p>
			
		</div>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
		
		<label class="label-margin"><spring:message code="cat.details"/></label><br>
		
			<c:if test="${not empty roofDTO.uploadedDetailsList}">
				<div id="holder-details" class="no-bg">
					<label for="details"><strong><spring:message code="cat.file"/></strong> <span><spring:message code="cat.drag"/></span></label>
					<input multiple="multiple" accept="image/*" name="details" id="details" type="file" style="visibility: hidden;">
				
					<c:forEach items="${roofDTO.uploadedDetailsList}" var="roofDetailsPath" varStatus="roofDetailsLoop">
						<img src="${context}${roofDetailsPath}" width="70">
					</c:forEach>
				</div>
			</c:if>
			<c:if test="${empty roofDTO.uploadedDetailsList}">
				<div id="holder-details">
					<label for="details"><strong><spring:message code="cat.file"/></strong> <span><spring:message code="cat.drag"/></span></label>
					<input multiple="multiple" accept="image/*" name="details" id="details" type="file" style="visibility: hidden;">
				</div>
			</c:if>
				
			<div class="col-12 text-center">		
				<button type="button" name="removeDetails" class="btn btn-outline-danger" onclick="deleteAppend('holder-details', 'details')"> 
					<i class="w3-medium fa fa-undo"></i>
				</button>
			</div>

		</div>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
		
		<label class="label-margin"><spring:message code="cat.sheets"/></label><br>
		
			<c:if test="${not empty roofDTO.uploadedSheetsList}">
				<div id="holder-sheets" class="no-bg">
					<label for="sheets"><strong><spring:message code="cat.file"/></strong> <span><spring:message code="cat.drag"/></span></label>
					<input multiple="multiple" accept="application/pdf" name="sheets" id="sheets" type="file" style="visibility: hidden;">
				
					<img src="${context}/images/logo_pdf.png" width="70">
				</div>				
			</c:if>
			<c:if test="${empty roofDTO.uploadedSheetsList}">
				<div id="holder-sheets">
					<label for="sheets"><strong><spring:message code="cat.file"/></strong> <span><spring:message code="cat.drag"/></span></label>
					<input multiple="multiple" accept="application/pdf" name="sheets" id="sheets" type="file" style="visibility: hidden;">
				</div>
			</c:if>
			
			<div class="col-12 text-center">
				<button type="button" name="removeDetails" class="btn btn-outline-danger" onclick="deleteAppend('holder-sheets', 'sheets')"> 
					<i class="w3-medium fa fa-undo"></i>
				</button>
			</div>
		
		</div>
	</div>
</div>

<script src="${context}/js/printErrorList.js"></script>
<script src="${context}/js/sendAsyncRq.js"></script>

<script>
// DRAG AND DROP OBJECTS
var holderImages = document.getElementById('holder-images'),
    tests = {
      filereader: typeof FileReader != 'undefined',
      dnd: 'draggable' in document.createElement('span'),
      formdata: !!window.FormData
    }, 
    support = {
      filereader: document.getElementById('filereader'),
      formdata: document.getElementById('formdata')
    };
    
var holderDetails = document.getElementById('holder-details'),
	tests = {
	  filereader: typeof FileReader != 'undefined',
	  dnd: 'draggable' in document.createElement('span'),
	  formdata: !!window.FormData
	}, 
	support = {
	  filereader: document.getElementById('filereader'),
	  formdata: document.getElementById('formdata')
	};
	
var holderSheets = document.getElementById('holder-sheets'),
	tests = {
	  filereader: typeof FileReader != 'undefined',
	  dnd: 'draggable' in document.createElement('span'),
	  formdata: !!window.FormData
	}, 
	support = {
	  filereader: document.getElementById('filereader'),
	  formdata: document.getElementById('formdata')
	};


// ACCEPTED FILE TYPES
var acceptedTypesImages = {
  	  'image/png': true,
  	  'image/jpeg': true,
  	  'image/gif': true
  	};
  	
var acceptedTypesSheets = {
      'application/pdf': true
   	 };
   	 
// ATTACHMENTS LIMITS
var divDefaultElement = 2;
var imageLimit = 9 + divDefaultElement;
var sheetLimit = 1 + divDefaultElement;
var imageSize = 5242880; //5MB
var sheetSize = 20971520; //20MB

// API TEST
"filereader formdata".split(' ').forEach(function (api) {
if (tests[api] === false) {
    support[api].className = 'fail';
  } else {
    support[api].className = 'hidden';
  }
});

if (tests.dnd) { 
    holderImages.ondragover = function () { this.className = 'hover'; return false; };
    holderImages.ondragend = function () { this.className = ''; return false; };
    holderImages.ondrop = function (e) {
      this.className = '';
      e.preventDefault();
      readfiles(e.dataTransfer.files, holderImages, 'images');
    }
    
    holderDetails.ondragover = function () { this.className = 'hover'; return false; };
    holderDetails.ondragend = function () { this.className = ''; return false; };
    holderDetails.ondrop = function (e) {
      this.className = '';
      e.preventDefault();
      readfiles(e.dataTransfer.files, holderDetails, 'details');
    }
    
    holderSheets.ondragover = function () { this.className = 'hover'; return false; };
    holderSheets.ondragend = function () { this.className = ''; return false; };
    holderSheets.ondrop = function (e) {
      this.className = '';
      e.preventDefault();
      readfiles(e.dataTransfer.files, holderSheets, 'sheets');
    }

  } 

// PREVIEW IMAGE
function previewImage(file, container) {
  
    if (tests.filereader === true) {
	    var reader = new FileReader();
	    
	    reader.onload = function (event) {
	      var image = new Image();
	      image.src = event.target.result;
	      //Check if image is portrait or landscape
	      if(image.height > image.width){
	    	  image.height = 55; // resize 
	      } else {
	    	  image.width = 70; // resize 
	      }
	      container.appendChild(image);
	      container.style.backgroundImage = 'none';
	    };

    	reader.readAsDataURL(file);
  	} 
    
}

// PREVIEW DOCUMENT
function previewDocument(file, container) {
    
    if (tests.filereader === true) {
	    var reader = new FileReader();
	    
	    reader.onload = function (event) {
	      var image = new Image();
	      image.src = '${context}/images/logo_pdf.png';
	      image.width = 70; // resize
	      container.appendChild(image);
	      container.style.backgroundImage = 'none';
	    };

    	reader.readAsDataURL(file);
  	} 
    
}

// FORM WHERE INCLUDING ATTACHEMENTS
var formData = tests.formdata ? new FormData() : null;

// READ FILES FROM INTERFACE
function readfiles(files, container, appender) {

    if(container.id == 'holder-images' || container.id == 'holder-details'){
        for (var i = 0; i < files.length; i++) {	
        	 if (tests.formdata && acceptedTypesImages[files[i].type] === true) {
             	if (container.childElementCount < imageLimit) { 
             		if(files[i].size < imageSize) {
             			formData.append(appender, files[i]);
                        previewImage(files[i], container);
             		} else {
             			alert("<spring:message code='error.images.size'/>");
             		}
             	} else {
             		alert("<spring:message code='error.images.length'/>");
             	}
             } else {
                 alert("<spring:message code='error.images'/>"); 
             }
        }
    } else if (container.id == 'holder-sheets'){
        for (var i = 0; i < files.length; i++) {
            if (tests.formdata && acceptedTypesSheets[files[i].type] === true) {
            	if (container.childElementCount < sheetLimit) { 
            		if(files[i].size < sheetSize) {
                		formData.append(appender, files[i]);
            			previewDocument(files[i], container);
            		} else {
             			alert("<spring:message code='error.sheets.size'/>");
             		}
            	} else {
            		alert("<spring:message code='error.sheets.length'/>");
            	}
            } else {
                alert("<spring:message code='error.sheets'/>"); 
            }  
        }
    } 
}

// SEND REQUEST TO BE
function sendFiles(formData, form){

    $('#processingModal').modal('toggle');
    
    if (tests.formdata) {
      
        $.ajax({
	        type: "POST",
	        contentType: false,
	        url: '${context}/api/save-image',
	        cache: false,
	        timeout: 600000,
	        data: formData,
	        enctype: 'multipart/form-data',
	        processData: false,  // Important!
	        success: function (data) {

	            if(data.code == 'OK'){
	                
	            } else {

	            }

	            console.log("SUCCESS : ", data);

	            form.submit();	            
	        },
	        error: function (e) {

	            console.log("ERROR : ", e);
	            return false;
	        }

		});

    }
}

// DELETE ALL THE ATTACHED FILES
function deleteAppend(container, appender){
    
 	formData.delete(appender)
    var append = document.getElementById(container); 

    for(var i = append.childNodes.length-1; i > 4; i--){ 
        append.removeChild(append.childNodes[i]);
    }

}

// VALIDATION TO SEND FORM
function submitFormAndImages(form){
    
    if(checkRequiredFields()){
        
        sendFiles(formData, form);

    } else {
        
    }

}

// CHECK THE REQUIRED FIELD
function checkRequiredFields(){
    
    if(checkAttached() && checkClimaSelected() && checkFields() && checkModelName()){
        $("#feedback").css("display","none");
        return true;
    } else {
        $("#feedback").css("display","block");
        return false;
    } 
}

// CHECK IF THERE IS ONE CLIMA SELECTED
function checkClimaSelected(){
    
    var climate = document.getElementsByName("climate");

    for (var i = 0; i < climate.length; i++) {
        if (climate[i].checked == true) return true;
    }

    return false;
}

// CHECK ALL THE OTHER FIELDS
function checkFields(){
    
    if ($("#maker").val() == '') return false;
    if ($("#model").val() == '') return false;
    if ($("#price").val() == '' || $("#price").val() <= 0 || $("#price").val() > 99999) return false;
    if ($("#weight").val() == '' || $("#weight").val() <= 0 || $("#weight").val() > 5000) return false;
    if ($("#wall").val() == '' || $("#wall").val() < 0 || $("#wall").val() > 5000) return false;
    if ($("#type").val() == null) return false;
    if ($("#slope").val() == null) return false;
    if ($("#storage").val() == null) return false;
    if ($("#implantation").val() == null) return false;
    if ($("#maintenance").val() == null) return false;
    if ($("#winds").val() == null) return false;
    if ($("#thermal").val() == null) return false;
    if ($("#regulation").val() == null) return false;
    if ($("#traffic").val() == null) return false;

    return true;
}

// CHECK IF THERE ARE ELEMENTS ATTACHED
function checkAttached(){

    if (holderImages.childElementCount < 3) return false;
    if (holderDetails.childElementCount < 3) return false;
    if (holderSheets.childElementCount < 3) return false;

    return true;
}

function checkModelExist(){
	
	 var rqData = {}
	 rqData["model"] = $("#model").val();
	   
	 var rqUrl = "${context}/api/search-model";

	 sendAsyncRq(rqUrl, rqData, "#feedback-model", null, null, null);
}

function checkModelName(){
	
	if($("#feedback-model").children().length > 0) return false;
	
	return true;
}

</script>
