<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="disabled" required="false" %>

<!-- INFO -->
<div class="row">	
		      	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
		
			<label class="label-margin font-medium"><spring:message code="cat.maker"/></label>
			<input disabled type="text" class="form-control form-control-sm dark font-medium" name="maker" id="maker" value="${roofDTO.maker}"> 

	    	<label class="label-margin font-medium"><spring:message code="cat.model"/></label>
	    	<input disabled type="text" class="form-control form-control-sm dark font-medium" name="model" id="model" value="${roofDTO.model}"> 
			
			<div class="row">
				<div class="col-4">
					<label class="label-margin font-medium"><spring:message code="cat.price"/></label>
					<input disabled type="text" step="0.01" max="99999.99" class="form-control form-control-sm dark font-medium" name="price" id="price" value="${fn:replace(roofDTO.price, '.', ',')} €/m²"> 
				</div>
				<div class="col-4">
					<label class="label-margin font-medium"><spring:message code="cat.weight"/></label>
					<input disabled type="text" min="0" max="5000" class="form-control form-control-sm dark font-medium" name="weight" id="weight" value="${roofDTO.weight} kg/m²"> 
				</div>
				<div class="col-4">
					<label class="label-margin font-medium"><spring:message code="cat.wall"/></label>
					<input disabled type="text" min="0" max="5000" class="form-control form-control-sm dark font-medium" name="wall" id="wall" value="${roofDTO.wall} cm"> 
				</div>
			</div>
			
			<label class="label-margin font-medium"><spring:message code="cat.climate"/></label><br>
			<div class="row">
				<div class="col-6">
					<div class="form-check">
						<c:forEach items="${roofDTO.climate}" var="climate" varStatus="loop" begin="0" end="2">
							<input checked class="form-check-input" type="checkbox" id="inlineCheckbox${loop.index}" value="${climate}" name="climate" disabled >
				 			<label class="form-check-label" for="inlineCheckbox${loop.index}"><spring:message code="${climate}"/></label><br>
						</c:forEach>
					</div>
				</div>
				<div class="col-6">
					<div class="form-check">
						<c:forEach items="${roofDTO.climate}" var="climate" varStatus="loop" begin="3" end="5">
							<input checked class="form-check-input" type="checkbox" id="inlineCheckbox${loop.index}" value="${climate}" name="climate" disabled >
				 			<label class="form-check-label" for="inlineCheckbox${loop.index}"><spring:message code="${climate}"/></label><br>
						</c:forEach>
					</div>
				</div>
				<div class="col-12 text-center">
					<label class="form-error-feedback" id="feedback" style="display:none;"><spring:message code="error.required"/></label>
				</div>
			</div>
			

		</div>	
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
	    <div class="square big">
	    	<util:labelField type="text" dto="${roofDTO.type}" message="cat.type" varName="type" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.slope}" message="cat.slope" varName="slope" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.storage}" message="cat.storage" varName="storage" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.implantation}" message="cat.implantation" varName="implantation" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.maintenance}" message="cat.maintenance" varName="maintenance" disabled="true" />
	    </div> 
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
	    <div class="square big">
	    	<util:labelField type="text" dto="${roofDTO.winds}" message="cat.winds" varName="winds" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.thermal}" message="cat.thermal" varName="thermal" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.regulation}" message="cat.regulation" varName="regulation" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.traffic}" message="cat.traffic" varName="traffic" disabled="true" />
	    	<util:labelField type="text" dto="${roofDTO.aesthetics}" message="cat.aesthetics" varName="aesthetics" disabled="true" />
		</div>
    </div>
 
</div>


<div class="row">

	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
		
			<label class="label-margin font-medium"><spring:message code="cat.photos"/></label><br>
			
				<div id="roofImages" class="carousel slide" data-ride="carousel" data-interval="false">
				
				  <ol class="carousel-indicators">
					  <c:forEach items="${roofDTO.uploadedImagesList}" var="roofImages" varStatus="roofImagesLoop">
					  	<c:choose>
					  		<c:when test="${roofImagesLoop.index == '0'}">
						  		<li data-target="#roofImages${roofLoop.index}" data-slide-to="${roofImagesLoop.index}" class="active"></li>
						    </c:when>
						    <c:otherwise>
						    	<li data-target="#roofImages${roofLoop.index}" data-slide-to="${roofImagesLoop.index}"></li>
						    </c:otherwise>
					    </c:choose>
					  </c:forEach>
				  </ol>
				  
				  <div class="carousel-inner">
				  	<c:forEach items="${roofDTO.uploadedImagesList}" var="roofImagesPath" varStatus="roofImagesLoop">
					  	<c:choose>
					  		<c:when test="${roofImagesLoop.index == '0'}">
						  		<div class="carousel-item active">
							      <img class="d-block w-100" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
							    </div>
						    </c:when>
						    <c:otherwise>
								<div class="carousel-item">
							      <img class="d-block w-100" src="${context}${roofImagesPath}" alt="${roofImagesLoop.index} slide">
							    </div>							    
							</c:otherwise>
					    </c:choose>
					</c:forEach>
				  </div>
				  
				  <a class="carousel-control-prev" href="#roofImages" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  
				  <a class="carousel-control-next" href="#roofImages" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				  
				</div>
			
		</div>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
		
			<label class="label-margin font-medium"><spring:message code="cat.details"/></label><br>
			
			<div id="roofDetails" class="carousel slide" data-ride="carousel" data-interval="false">
				
				  <ol class="carousel-indicators">
					  <c:forEach items="${roofDTO.uploadedDetailsList}" var="roofDetails" varStatus="roofDetailsLoop">
					  	<c:choose>
					  		<c:when test="${roofDetailsLoop.index == '0'}">
						  		<li data-target="#roofDetails${roofLoop.index}" data-slide-to="${roofDetailsLoop.index}" class="active"></li>
						    </c:when>
						    <c:otherwise>
						    	<li data-target="#roofDetails${roofLoop.index}" data-slide-to="${roofDetailsLoop.index}"></li>
						    </c:otherwise>
					    </c:choose>
					  </c:forEach>
				  </ol>
				  
				  <div class="carousel-inner">
				  	<c:forEach items="${roofDTO.uploadedDetailsList}" var="roofDetailsPath" varStatus="roofDetailsLoop">
					  	<c:choose>
					  		<c:when test="${roofDetailsLoop.index == '0'}">
						  		<div class="carousel-item active">
							      <img class="d-block w-100" src="${context}${roofDetailsPath}" alt="${roofDetailsLoop.index} slide">
							    </div>
						    </c:when>
						    <c:otherwise>
								<div class="carousel-item">
							      <img class="d-block w-100" src="${context}${roofDetailsPath}" alt="${roofDetailsLoop.index} slide">
							    </div>							    
							</c:otherwise>
					    </c:choose>
					</c:forEach>
				  </div>
				  
				  <a class="carousel-control-prev" href="#roofDetails" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  
				  <a class="carousel-control-next" href="#roofDetails" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				  
				</div>
			
			
		</div>
	</div>
	
	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		<div class="square big">
			<label class="label-margin font-medium"><spring:message code="cat.sheets"/></label><br>
			<div id="pdfViewer" style="height: 90%;"></div>
		</div>
	</div>
	
</div>

<input type="hidden" name="idRoof" value="${roofDTO.idRoof}"/>


<script src="${context}/js/pdfobject.js"></script>
<c:if test="${not empty  roofDTO.uploadedSheetsList[0]}">
	<script>PDFObject.embed("${context}${roofDTO.uploadedSheetsList[0]}", "#pdfViewer");</script>
</c:if>
