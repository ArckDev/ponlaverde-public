<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/utils" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="name" required="true" %>
<%@ attribute name="value" required="true" %>
<%@ attribute name="margin" required="false" %>
<%@ attribute name="onchange" required="false" %>
<%@ attribute name="disabled" required="false" %>
<%@ attribute name="infoTitle" required="false" %>
<%@ attribute name="infoMessage" required="false" %>



<c:set var="nameLower" value="${fn:toLowerCase(name)}"/>

<div class="row ${margin}">
	<div class="col-lg-4">
		<c:if test="${infoTitle != null}">
			<util:info title="${infoTitle}" message="${infoMessage}"/>
		</c:if>
		<label for="range${name}" class="label center-v"><spring:message code="rec.${nameLower}"/></label>
	</div>
	<div class="col-lg-7">
		<input ${disabled} type="range" min=0 max=10 value="${value}" class="slider" id="range${name}" name="${nameLower}" onchange="${onchange}">
	</div>
	<div class="col-lg-1">
		<p class="italic-small text-center center-v"><span id="value${name}"></span></p>
	</div>
</div>

<script>
var sliderAesthetics = document.getElementById("rangeAesthetics");
var outputAesthetics = document.getElementById("valueAesthetics");
outputAesthetics.innerHTML = sliderAesthetics.value;

sliderAesthetics.oninput = function() {
    outputAesthetics.innerHTML = this.value;
}

var sliderImplantation = document.getElementById("rangeImplantation");
var outputImplantation = document.getElementById("valueImplantation");
outputImplantation.innerHTML = sliderImplantation.value;

sliderImplantation.oninput = function() {
    outputImplantation.innerHTML = this.value;
}

var sliderMaintenance = document.getElementById("rangeMaintenance");
var outputMaintenance = document.getElementById("valueMaintenance");
outputMaintenance.innerHTML = sliderMaintenance.value;

sliderMaintenance.oninput = function() {
    outputMaintenance.innerHTML = this.value;
}

var sliderPrice = document.getElementById("rangePrice");
var outputPrice = document.getElementById("valuePrice");
outputPrice.innerHTML = sliderPrice.value;

sliderPrice.oninput = function() {
    outputPrice.innerHTML = this.value;
}

var sliderRegulation = document.getElementById("rangeRegulation");
var outputRegulation = document.getElementById("valueRegulation");
outputRegulation.innerHTML = sliderRegulation.value;

sliderRegulation.oninput = function() {
    outputRegulation.innerHTML = this.value;
}

var sliderStorage = document.getElementById("rangeStorage");
var outputStorage = document.getElementById("valueStorage");
outputStorage.innerHTML = sliderStorage.value;

sliderStorage.oninput = function() {
    outputStorage.innerHTML = this.value;
}

var sliderThermal = document.getElementById("rangeThermal");
var outputThermal = document.getElementById("valueThermal");
outputThermal.innerHTML = sliderThermal.value;

sliderThermal.oninput = function() {
    outputThermal.innerHTML = this.value;
}

var sliderTraffic = document.getElementById("rangeTraffic");
var outputTraffic = document.getElementById("valueTraffic");
outputTraffic.innerHTML = sliderTraffic.value;

sliderTraffic.oninput = function() {
    outputTraffic.innerHTML = this.value;
}

var sliderWall = document.getElementById("rangeWall");
var outputWall = document.getElementById("valueWall");
outputWall.innerHTML = sliderWall.value;

sliderWall.oninput = function() {
    outputWall.innerHTML = this.value;
}

var sliderWeight = document.getElementById("rangeWeight");
var outputWeight = document.getElementById("valueWeight");
outputWeight.innerHTML = sliderWeight.value;

sliderWeight.oninput = function() {
    outputWeight.innerHTML = this.value;
}

var sliderWinds = document.getElementById("rangeWinds");
var outputWinds = document.getElementById("valueWinds");
outputWinds.innerHTML = sliderWinds.value;

sliderWinds.oninput = function() {
    outputWinds.innerHTML = this.value;
}
</script>

