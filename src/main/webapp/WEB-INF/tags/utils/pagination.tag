<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="showDetails" required="false"%>

<c:if test="${roofList.getTotalPages() > 1}">
	<div class="row">
	
		<div class="col-12 text-center">
		
			<!-- ELEMENTS SHOWN AND TOTAL -->
			<c:if test="${showDetails eq 'Y'}">
				<c:if test="${roofList.getValSize() != null}">
					<spring:message code="pag.showing-composed" arguments="${roofList.getValSize()},${roofList.getOwnSize()}"/><br>
					<spring:message code="pag.total-composed"  arguments="${roofList.getTotalValElements()},${roofList.getTotalOwnElements()}"/>
				 </c:if>
				 <c:if test="${roofList.getValSize() == null}">
					<spring:message code="pag.showing" arguments="${roofList.getSize()}"/>
					<spring:message code="pag.total" arguments="${roofList.getTotalElements()}"/> 
				 </c:if>
			 </c:if>

			<nav style="margin-bottom: 30px;">
				<ul class="pagination justify-content-center">
					
					<!-- GO TO FIRST PAGE -->
					<c:choose>
						<c:when test="${roofList.getNumber() == 0}">
							<li class="page-item disabled"><a class="page-link" href="1"><spring:message code="pag.first"/></a></li>
						</c:when>
						<c:otherwise>
							<li class="page-item"><a class="page-link" href="1"><spring:message code="pag.first"/></a></li>
						</c:otherwise>
					</c:choose>
		
					<!-- BEGIN SET VISIBLE PAGES -->
					
					<!-- FIRST PAGE -->
					<c:if test="${roofList.getNumber() == 0 && roofList.getTotalPages() == 2}">
						<c:set var="init" value="1"/>
						<c:set var="end" value="2"/>
					</c:if>
					<c:if test="${roofList.getNumber() == 0 && roofList.getTotalPages() > 2}">
						<c:set var="init" value="1"/>
						<c:set var="end" value="${roofList.getNumber()+3}"/>
					</c:if>
					
					<!-- NEXT PAGES -->
					<c:if test="${roofList.getNumber() > 0  && roofList.getNumber()+1 < roofList.getTotalPages()+1}">
						<c:set var="init" value="${roofList.getNumber()}"/>
						<c:set var="end" value="${roofList.getNumber()+2}"/>
					</c:if>
					
					<!-- LAST PAGE -->
					<c:if test="${roofList.getNumber()+1 == roofList.getTotalPages() && roofList.getNumber() < 2}">
						<c:set var="init" value="${roofList.getNumber()}"/>
						<c:set var="end" value="${roofList.getNumber()+1}"/>
					</c:if>
					<c:if test="${roofList.getNumber()+1 == roofList.getTotalPages() && roofList.getNumber() >= 2}">
						<c:set var="init" value="${roofList.getNumber()-1}"/>
						<c:set var="end" value="${roofList.getNumber()+1}"/>
					</c:if>
					
					<!-- END SET VISIBLE PAGES -->
					
					<!-- BUILD PAGINATION -->
					<c:forEach var = "i" begin = "${init}" end = "${end}">
						<c:choose>
							<c:when test="${roofList.getNumber()+1 == i}">
								<li class="page-item active"><a class="page-link" href="${i}">${i}</a></li>
							</c:when>
							<c:otherwise>
								<li class="page-item"><a class="page-link" href="${i}">${i}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
	
					<!-- GO TO LAST PAGE -->
					<c:choose>
						<c:when test="${roofList.getNumber()+1 == roofList.getTotalPages()}">
							<li class="page-item disabled"><a class="page-link" href="${roofList.getTotalPages()}"><spring:message code="pag.last"/></a></li>
						</c:when>
						<c:otherwise>
							<li class="page-item"><a class="page-link" href="${roofList.getTotalPages()}"><spring:message code="pag.last"/></a></li>
						</c:otherwise>
					</c:choose>
	
				</ul>
			</nav>
	
		</div>
		
	</div>
</c:if>