<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="activeElement" required="false" %>

<!-- REMARKS SELECTED OPTION -->
<c:choose>
	<c:when test="${activeElement eq 'dashboard'}">
    	<c:set var="dashboardActive" value="active"/>
  	</c:when>
  	<c:otherwise>
   		<c:set var="dashboardActive" value=""/>
  	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${activeElement eq 'catalogue'}">
    	<c:set var="catalogueActive" value="active"/>
  	</c:when>
  	<c:otherwise>
   		<c:set var="catalogueActive" value=""/>
  	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${activeElement eq 'validate'}">
    	<c:set var="validateActive" value="active"/>
  	</c:when>
  	<c:otherwise>
   		<c:set var="validateActive" value=""/>
  	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${activeElement eq 'errors'}">
    	<c:set var="errorsActive" value="active"/>
  	</c:when>
  	<c:otherwise>
   		<c:set var="errorsActive" value=""/>
  	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${activeElement eq 'settings'}">
    	<c:set var="settingsActive" value="active"/>
  	</c:when>
  	<c:otherwise>
   		<c:set var="settingsActive" value=""/>
  	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${activeElement eq 'recommender'}">
    	<c:set var="recommenderActive" value="active"/>
  	</c:when>
  	<c:otherwise>
   		<c:set var="recommenderActive" value=""/>
  	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${activeElement eq 'saved'}">
    	<c:set var="savedActive" value="active"/>
  	</c:when>
  	<c:otherwise>
   		<c:set var="savedActive" value=""/>
  	</c:otherwise>
</c:choose>

<!-- CHECK IF A FEATURE IS ENABLED -->
<c:set var="dashboardEnabled">
	<spring:eval expression="@featureStatus.isActive('DAS')"/>
</c:set>
<c:set var="catalogueEnabled">
	<spring:eval expression="@featureStatus.isActive('CAT')"/>
</c:set>
<c:set var="recommenderEnabled">
	<spring:eval expression="@featureStatus.isActive('REC')"/>
</c:set>
<c:set var="errorsEnabled">
	<spring:eval expression="@featureStatus.isActive('ERR')"/>
</c:set>
<c:set var="settingsEnabled">
	<spring:eval expression="@featureStatus.isActive('SET')"/>
</c:set>
<c:set var="validateEnabled">
	<spring:eval expression="@featureStatus.isActive('VAL')"/>
</c:set>
<c:set var="savedEnabled">
	<spring:eval expression="@featureStatus.isActive('SAV')"/>
</c:set>



  <nav class="navbar navbar-expand-lg navbar-dark bg3 fixed-top">
  
  	<security:authorize access="hasRole('ROLE_AD')"> 
	  	<a class="navbar-brand" href="${context}/admin/menu">
	      <img src="${context}/images/logo_banner_no_bg.png" width="30" height="30" class="d-inline-block align-top" alt="">
	      <span class="ponla"><spring:message code="ponla"/></span><span class="verde"><spring:message code="verde"/></span>
	    </a>
  	</security:authorize>
  	<security:authorize access="hasRole('ROLE_IN')"> 
	  	<a class="navbar-brand" href="${context}/individual/menu">
	      <img src="${context}/images/logo_banner_no_bg.png" width="30" height="30" class="d-inline-block align-top" alt="">
	      <span class="ponla"><spring:message code="ponla"/></span><span class="verde"><spring:message code="verde"/></span>
	    </a>
  	</security:authorize>
  	<security:authorize access="hasRole('ROLE_SU')"> 
	  	<a class="navbar-brand" href="${context}/supplier/menu">
	      <img src="${context}/images/logo_banner_no_bg.png" width="30" height="30" class="d-inline-block align-top" alt="">
	      <span class="ponla"><spring:message code="ponla"/></span><span class="verde"><spring:message code="verde"/></span>
	    </a>
  	</security:authorize>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarLogged" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarLogged">
 
 		<!-- OPTIONS LINKS -->
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		
			<security:authorize access="hasRole('ROLE_AD')"> 
				<c:if test="${dashboardEnabled}">		
					<li class="nav-item ${dashboardActive}">
			        	<a class="nav-link" href="${context}/admin/dashboard/"><spring:message code="app.dashboard"/></a>
			        </li>
		        </c:if>
		        <c:if test="${catalogueEnabled}">		
					<li class="nav-item ${catalogueActive}">
						<a class="nav-link" href="${context}/admin/catalogue/"><spring:message code="app.catalogue"/></a>
			        </li>	
			    </c:if>		
			    <c:if test="${validateEnabled}">		        
			        <li class="nav-item ${validateActive}">
			          	<a class="nav-link" href="${context}/admin/validation/"><spring:message code="app.validate2"/></a>
			        </li>
			    </c:if>	
			    <c:if test="${errorsEnabled}">		        
			        <li class="nav-item ${errorsActive}">
			          	<a class="nav-link" href="${context}/admin/error/"><spring:message code="app.errors1"/></a>
			        </li>
			    </c:if>	
		        <li class="nav-item ${settingsActive}">
		          	<a class="nav-link" href="${context}/admin/settings/"><spring:message code="app.settings"/></a>
		        </li>
			</security:authorize>
			
			<security:authorize access="hasRole('ROLE_IN')">
				<c:if test="${catalogueEnabled}"> 			
					<li class="nav-item ${catalogueActive}">
						<a class="nav-link" href="${context}/individual/catalogue/"><spring:message code="app.catalogue"/></a>
			        </li>
			    </c:if>
			    <c:if test="${recommenderEnabled}"> 					        	        	        
			        <li class="nav-item ${recommenderActive}">
			          	<a class="nav-link" href="${context}/individual/recommender/"><spring:message code="app.recommender1"/></a>
			        </li>
			    </c:if>
			    <c:if test="${savedEnabled}"> 	
			        <li class="nav-item ${savedActive}">
			          	<a class="nav-link" href="${context}/individual/saved-list/"><spring:message code="app.saved.recommendations2"/></a>
			        </li>
			    </c:if>
			    <c:if test="${settingsEnabled}"> 	
			        <li class="nav-item ${settingsActive}">
			          	<a class="nav-link" href="${context}/individual/settings/"><spring:message code="app.settings"/></a>
			        </li>
			    </c:if>
			</security:authorize>
			
			<security:authorize access="hasRole('ROLE_SU')"> 			
				<c:if test="${dashboardEnabled}">	
					<li class="nav-item ${dashboardActive}">
			        	<a class="nav-link" href="${context}/supplier/dashboard/"><spring:message code="app.dashboard"/></a>
			        </li>
			    </c:if>
				<c:if test="${catalogueEnabled}">	
					<li class="nav-item ${catalogueActive}">
						<a class="nav-link" href="${context}/supplier/catalogue/"><spring:message code="app.catalogue"/></a>
			        </li>				        
			    </c:if>
			    <c:if test="${validateEnabled}">	
			        <li class="nav-item ${validateActive}">
			          	<a class="nav-link" href="${context}/supplier/validation/"><spring:message code="app.validate2"/></a>
			        </li>	
			    </c:if>
			    <c:if test="${errorsEnabled}">	
			        <li class="nav-item ${errorsActive}">
			          	<a class="nav-link" href="${context}/supplier/error/"><spring:message code="app.errors1"/></a>
			        </li>		        	        
			    </c:if>
			    <c:if test="${settingsEnabled}">	
			        <li class="nav-item ${settingsActive}">
			          	<a class="nav-link" href="${context}/supplier/settings/"><spring:message code="app.settings"/></a>
			        </li>
			    </c:if>
			</security:authorize>
	
		</ul>
		
		<div class="dropdown-divider"></div>
		
		<!-- LANGUAGE LINKS -->
		<ul class="navbar-nav">
			<li class="nav-item dropdown">
	          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	            <spring:message code="language"/>
	          </a>
	          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	            <a class="dropdown-item" href="?lang=ca">CAT</a>
	            <a class="dropdown-item" href="?lang=es">CAS</a>
	            <a class="dropdown-item" href="?lang=en">ENG</a>
	          </div>
	        </li>
		</ul>
		
		<!-- BUTTON TRIGGER MODAL -->
		<a href="${context}/logout" class="btn btn-outline-danger margin" role="button"><i class="w3-medium fa fa-times"></i> <spring:message code="lbl.logout"/></a>
		
	</div>
	
  </nav>