<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />


<c:if test="${not empty roofDTO.errorReportedListDTO}">
	<div class="row">
		<div class="col-12">
			<div class="square">
			
				<div class="row">
					<div class="col-12 ">
						<label class="label no-margin"><spring:message code="cat.errors-reported"/></label>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 ">
						<c:forEach items="${roofDTO.errorReportedListDTO}" var="error">
							<fmt:formatDate var="date" value="${error.reportDate}" pattern="dd/MM/yy (HH:mm)" />
							<p class="no-margin">${date} - ${error.errorReported}</p>
						</c:forEach>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</c:if>