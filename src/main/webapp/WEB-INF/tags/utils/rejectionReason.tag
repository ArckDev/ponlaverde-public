<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />


<c:if test="${not empty roofDTO.rejectionReason}">
	<div class="row">
		<div class="col-12">
			<div class="square">
			
				<div class="row">
					<div class="col-12 ">
						<label class="label no-margin"><spring:message code="cat.rejection"/></label>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12 ">
						<p>${roofDTO.rejectionReason}</p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</c:if>