<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />


<%@ attribute name="message" required="true" %>
<%@ attribute name="varName" required="true" %>
<%@ attribute name="type" required="true" %>
<%@ attribute name="dto" required="true" %>
<%@ attribute name="disabled" required="false" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="onchange" required="false" %>


<label class="label-margin font-medium"><spring:message code="${message}"/></label>
<c:if test="${disabled == null}">
	<input type="${type}" class="form-control form-control-sm dark font-medium" name="${varName}" id="${varName}" value="<spring:message code="${dto}"/>"> 
</c:if>
<c:if test="${disabled == true}">
	<input disabled type="${type}" class="form-control form-control-sm dark font-medium" name="${varName}" id="${varName}" value="<spring:message code="${dto}"/>"> 
</c:if>