<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="modal" tagdir="/WEB-INF/tags/modal" %>

<%@ attribute name="title" required="true" %>
<%@ attribute name="message" required="false" %>
<%@ attribute name="imagePath" required="false" %>


<c:set var="context" value="${pageContext.request.contextPath}" />
<c:set var="modalTitleTemp" value="${fn:replace(title, ' ', '_')}"></c:set>
<c:set var="modalTitle" value="${fn:replace(modalTitleTemp, '\\'', '_')}"></c:set>


<c:choose>
	<c:when test="${imagePath == null}">
		<a class="text-info open-info" data-toggle="modal" data-target="#infoGenericModal${modalTitle}">
			<i class="w3-medium fa fa-question-circle-o"></i> 
		</a>
		<modal:infoGenericModal title="${title}" message="${message}"/>	
	</c:when>
	<c:otherwise>
		<a class="text-info open-info" data-toggle="modal"
			data-target="#infoImageModal${modalTitle}">
			<i class="w3-medium fa fa-question-circle-o"></i> 
		</a>
		<modal:infoImageModal title="${title}" message="${message}" imagePath="${imagePath}"/>		
	</c:otherwise>
</c:choose>

	