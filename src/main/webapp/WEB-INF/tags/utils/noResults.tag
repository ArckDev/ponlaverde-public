<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />


<div class="col-12 text-center">
	<div class="square">
		<p><spring:message code="cat.no-results" /></p>
		<div class="wb" >
			<img src="${context}/images/ack_notfound_ico.png" style="max-width: 15%;">
		</div>
	</div>
</div>
