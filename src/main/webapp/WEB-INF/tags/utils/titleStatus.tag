<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="title" required="true" %>

<div class="row">
	<div class="col-12">
		<div class="square">
			<div class="row">
				<div class="col-lg-11 col-md-11 col-sm-10 col-xs-9">
					<p class="title"><spring:message code="${title}"/></p>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-2 col-xs-3 text-center">
					<c:if test="${roofDTO.status eq 'VAL'}">
						<p class="circle validated"><spring:message code="${roofDTO.status}"/></p>
					</c:if>
					<c:if test="${roofDTO.status eq 'PEN'}">
						<p class="circle pending"><spring:message code="${roofDTO.status}"/></p>
					</c:if>
					<c:if test="${roofDTO.status eq 'IND'}">
						<p class="circle individual"><spring:message code="${roofDTO.status}"/></p>
					</c:if>
					<c:if test="${roofDTO.status eq 'REJ'}">
						<p class="circle rejected"><spring:message code="${roofDTO.status}"/></p>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>