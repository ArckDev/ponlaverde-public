<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />


<%@ attribute name="message" required="true" %>
<%@ attribute name="varName" required="true" %>
<%@ attribute name="map" required="false" type="java.util.TreeMap" %>
<%@ attribute name="list" required="false" type="java.util.List" %>
<%@ attribute name="dto" required="true" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="onchange" required="false" %>


<label class="label-margin font-medium"><spring:message code="${message}"/></label>
<c:if test="${required == true}">
	<select required class="form-control form-control-sm dark font-medium" name="${varName}" id="${varName}" onchange="${onchange}">
</c:if>
<c:if test="${required == false || required == null}">
	<select class="form-control form-control-sm dark font-medium" name="${varName}" id="${varName}" onchange="${onchange}">
</c:if>
<c:choose>
	<c:when test="${map != null}">
		<option selected disabled hidden><spring:message code="${message}.plh"/></option>
		<c:forEach items="${map}" var="map" varStatus="loop">
			<c:choose>
				<c:when test="${dto eq map.key}">
			  		<option selected value="${map.key}">${map.value}</option>
			  	</c:when>
			  	<c:otherwise>
			  		<option value="${map.key}">${map.value}</option>
			  	</c:otherwise>
			 </c:choose>
		</c:forEach>
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${list != null}">
		<option selected disabled hidden><spring:message code="${message}.plh"/></option>
		<c:forEach items="${list}" var="list" varStatus="loop">
			<c:choose>
				<c:when test="${dto eq list}">
			  		<option selected value="${list}">${list}</option>
			  	</c:when>
			  	<c:otherwise>
			  		<option value="${list}">${list}</option>
			  	</c:otherwise>
			 </c:choose>
		</c:forEach>
	</c:when>
</c:choose>

</select>