<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="title" required="false" %>
<%@ attribute name="titleDirect" required="false" %>

<div class="row justify-content-center">
	<div class="col-12">
		<div class="square">
			<div class="row">
				<div class="col-12">
					<c:if test="${title != null}">
						<p class="title"><spring:message code="${title}" /></p>
					</c:if>
					<c:if test="${titleDirect != null}">
						<p class="title">${titleDirect}</p>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>