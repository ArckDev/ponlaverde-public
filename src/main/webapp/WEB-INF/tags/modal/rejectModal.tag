<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="type" required="true" %>
<%@ attribute name="message" required="true" %>
<%@ attribute name="submitButton" required="true" %>
<%@ attribute name="submit" required="true" %>


<div class="modal fade bd-example-modal-sm" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content bg3">
   		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body centered">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 success-logo">
				<img src="${context}/images/ack_${type}_ico.png">
			</div>
			<div class="col-12 text-center">
				<div class="alert alert-success" role="alert" style="margin-top:10%;">
					<spring:message code="${message}"/>
				</div>
			</div>
			<div class="col-12 text-center">
				<div style="margin-top:10%;">
					<textarea name="rejectionReason" cols="20" rows="3" maxlength="75"></textarea>
				</div>
			</div>
		</div>
		<div class="modal-footer centered text-center">
			<div class="row text-center" >
				<div class="col-6">
					<button type="button" class="btn btn-outline-warning btn-lg margin-all" data-dismiss="modal" data-dismiss="modal">
					<spring:message code="button.return" /></button>
				</div>
				<div class="col-6">
					<button type="submit" formaction="${submit}" class="btn btn-outline-success btn-lg margin-all">
					<spring:message code="${submitButton}" /></button>
				</div>	
			</div>
      	</div>
	</div>

  </div>
</div>

