<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<%@ taglib  prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions"%>

<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="title" required="true" %>
<%@ attribute name="message" required="true" %>

<c:set var="modalTitleTemp" value="${fn:replace(title, ' ', '_')}"></c:set>
<c:set var="modalTitle" value="${fn:replace(modalTitleTemp, '\\'', '_')}"></c:set>
	
	<div class="modal fade" id="infoGenericModal${modalTitle}" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content bg3">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body centered">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 success-logo">
						<img src="${context}/images/ack_question_ico.png" style="width: 20%;">
					</div>
					<div class="col-12 text-center no-overflow">
						<h3 style="margin-top: 40px;margin-bottom: 0px;">${title}</h3>
					</div>
					<div class="col-12 text-justify no-overflow">
						<div class="alert bg-secondary" role="alert" style="margin-top:10%;" >
							<p>${message}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>