<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
	
<c:set var="context" value="${pageContext.request.contextPath}" />

<%@ attribute name="title" required="true" %>
<%@ attribute name="message" required="false" %>
<%@ attribute name="imagePath" required="true" %>

	<div class="modal fade" id="infoImageModal${title}" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content bg3">
				<div class="modal-header centered">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body centered">
					<div class="col-12 text-center">
						<h3 style="margin-bottom: 40px;">${title}</h3>
					</div>
					<div class="col-12 text-center" style="margin-bottom: 15px;">
						<img src="${imagePath}">
					</div>
					<div class="col-12 text-center">
						<c:if test="${message != ''}">
							<div class="alert bg-secondary" role="alert" style="margin-top:10%;" >
							 	<p>${message}</p>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>