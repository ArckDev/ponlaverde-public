package com.upc.arcktech.ponlaverde.constants;

public enum UserRole {
	ADMIN("AD"), INDIVIDUAL("IN"), SUPPLIER("SU");

	private String code;

	private UserRole(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	@Override
    public String toString() {
        return this.getCode();
    }


}
