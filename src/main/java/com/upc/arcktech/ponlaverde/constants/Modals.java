package com.upc.arcktech.ponlaverde.constants;

public enum Modals {
	
	SUCCESS_MODAL("#successGenericModal"),
	WARNING_MODAL("#warningGenericModal");
	
	
	private String modalName;

	private Modals(String modalName) {
		this.modalName = modalName;
	}

	public String getModalName() {
		return modalName;
	}

	@Override
    public String toString() {
        return this.getModalName();
    }
	
	
	

}
