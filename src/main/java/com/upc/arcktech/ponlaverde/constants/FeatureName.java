package com.upc.arcktech.ponlaverde.constants;


public enum FeatureName {
	CATALOGUE("CAT"), DASHBOARD("DAS"), RECOMMENDER("REC"), ERROR("ERR"), SETTINGS("SET"), VALIDATION("VAL"), 
	RECOMMENDER_SAVE("SAV");

	private String code;

	private FeatureName(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	@Override
    public String toString() {
        return this.getCode();
    }


}
