package com.upc.arcktech.ponlaverde.constants;


public enum RoofStatus {
	VALIDATED("VAL"), INDIVIDUAL("IND"), PENDING("PEN"), REJECTED("REJ");

	private String code;

	private RoofStatus(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	@Override
    public String toString() {
        return this.getCode();
    }


}
