package com.upc.arcktech.ponlaverde.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.constants.RoofStatus;
import com.upc.arcktech.ponlaverde.dao.RoofDAO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;

@Repository
public class RoofDAOImpl<T> implements RoofDAO {
	
	@PersistenceContext
    private EntityManager em;

	/* 
	 * This method filters all the roofs with individual status.
	 * Used by ADMIN.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Roof> filterAllIndividualRoofs(RoofDTO roofDTO) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
        
        if (roofDTO.getAesthetics() != null) {
        	criteria.add(Restrictions.eq("aesthetics", roofDTO.getAesthetics()));
	    }
	    if (!roofDTO.getClimate().isEmpty()) {
	    	criteria.createCriteria("climate").add(Restrictions.eq("code", roofDTO.getClimate().get(0)));
	    }
	    if (roofDTO.getImplantation() != null) {
	    	criteria.add(Restrictions.eq("implantation", roofDTO.getImplantation()));
	    }
	    if (roofDTO.getMaintenance() != null) {
	    	criteria.add(Restrictions.eq("maintenance", roofDTO.getMaintenance()));
	    }
	    if (roofDTO.getMaker() != null) {
	    	criteria.add(Restrictions.eq("maker", roofDTO.getMaker()));
	    }
	    if (roofDTO.getPrice() != null) {
	    	criteria.add(Restrictions.le("price", roofDTO.getPrice()));
	    }
	    if (roofDTO.getRegulation() != null) {
	    	criteria.add(Restrictions.eq("regulation", roofDTO.getRegulation()));
	    }
	    if (roofDTO.getSlope() != null) {
	    	criteria.add(Restrictions.eq("slope", roofDTO.getSlope()));
	    }
	    if (roofDTO.getStorage() != null) {
	    	criteria.add(Restrictions.eq("storage", roofDTO.getStorage()));
	    }
	    if (roofDTO.getThermal() != null) {
	    	criteria.add(Restrictions.eq("thermal", roofDTO.getThermal()));
	    }
	    if (roofDTO.getTraffic() != null) {
	    	criteria.add(Restrictions.eq("traffic", roofDTO.getTraffic()));
	    }
	    if (roofDTO.getType() != null) {
	    	criteria.add(Restrictions.eq("type", roofDTO.getType()));
	    }
	    if (roofDTO.getWall() != null) {
	    	criteria.add(Restrictions.le("wall", roofDTO.getWall()));
	    }
	    if (roofDTO.getWeight() != null) {
	    	criteria.add(Restrictions.le("weight", roofDTO.getWeight()));
	    }
	    if (roofDTO.getWinds() != null) {
	    	criteria.add(Restrictions.eq("winds", roofDTO.getWinds()));
	    }

        
		List<Roof> roofs = criteria.list();
		
		
		return roofs;
	}
	
	/* 
	 * This method filters all the roofs with individual status.
	 * Used by ADMIN.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> filterAllIndividualRoofs(RoofDTO roofDTO, Pageable pageable) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
		criteriaCount.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
        
        setCriteria(roofDTO, criteria);
        setCriteria(roofDTO, criteriaCount);

		Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		

		return pagedResults;
	}

	/* 
	 * This method filters all the roofs with validated status.
	 * Used by ADMIN and INDIVIDUAL.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> filterValidatedRoofs(RoofDTO roofDTO, Pageable pageable) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteriaCount.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
        
        setCriteria(roofDTO, criteria);
        setCriteria(roofDTO, criteriaCount);

        Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		
		return pagedResults;
	}
	
	/* 
	 * This method filters all the roofs with validated status and enabled.
	 * Used by INDIVIDUAL.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> filterValidatedEnabledRoofs(RoofDTO roofDTO, Pageable pageable) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteria.add(Restrictions.eq("enabled", true));
		
		criteriaCount.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteriaCount.add(Restrictions.eq("enabled", true));
        
        setCriteria(roofDTO, criteria);
        setCriteria(roofDTO, criteriaCount);
 
        Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);

		return pagedResults;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Roof> filterValidatedEnabledRoofs(RoofDTO roofDTO) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteria.add(Restrictions.eq("enabled", true));
        
		setCriteria(roofDTO, criteria);

		List<Roof> roofs = criteria.list();

		return roofs;
	}
	
	/* 
	 * This method filters all the roofs owned by a user no matter which status is.
	 * Used by SUPPLIER.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> filterOwnedValidatedRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		
		criteriaCount.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteriaCount.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
        
        setCriteria(roofDTO, criteria);
        setCriteria(roofDTO, criteriaCount);

		Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		return pagedResults;
	}
	
	/* 
	 * This method filters all the roofs owned by a user with status individual (no moved to validated status).
	 * Used by INDIVIDUAL.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> filterOwnedIndividualRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
		criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		
		criteriaCount.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
		criteriaCount.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
        
        setCriteria(roofDTO, criteria);
        setCriteria(roofDTO, criteriaCount);
        
		Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);	
		
		return pagedResults;
	}
	
	/* 
	 * This method filters all the roofs owned by a user with status individual and Enabled (no moved to validated status).
	 * Used by INDIVIDUAL.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Roof> filterOwnedIndividualEnabledRoofs(RoofDTO roofDTO, UserDTO userDTO) {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
        
		criteria.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
		criteria.add(Restrictions.eq("enabled", true));
		criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
        
        if (roofDTO.getAesthetics() != null) {
        	criteria.add(Restrictions.eq("aesthetics", roofDTO.getAesthetics()));
	    }
	    if (!roofDTO.getClimate().isEmpty()) {
	    	criteria.createCriteria("climate").add(Restrictions.eq("code", roofDTO.getClimate().get(0)));
	    }
	    if (roofDTO.getImplantation() != null) {
	    	criteria.add(Restrictions.eq("implantation", roofDTO.getImplantation()));
	    }
	    if (roofDTO.getMaintenance() != null) {
	    	criteria.add(Restrictions.eq("maintenance", roofDTO.getMaintenance()));
	    }
	    if (roofDTO.getMaker() != null) {
	    	criteria.add(Restrictions.eq("maker", roofDTO.getMaker()));
	    }
	    if (roofDTO.getPrice() != null) {
	    	criteria.add(Restrictions.le("price", roofDTO.getPrice()));
	    }
	    if (roofDTO.getRegulation() != null) {
	    	criteria.add(Restrictions.eq("regulation", roofDTO.getRegulation()));
	    }
	    if (roofDTO.getSlope() != null) {
	    	criteria.add(Restrictions.eq("slope", roofDTO.getSlope()));
	    }
	    if (roofDTO.getStorage() != null) {
	    	criteria.add(Restrictions.eq("storage", roofDTO.getStorage()));
	    }
	    if (roofDTO.getThermal() != null) {
	    	criteria.add(Restrictions.eq("thermal", roofDTO.getThermal()));
	    }
	    if (roofDTO.getTraffic() != null) {
	    	criteria.add(Restrictions.eq("traffic", roofDTO.getTraffic()));
	    }
	    if (roofDTO.getType() != null) {
	    	criteria.add(Restrictions.eq("type", roofDTO.getType()));
	    }
	    if (roofDTO.getWall() != null) {
	    	criteria.add(Restrictions.le("wall", roofDTO.getWall()));
	    }
	    if (roofDTO.getWeight() != null) {
	    	criteria.add(Restrictions.le("weight", roofDTO.getWeight()));
	    }
	    if (roofDTO.getWinds() != null) {
	    	criteria.add(Restrictions.eq("winds", roofDTO.getWinds()));
	    }

        
		List<Roof> roofs = criteria.list();
		
		
		return roofs;
	}
	
	/* 
	 * This method retrieves all the roofs owned by a user with status validated (not pending or rejected).
	 * Used by SUPPLIER.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> findOwnedRoofsValidated(UserDTO userDTO, Pageable pageable) {
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);

	    criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	    criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
	    
	    criteriaCount.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	    criteriaCount.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
	
	    Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		return pagedResults;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> findOwnedRoofsRejected(UserDTO userDTO, Pageable pageable) {
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);

	    criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteria.add(Restrictions.eq("status", RoofStatus.REJECTED.getCode()));
		
		criteriaCount.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteriaCount.add(Restrictions.eq("status", RoofStatus.REJECTED.getCode()));
	
	    Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		return pagedResults;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> findOwnedRoofsPending(UserDTO userDTO, Pageable pageable) {
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);

	    criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	    criteria.add(Restrictions.eq("status", RoofStatus.PENDING.getCode()));
	    
	    criteriaCount.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	    criteriaCount.add(Restrictions.eq("status", RoofStatus.PENDING.getCode()));
	
	    Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		return pagedResults;		
	}
	
	
	/* 
	 * This method retrieves all the roofs owned by a user with status individual (no moved to validated status).
	 * Used by INDIVIDUAL.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> findOwnedIndividualRoofs(UserDTO userDTO, Pageable pageable) {
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);

		criteria.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
	    criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	    
	    criteriaCount.add(Restrictions.eq("status", RoofStatus.INDIVIDUAL.getCode()));
	    criteriaCount.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	
	    Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		return pagedResults;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> findValidatedEnabledRoofs(Pageable pageable) {
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);

		criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteria.add(Restrictions.eq("enabled", true));
		
		criteriaCount.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteriaCount.add(Restrictions.eq("enabled", true));

		Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		return pagedResults;		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<Roof> findOwnedRoofsWithErrors(UserDTO userDTO, Pageable pageable) {
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		Criteria criteriaCount = em.unwrap(Session.class).createCriteria(Roof.class);
		
	    criteria.createCriteria("errorReported").createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	    
	    criteriaCount.createCriteria("errorReported").createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
	    criteriaCount.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	
	    Page<Roof> pagedResults = (Page<Roof>) getPagedResults(pageable, criteria, criteriaCount);
		
		return pagedResults;		
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Roof> findRoofsWithErrors() {
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		
	    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	
	    List<Roof> roofs = criteria.list();
		
		return roofs;		
	}

	
	@Override
	public Integer getMaxPrice() {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);

		criteria.add(Restrictions.ne("status", RoofStatus.PENDING.getCode()));
		criteria.setProjection(Projections.max("price"));
		
		Double maxPriceValidated = (Double) criteria.uniqueResult();		
		Integer maxPrice = (int) Math.ceil(maxPriceValidated);
		
		
		return maxPrice;
	}
	
	
	@Override
	public Integer getMinPrice() {

		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);

		criteria.add(Restrictions.ne("status", RoofStatus.PENDING.getCode()));
		criteria.setProjection(Projections.min("price"));
		
		Double minPriceValidated = (Double) criteria.uniqueResult();
		Integer minPrice = (int) Math.ceil(minPriceValidated);
		
		
		return minPrice;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllMakerByOwner(UserDTO userDTO) {

		Criteria criteriaOwner = em.unwrap(Session.class).createCriteria(Roof.class);
		
		criteriaOwner.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteriaOwner.setProjection(Projections.distinct(Projections.property("maker")));

		List<String> roofsMakerOwner = criteriaOwner.list();

		
		return roofsMakerOwner;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllMakerValidated() {

		Criteria criteriaValidated = em.unwrap(Session.class).createCriteria(Roof.class);
		
		criteriaValidated.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteriaValidated.setProjection(Projections.distinct(Projections.property("maker")));
		
		List<String> roofsMakerValidated = criteriaValidated.list();

		
		return roofsMakerValidated;		
	}
	
	
	@Override
	public Double getMaxPriceByOwner(UserDTO userDTO) {

		Double maxPriceOwner = (Double) this.getMaxValueByOwner(userDTO, "price");

		return maxPriceOwner;
	}
	
	
	@Override
	public Double getMinPriceByOwner(UserDTO userDTO) {

		Double minPriceOwner = (Double) this.getMinValueByOwner(userDTO, "price");

		return minPriceOwner;
	}
	
	
	@Override
	public Integer getMaxWeightByOwner(UserDTO userDTO) {

		Integer maxWeightOwner = (Integer) this.getMaxValueByOwner(userDTO, "weight");
		
		return maxWeightOwner;
	}
	
	
	@Override
	public Integer getMinWeightByOwner(UserDTO userDTO) {

		Integer minWeightOwner = (Integer) this.getMinValueByOwner(userDTO, "weight");

		return minWeightOwner;
	}
	
	
	@Override
	public Integer getMaxWallByOwner(UserDTO userDTO) {

		Integer maxWallOwner = (Integer) this.getMaxValueByOwner(userDTO, "wall");

		return maxWallOwner;
	}
	
	
	@Override
	public Integer getMinWallByOwner(UserDTO userDTO) {

		Integer minWallOwner = (Integer) this.getMinValueByOwner(userDTO, "wall");

		return minWallOwner;
	}
	
	
	
	@Override
	public Double getMaxPriceValidatedByOwner(UserDTO userDTO) {

		Double maxPriceOwner = (Double) this.getMaxValueValidatedByOwner(userDTO, "price");

		return maxPriceOwner;
	}
	
	
	@Override
	public Double getMinPriceValidatedByOwner(UserDTO userDTO) {

		Double minPriceOwner = (Double) this.getMinValueValidatedByOwner(userDTO, "price");

		return minPriceOwner;
	}
	
	
	@Override
	public Integer getMaxWeightValidatedByOwner(UserDTO userDTO) {

		Integer maxWeightOwner = (Integer) this.getMaxValueValidatedByOwner(userDTO, "weight");
		
		return maxWeightOwner;
	}
	
	
	@Override
	public Integer getMinWeightValidatedByOwner(UserDTO userDTO) {

		Integer minWeightOwner = (Integer) this.getMinValueValidatedByOwner(userDTO, "weight");

		return minWeightOwner;
	}
	
	
	@Override
	public Integer getMaxWallValidatedByOwner(UserDTO userDTO) {

		Integer maxWallOwner = (Integer) this.getMaxValueValidatedByOwner(userDTO, "wall");

		return maxWallOwner;
	}
	
	
	@Override
	public Integer getMinWallValidatedByOwner(UserDTO userDTO) {

		Integer minWallOwner = (Integer) this.getMinValueValidatedByOwner(userDTO, "wall");

		return minWallOwner;
	}
	
	
private void setCriteria(RoofDTO roofDTO, Criteria criteria) {
		
		if (roofDTO.getAesthetics() != null) {
        	criteria.add(Restrictions.eq("aesthetics", roofDTO.getAesthetics()));
	    }
	    if (!roofDTO.getClimate().isEmpty()) {
	    	criteria.createCriteria("climate").add(Restrictions.eq("code", roofDTO.getClimate().get(0)));
	    }
	    if (roofDTO.getImplantation() != null) {
	    	criteria.add(Restrictions.eq("implantation", roofDTO.getImplantation()));
	    }
	    if (roofDTO.getMaintenance() != null) {
	    	criteria.add(Restrictions.eq("maintenance", roofDTO.getMaintenance()));
	    }
	    if (roofDTO.getMaker() != null) {
	    	criteria.add(Restrictions.eq("maker", roofDTO.getMaker()));
	    }
	    if (roofDTO.getPrice() != null) {
	    	criteria.add(Restrictions.le("price", roofDTO.getPrice()));
	    }
	    if (roofDTO.getRegulation() != null) {
	    	criteria.add(Restrictions.eq("regulation", roofDTO.getRegulation()));
	    }
	    if (roofDTO.getSlope() != null) {
	    	criteria.add(Restrictions.eq("slope", roofDTO.getSlope()));
	    }
	    if (roofDTO.getStorage() != null) {
	    	criteria.add(Restrictions.eq("storage", roofDTO.getStorage()));
	    }
	    if (roofDTO.getThermal() != null) {
	    	criteria.add(Restrictions.eq("thermal", roofDTO.getThermal()));
	    }
	    if (roofDTO.getTraffic() != null) {
	    	criteria.add(Restrictions.eq("traffic", roofDTO.getTraffic()));
	    }
	    if (roofDTO.getType() != null) {
	    	criteria.add(Restrictions.eq("type", roofDTO.getType()));
	    }
	    if (roofDTO.getWall() != null) {
	    	criteria.add(Restrictions.le("wall", roofDTO.getWall()));
	    }
	    if (roofDTO.getWeight() != null) {
	    	criteria.add(Restrictions.le("weight", roofDTO.getWeight()));
	    }
	    if (roofDTO.getWinds() != null) {
	    	criteria.add(Restrictions.eq("winds", roofDTO.getWinds()));
	    }
	}

	
	@SuppressWarnings("unchecked")
	private Page<T> getPagedResults(Pageable pageable, Criteria criteria, Criteria criteriaCount) {
		
		// SET QUERY LIMITS
		criteria.setFirstResult(pageable.getOffset());
	    criteria.setMaxResults(pageable.getPageSize());
	    
	    // GET GENERIC LIST OF RESULTS
		List<T> list = criteria.list();
		
		// GET TOTAL RESULTS NUMBER
		criteriaCount.setProjection(Projections.rowCount());
		Long count = (Long) criteriaCount.uniqueResult();
		
		// CREATE GENERIC PAGED RESULTS
		Page<T> pagedResults = new PageImpl<T>(list, pageable, count);
		
		return pagedResults;
	}
	
	private Object getMinValueByOwner (UserDTO userDTO, String param) {
		
		Object minValueOwner = null;
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		
		criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteria.setProjection(Projections.min(param));
		
		if(criteria.uniqueResult() instanceof Double) {
			minValueOwner = (Double) criteria.uniqueResult();
		} else if (criteria.uniqueResult() instanceof Integer) {
			minValueOwner = (Integer) criteria.uniqueResult();
		}

		
		return minValueOwner;
	}
	
	private Object getMaxValueByOwner (UserDTO userDTO, String param) {
		
		Object maxValueOwner = null;
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		
		criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteria.setProjection(Projections.max(param));
		
		if(criteria.uniqueResult() instanceof Double) {
			maxValueOwner = (Double) criteria.uniqueResult();
		} else if (criteria.uniqueResult() instanceof Integer) {
			maxValueOwner = (Integer) criteria.uniqueResult();
		}

		
		return maxValueOwner;
	}

	private Object getMinValueValidatedByOwner (UserDTO userDTO, String param) {
		
		Object minValueOwner = null;
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		
		criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteria.setProjection(Projections.min(param));
		
		if(criteria.uniqueResult() instanceof Double) {
			minValueOwner = (Double) criteria.uniqueResult();
		} else if (criteria.uniqueResult() instanceof Integer) {
			minValueOwner = (Integer) criteria.uniqueResult();
		}

		
		return minValueOwner;
	}
	
	private Object getMaxValueValidatedByOwner (UserDTO userDTO, String param) {
		
		Object maxValueOwner = null;
		
		Criteria criteria = em.unwrap(Session.class).createCriteria(Roof.class);
		
		criteria.createCriteria("user").add(Restrictions.eq("idUser", userDTO.getIdUser()));
		criteria.add(Restrictions.eq("status", RoofStatus.VALIDATED.getCode()));
		criteria.setProjection(Projections.max(param));
		
		if(criteria.uniqueResult() instanceof Double) {
			maxValueOwner = (Double) criteria.uniqueResult();
		} else if (criteria.uniqueResult() instanceof Integer) {
			maxValueOwner = (Integer) criteria.uniqueResult();
		}

		
		return maxValueOwner;
	}


}
