package com.upc.arcktech.ponlaverde.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;

public interface RoofDAO {

	List<Roof> filterAllIndividualRoofs(RoofDTO roofDTO);

	Page<Roof> filterAllIndividualRoofs(RoofDTO roofDTO, Pageable pageable);

	List<Roof> filterOwnedIndividualEnabledRoofs(RoofDTO roofDTO, UserDTO userDTO);

	Integer getMaxPrice();

	Integer getMinPrice();

	List<Roof> findRoofsWithErrors();

	List<String> getAllMakerByOwner(UserDTO userDTO);

	List<String> getAllMakerValidated();

	Double getMinPriceByOwner(UserDTO userDTO);

	Double getMaxPriceByOwner(UserDTO userDTO);

	Integer getMinWeightByOwner(UserDTO userDTO);

	Integer getMaxWeightByOwner(UserDTO userDTO);

	Integer getMinWallByOwner(UserDTO userDTO);

	Integer getMaxWallByOwner(UserDTO userDTO);

	Double getMaxPriceValidatedByOwner(UserDTO userDTO);

	Double getMinPriceValidatedByOwner(UserDTO userDTO);

	Integer getMaxWeightValidatedByOwner(UserDTO userDTO);

	Integer getMinWeightValidatedByOwner(UserDTO userDTO);

	Integer getMaxWallValidatedByOwner(UserDTO userDTO);

	Integer getMinWallValidatedByOwner(UserDTO userDTO);

	Page<Roof> filterValidatedRoofs(RoofDTO roofDTO, Pageable pageable);

	Page<Roof> findValidatedEnabledRoofs(Pageable pageable);

	Page<Roof> findOwnedIndividualRoofs(UserDTO userDTO, Pageable pageable);

	Page<Roof> filterValidatedEnabledRoofs(RoofDTO roofDTO, Pageable pageable);

	Page<Roof> filterOwnedIndividualRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable);

	Page<Roof> findOwnedRoofsValidated(UserDTO userDTO, Pageable pageable);

	Page<Roof> filterOwnedValidatedRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable);

	List<Roof> filterValidatedEnabledRoofs(RoofDTO roofDTO);

	Page<Roof> findOwnedRoofsWithErrors(UserDTO userDTO, Pageable pageable);

	Page<Roof> findOwnedRoofsPending(UserDTO userDTO, Pageable pageable);

	Page<Roof> findOwnedRoofsRejected(UserDTO userDTO, Pageable pageable);


}
