package com.upc.arcktech.ponlaverde.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.UserTempDTO;
import com.upc.arcktech.ponlaverde.entity.UserTemp;
import com.upc.arcktech.ponlaverde.repository.UserTempRepository;
import com.upc.arcktech.ponlaverde.service.UserTempService;
import com.upc.arcktech.ponlaverde.transformer.UserTempTransformer;

@Service
public class UserTempServiceImpl implements UserTempService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserTempRepository userTempRepository;

	@Override
	public UserTempDTO verifyAccount(UserTempDTO userTempDTO) {

		UserTemp userTemp = userTempRepository.findByUsernameAndSKey(userTempDTO.getUsername(), userTempDTO.getsKey());
		
		UserTempDTO verifiedUser = null;
		if(userTemp != null) {
			verifiedUser = UserTempTransformer.entityToDTO(userTemp);
			logger.info("ACCOUNT VERIFIED: " + userTempDTO.toString());
		} else {
			logger.info("ACCOUNT NOT VERIFIED: " + userTempDTO.toString());
		}
		
		return verifiedUser;
	}

	@Override
	public Boolean addTempUser(UserTempDTO userTempDTO) {
		
		Boolean userStored = false;
		
		UserTemp userTemp = UserTempTransformer.DTOtoEntity(userTempDTO);
		
		Date now = new Date();
		userTemp.setRegisterDate(now);
		userTemp.setLastEmail(now);
		
		UserTemp savedUserPending = userTempRepository.save(userTemp);
		
		if(savedUserPending != null) {
			userStored = true;
		}
		
		logger.info("USER ADDED TO PENDING LIST: " + userTempDTO.toString());
		return userStored;
	}
	
	@Override
	public void removeTempUser(UserTempDTO userTempDTO) {
		
		UserTemp userTemp = UserTempTransformer.DTOtoEntity(userTempDTO);
		userTempRepository.delete(userTemp);
		
		logger.info("USER REMOVED FROM PENDING LIST: " + userTempDTO.toString());
	}

}
