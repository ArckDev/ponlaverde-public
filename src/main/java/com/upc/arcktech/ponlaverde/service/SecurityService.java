package com.upc.arcktech.ponlaverde.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
