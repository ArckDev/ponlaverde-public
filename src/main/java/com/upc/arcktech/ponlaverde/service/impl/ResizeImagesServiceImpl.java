/*
 *-----------------------------------------------------------------------------
 * name             : ResizeImages.java
 * project          : MailService
 * created          : Jose Rodriguez - Getronics 04/2018
 * language         : java
 * environment      : jdk1.8
 *-----------------------------------------------------------------------------
 */
package com.upc.arcktech.ponlaverde.service.impl;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.service.ResizeImagesService;

/**
 * @author jose.rodriguez
 *
 */
@Service
public class ResizeImagesServiceImpl implements ResizeImagesService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${max.width}")
	private int maxWidth;
	@Value("${max.heigth}")
	private int maxHeigth;

		
	@Override
	public void checkSizeAndResizeImage(List<File> imageFileList) {

		try {
			for(File fileEntry: imageFileList) {
				
				String ext = FilenameUtils.getExtension(fileEntry.getName()).toLowerCase();
				
				BufferedImage bimg = ImageIO.read(fileEntry);
				int width          = bimg.getWidth();
				int height         = bimg.getHeight();
				
				if(width>maxWidth) {
					BufferedImage resizedImage = resizeImage(bimg, width, height, maxWidth, maxHeigth);
					saveResizedImage(resizedImage, fileEntry, ext);
				}
			}
			
		} catch (IOException e) {
			logger.warn("TRACE: ", e);
		}

	}


	private BufferedImage resizeImage(BufferedImage bimg, int width, int height, int resizedWidth, int resizedHeight) {
		
		double aspectRatio = (double) width / (double) height;
		double resizedAspectRatio = (double) resizedWidth / (double) resizedHeight;
		double newAspectRatio;
		
		/** 
		 *  Check if the resized aspect ratio is not +-10% of original aspect ratio in order to adapt 
		 *  the image to the resized aspect ratio.
		 *  Else use the original aspect ratio of the image.
		 */
		
		if( aspectRatio/resizedAspectRatio < 1.1 &&  aspectRatio/resizedAspectRatio > 0.9) {
			newAspectRatio = resizedAspectRatio;
		} else {
			newAspectRatio = aspectRatio;
		}

		resizedHeight = (int) (resizedWidth / newAspectRatio);

        // Draw the scaled image
        BufferedImage resizedImage = new BufferedImage(resizedWidth, resizedHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(bimg, 0, 0, resizedWidth, resizedHeight, null);

        
        return resizedImage;

	}


	@Override
	public void saveResizedImage(BufferedImage resizedImage, File fileEntry, String ext) {
		// TODO Auto-generated method stub
		
		try {
			//Delete previous image
			fileEntry.delete();
			logger.info("DELETED "+fileEntry.getName()+" from file system");
			
			//Save new image
			ImageIO.write(resizedImage, ext, fileEntry);
			logger.info("SAVED "+fileEntry.getName()+" RESIZED to file system with size: "+
										resizedImage.getWidth()+"x"+resizedImage.getHeight());
		
		} catch (IOException e) {
			logger.warn("TRACE: ", e);
		}

	}
	
	
	

}
