package com.upc.arcktech.ponlaverde.service.impl;

import static com.upc.arcktech.ponlaverde.service.FilePathServiceConstants.ROOFS_PATH;
import static com.upc.arcktech.ponlaverde.service.FilePathServiceConstants.TEMP_PATH;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.upc.arcktech.ponlaverde.constants.RoofStatus;
import com.upc.arcktech.ponlaverde.dao.RoofDAO;
import com.upc.arcktech.ponlaverde.dto.PagedResultsDTO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.RoofLimitValuesDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;
import com.upc.arcktech.ponlaverde.entity.RoofClimate;
import com.upc.arcktech.ponlaverde.entity.RoofRejected;
import com.upc.arcktech.ponlaverde.entity.RoofReportedError;
import com.upc.arcktech.ponlaverde.entity.User;
import com.upc.arcktech.ponlaverde.repository.RoofClimateRepository;
import com.upc.arcktech.ponlaverde.repository.RoofRejectedRepository;
import com.upc.arcktech.ponlaverde.repository.RoofReportedErrorRepository;
import com.upc.arcktech.ponlaverde.repository.RoofRepository;
import com.upc.arcktech.ponlaverde.service.FileFolderService;
import com.upc.arcktech.ponlaverde.service.RoofService;
import com.upc.arcktech.ponlaverde.service.UserService;
import com.upc.arcktech.ponlaverde.transformer.RoofTransformer;
import com.upc.arcktech.ponlaverde.transformer.UserTransformer;
import com.upc.arcktech.ponlaverde.utils.NumberUtils;
import com.upc.arcktech.ponlaverde.utils.SecretKeyUtils;

@Service
@Transactional
public class RoofServiceImpl implements RoofService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	RoofRepository roofRepository;
	@Autowired
	RoofDAO roofDAO;
	@Autowired
	RoofClimateRepository roofClimateRepository;
	@Autowired
	FileFolderService fileFolderService;
	@Autowired
	UserService userService;
	@Autowired
	RoofRejectedRepository rejectedRoofRepository;
	@Autowired
	RoofReportedErrorRepository reportedErrorRoofRepository;

	@Value("${root.file-path}")
	public String rootPath;


	
	@Override
	public Boolean addRoof(RoofDTO roofDTO, RoofStatus status) {

		Roof storedRoof = null;
		File sourcePath = null;
		
		try {
			
			if(roofDTO.getIdRoof() != null) {
				storedRoof = this.updateRoof(roofDTO, status);
				
			} else {
				// SET OWNER
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				UserDTO userDTO = userService.getUserByUsername(auth.getName());
				User user = UserTransformer.DTOtoEntity(userDTO);
				Collection<User> userOwner = new ArrayList<User>();
				userOwner.add(user);
				
				// STORE ROOF
				Roof newRoof = RoofTransformer.DTOtoEntity(roofDTO);
				newRoof.setStatus(status.toString());
				newRoof.setEnabled(true);
				newRoof.setCreationDate(new Date());
				newRoof.setUser(userOwner);
				switch (roofDTO.getType()) {
					case "EXT":
						newRoof.setAesthetics("BAD");
						break;
					case "SEM":
						newRoof.setAesthetics("MED");
						break;
					case "INT":
						newRoof.setAesthetics("GOO");
						break;
					default:
						break;
				}

				storedRoof = roofRepository.save(newRoof);
			}
			
			// STORE LINKED FILES
			sourcePath = new File(rootPath + ROOFS_PATH + "/Roof-" + storedRoof.getIdRoof());
			sourcePath.mkdirs();
			
			fileFolderService.storeAttachementsFromTempFolder(fileFolderService, storedRoof.getIdRoof(), roofDTO.getUploadedImagesList(), "PHOTO_");
			fileFolderService.storeAttachementsFromTempFolder(fileFolderService, storedRoof.getIdRoof(), roofDTO.getUploadedDetailsList(), "DETAIL_");
			fileFolderService.storeAttachementsFromTempFolder(fileFolderService, storedRoof.getIdRoof(), roofDTO.getUploadedSheetsList(), "SHEET_");
			
			//CREATE ZIP RESOURCES
			fileFolderService.zipDirectory(sourcePath.getAbsolutePath(), storedRoof.getIdRoof());
			
	
			logger.info("CREATED ROOF: "+roofDTO.toString());
			
		} catch (IOException e) {
			sourcePath.delete();
			logger.error(e.getMessage());
		}

		return storedRoof != null? true : false;
	}
	
	@Override
	public Roof updateRoof(RoofDTO roofDTO, RoofStatus status) {

		Roof roofUpdated = null;
		List<RoofClimate> roofClimateList = new ArrayList<>();		

		// GET STORED ROOF
		Roof roofStored = this.roofRepository.findOne(roofDTO.getIdRoof());
		List<RoofClimate> roofClimates = roofStored.getClimate();
		roofClimateRepository.delete(roofClimates);
		
		// UPDATE ROOF
		roofStored.setClimate(roofClimateList);
		roofStored.setImplantation(roofDTO.getImplantation());
		roofStored.setMaintenance(roofDTO.getMaintenance());
		roofStored.setModel(roofDTO.getModel());
		roofStored.setPrice(roofDTO.getPrice());
		roofStored.setRegulation(roofDTO.getRegulation());
		roofStored.setSlope(roofDTO.getSlope());
		roofStored.setStatus(status.toString()); 
		roofStored.setStorage(roofDTO.getStorage());
		roofStored.setThermal(roofDTO.getThermal());
		roofStored.setTraffic(roofDTO.getTraffic());
		roofStored.setType(roofDTO.getType());
		roofStored.setWall(roofDTO.getWall());
		roofStored.setWeight(roofDTO.getWeight());
		roofStored.setWinds(roofDTO.getWinds());
		
		switch (roofDTO.getType()) {
			case "EXT":
				roofStored.setAesthetics("BAD");
				break;
			case "SEM":
				roofStored.setAesthetics("MED");
				break;
			case "INT":
				roofStored.setAesthetics("GOO");
				break;
			default:
				break;
		}
		
		for(String climate: roofDTO.getClimate()) {
			RoofClimate roofClimate = new RoofClimate();
			roofClimate.setCode(climate);
			roofClimateList.add(roofClimate);
		}
		
		roofUpdated = roofRepository.save(roofStored);

		logger.info("UPDATED ROOF: "+roofDTO.toString());

		return roofUpdated;
	}

	@Override
	public void saveTempFiles(MultipartFile[] uploadingFiles, List<String> uploadedFilesList) {
		
		try {
			
			new File(rootPath+TEMP_PATH).mkdirs();
	
			uploadedFilesList.clear();
			
			for(MultipartFile uploadedFile : uploadingFiles) {
				
				if(!"".equals(uploadedFile.getOriginalFilename())){
		            File file = new File(rootPath + TEMP_PATH + SecretKeyUtils.generateHashedRandomString() + "." + FilenameUtils.getExtension(uploadedFile.getOriginalFilename()));
		            uploadedFile.transferTo(file);
		            
		            uploadedFilesList.add(file.getName());
				}
	        }
		
		
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	@Override
	public void validateRoof(Long idRoof) {
		
		
		try {
			
			Roof roof = roofRepository.findOne(idRoof);
			roof.setStatus(RoofStatus.VALIDATED.toString());

			//Delete rejected roof if exists
			if(roof.getRejectedRoof() != null) {
				roof.getRejectedRoof().remove(0);
			}
			
			roofRepository.saveAndFlush(roof);
			
			logger.info("VALIDATE ROOF " + idRoof);
			
		}catch (Exception e) {
			logger.error("ERROR VALIDATING ROOF " + idRoof + " " + e.getMessage());
		}
		
		
	}
	
	@Override
	public void rejectRoof(RoofDTO roofDTO) {
		
		try {
			
			RoofRejected rejectedRoof = new RoofRejected();	
			Roof roof = roofRepository.findOne(roofDTO.getIdRoof());
			
			if(!roof.getRejectedRoof().isEmpty()) {
				rejectedRoof = roof.getRejectedRoof().get(0);
				rejectedRoof.setRejectionReason(roofDTO.getRejectionReason());
				rejectedRoof.setRejectionDate(new Date());
			} else {
				rejectedRoof = new RoofRejected();
				rejectedRoof.setRejectionReason(roofDTO.getRejectionReason());
				rejectedRoof.setRejectionDate(new Date());
			}

			roof.getRejectedRoof().add(rejectedRoof);
			roof.setStatus(RoofStatus.REJECTED.toString());
			
			roofRepository.save(roof);

			logger.info("REJECTED ROOF " + roofDTO.getIdRoof());
			
		} catch (Exception e) {
			logger.error("ERROR REJECTING ROOF " + roofDTO.getIdRoof() + " " + e.getMessage());
		}

		
	}
	
	@Override
	public Boolean enableDisableRoof(Long idRoof) {
		
		Roof roof = null;
		
		try {
			
			roof = roofRepository.findOne(idRoof);
			
			if(roof.getEnabled()) {
				roof.setEnabled(false);
			} else {
				roof.setEnabled(true);
			}

			roofRepository.save(roof);

			logger.info("ROOF " + idRoof + " ENABLED: " +  roof.getEnabled());
			
		} catch (Exception e) {
			logger.error("ERROR ENABLING/DISABLING ROOF " + idRoof + " " + e.getMessage());
		}
		
		return roof.getEnabled();
	}
	
	@Override
	public void deleteRoof(Long idRoof) {

		try {
			roofRepository.delete(idRoof);
			
			//Delete resources
			fileFolderService.deleteAttachments(idRoof);
			
			logger.info("DELETED ROOF" + idRoof);
			
		} catch (Exception e) {
			logger.error("ERROR DELETING ROOF " + idRoof + " " + e.getMessage());
		}
		
	}
	
	@Override
	public void moveRoofToGlobal(Long idRoof) {

		try {
			Roof roof = roofRepository.findOne(idRoof);
			roof.setStatus(RoofStatus.VALIDATED.getCode());
			roofRepository.save(roof);
			
			logger.info("MOVED ROOF " + idRoof +" TO GLOBAL CATALOGUE");
			
		} catch (Exception e) {
			logger.error("ERROR MOVING ROOF " + idRoof + " " + e.getMessage());
		}
		
	}
	
	@Override
	public void restoreRoofToOwner(Long idRoof) {

		try {
			Roof roof = roofRepository.findOne(idRoof);
			roof.setStatus(RoofStatus.INDIVIDUAL.getCode());
			roofRepository.save(roof);
			
			logger.info("RETURNED ROOF " + idRoof +" TO OWNER");
			
		} catch (Exception e) {
			logger.error("ERROR RETURNING ROOF " + idRoof + " " + e.getMessage());
		}
		
	}


	@Override
	public void reportError(RoofDTO roofDTO) {
		
		RoofReportedError reportedErrorRoof = new RoofReportedError();
		
		try {
			Roof roof = roofRepository.findOne(roofDTO.getIdRoof());
			
			reportedErrorRoof.setReportDate(new Date());
			reportedErrorRoof.setErrorReported(roofDTO.getErrorReported());
			reportedErrorRoof.setUser(roof.getUser().iterator().next());
			reportedErrorRoof.setRoof(roof);
			
			reportedErrorRoofRepository.save(reportedErrorRoof);
			
			logger.info("REPORTED ERROR IN ROOF" + roofDTO.getIdRoof() + ". Error:" + roofDTO.getErrorReported());
			
		} catch (Exception e) {
			logger.error("ERROR REPORTING ROOF ERROR " + roofDTO.getIdRoof() + " " + e.getMessage());
		}
		
	}
	
	@Override
	public void discardError(RoofDTO roofDTO) {

		try {
			Roof roof = roofRepository.findOne(roofDTO.getIdRoof());
			
			reportedErrorRoofRepository.deleteInBatch(roof.getErrorReported());
			
			logger.info("DISCARDED ERRORS REPORTED FOR ROOF" + roofDTO.getIdRoof());
			
		} catch (Exception e) {
			logger.error("ERROR DISCARDING ERRORS REPORTED FOR ROOF " + roofDTO.getIdRoof() + " " + e.getMessage());
		}
		
	}

	// FIND

	@Override
	public PagedResultsDTO findAllByStatus(RoofStatus status, Pageable pageable) {

		Page<Roof> roofs = roofRepository.findAllByStatus(status.toString(), pageable);
		PagedResultsDTO pagedResultsDTO = buildPagination(pageable, roofs);
		
		return pagedResultsDTO;
	}
	
	@Override
	public PagedResultsDTO findAllByStatus(List<String> status, Pageable pageable) {

		Page<Roof> roofs = roofRepository.findAllByStatusIn(status, pageable);		
		PagedResultsDTO pagedResultsDTO = buildPagination(pageable, roofs);
		
		return pagedResultsDTO;
	}
	
	@Override
	public RoofDTO findRoofById(Long idRoof) {
		
		Roof roof = roofRepository.findOne(idRoof);

		return RoofTransformer.entityToDTO(roof);
	}
	

	@Override
	public PagedResultsDTO findValidatedEnabledAndOwnedRoofs(UserDTO userDTO, Pageable pageable) {

		Page<Roof> validatedRoofList = roofDAO.findValidatedEnabledRoofs(pageable);
		Page<Roof> ownedRoofList = roofDAO.findOwnedIndividualRoofs(userDTO, pageable);

		PagedResultsDTO paginationDTO = buildMergedPagination(pageable, validatedRoofList, ownedRoofList);
		
		return paginationDTO;
	}

	
	@Override
	public PagedResultsDTO findOwnedRoofsValidated(UserDTO userDTO, Pageable pageable) {
	
		Page<Roof> roofs = roofDAO.findOwnedRoofsValidated(userDTO, pageable);
		PagedResultsDTO pagedResultsDTO = buildPagination(pageable, roofs);
		
		return pagedResultsDTO;
	}
	
	@Override
	public PagedResultsDTO findOwnedRoofsRejectedOrPending(UserDTO userDTO, Pageable pageable) {

		Page<Roof> ownedPendingRoofList = roofDAO.findOwnedRoofsPending(userDTO, pageable);
		Page<Roof> ownedRejectedRoofList = roofDAO.findOwnedRoofsRejected(userDTO, pageable);
		
		PagedResultsDTO paginationDTO = buildMergedPagination(pageable, ownedPendingRoofList, ownedRejectedRoofList);
		
		return paginationDTO;
	}
	
	@Override
	public PagedResultsDTO findOwnedRoofsWithErrors(UserDTO userDTO, Pageable pageable) {

		Page<Roof> roofs = roofDAO.findOwnedRoofsWithErrors(userDTO, pageable);
		PagedResultsDTO pagedResultsDTO = buildPagination(pageable, roofs);
		
		return pagedResultsDTO;
	}

	@Override
	public PagedResultsDTO findRoofsWithErrors(Pageable pageable) {

		Page<Roof> roofs = roofRepository.findAllRoofsWithErrors(pageable);
		PagedResultsDTO pagedResultsDTO = buildPagination(pageable, roofs);
		
		return pagedResultsDTO;
	}
	
	// FILTER
	
	@Override
	public PagedResultsDTO filterValidatedAndAllIndividualRoofs(RoofDTO roofDTO, Pageable pageable){

		Page<Roof> validatedRoofList = roofDAO.filterValidatedRoofs(roofDTO, pageable);
		Page<Roof> invididualRoofList = roofDAO.filterAllIndividualRoofs(roofDTO, pageable);
		
		PagedResultsDTO paginationDTO = buildMergedPagination(pageable, validatedRoofList, invididualRoofList);
		
		return paginationDTO;
	}
	
	@Override
	public PagedResultsDTO filterValidatedEnabledAndOwnedRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable){

		Page<Roof> validatedRoofList = roofDAO.filterValidatedEnabledRoofs(roofDTO, pageable);
		Page<Roof> ownedRoofList = roofDAO.filterOwnedIndividualRoofs(roofDTO, userDTO, pageable);
		
		PagedResultsDTO paginationDTO = buildMergedPagination(pageable, validatedRoofList, ownedRoofList);
		
		return paginationDTO;
	}
	
	/* 
	 * Used in Roof recommender
	 */
	@Override
	public List<RoofDTO> filterValidatedEnabledAndOwnedEnabledRoofs(RoofDTO roofDTO, UserDTO userDTO){
		
		List<Roof> mergedRoofList = this.mergeList(roofDAO.filterValidatedEnabledRoofs(roofDTO), 
												   roofDAO.filterOwnedIndividualEnabledRoofs(roofDTO, userDTO));

	    return RoofTransformer.entityToDTO(mergedRoofList);
	}
	
	@Override
	public PagedResultsDTO filterOwnedValidatedRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable){
		
		Page<Roof> roofs = roofDAO.filterOwnedValidatedRoofs(roofDTO, userDTO, pageable);		
		PagedResultsDTO pagedResultsDTO = buildPagination(pageable, roofs);
		
		return pagedResultsDTO;
	}

	@Override
	public void getLinkedResources(List<RoofDTO> roofDTOList) {
		for(RoofDTO roofDTO: roofDTOList) {
			
			this.getLinkedResources(roofDTO);
		}
	}
	
	@Override
	public void getLinkedResources(RoofDTO roofDTO) {
		
		File roofFolder = new File(rootPath + ROOFS_PATH + "/Roof-" + roofDTO.getIdRoof());
		
		if(roofFolder.isDirectory()) {
			List<File> roofFiles = fileFolderService.getFilesForFolder(roofFolder);
			
			fileFolderService.separateFileType(roofFiles, roofDTO);
		}

	}
	
	@Override
	public RoofLimitValuesDTO getRoofLimitValuesAdmin() {

		RoofLimitValuesDTO roofLimitValuesDTO = new RoofLimitValuesDTO();
		
		roofLimitValuesDTO.setMakerList(roofRepository.getAllMakers());
		
		Double maxPrice = roofRepository.getMaxPrice();
		Double minPrice = roofRepository.getMinPrice();
		if(maxPrice != null) {
			roofLimitValuesDTO.setPriceMax((int) Math.ceil(maxPrice));
		}
		if(minPrice != null) {
			roofLimitValuesDTO.setPriceMin((int) Math.ceil(minPrice));
		}

		roofLimitValuesDTO.setWeightMax(roofRepository.getMaxWeight());
		roofLimitValuesDTO.setWeightMin(roofRepository.getMinWeight());
		roofLimitValuesDTO.setWallMax(roofRepository.getMaxWall());
		roofLimitValuesDTO.setWallMin(roofRepository.getMinWall());

		return roofLimitValuesDTO;
	}
	
	@Override
	public RoofLimitValuesDTO getRoofLimitValuesIndividual(UserDTO userDTO) {
		
		List<String> makerList = mergeListAndSort(roofRepository.getAllMakersValidated(), roofDAO.getAllMakerByOwner(userDTO));
		
		Double maxOwnedPrice = roofDAO.getMaxPriceByOwner(userDTO);
		Double maxValidatedPrice = roofRepository.getMaxPriceValidated();
		Integer maxPrice = NumberUtils.getBiggerNumberAndRoundUp(maxOwnedPrice, maxValidatedPrice);
		
		Double minOwnedPrice = roofDAO.getMinPriceByOwner(userDTO);
		Double minValidatedPrice = roofRepository.getMinPriceValidated();
		Integer minPrice = NumberUtils.getLowerNumberAndRoundUp(minOwnedPrice, minValidatedPrice);
		

		Integer maxOwnedWeight = roofDAO.getMaxWeightByOwner(userDTO);
		Integer maxValidatedWeight = roofRepository.getMaxWeightValidated();
		Integer maxWeight = NumberUtils.getBiggerNumber(maxOwnedWeight, maxValidatedWeight);
		
		Integer minOwnedWeight = roofDAO.getMinWeightByOwner(userDTO);
		Integer minValidatedWeight = roofRepository.getMinWeightValidated();
		Integer minWeight = NumberUtils.getLowerNumber(minOwnedWeight, minValidatedWeight);
		
		
		Integer maxOwnedWall = roofDAO.getMaxWallByOwner(userDTO);
		Integer maxValidatedWall = roofRepository.getMaxWallValidated();
		Integer maxWall = NumberUtils.getBiggerNumber(maxOwnedWall, maxValidatedWall);
		
		Integer minOwnedWall = roofDAO.getMinWallByOwner(userDTO);
		Integer minValidatedWall = roofRepository.getMinWallValidated();
		Integer minWall = NumberUtils.getLowerNumber(minOwnedWall, minValidatedWall);
		

		RoofLimitValuesDTO roofLimitValuesDTO = new RoofLimitValuesDTO();
		
		roofLimitValuesDTO.setMakerList(makerList);
		roofLimitValuesDTO.setPriceMax(maxPrice);
		roofLimitValuesDTO.setPriceMin(minPrice);
		roofLimitValuesDTO.setWeightMax(maxWeight);
		roofLimitValuesDTO.setWeightMin(minWeight);
		roofLimitValuesDTO.setWallMax(maxWall);
		roofLimitValuesDTO.setWallMin(minWall);

		return roofLimitValuesDTO;
	}
	
	@Override
	public RoofLimitValuesDTO getRoofLimitValuesSupplier(UserDTO userDTO) {

		RoofLimitValuesDTO roofLimitValuesDTO = new RoofLimitValuesDTO();
		
		roofLimitValuesDTO.setMakerList(roofDAO.getAllMakerByOwner(userDTO));
		
		Double maxPrice = roofDAO.getMaxPriceValidatedByOwner(userDTO);
		Double minPrice = roofDAO.getMinPriceValidatedByOwner(userDTO);
		if(maxPrice != null) {
			roofLimitValuesDTO.setPriceMax((int) Math.ceil(maxPrice));
		}
		if(minPrice != null) {
			roofLimitValuesDTO.setPriceMin((int) Math.ceil(minPrice));
		}

		roofLimitValuesDTO.setWeightMax(roofDAO.getMaxWeightValidatedByOwner(userDTO));
		roofLimitValuesDTO.setWeightMin(roofDAO.getMinWeightValidatedByOwner(userDTO));
		roofLimitValuesDTO.setWallMax(roofDAO.getMaxWallValidatedByOwner(userDTO));
		roofLimitValuesDTO.setWallMin(roofDAO.getMinWallValidatedByOwner(userDTO));

		return roofLimitValuesDTO;
	}
	
	//STATUS
	
	@Override
	public List<String> getAdminAvailableStatus() {
		
		List<String> roofStatus = new ArrayList<>();
		roofStatus.add(RoofStatus.VALIDATED.getCode());
		roofStatus.add(RoofStatus.INDIVIDUAL.getCode());
		
		return roofStatus;
	}
	
	@Override
	public List<String> getSupplierAvailableStatus() {
		
		List<String> roofStatus = new ArrayList<>();
		roofStatus.add(RoofStatus.PENDING.getCode());
		roofStatus.add(RoofStatus.REJECTED.getCode());
		
		return roofStatus;
	}
	
	@Override
	public Boolean checkIfModelExist(Long idUser, String model) {
		
		Integer results = roofRepository.findByModelOwned(idUser, model.toLowerCase());
		
		return results == 0? false: true;
	}
	
	
	private PagedResultsDTO buildMergedPagination(Pageable pageable, Page<Roof> valList, Page<Roof> ownList) {

		List<Roof> list = new ArrayList<>();
		list.addAll(valList.getContent());
		list.addAll(ownList.getContent());
		
		Long totalElements = valList.getTotalElements() + ownList.getTotalElements();
	
		Integer pages = valList.getTotalElements() >= ownList.getTotalElements()
				? valList.getTotalPages()
				: ownList.getTotalPages();
		
		PagedResultsDTO paginationDTO = new PagedResultsDTO();
		paginationDTO.setElements(RoofTransformer.entityToDTO(list));
		paginationDTO.setTotalElements(totalElements);
		paginationDTO.setTotalValElements(valList.getTotalElements());
		paginationDTO.setTotalOwnElements(ownList.getTotalElements());
		paginationDTO.setTotalPages(pages);
		paginationDTO.setNumber(pageable.getPageNumber());
		paginationDTO.setSize(list.size());
		paginationDTO.setValSize(valList.getContent().size());
		paginationDTO.setOwnSize(ownList.getContent().size());
		

		return paginationDTO;
	}
	
	private PagedResultsDTO buildPagination(Pageable pageable, Page<Roof> list) {
		
		PagedResultsDTO paginationDTO = new PagedResultsDTO();
		paginationDTO.setElements(RoofTransformer.entityToDTO(list.getContent()));
		paginationDTO.setTotalElements(list.getTotalElements());
		paginationDTO.setTotalPages(list.getTotalPages());
		paginationDTO.setNumber(pageable.getPageNumber());
		paginationDTO.setSize(list.getContent().size());

		return paginationDTO;
	}
		
	private List<String> mergeListAndSort(List<String> list1, List<String> list2){
		
		List<String> mergedList = (List<String>) mergeList(list1, list2);
		
		//Set in a HashSet to remove duplicates
		Set<String> mergedSet = new HashSet<>(mergedList);
		mergedList.clear();
		mergedList.addAll(mergedSet);

		Collections.sort(mergedList);
		
		return mergedList;	
	}
	
	private <T> List<T> mergeList(List<T> list1, List<T> list2) {
		
		List<T> mergedList = new ArrayList<>();
		
		mergedList.addAll(list1);
		mergedList.addAll(list2);
		
		
		return mergedList;
	}

	
}
