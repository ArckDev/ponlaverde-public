package com.upc.arcktech.ponlaverde.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.UserRole;
import com.upc.arcktech.ponlaverde.dto.StatAverageRatedValuesDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;
import com.upc.arcktech.ponlaverde.dto.StatListAndTotalDTO;
import com.upc.arcktech.ponlaverde.dto.StatMaxMinDTO;
import com.upc.arcktech.ponlaverde.dto.StatUsedFeatureDTO;
import com.upc.arcktech.ponlaverde.dto.StatYearMonthDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoof;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoofDetails;
import com.upc.arcktech.ponlaverde.repository.AccessRepository;
import com.upc.arcktech.ponlaverde.repository.ConnectionRepository;
import com.upc.arcktech.ponlaverde.repository.RoofRepository;
import com.upc.arcktech.ponlaverde.repository.UsedFeatureRepository;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.repository.UserSavedRoofRepository;
import com.upc.arcktech.ponlaverde.service.DashboardService;
import com.upc.arcktech.ponlaverde.utils.MapsTransformUtils;


/**
 * @author jose.rodriguez
 *
 */
@Service
@Transactional
public class DashboardServiceImpl implements DashboardService {
	
	private static final String MONTH_MAPPING = "list/month-map";
	private static final String MESSAGES = "i18n/messages";
	private static final String ROOF_STATUS = "list/roofStatus";
	private static final String ROOF_SLOPE = "list/roofSlope";
	private static final String CLIMATE = "list/climate";
	private static final String COUNTRIES = "list/countries";
	
	@Autowired
	SessionRegistry sessionRegistry;
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserSavedRoofRepository userSavedRoofRepository;
	@Autowired
	ConnectionRepository connectionRepository;
	@Autowired
	AccessRepository accessRepository;
	@Autowired
	RoofRepository roofRepository;
	@Autowired
	UsedFeatureRepository usedFeatureRepository;

	
	// ADMIN

	/*
	 * Incremental number of individual users registered in the system group by month.
	 */
	@Override
	public StatListAndTotalDTO<StatCountByYearMonthDTO> individualUserRegistered() {
		
		List<StatCountByYearMonthDTO> list = userRepository
				.countUsersRegisteredByUsertypeGroupByYearAndMonth(UserRole.INDIVIDUAL.getCode());
		
		StatListAndTotalDTO<StatCountByYearMonthDTO> response = new StatListAndTotalDTO<>();
		response.setList(buildIncrementalListWithMonthMapping(list));
		response.setTotal(userRepository.getCountIndividualUser());

		return response;
	}
	
	/*
	 * Incremental number of supplier users registered in the system group by month.
	 */
	@Override
	public StatListAndTotalDTO<StatCountByYearMonthDTO> supplierUserRegistered() {

		List<StatCountByYearMonthDTO> list = userRepository
				.countUsersRegisteredByUsertypeGroupByYearAndMonth(UserRole.SUPPLIER.getCode());	
		
		StatListAndTotalDTO<StatCountByYearMonthDTO> response = new StatListAndTotalDTO<>();
		response.setList(buildIncrementalListWithMonthMapping(list));
		response.setTotal(userRepository.getCountSupplierUser());

		return response;
	}
	
	/*
	 * Incremental number of connections to the system group by month.
	 */
	@Override
	public StatListAndTotalDTO<StatCountByYearMonthDTO> connectionsToSystem() {
		
		List<StatCountByYearMonthDTO> list = connectionRepository.countConnectionsGroupByYearAndMonth();
		
		StatListAndTotalDTO<StatCountByYearMonthDTO> response = new StatListAndTotalDTO<>();
		response.setList(buildIncrementalListWithMonthMapping(list));
		response.setTotal(connectionRepository.getCountConnections());
		
		return response;
	}
		
	/*
	 * Incremental number of logins to the system group by month.
	 */
	@Override
	public StatListAndTotalDTO<StatCountByYearMonthDTO> loginsToSystem() {
		
		List<StatCountByYearMonthDTO> list = accessRepository.countAccessGroupByYearAndMonth();
		
		StatListAndTotalDTO<StatCountByYearMonthDTO> response = new StatListAndTotalDTO<>();
		response.setList(buildIncrementalListWithMonthMapping(list));
		response.setTotal(accessRepository.getCountAccess());

		return response;
	}

	/*
	 * Real time connected users.
	 */
	@Override
	public List<StatCountByFieldDTO> currentConnectedUsers() {
		
		ResourceBundle messageBundle = getResourceBundle(MESSAGES);

		List<StatCountByFieldDTO> usersConnectedList = new ArrayList<>();
		
		List<Object> principals = sessionRegistry.getAllPrincipals()
				.stream()
				.filter(u -> !sessionRegistry.getAllSessions(u, false).isEmpty())
				.map(Object::toString)
				.collect(Collectors.toList());
		
		StatCountByFieldDTO usersConnected = new StatCountByFieldDTO();
		usersConnected.setCount((long) principals.size());
		usersConnected.setField(messageBundle.getString("dash.connected"));
		usersConnectedList.add(usersConnected);
		
		StatCountByFieldDTO usersTotal = new StatCountByFieldDTO();
		usersTotal.setCount(userRepository.count());
		usersTotal.setField(messageBundle.getString("dash.totals"));
		usersConnectedList.add(usersTotal);

		return usersConnectedList;
	}

	/*
	 * Roofs most commonly chosen by the users in recommender feature. Top 5.
	 */
	@Override
	public List<StatCountByMakerModelDTO> mostPopularChosenRoofsInRecommender() {
		
		List<StatCountByMakerModelDTO> chosenRoofs = userSavedRoofRepository
				.countChoosenRoofsInRecommender(new PageRequest(0, 10));
		
		return chosenRoofs;
	}
	
	/*
	 * Roofs most commonly suggested in recommender feature. Top 5.
	 */
	@Override
	public List<StatCountByMakerModelDTO> mostSuggestedRoofsInRecommender() {
		
		List<StatCountByMakerModelDTO> suggestedRoofs = userSavedRoofRepository
				.countSuggestedRoofsInRecommender(new PageRequest(0, 10));
		
		return suggestedRoofs;
	}

	/*
	 * How many times the higher rated roof is chosen.
	 */
	@Override
	public Integer successRateInRecommender() {
		
		List<UserSavedRoof> userSavedRoofList = userSavedRoofRepository.findAll();
		Integer totalProcesses = userSavedRoofList.size();
		
		Integer successedProcesses = 0;
		for(UserSavedRoof userSavedRoof: userSavedRoofList) {
			
			// Find the higher score of every saved instance
			Integer higherScore = 0;
			for(UserSavedRoofDetails userSavedRoofDetails: userSavedRoof.getUserSavedRoofDetails()) {		
				if(userSavedRoofDetails.getFinalRate() > higherScore) {
					higherScore = userSavedRoofDetails.getFinalRate();
				}
			}
			// Check if the higher scored is the chosen
			for(UserSavedRoofDetails userSavedRoofDetails: userSavedRoof.getUserSavedRoofDetails()) {			
				if(userSavedRoofDetails.getSelected() && userSavedRoofDetails.getFinalRate() == higherScore) {
					successedProcesses++;
				}
			}				
		}
		
		Integer successRate = (successedProcesses * 100)/ totalProcesses;		
		
		return successRate;
	}

	/*
	 * The most common type of devices with connections to the system.
	 */
	@Override
	public List<StatCountByFieldDTO> accessDeviceType() {
		
		List<StatCountByFieldDTO> typeDevice = accessRepository.countAccessGroupByDeviceType();
		
		return typeDevice;
	}
	
	/*
	 * The most common OS of the devices with connections to the system.
	 */
	@Override
	public List<StatCountByFieldDTO> accessDeviceOS() {
		
		List<StatCountByFieldDTO> osDevice = accessRepository.countAccessGroupByOS();
		
		return osDevice;
	}

	/*
	 * The most common cities with connections to the system.
	 */
	@Override
	public List<StatCountByCityCountryDTO> commonCitiesConnection() {
		
		List<StatCountByCityCountryDTO> connectionsFromCities = connectionRepository
				.countConnectionsGroupByCity(null);
		
		return connectionsFromCities;
	}
	
	/*
	 * The most countries with connections to the system.
	 */
	@Override
	public List<StatCountByFieldDTO> commonCountriesConnection() {
		
		ResourceBundle messageBundle = getResourceBundle(COUNTRIES);
		
		List<StatCountByFieldDTO> list = connectionRepository
				.countConnectionsGroupByCountry(new PageRequest(0, 10));
		
		for(StatCountByFieldDTO statCountByFieldDTO: list) {
			statCountByFieldDTO.setField(messageBundle.getString(statCountByFieldDTO.getField()));
		}
				
		return list;	
	}

	/*
	 * Number of users by city and country.
	 */
	@Override
	public List<StatCountByCityCountryDTO> usersByCityAndCountry() {
		
		List<StatCountByCityCountryDTO> citiesOfUsers = userRepository
				.countUsersRegisteredByUsertypeGroupByCityAndCountry(new PageRequest(0, 10));
		
		return citiesOfUsers;
	}

	/*
	 * Use of features group by month.
	 */
	@Override
	public StatUsedFeatureDTO useOfFeatures() {

		//GET DATA FROM DB
		List<StatCountByYearMonthDTO> catDataList = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(FeatureName.CATALOGUE.getCode());
		List<StatCountByYearMonthDTO> dasDataList = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(FeatureName.DASHBOARD.getCode());
		List<StatCountByYearMonthDTO> errDataList = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(FeatureName.ERROR.getCode());
		List<StatCountByYearMonthDTO> recDataList = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(FeatureName.RECOMMENDER.getCode());
		List<StatCountByYearMonthDTO> savDataList = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(FeatureName.RECOMMENDER_SAVE.getCode());
		List<StatCountByYearMonthDTO> setDataList = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(FeatureName.SETTINGS.getCode());
		List<StatCountByYearMonthDTO> valDataList = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(FeatureName.VALIDATION.getCode());

		//FIND LIMIT VALUES YEAR AND MONTH
		StatMaxMinDTO years = usedFeatureRepository.maxAndMinYear();
		Integer minMonth = usedFeatureRepository.minMonthForYear(years.getMin());
		Integer maxMonth = usedFeatureRepository.maxMonthForYear(years.getMax());
		
		//BUILD YEAR-MONTH RANGE
		List<StatYearMonthDTO> yearMonthRange = buildYearMonthRange(years, minMonth, maxMonth);
		
		//COMPARE EVERY LIST WITH THE FULL YEAR-MONTH RANGE AND SET 0 IF A MONTH IS NULL
		List<StatCountByYearMonthDTO> catOutList = fillMonthWithoutValues(catDataList, yearMonthRange);
		List<StatCountByYearMonthDTO> dasOutList = fillMonthWithoutValues(dasDataList, yearMonthRange);
		List<StatCountByYearMonthDTO> errOutList = fillMonthWithoutValues(errDataList, yearMonthRange);
		List<StatCountByYearMonthDTO> recOutList = fillMonthWithoutValues(recDataList, yearMonthRange);
		List<StatCountByYearMonthDTO> savOutList = fillMonthWithoutValues(savDataList, yearMonthRange);
		List<StatCountByYearMonthDTO> setOutList = fillMonthWithoutValues(setDataList, yearMonthRange);
		List<StatCountByYearMonthDTO> valOutList = fillMonthWithoutValues(valDataList, yearMonthRange);

		//CREATE RESPONSE AND MAP MONTH NAME
		StatUsedFeatureDTO response = new StatUsedFeatureDTO();
		response.setCat(mapMonthName(catOutList));
		response.setDas(mapMonthName(dasOutList));
		response.setErr(mapMonthName(errOutList));
		response.setRec(mapMonthName(recOutList));
		response.setSav(mapMonthName(savOutList));
		response.setSet(mapMonthName(setOutList));
		response.setVal(mapMonthName(valOutList));
		
		return response;
	}

	/*
	 * The average score of variables in recommender feature.
	 */
	@Override
	public List<StatCountByFieldDTO> averageScoreVariablesInRecommender() {
		
		ResourceBundle messageBundle = getResourceBundle(MESSAGES);
		
		StatAverageRatedValuesDTO valuesVars = userSavedRoofRepository.averageRatedVariablesInRecommender();
		SortedMap<String, Double> map = MapsTransformUtils.convertObjectToDoubleSortedMap(valuesVars);

		// Mapping locale translation
		SortedMap<String, Double> mappedMap = new TreeMap<String, Double>();
		for (Map.Entry<String, Double> entry : map.entrySet()){
			mappedMap.put(messageBundle.getString("cat." + entry.getKey()), entry.getValue());
		}

		// Order values desc
		Map<String, Double> valuesDescSortedMap = MapsTransformUtils.sortByValueDoubleDescWithLimit(mappedMap, null);

		List<StatCountByFieldDTO> list = valuesDescSortedMap.entrySet().stream()
				.map(e -> new StatCountByFieldDTO(e.getKey(), e.getValue())).collect(Collectors.toList());
		

		return list;
	}
	
	/*
	 * Number of roofs created in the system and group by status.
	 */
	@Override
	public StatListAndTotalDTO<StatCountByFieldDTO> systemTotalRoofsByStatus() {
		
		ResourceBundle messageBundle = getResourceBundle(ROOF_STATUS);
		
		List<StatCountByFieldDTO> list = roofRepository.countRoofsGroupByStatus();
		
		for(StatCountByFieldDTO statCountByFieldDTO: list) {
			statCountByFieldDTO.setField(messageBundle.getString("STATUS."+statCountByFieldDTO.getField()));
		}

		StatListAndTotalDTO<StatCountByFieldDTO> response = new StatListAndTotalDTO<>();
		response.setList(list);
		response.setTotal((int) roofRepository.count());
				
		return response;
	}

	/*
	 * Slope of the roofs selected in recommender feature.
	 */
	@Override
	public List<StatCountByFieldDTO> roofSlopeSelectedInRecommender() {
		
		ResourceBundle messageBundle = getResourceBundle(ROOF_SLOPE);
		
		List<StatCountByFieldDTO> list = userSavedRoofRepository.countSlopeSelectedInRecommender();
		
		for(StatCountByFieldDTO statCountByFieldDTO: list) {
			statCountByFieldDTO.setField(messageBundle.getString(statCountByFieldDTO.getField()));
		}
		
		return list;
	}

	/*
	 * Climate of the roofs selected in recommender feature.
	 */
	@Override
	public List<StatCountByFieldDTO> roofClimateSelectedInRecommender() {
		
		ResourceBundle messageBundle = getResourceBundle(CLIMATE);
		
		List<StatCountByFieldDTO> list = userSavedRoofRepository.countClimatesSelectedInRecommender();
		
		for(StatCountByFieldDTO statCountByFieldDTO: list) {
			statCountByFieldDTO.setField(messageBundle.getString(statCountByFieldDTO.getField()));
		}
		
		return list;
	}

	// SUPPLIER
	
	@Override
	public List<StatCountByMakerModelDTO> supplierMostChosenRoofInRecommender(UserDTO userDTO) {
		
		List<StatCountByMakerModelDTO> chosenRoofs = userSavedRoofRepository
				.countSupplierSelectedRoofsGroupByModelInRecommender(userDTO.getIdUser(), new PageRequest(0, 10));
		
		return chosenRoofs;
	}

	@Override
	public List<StatCountByMakerModelDTO> supplierMostSuggestedRoofsInRecommender(UserDTO userDTO) {
		
		List<StatCountByMakerModelDTO> suggestedRoofs = userSavedRoofRepository
				.countSupplierRoofsGroupByModelInRecommender(userDTO.getIdUser(), new PageRequest(0, 10));
		
		return suggestedRoofs;
	}

	@Override
	public List<StatCountByCityCountryDTO> supplierCommonUserCitiesInRecommender(UserDTO userDTO) {
		
		List<StatCountByCityCountryDTO> userCities = userRepository
				.countCityOfUserChosenRoofSupplierInRecommender(userDTO.getIdUser(), new PageRequest(0, 10));
		
		return userCities;
	}

	@Override
	public StatListAndTotalDTO<StatCountByFieldDTO> supplierTotalRoofsByStatus(UserDTO userDTO) {
		
		ResourceBundle messageBundle = getResourceBundle(ROOF_STATUS);
		
		List<StatCountByFieldDTO> list = roofRepository.countOwnedRoofsGroupByStatus(userDTO.getIdUser());
		
		for(StatCountByFieldDTO statCountByFieldDTO: list) {
			statCountByFieldDTO.setField(messageBundle.getString("STATUS."+statCountByFieldDTO.getField()));
		}

		StatListAndTotalDTO<StatCountByFieldDTO> response = new StatListAndTotalDTO<>();
		response.setList(list);
		response.setTotal(roofRepository.countOwnedRoofs(userDTO.getIdUser()));
		
		
		return response;
	}
	
	// PRIVATE

	/*
	 * It gets a resource by name
	 */
	private ResourceBundle getResourceBundle(String resource) {
		
		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle(resource, locale);
		
		return messageBundle;
	}

	/*
	 * It builds a list with incremental values from a list with values grouped by months
	 */
	private List<StatCountByYearMonthDTO> buildIncrementalListWithMonthMapping(List<StatCountByYearMonthDTO> list) {
		
		ResourceBundle messageBundle = getResourceBundle(MONTH_MAPPING);
		List<StatCountByYearMonthDTO> incrementalList = new ArrayList<>();
		
		Long currentValue = 0L;
		for(StatCountByYearMonthDTO dto: list) {
			
			StatCountByYearMonthDTO incrementalDTO = new StatCountByYearMonthDTO();
			currentValue = currentValue + dto.getCount();
			incrementalDTO.setCount(currentValue);
			incrementalDTO.setMonth(dto.getMonth());
			incrementalDTO.setMonthName(messageBundle.getString(dto.getMonth().toString()));
			incrementalDTO.setYear(dto.getYear());
			
			incrementalList.add(incrementalDTO);
		}
		return incrementalList;
	}

	/*
	 * It maps month number into locale month name
	 */
	private List<StatCountByYearMonthDTO> mapMonthName(List<StatCountByYearMonthDTO> list) {
		ResourceBundle messageBundle = getResourceBundle(MONTH_MAPPING);
			for(StatCountByYearMonthDTO dto: list) {
				dto.setMonthName(messageBundle.getString(dto.getMonth().toString()));
			}
		return list;
	}
	
	/*
	 * It fills a list with the values provided for every month and 0 if there's no value
	 */
	private List<StatCountByYearMonthDTO> fillMonthWithoutValues(List<StatCountByYearMonthDTO> dataList,
			List<StatYearMonthDTO> yearMonthRange) {
		
		List<StatCountByYearMonthDTO> outList = new ArrayList<>();

		for (StatYearMonthDTO yearMonthDTO : yearMonthRange) {
			outList.add(new StatCountByYearMonthDTO(0L, yearMonthDTO.getYear(), yearMonthDTO.getMonth()));
		}

		for(StatCountByYearMonthDTO outDTO: outList) {
			for(StatCountByYearMonthDTO dataDTO: dataList) {
				if(outDTO.getYear().equals(dataDTO.getYear()) && 
						outDTO.getMonth().equals(dataDTO.getMonth())) {
					outDTO.setCount(dataDTO.getCount());
				} 
			}			
		}
		
		return outList;
	}

	/*
	 * It builds the full range of month between a max and min year and max and min month
	 */
	private List<StatYearMonthDTO> buildYearMonthRange(StatMaxMinDTO years, Integer minMonth, Integer maxMonth) {
		
		//BUILD YEARS RANGE
		List<Integer> yearsRange = new ArrayList<>();
		for(int i = years.getMin(); i <= years.getMax(); i++) {
			yearsRange.add(i);
		}

		//BUILD YEAR-MONTH RANGE
		List<StatYearMonthDTO> yearMonthRange = new ArrayList<>();	
		for(int i = 0; i < yearsRange.size(); i++) {

			//first year
			if(yearsRange.get(i).equals(years.getMin())) {
				for(int j = minMonth; j <= 12; j++) {
					yearMonthRange.add(new StatYearMonthDTO(yearsRange.get(i), j));
				}
			//middle years
			} else if(yearsRange.get(i) > years.getMin() && yearsRange.get(i) < years.getMax()) {
				for(int j = 1; j <= 12; j++) {
					yearMonthRange.add(new StatYearMonthDTO(yearsRange.get(i), j));
				}
			//last year
			} else {
				for(int j = 1; j <= maxMonth; j++) {
					yearMonthRange.add(new StatYearMonthDTO(yearsRange.get(i), j));
				}
			}
			
		}
		return yearMonthRange;
	}

	
}
