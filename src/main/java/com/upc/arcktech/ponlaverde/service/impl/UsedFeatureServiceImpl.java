package com.upc.arcktech.ponlaverde.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.UsedFeatureDTO;
import com.upc.arcktech.ponlaverde.repository.UsedFeatureRepository;
import com.upc.arcktech.ponlaverde.service.UsedFeatureService;
import com.upc.arcktech.ponlaverde.transformer.UsedFeatureTransformer;

@Service
public class UsedFeatureServiceImpl implements UsedFeatureService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UsedFeatureRepository usedFeatureRepository;

	@Override
	public void addAccess(UsedFeatureDTO usedFeatureDTO) {
		
		usedFeatureRepository.save(UsedFeatureTransformer.DTOtoEntity(usedFeatureDTO));
		
		logger.info("STORED FEATURE ACCESS: " + usedFeatureDTO.toString());
	}

}
