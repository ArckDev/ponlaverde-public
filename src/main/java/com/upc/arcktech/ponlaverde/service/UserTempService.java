package com.upc.arcktech.ponlaverde.service;

import com.upc.arcktech.ponlaverde.dto.UserTempDTO;


public interface UserTempService {
	
	Boolean addTempUser(UserTempDTO userTempDTO);
	
	UserTempDTO verifyAccount (UserTempDTO userTempDTO);

	void removeTempUser(UserTempDTO userTempDTO);

}
