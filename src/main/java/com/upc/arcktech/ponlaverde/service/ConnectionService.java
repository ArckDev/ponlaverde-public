package com.upc.arcktech.ponlaverde.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;



public interface ConnectionService {
	
	void addConnection(HttpServletRequest request) throws IOException;

}
