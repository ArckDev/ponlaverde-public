package com.upc.arcktech.ponlaverde.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.WelcomeMetricsDTO;
import com.upc.arcktech.ponlaverde.repository.ConnectionRepository;
import com.upc.arcktech.ponlaverde.repository.RoofRepository;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.service.WelcomeMetricsService;

@Service
public class WelcomeMetricsServiceImpl implements WelcomeMetricsService {
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	RoofRepository roofRepository;
	@Autowired
	ConnectionRepository connectionRepository;

	@Override
	public WelcomeMetricsDTO getWelcomeMetrics() {
		
		WelcomeMetricsDTO welcomeMetricsDTO  = new WelcomeMetricsDTO();
		
		welcomeMetricsDTO.setCatalogue(roofRepository.countRoofs());
		welcomeMetricsDTO.setSuppliers(userRepository.getCountSupplierUser());
		welcomeMetricsDTO.setUsers(userRepository.getCountIndividualUser());
		welcomeMetricsDTO.setVisits(connectionRepository.getCountConnections());
		
		return welcomeMetricsDTO;
	}

}
