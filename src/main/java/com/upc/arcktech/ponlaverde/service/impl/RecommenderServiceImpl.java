package com.upc.arcktech.ponlaverde.service.impl;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.upc.arcktech.ponlaverde.dto.BestRoofDTO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.RoofLimitValuesDTO;
import com.upc.arcktech.ponlaverde.dto.RoofRatingDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.dto.UserRoofRatingDTO;
import com.upc.arcktech.ponlaverde.dto.UserSavedRoofDTO;
import com.upc.arcktech.ponlaverde.entity.User;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoof;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoofDetails;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.repository.UserSavedRoofRepository;
import com.upc.arcktech.ponlaverde.service.RecommenderService;
import com.upc.arcktech.ponlaverde.service.RoofService;
import com.upc.arcktech.ponlaverde.transformer.RoofRatingTransformer;
import com.upc.arcktech.ponlaverde.transformer.UserRoofRatingTransformer;
import com.upc.arcktech.ponlaverde.transformer.UserSavedRoofDetailsTransformer;
import com.upc.arcktech.ponlaverde.transformer.UserSavedRoofTransformer;
import com.upc.arcktech.ponlaverde.utils.MapsTransformUtils;

@Service
@Transactional
public class RecommenderServiceImpl implements RecommenderService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	RoofService roofService;
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserSavedRoofRepository userSavedRoofRepository;


	@Override
	public List<BestRoofDTO> recommenderProcess(UserDTO userDTO, UserRoofRatingDTO userRoofRatingDTO){

		// GET THE ROOF LIST FILTERED BY CLIMATE AND SLOPE CRITERIA
		RoofDTO roofDTO = UserRoofRatingTransformer.userRoofRatingToRoof(userRoofRatingDTO);
		List<RoofDTO> roofFilteredList = roofService.filterValidatedEnabledAndOwnedEnabledRoofs(roofDTO, userDTO);
		
		// MAPPING PROCESS: IT CONVERTS ALL THE FIELD INTO NUMERICAL VALUES
		List<RoofRatingDTO> roofMappedList = this.mappingProcess(roofFilteredList);
		
		// CALCULATE FINAL WEIGHING
		SortedMap<String, Double> finalWeighingMap = this.calculateFinalWeighing(userRoofRatingDTO);

		// RATING PROCESS (finalWeighing x roofRatingDTO)
		Integer results = Integer.parseInt(userRoofRatingDTO.getResults());
		List<BestRoofDTO> bestRoofs = this.getRoofsRated(roofFilteredList, roofMappedList, finalWeighingMap, results);

		logger.info("PERFORMED RECOMMENDATION FOR USER: " + userDTO.toString());
		
		return bestRoofs;
	}

	@Override
	public User saveRecommendation(List<BestRoofDTO> bestRoofs, UserSavedRoofDTO userSavedRoofDTO, UserDTO userDTO) {

		List<UserSavedRoofDetails> userSavedRoofDetailsList = UserSavedRoofDetailsTransformer.bestRoofDTOtoUserSavedRoofDetails(bestRoofs);
		
		for(UserSavedRoofDetails userSavedRoofDetails: userSavedRoofDetailsList) {
			
			if(userSavedRoofDetails.getRoof().getIdRoof().equals(userSavedRoofDTO.getSelected())) {
				userSavedRoofDetails.setSelected(true);
			}
			
		}
		
		UserSavedRoof userSavedRoof = UserSavedRoofTransformer.DTOtoEntity(userSavedRoofDTO);
		userSavedRoof.setUserSavedRoofDetails(userSavedRoofDetailsList);
		userSavedRoof.setSavedDate(new Date());
		
		User user = userRepository.findOne(userDTO.getIdUser());
		user.getUserSavedRoof().add(userSavedRoof);
		
		User userSaved = userRepository.save(user);
		
		logger.info("SAVED RECOMMENDATION: " + userSavedRoofDTO.toString());
		
		return userSaved;
	}
	
	@Override
	public void deleteRecommendation(UserSavedRoofDTO userSavedRoofDTO, Long userId) {

		try {
		
			User user = userRepository.getOne(userId);
			
			List<UserSavedRoof> userSavedRoofsList = user.getUserSavedRoof();
			
			for (int i = 0; i < userSavedRoofsList.size(); i++) {
				if(userSavedRoofsList.get(i).getIdUserSavedRoof().equals(userSavedRoofDTO.getIdUserSavedRoof())) {
					userSavedRoofsList.remove(i);
				}
			}
			
			userRepository.saveAndFlush(user);
			
			logger.info("DELETED RECOMMENDATION " + userSavedRoofDTO.getIdUserSavedRoof());
			
		} catch (Exception e) {
			logger.error("ERROR DELETING RECOMMENDATION " + userSavedRoofDTO.getIdUserSavedRoof() + " " + e.getMessage());
		}
	}

	
	/* 
	 * Method created in order to test the final weighing map
	 */
	@Override
	public Double checkValue(UserRoofRatingDTO userRoofRatingDTO) {
		
		SortedMap<String, Double> finalWeighingMap = calculateFinalWeighing(userRoofRatingDTO);
		Double checkValue = (finalWeighingMap.values().stream().mapToDouble(Double::doubleValue).sum());
		
		return checkValue;
	}
	
	
	/* ################################################################################################################## */
	/* ###############################################       PRIVATE      ############################################### */
	/* ################################################################################################################## */

	
	/**
	 * @param roofFilteredList
	 * @param roofLimitValuesDTO
	 * @return
	 */
	private List<RoofRatingDTO> mappingProcess(List<RoofDTO> roofFilteredList) {
		
		List<RoofRatingDTO> roofToRateList = new ArrayList<>();
		
		// Get max and min values of: price, weight and wall
		RoofLimitValuesDTO roofLimitValuesDTO = getLimitValuesRoofsRetrieved(roofFilteredList);		

		
		for(RoofDTO roofFiltered: roofFilteredList) {

			// Mapping non-numerical values into numerical.
			RoofRatingDTO roofRatingDTO = RoofRatingTransformer.roofToRoofRatingMapping(roofFiltered);
		
			// Lineal interpolation of price, weight and wall
			this.getInterpolatedValues(roofLimitValuesDTO, roofFiltered, roofRatingDTO);

			roofToRateList.add(roofRatingDTO);
		}
		
		return roofToRateList;
	}
	
	/**
	 * Get the max and the min value for: price, wall and weight
	 * 
	 * @param roofFilteredList
	 * @return
	 */
	private RoofLimitValuesDTO getLimitValuesRoofsRetrieved(List<RoofDTO> roofFilteredList) {
		
		final Integer MIN_VALUE = 10000000;
		final Integer MAX_VALUE = 0;
		
		final Double MIN_VALUE_D = Double.valueOf(MIN_VALUE);
		final Double MAX_VALUE_D = Double.valueOf(MAX_VALUE);
		
		RoofLimitValuesDTO roofLimitValuesDTO = initRoofLimitValuesDTO(MIN_VALUE, MAX_VALUE, MIN_VALUE_D, MAX_VALUE_D);	
		
		for(RoofDTO roofFiltered: roofFilteredList) {
			
			if(roofFiltered.getPrice() > roofLimitValuesDTO.getPriceMaxD()) {
				roofLimitValuesDTO.setPriceMaxD(roofFiltered.getPrice());
			}
			if(roofFiltered.getPrice() < roofLimitValuesDTO.getPriceMinD()) {
				roofLimitValuesDTO.setPriceMinD(roofFiltered.getPrice());
			}
			
			if(roofFiltered.getWeight() > roofLimitValuesDTO.getWeightMax()) {
				roofLimitValuesDTO.setWeightMax(roofFiltered.getWeight());
			}
			if(roofFiltered.getWeight() < roofLimitValuesDTO.getWeightMin()) {
				roofLimitValuesDTO.setWeightMin(roofFiltered.getWeight());
			}
			
			if(roofFiltered.getWall() > roofLimitValuesDTO.getWallMax()) {
				roofLimitValuesDTO.setWallMax(roofFiltered.getWall());
			}
			if(roofFiltered.getWall() < roofLimitValuesDTO.getWallMin()) {
				roofLimitValuesDTO.setWallMin(roofFiltered.getWall());
			}
		}
		
		return roofLimitValuesDTO;
	}

	/**
	 * Set RoofLimitValuesDTO with limit values to compare later
	 * 
	 * @param MIN_VALUE
	 * @param MAX_VALUE
	 * @param MIN_VALUE_D
	 * @param MAX_VALUE_D
	 * @return
	 */
	private RoofLimitValuesDTO initRoofLimitValuesDTO(final Integer MIN_VALUE, final Integer MAX_VALUE,
			final Double MIN_VALUE_D, final Double MAX_VALUE_D) {
		
		RoofLimitValuesDTO roofLimitValuesDTO = new RoofLimitValuesDTO();

		// Set with dummy data to start
		roofLimitValuesDTO.setPriceMaxD(MAX_VALUE_D);
		roofLimitValuesDTO.setPriceMinD(MIN_VALUE_D);
		roofLimitValuesDTO.setWallMax(MAX_VALUE);
		roofLimitValuesDTO.setWallMin(MIN_VALUE);
		roofLimitValuesDTO.setWeightMax(MAX_VALUE);
		roofLimitValuesDTO.setWeightMin(MIN_VALUE);
		
		return roofLimitValuesDTO;
	}
	
	/**
	 * @param roofLimitValuesDTO
	 * @param roofFiltered
	 * @param roofRatingDTO
	 */
	private void getInterpolatedValues(RoofLimitValuesDTO roofLimitValuesDTO, RoofDTO roofFiltered, RoofRatingDTO roofRatingDTO) {
		
		if(roofLimitValuesDTO.getPriceMaxD() > roofLimitValuesDTO.getPriceMinD()) {
			roofRatingDTO.setPrice((int) Math.round(((roofLimitValuesDTO.getPriceMaxD() - roofFiltered.getPrice())*100) /
					(roofLimitValuesDTO.getPriceMaxD() - roofLimitValuesDTO.getPriceMinD())));
		} else {
			roofRatingDTO.setPrice(100);
		}
		
		if(roofLimitValuesDTO.getWeightMax() > roofLimitValuesDTO.getWeightMin()) {
			roofRatingDTO.setWeight((int) Math.round(((roofLimitValuesDTO.getWeightMax() - roofFiltered.getWeight())*100) / 
					(roofLimitValuesDTO.getWeightMax() - roofLimitValuesDTO.getWeightMin())));
		} else {
			roofRatingDTO.setWeight(100);
		}
		
		if(roofLimitValuesDTO.getWallMax() > roofLimitValuesDTO.getWallMin()) {
			roofRatingDTO.setWall((int) Math.round(((roofLimitValuesDTO.getWallMax() - roofFiltered.getWall())*100) /
					(roofLimitValuesDTO.getWallMax() - roofLimitValuesDTO.getWallMin())));
		} else {
			roofRatingDTO.setWall(100);
		}

	}
	
	
	
	/**
	 * Process to calculate the final weighing that will be used to rate every roof.
	 * 
	 * @param userRoofRatingDTO
	 * @return
	 */
	private SortedMap<String, Double> calculateFinalWeighing(UserRoofRatingDTO userRoofRatingDTO) {
		
		// GET STATIC WEIGHING FROM PROPERTIES
		SortedMap<String, Integer> technicalWeighingMap = MapsTransformUtils.recourceToIntegerMap("parameters/technicalWeighing");

		// GET USER RATE
		SortedMap<String, Integer> userRateMap = buildUserRateMap(userRoofRatingDTO);

		// GET THE LOWEST VALUES ENTERED BY THE USER
		Integer minValue = Collections.min(userRateMap.values());

		// SET THE DIFFERENCE OF EACH FIELD RELATED TO LOWEST
		SortedMap<String, Integer> differenceMap = buildDifferenceMap(userRateMap, minValue);

		// GET THE CONVERSION MAP
		SortedMap<String, Double> conversionMap = buildMappingMap(differenceMap);

		// GET THE CORRECTION MAP
		SortedMap<String, Double> correctionMap = buildCorrectionMap(technicalWeighingMap, conversionMap);

		// GET PERCENTAGE VALUE
		Double percentageValue = (correctionMap.values().stream().mapToDouble(Double::doubleValue).sum()) / 100;

		// GET FINAL WEIGHING
		SortedMap<String, Double> finalWeighingMap = getFinalWeighing(correctionMap, percentageValue);

		
		return finalWeighingMap;
	}

	/**
	 * Build a map with the rate entered by the user for every field.
	 * 
	 * @param userRoofRatingDTO
	 * @return
	 */
	private SortedMap<String, Integer> buildUserRateMap(UserRoofRatingDTO userRoofRatingDTO) {
		
		SortedMap<String, Integer> userRateMap = MapsTransformUtils.convertObjectToIntegerSortedMap(userRoofRatingDTO);
		userRateMap.remove("slope");
		userRateMap.remove("climate");
		userRateMap.remove("results");
		
		return userRateMap;
	}
	
	/**
	 * Build a map with the difference of every entry regarding to the minValue
	 * 
	 * @param userRateMap
	 * @param minValue
	 * @return
	 */
	private SortedMap<String, Integer> buildDifferenceMap(SortedMap<String, Integer> userRateMap, Integer minValue) {
		
		SortedMap<String, Integer> differenceMap = new TreeMap<String, Integer>();
		
		for (Map.Entry<String, Integer> entry : userRateMap.entrySet()){
			differenceMap.put(entry.getKey(), entry.getValue()-minValue);
		}
		
		return differenceMap;
	}
	
	/**
	 * Build a map with the mapping of every entry of differenceMap
	 * 
	 * @param differenceMap
	 * @return
	 */
	private SortedMap<String, Double> buildMappingMap(SortedMap<String, Integer> differenceMap) {
		
		// Non-linear static conversion for a difference value
		SortedMap<String, Double> weighingMapping = MapsTransformUtils.recourceToDoubleMap("parameters/weighingMapping");
		SortedMap<String, Double> conversionMap = new TreeMap<String, Double>();
		
		for (Map.Entry<String, Integer> entry : differenceMap.entrySet()){
			String conversion = entry.getValue().toString();
			conversionMap.put(entry.getKey(), weighingMapping.get(conversion));
		}
		
		return conversionMap;
	}

	/**
	 * Build a map of correction values. They are the multiplication of thecnicalWeighing by conversionMap. 
	 * 
	 * @param technicalWeighingMap
	 * @param conversionMap
	 * @return
	 */
	private SortedMap<String, Double> buildCorrectionMap(SortedMap<String, Integer> technicalWeighingMap,
			SortedMap<String, Double> conversionMap) {
		
		SortedMap<String, Double> correctionMap = new TreeMap<String, Double>();
		
		for (Map.Entry<String, Double> entry : conversionMap.entrySet()){
			Double correction = entry.getValue() * technicalWeighingMap.get(entry.getKey());

			correctionMap.put(entry.getKey(), correction);
		}
		return correctionMap;
	}	
	
	/**
	 * Build a map with final weighing that will be used to rate every roof.
	 * 
	 * @param correctionMap
	 * @param percentageValue
	 * @return
	 */
	private SortedMap<String, Double> getFinalWeighing(SortedMap<String, Double> correctionMap,
			Double percentageValue) {
		
		SortedMap<String, Double> finalWeighingMap = new TreeMap<String, Double>();
		
		for (Map.Entry<String, Double> entry : correctionMap.entrySet()){
			Double finalWeighing = entry.getValue() / percentageValue;

			finalWeighingMap.put(entry.getKey(), finalWeighing);
		}
		return finalWeighingMap;
	}

	
	
	/**
	 * Calculate the rating of every roof with the values in finalWeighingMap and returns 
	 * a list with the best choices.
	 * 
	 * @param roofFilteredList
	 * @param roofMappedList
	 * @param finalWeighingMap
	 */
	private List<BestRoofDTO> getRoofsRated(List<RoofDTO> roofFilteredList, List<RoofRatingDTO> roofMappedList,
			SortedMap<String, Double> finalWeighingMap, Integer results) {

		// SET THE RATE OF EVERY ROOF 
		Map<Long, Integer> ratingResult = setRoofRate(roofMappedList, finalWeighingMap);
		
		// ORDER DESCENDING AND LIMITED THE RATING RESULTS
		Map<Long, Integer> ratingResultOrdered;
		if(ratingResult.size()>results) {
			ratingResultOrdered = MapsTransformUtils.sortByValueDescWithLimit(ratingResult, results);
		} else {
			ratingResultOrdered = MapsTransformUtils.sortByValueDescWithLimit(ratingResult, ratingResult.size());
		}
		

		// SET THE BEST ROOFS INTO A LIST
		List<BestRoofDTO> bestRoofs = setBestRoofs(roofFilteredList, ratingResultOrdered, roofMappedList);
		
		
		return bestRoofs;
	}

	/**
	 * Set the rate of every roof
	 * 
	 * @param roofToRateList
	 * @param finalWeighingMap
	 * @return
	 */
	private Map<Long, Integer> setRoofRate(List<RoofRatingDTO> roofToRateList,
			SortedMap<String, Double> finalWeighingMap) {
		
		Map<Long, Integer> ratingResult = new HashMap<Long, Integer>();
		
		// Set the rate of each roof into a map key by idRoof
		for(RoofRatingDTO roofToAnalize: roofToRateList) {
			
			// Map with the roof mapped 
			Map<String, Integer> roofToRateMap = MapsTransformUtils.convertObjectToIntegerSortedMap(roofToAnalize);
			roofToRateMap.remove("idRoof");
			
			// Get the rate of each roof
			Double rate = 0D;
			for (Map.Entry<String, Integer> entry : roofToRateMap.entrySet()){	
				rate = rate + entry.getValue() * finalWeighingMap.get(entry.getKey());
			}

			ratingResult.put(roofToAnalize.getIdRoof(), (int) Math.round(rate / 100));
		}
		
		return ratingResult;
	}

	
	/**
	 * Returns the best choices of the rating results
	 * 
	 * @param roofFilteredList
	 * @param ratingResultOrdered
	 * @return
	 */
	private List<BestRoofDTO> setBestRoofs(List<RoofDTO> roofFilteredList, Map<Long, Integer> ratingResultOrdered,
			List<RoofRatingDTO> roofMappedList) {
		
		List<BestRoofDTO> bestRoofs = new ArrayList<>(); 
		
		// Set the rate in map into roofDTO
		for (Map.Entry<Long, Integer> entry : ratingResultOrdered.entrySet()){	
			
			BestRoofDTO bestRoofDTO = new BestRoofDTO();
			
			for(RoofDTO roofFiltered: roofFilteredList) {
				
				if(entry.getKey().equals(roofFiltered.getIdRoof())) {
					roofFiltered.setRate(ratingResultOrdered.get(roofFiltered.getIdRoof()));
					bestRoofDTO.setRoofDTO(roofFiltered);
					
					for(RoofRatingDTO roofRatingDTO: roofMappedList) {
						
						if(roofFiltered.getIdRoof().equals(roofRatingDTO.getIdRoof())) {
							bestRoofDTO.setRoofRatingDTO(roofRatingDTO);
							
							bestRoofs.add(bestRoofDTO);
						}
					}
				}

			}
		}
		return bestRoofs;
	}
}
