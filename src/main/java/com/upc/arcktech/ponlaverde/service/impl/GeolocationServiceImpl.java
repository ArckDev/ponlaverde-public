package com.upc.arcktech.ponlaverde.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.upc.arcktech.ponlaverde.service.GeolocationService;

import io.ipgeolocation.api.Geolocation;
import io.ipgeolocation.api.GeolocationParams;
import io.ipgeolocation.api.IPGeolocationAPI;

@Service
public class GeolocationServiceImpl implements GeolocationService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${geolite.name}")
	private String geolite;
	
	@Value("${geolocation-api.key}")
	private String key;

	@Override
	public CityResponse getAccessLocation(String ipAddress)  {

		CityResponse location = null;
		
		try {	
			ClassLoader classLoader = getClass().getClassLoader();
			File database = new File(classLoader.getResource("static/geolite/"+geolite).getFile());
			DatabaseReader reader = new DatabaseReader.Builder(database).build();
			InetAddress inetAddress = InetAddress.getByName(ipAddress);
			location = reader.city(inetAddress);
			
		} catch (IOException e) {
			logger.error(e.getMessage());
			
		} catch (GeoIp2Exception e) {
			logger.error(e.getMessage());
		}

		return location;
	}
	
	@Override
	public Geolocation getAPIAccessLocation(String ipAddress) {

		// Create IPGeolocationAPI object, passing your valid API key
		IPGeolocationAPI api = new IPGeolocationAPI(key);
		
		GeolocationParams geoParams = new GeolocationParams();
		geoParams.setIPAddress(ipAddress);
		geoParams.setFields("geo");

		Geolocation geolocation = api.getGeolocation(geoParams);

		if(geolocation.getStatus() == 200) {
			return geolocation;
			
		} else {
			return null;
		}

	}

}
