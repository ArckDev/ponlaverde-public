package com.upc.arcktech.ponlaverde.service;

import java.io.IOException;

import com.maxmind.geoip2.model.CityResponse;

import io.ipgeolocation.api.Geolocation;

public interface GeolocationService {
	
	CityResponse getAccessLocation(String ipAddress) throws IOException;

	Geolocation getAPIAccessLocation(String ipAddress);

}
