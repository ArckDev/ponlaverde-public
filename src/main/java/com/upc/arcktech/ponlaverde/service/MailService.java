package com.upc.arcktech.ponlaverde.service;


import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;


/**
 * @author jose.rodriguez
 *
 */
public interface MailService {
	
	boolean sendEmail(int serviceId, HashMap<String,String> params) throws IOException, SQLException;

}
