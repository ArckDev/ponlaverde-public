package com.upc.arcktech.ponlaverde.service;

import java.util.List;

import com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO;
import com.upc.arcktech.ponlaverde.dto.StatListAndTotalDTO;
import com.upc.arcktech.ponlaverde.dto.StatUsedFeatureDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;

public interface DashboardService {

	// ADMIN
	
	/*
	 * Incremental number of individual users registered in the system group by month.
	 */
	StatListAndTotalDTO<?> individualUserRegistered();	
	
	/*
	 * Incremental number of supplier users registered in the system group by month.
	 */
	StatListAndTotalDTO<?> supplierUserRegistered();
	
	/*
	 * Incremental number of connections to the system group by month.
	 */
	StatListAndTotalDTO<?> connectionsToSystem();
	
	/*
	 * Incremental number of logins to the system group by month.
	 */
	StatListAndTotalDTO<?> loginsToSystem();
	
	/*
	 * Real time connected users.
	 */
	List<StatCountByFieldDTO> currentConnectedUsers();
	
	/*
	 * Roofs most commonly chosen by the users in recommender feature. Top 5.
	 */
	List<StatCountByMakerModelDTO> mostPopularChosenRoofsInRecommender();
	
	/*
	 * Roofs most commonly suggested in recommender feature. Top 5.
	 */
	List<StatCountByMakerModelDTO> mostSuggestedRoofsInRecommender();
	
	/*
	 * How many times the higher rated roof is chosen.
	 */
	Integer successRateInRecommender();
	
	/*
	 * The most common type of devices with connections to the system.
	 */
	List<StatCountByFieldDTO> accessDeviceType();
	
	/*
	 * The most common OS of the devices with connections to the system.
	 */
	List<StatCountByFieldDTO> accessDeviceOS();
	
	/*
	 * The most common cities with connections to the system.
	 */
	List<StatCountByCityCountryDTO> commonCitiesConnection();
	
	/*
	 * The most countries with connections to the system.
	 */
	List<StatCountByFieldDTO> commonCountriesConnection();
	
	/*
	 * Number of users by city and country.
	 */
	List<StatCountByCityCountryDTO> usersByCityAndCountry();
	
	/*
	 * Use of features group by month.
	 */
	StatUsedFeatureDTO useOfFeatures();
	
	/*
	 * The average score of variables in recommender feature.
	 */
	List<StatCountByFieldDTO> averageScoreVariablesInRecommender();
	
	/*
	 * Number of roofs created in the system and group by status.
	 */
	StatListAndTotalDTO<?> systemTotalRoofsByStatus();
	
	/*
	 * Slope of the roofs selected in recommender feature.
	 */
	List<StatCountByFieldDTO> roofSlopeSelectedInRecommender();
	
	/*
	 * Climate of the roofs selected in recommender feature.
	 */
	List<StatCountByFieldDTO> roofClimateSelectedInRecommender();
	
	
	// SUPPLIER
	
	/*
	 * The supplier's roof most commonly chosen by the users in recommender feature.
	 */
	List<StatCountByMakerModelDTO> supplierMostChosenRoofInRecommender(UserDTO userDTO);
	
	/*
	 * How many times appears a roof in recommender feature. Top 5. 
	 */
	List<StatCountByMakerModelDTO> supplierMostSuggestedRoofsInRecommender(UserDTO userDTO);

	/*
	 * Which are the cities where the users that choose supplier's roofs live.
	 */
	List<StatCountByCityCountryDTO> supplierCommonUserCitiesInRecommender(UserDTO userDTO);
	
	/*
	 * The number of roofs owned by the supplier group by status.
	 */
	StatListAndTotalDTO<StatCountByFieldDTO> supplierTotalRoofsByStatus(UserDTO userDTO);

	


}
