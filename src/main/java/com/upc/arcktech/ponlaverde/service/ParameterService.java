package com.upc.arcktech.ponlaverde.service;

import java.util.List;

import com.upc.arcktech.ponlaverde.constants.Parameters;
import com.upc.arcktech.ponlaverde.dto.ParameterDTO;
import com.upc.arcktech.ponlaverde.dto.ParametersListDTO;

public interface ParameterService {
	

	ParameterDTO findByParameter(Parameters param);

	List<ParameterDTO> getAllParameters();

	void updateParameters(ParametersListDTO parametersListDTO);

}
