package com.upc.arcktech.ponlaverde.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.HtmlMessagesDTO;
import com.upc.arcktech.ponlaverde.entity.HtmlMessages;
import com.upc.arcktech.ponlaverde.repository.HtmlMessagesRepository;
import com.upc.arcktech.ponlaverde.service.HtmlMessagesService;
import com.upc.arcktech.ponlaverde.transformer.HtmlMessagesTransformer;

@Service
public class HtmlMessagesServiceImpl implements HtmlMessagesService {
	
	@Autowired
	HtmlMessagesRepository htmlMessagesRepository;

	@Override
	public HtmlMessagesDTO getMessage(Integer serviceId) {
		
		HtmlMessages htmlMessages = htmlMessagesRepository.findByServiceId(serviceId);
		
		HtmlMessagesDTO htmlMessagesDTO = HtmlMessagesTransformer.entityToDTO(htmlMessages);
		
		return htmlMessagesDTO;
	}

}
