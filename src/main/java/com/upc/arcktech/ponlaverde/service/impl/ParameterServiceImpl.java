package com.upc.arcktech.ponlaverde.service.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.constants.Parameters;
import com.upc.arcktech.ponlaverde.dto.ParameterDTO;
import com.upc.arcktech.ponlaverde.dto.ParametersListDTO;
import com.upc.arcktech.ponlaverde.entity.Parameter;
import com.upc.arcktech.ponlaverde.repository.ParameterRepository;
import com.upc.arcktech.ponlaverde.service.ParameterService;
import com.upc.arcktech.ponlaverde.transformer.ParameterTransformer;

@Service
public class ParameterServiceImpl implements ParameterService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ParameterRepository parameterRepository;

	@Override
	public ParameterDTO findByParameter(Parameters param) {
		
		Parameter parameter = parameterRepository.findByParameter(param.toString());

		return ParameterTransformer.entityToDTO(parameter);
	}
	
	@Override
	public List<ParameterDTO> getAllParameters(){
		
		List<Parameter> parametersList = parameterRepository.findAll();
		
		return ParameterTransformer.entityToDTO(parametersList);
	}
	
	@Override
	public void updateParameters(ParametersListDTO parametersListDTO) {
		
		try {
			
			List<Parameter> parametersList = parameterRepository.findAll();
			
			//Set ALL as inactive
			for(Parameter parameter: parametersList) {
				parameter.setActive(false);
			}
			
			//Set SELECTED as active
			if(parametersListDTO.getParameter() != null) {
				for(Parameter parameter: parametersList) {
					if(parametersListDTO.getParameter().contains(parameter.getParameter())) {
						parameter.setActive(true);
					}
				}
			}
			
			parameterRepository.save(parametersList);
			logger.info("UPDATED PARAMETERS");
			
		} catch (Exception e) {
			logger.error("ERROR UPDATING PARAMETERS " + e.getMessage());
		}

	}

}
