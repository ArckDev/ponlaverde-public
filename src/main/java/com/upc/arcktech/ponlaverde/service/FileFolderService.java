package com.upc.arcktech.ponlaverde.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.upc.arcktech.ponlaverde.dto.RoofDTO;

public interface FileFolderService {
	
	Map<String, String> listFilesForFolder(File folder);
	
	List<File> getFilesForFolder(File folder);

	RoofDTO separateFileType(List<File> roofFiles, RoofDTO roofDTO);

	void zipDirectory(String sourcePath, Long roofId);

	void storeAttachementsFromTempFolder(FileFolderService fileFolderService, Long roofId, List<String> uploadedList,
			String listType) throws IOException;

	void deleteAttachments(Long idRoof);

}
