/*
 *-----------------------------------------------------------------------------
 * name             : ResizeImages.java
 * project          : MailService
 * created          : Jose Rodriguez - Getronics 04/2018
 * language         : java
 * environment      : jdk1.8
 *-----------------------------------------------------------------------------
 */
package com.upc.arcktech.ponlaverde.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

/**
 * @author jose.rodriguez
 *
 */
public interface ResizeImagesService {
	
	
	void checkSizeAndResizeImage(List<File> imageFileList);

	void saveResizedImage(BufferedImage resizedImage, File fileEntry, String ext);

}
