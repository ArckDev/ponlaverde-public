package com.upc.arcktech.ponlaverde.service;

import com.upc.arcktech.ponlaverde.dto.WelcomeMetricsDTO;

public interface WelcomeMetricsService {
	
	WelcomeMetricsDTO getWelcomeMetrics();

}
