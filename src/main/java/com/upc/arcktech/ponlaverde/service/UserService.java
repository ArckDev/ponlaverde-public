package com.upc.arcktech.ponlaverde.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.User;


public interface UserService {
	
	Boolean addUser(UserDTO userDTO, HttpServletRequest request) throws NoSuchAlgorithmException, IOException, SQLException;
	
	void updateUser(UserDTO userDTO);
	
	Boolean findUser (UserDTO userDTO);

	UserDTO activateUser(UserDTO userDTO);
	
	User findByUsername(String username);

	UserDTO getUserByUsername(String username);
	
	Boolean restorePassword(UserDTO userDTO);

	Boolean requestRestorePassword(UserDTO userDTO, HttpServletRequest request);

}
