package com.upc.arcktech.ponlaverde.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.maxmind.geoip2.model.CityResponse;
import com.upc.arcktech.ponlaverde.dto.BaseAccessConnectionDTO;
import com.upc.arcktech.ponlaverde.service.GeolocationService;

import io.ipgeolocation.api.Geolocation;

public class BaseAccessConnectionServiceImpl {
	
	private static final String DESKTOP = "Desktop";
	private static final String MOBILE = "Mobile";
	
	@Autowired
	GeolocationService geolocationService;

	
	protected void setDeviceInfo(HttpServletRequest request, BaseAccessConnectionDTO connectionDTO) {
		
		String deviceInfo = request.getHeader("User-Agent");
		
		if(deviceInfo.contains(MOBILE)) {
			connectionDTO.setDeviceType(MOBILE);
		} else {
			connectionDTO.setDeviceType(DESKTOP);
		}

		Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(deviceInfo);
	   
		if(m.find()) {
			List<String> deviceDetails = new ArrayList<>(Arrays.asList(m.group(1).split(";")));
			
			if(!deviceDetails.isEmpty()) {
				
				if("Linux".equals(deviceDetails.get(0).trim()) && deviceDetails.get(1).contains("Android")) {
					connectionDTO.setOs("Android");
					connectionDTO.setDeviceModel(deviceDetails.get(2).trim());
				
				} else if ("iPad".equals(deviceDetails.get(0).trim()) || "iPhone".equals(deviceDetails.get(0).trim())) {
					connectionDTO.setOs("iOS");
					connectionDTO.setDeviceModel(deviceDetails.get(0).trim());
					
				} else if(deviceDetails.size() > 1 && "Macintosh".equals(deviceDetails.get(0).trim())) {
					connectionDTO.setOs("iOS");
					connectionDTO.setDeviceModel(deviceDetails.get(2).trim());
					
				} else if(deviceDetails.size() > 1 && deviceDetails.get(0).contains("Windows")) {
					connectionDTO.setOs("Windows");
					connectionDTO.setDeviceModel(deviceDetails.get(0).trim() + " / " + deviceDetails.get(2).trim());
				}
				
			}

		}
	}

	protected void setGeolocation(BaseAccessConnectionDTO connectionDTO) throws IOException {
		
		CityResponse location = geolocationService.getAccessLocation(connectionDTO.getIpAccess());
		
		if (location != null && location.getCity().getName() != null) {
			connectionDTO.setCity(location.getCity().getName());
			connectionDTO.setCountry(location.getCountry().getIsoCode());
			
		} else {
			Geolocation geolocation = geolocationService.getAPIAccessLocation(connectionDTO.getIpAccess());
			if(geolocation != null) {
				connectionDTO.setCity(geolocation.getCity());
				connectionDTO.setCountry(geolocation.getCountryCode2());
			}
		}
	}


}
