package com.upc.arcktech.ponlaverde.service.impl;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.ConnectionDTO;
import com.upc.arcktech.ponlaverde.repository.ConnectionRepository;
import com.upc.arcktech.ponlaverde.service.ConnectionService;
import com.upc.arcktech.ponlaverde.transformer.ConnectionTransformer;

@Service
public class ConnectionServiceImpl extends BaseAccessConnectionServiceImpl implements ConnectionService {

	@Autowired
	ConnectionRepository connectionRepository;

	@Override
	public void addConnection(HttpServletRequest request) throws IOException  {
		
		ConnectionDTO connectionDTO = new ConnectionDTO();
		connectionDTO.setIpAccess(request.getRemoteAddr());
		connectionDTO.setAccessDate(new Date());

		this.setGeolocation(connectionDTO);
		this.setDeviceInfo(request, connectionDTO);

		connectionRepository.save(ConnectionTransformer.DTOtoEntity(connectionDTO));
	}

	

}
