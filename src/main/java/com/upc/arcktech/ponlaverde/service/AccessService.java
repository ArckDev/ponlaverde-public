package com.upc.arcktech.ponlaverde.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;



public interface AccessService {
	
	void addAccess(HttpServletRequest request) throws IOException;

}
