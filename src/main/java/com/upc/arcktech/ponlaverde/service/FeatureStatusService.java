package com.upc.arcktech.ponlaverde.service;

import java.util.List;

import com.upc.arcktech.ponlaverde.dto.FeatureStatusDTO;
import com.upc.arcktech.ponlaverde.dto.FeatureStatusListDTO;

public interface FeatureStatusService {
	
	List<FeatureStatusDTO> getAllFeaturesStatus();
	
	Boolean isActive(String feature);

	List<FeatureStatusDTO> updateFeaturesStatus(FeatureStatusListDTO featuresStatusDTOList);

}
