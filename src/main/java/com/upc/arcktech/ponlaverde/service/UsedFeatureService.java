package com.upc.arcktech.ponlaverde.service;

import com.upc.arcktech.ponlaverde.dto.UsedFeatureDTO;

public interface UsedFeatureService {

	void addAccess(UsedFeatureDTO usedFeatureDTO);

}
