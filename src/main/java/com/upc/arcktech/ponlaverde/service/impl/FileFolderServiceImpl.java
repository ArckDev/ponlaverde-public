/*
 *-----------------------------------------------------------------------------
 * name             : ListFilesForFolder.java
 * project          : MailService
 * created          : Jose Rodriguez - Getronics 04/2018
 * language         : java
 * environment      : jdk1.8
 *-----------------------------------------------------------------------------
 */
package com.upc.arcktech.ponlaverde.service.impl;

import static com.upc.arcktech.ponlaverde.service.FilePathServiceConstants.DOWNLOAD_PATH;
import static com.upc.arcktech.ponlaverde.service.FilePathServiceConstants.ROOF;
import static com.upc.arcktech.ponlaverde.service.FilePathServiceConstants.ROOFS_PATH;
import static com.upc.arcktech.ponlaverde.service.FilePathServiceConstants.TEMP_PATH;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.service.FileFolderService;


/**
 * @author jose.rodriguez
 *
 */
@Service
public class FileFolderServiceImpl implements FileFolderService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${root.file-path}")
	private String rootPath;	
	@Value("${web.file-path}")
	private String webPath;


	@Override
	public Map<String, String> listFilesForFolder(File folder) {
		
		Map<String, String> folderImages = new HashMap<String, String>();
		int images = 0;

	    for (File fileEntry : folder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	        	
	        	String ext = FilenameUtils.getExtension(fileEntry.getName()).toLowerCase();
	        	if( ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg") || ext.equals("gif") || ext.equals("bmp") || ext.equals("tiff") || ext.equals("svg") ) {
	        		folderImages.put(FilenameUtils.removeExtension(fileEntry.getName()), fileEntry.getAbsolutePath());
	        		images++;
	        	}
	        	
	        } else {
	        	listFilesForFolder(fileEntry);
	        }
	    }
	   
	    logger.debug("Found "+images+" image(s) in path "+folder.toString());
		return folderImages;
	}
	

	@Override
	public List<File> getFilesForFolder(File folder) {
		
		List<File> fileList = new ArrayList<File>();
		int files = 0;

	    for (File fileEntry : folder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	        	
	        	String ext = FilenameUtils.getExtension(fileEntry.getName()).toLowerCase();
	        	if( ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg") || ext.equals("gif") || ext.equals("pdf")  ) {
	        		fileList.add(fileEntry);
	        		files++;
	        	}
	        	
	        } else {
	        	getFilesForFolder(fileEntry);
	        }
	    }
	   
	    logger.debug("Get "+files+" file(s) from path "+folder.toString());
		return fileList;
	}
	
	@Override
	public RoofDTO separateFileType(List<File> roofFiles, RoofDTO roofDTO) {
		
		List<String> uploadedImagesList = new ArrayList<>();
	    List<String> uploadedDetailsList = new ArrayList<>();
	    List<String> uploadedSheetsList = new ArrayList<>();
		
		
		 for (File roofFile : roofFiles) {
			 
			 if (roofFile.getName().contains("PHOTO")) {
				 uploadedImagesList.add((roofFile.getPath().replace("\\", "/")).replace(rootPath, webPath));
				 logger.debug("IMAGE PATH "+uploadedImagesList.get(0));
			 } else if (roofFile.getName().contains("DETAIL")) {
				 uploadedDetailsList.add((roofFile.getPath().replace("\\", "/")).replace(rootPath, webPath));
				 logger.debug("DETAIL PATH "+uploadedDetailsList.get(0));
			 } else if (roofFile.getName().contains("SHEET")) {
				 uploadedSheetsList.add((roofFile.getPath().replace("\\", "/")).replace(rootPath, webPath));
			 }
			 
		 }
		
		 roofDTO.setUploadedImagesList(uploadedImagesList);
		 roofDTO.setUploadedDetailsList(uploadedDetailsList);
		 roofDTO.setUploadedSheetsList(uploadedSheetsList);
		
		return roofDTO;
	}
	
	 
	 @Override
	 public void storeAttachementsFromTempFolder(FileFolderService fileFolderService, Long roofId, List<String> uploadedList, String listType) throws IOException {
			
			if(!uploadedList.isEmpty()) {
				
				int i = 1;
			
				for(String uploadedFilePath : uploadedList) {
					if(!"".equals(uploadedFilePath)){
						File uploadedFile = new File(rootPath + TEMP_PATH + uploadedFilePath);
						uploadedFile.renameTo(new File(rootPath + ROOFS_PATH + ROOF + roofId + "/" + listType + i + "." + FilenameUtils.getExtension(uploadedFilePath)));
					}
					i++;
		        }
				
			}
		
		}
	 
	 
	 @Override
	 public void deleteAttachments (Long idRoof) {
			
			try {
				File roofAttachments = new File(rootPath + ROOFS_PATH + ROOF + idRoof);
				File roofZipAttachments = new File(rootPath + DOWNLOAD_PATH + ROOF + idRoof + ".zip");
				
				if(roofAttachments.exists() && roofZipAttachments.exists()) {
					FileUtils.deleteDirectory(roofAttachments);
					FileUtils.forceDelete(roofZipAttachments);
					
					logger.error("ATTACHEMENT DELETED FOR ROOF " + idRoof);
				}

				
			} catch (Exception e) {
				logger.error("ERROR DELETING ATTACHEMENT FOR ROOF " + idRoof + " " + e.getMessage());
			}

		}


	@Override
	public void zipDirectory (String sourcePath, Long roofId)  {
		
		try {
			new File(rootPath+DOWNLOAD_PATH).mkdirs();
			
			FileOutputStream fos = new FileOutputStream(rootPath + DOWNLOAD_PATH + "Roof-" + roofId + ".zip");
	        ZipOutputStream zipOut = new ZipOutputStream(fos);
	        File fileToZip = new File(sourcePath);
	 
	        zipFile(fileToZip, fileToZip.getName(), zipOut);
	        zipOut.close();
	        fos.close();
	        
	        logger.debug("DIRECTORY ZIPPED SUCCESFULLY: " + sourcePath);
			
		} catch (IOException e) {
			logger.error("ERROR ZIPPING DIRECTORY: " + sourcePath + " " + e.getMessage());
		}
        
	}
	
	 private void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) {
        
		 try {
			 if (fileToZip.isHidden()) {
		            return;
		        }
		        if (fileToZip.isDirectory()) {
		            if (fileName.endsWith("/")) {
		                zipOut.putNextEntry(new ZipEntry(fileName));
		                zipOut.closeEntry();
		            } else {
		                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
		                zipOut.closeEntry();
		            }
		            File[] children = fileToZip.listFiles();
		            for (File childFile : children) {
		                zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
		            }
		            return;
		        }
		        FileInputStream fis = new FileInputStream(fileToZip);
		        ZipEntry zipEntry = new ZipEntry(fileName);
		        zipOut.putNextEntry(zipEntry);
		        byte[] bytes = new byte[1024];
		        int length;
		        while ((length = fis.read(bytes)) >= 0) {
		            zipOut.write(bytes, 0, length);
		        }
		        fis.close();
			    
		 }catch (IOException e) {
			 logger.error("ERROR ZIPPING FILE: " + fileName + " " + e.getMessage());
		 }
		 
	 }
}

