package com.upc.arcktech.ponlaverde.service.impl;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.AccessDTO;
import com.upc.arcktech.ponlaverde.entity.User;
import com.upc.arcktech.ponlaverde.repository.AccessRepository;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.service.AccessService;
import com.upc.arcktech.ponlaverde.transformer.AccessTransformer;
import com.upc.arcktech.ponlaverde.transformer.UserTransformer;

@Service
public class AccessServiceImpl extends BaseAccessConnectionServiceImpl implements AccessService  {

	@Autowired
	AccessRepository accessRepository;
	@Autowired
	UserRepository userRepository;
	
	@Override
	public void addAccess(HttpServletRequest request) throws IOException  {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userRepository.findByUsername(auth.getName());
		
		AccessDTO accessDTO = new AccessDTO();
		accessDTO.setIpAccess(request.getRemoteAddr());
		accessDTO.setAccessDate(new Date());
		accessDTO.setSession(request.getRequestedSessionId());
		accessDTO.setUserDTO(UserTransformer.entityToDTO(user));

		this.setGeolocation(accessDTO);
		this.setDeviceInfo(request, accessDTO);

		accessRepository.save(AccessTransformer.DTOtoEntity(accessDTO));
	}

	
}
