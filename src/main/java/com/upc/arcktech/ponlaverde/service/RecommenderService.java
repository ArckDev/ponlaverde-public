package com.upc.arcktech.ponlaverde.service;

import java.util.List;

import com.upc.arcktech.ponlaverde.dto.BestRoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.dto.UserRoofRatingDTO;
import com.upc.arcktech.ponlaverde.dto.UserSavedRoofDTO;
import com.upc.arcktech.ponlaverde.entity.User;


public interface RecommenderService {


	Double checkValue(UserRoofRatingDTO userRoofRatingDTO);

	void deleteRecommendation(UserSavedRoofDTO userSavedRoofDTO, Long userId);

	User saveRecommendation(List<BestRoofDTO> bestRoofs, UserSavedRoofDTO userSavedRoofDTO, UserDTO userDTO);

	List<BestRoofDTO> recommenderProcess(UserDTO userDTO, UserRoofRatingDTO userRoofRatingDTO);


	


}
