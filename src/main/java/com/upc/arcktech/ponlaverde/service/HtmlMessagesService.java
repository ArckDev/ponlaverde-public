package com.upc.arcktech.ponlaverde.service;

import com.upc.arcktech.ponlaverde.dto.HtmlMessagesDTO;



public interface HtmlMessagesService {
	
	HtmlMessagesDTO getMessage(Integer serviceId);

}
