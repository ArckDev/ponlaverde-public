package com.upc.arcktech.ponlaverde.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.arcktech.ponlaverde.dto.FeatureStatusDTO;
import com.upc.arcktech.ponlaverde.dto.FeatureStatusListDTO;
import com.upc.arcktech.ponlaverde.entity.FeatureStatus;
import com.upc.arcktech.ponlaverde.repository.FeatureStatusRepository;
import com.upc.arcktech.ponlaverde.service.FeatureStatusService;
import com.upc.arcktech.ponlaverde.transformer.FeaturesStatusTransformer;

@Service("featureStatus")
public class FeatureStatusServiceImpl implements FeatureStatusService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	FeatureStatusRepository featureStatusRepository;

	
	@Override
	public List<FeatureStatusDTO> getAllFeaturesStatus() {

		List<FeatureStatus> featuresStatusList = featureStatusRepository.findAll();

		return FeaturesStatusTransformer.entityToDTO(featuresStatusList);
	}

	@Override
	public Boolean isActive(String feature) {

		return featureStatusRepository.findByName(feature).getActive();
	}

	@Override
	public List<FeatureStatusDTO> updateFeaturesStatus(FeatureStatusListDTO featureStatusListDTO) {
		
		List<FeatureStatus> featuresStatusListUpdated = null;
		
		try {
			
			List<FeatureStatus> featuresStatusList = featureStatusRepository.findAll();

			//Set ALL as inactive
			for(FeatureStatus featuresStatus: featuresStatusList) {
				featuresStatus.setActive(false);
			}
			
			//Set SELECTED as active
			if(featureStatusListDTO.getFeatureStatus() != null) {
				for(FeatureStatus featuresStatus: featuresStatusList) {
					if(featureStatusListDTO.getFeatureStatus().contains(featuresStatus.getName())) {
						featuresStatus.setActive(true);
					}
				}
			}
			

			featuresStatusListUpdated = featureStatusRepository.save(featuresStatusList);
			logger.info("UPDATED FEATURES STATUS: "+ featuresStatusList);
			
		} catch (Exception e) {
			logger.error("ERROR UPDATING FEATURES STATUS " + e.getMessage());
		}
		
		return FeaturesStatusTransformer.entityToDTO(featuresStatusListUpdated);
	}

}


