package com.upc.arcktech.ponlaverde.service.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.upc.arcktech.ponlaverde.constants.UserRole;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.dto.UserTempDTO;
import com.upc.arcktech.ponlaverde.entity.Role;
import com.upc.arcktech.ponlaverde.entity.User;
import com.upc.arcktech.ponlaverde.repository.RoleRepository;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.service.MailService;
import com.upc.arcktech.ponlaverde.service.UserTempService;
import com.upc.arcktech.ponlaverde.service.UserService;
import com.upc.arcktech.ponlaverde.transformer.UserTransformer;
import com.upc.arcktech.ponlaverde.utils.LinkGeneratorUtils;
import com.upc.arcktech.ponlaverde.utils.SecretKeyUtils;



@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	MailService mailService;	
	@Autowired
	UserTempService userTempService;
	@Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
	

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public Boolean addUser(UserDTO userDTO, HttpServletRequest request) {
		
		Boolean result = false;

		try {
			User newUser = UserTransformer.DTOtoEntity(userDTO);
			
			Role role = roleRepository.findByName("ROLE_"+newUser.getUserType());
			
			newUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));
			newUser.setRoles(Arrays.asList(role));
			newUser.setEnabled(false);
			newUser.setRegisterDate(new Date());
			newUser.setLastUpdateDate(new Date());
			
			User storedUser = userRepository.save(newUser);
			
			if(storedUser != null) {
				
				//Building the activation link
				String sKey = SecretKeyUtils.generateHashedRandomString();
		        String link = LinkGeneratorUtils.linkActivateAccount(newUser.getUsername(), sKey, request);
		    
		        
		        HashMap<String, String> parametersValues = new HashMap<String, String>();
		        
				if(UserRole.INDIVIDUAL.equals(userDTO.getUserType())) {
					parametersValues.put("name", userDTO.getUsername());
				} else if (UserRole.SUPPLIER.equals(userDTO.getUserType())){
					parametersValues.put("name", userDTO.getCompany());
				} else {
					parametersValues.put("name", userDTO.getUsername());
				}
				parametersValues.put("link", link);
				parametersValues.put("email", userDTO.getUsername());
				
				
				Boolean sendEmail = mailService.sendEmail(101, parametersValues);
				
				
				if(sendEmail) {
					// STORE REGISTER IN USER TEMP TABLE
					UserTempDTO userTempDTO = new UserTempDTO();
					userTempDTO.setUsername(userDTO.getUsername());
					userTempDTO.setsKey(sKey);
					
					userTempService.addTempUser(userTempDTO);
				}
				
				result = true;
				logger.info("USER CREATED: "+userDTO.toString());
			}
		
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		

		return result;
	}
	
	@Override
	public void updateUser(UserDTO userDTO) {
		
		User user = userRepository.findOne(userDTO.getIdUser());
		
		user.setCity(userDTO.getCity());
		user.setCompany(userDTO.getCompany());
		user.setCountry(userDTO.getCountry());
		user.setDateOfBirth(userDTO.getDateOfBirth());
		user.setName(userDTO.getName());
		user.setOccupation(userDTO.getOccupation());
		user.setSurnames(userDTO.getSurnames());
		user.setLastUpdateDate(new Date());
		
		if(userDTO.getPassword() != null) {
			user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
		}
		
		logger.info("USER UPDATED: "+userDTO.toString());
	}

	@Async
	@Override
	public Boolean findUser(UserDTO userDTO) {

		Boolean userExist = false;
		
		User user = userRepository.findByUsername(userDTO.getUsername().toLowerCase());
		
		if(user != null) {
			userExist = true;
		}

		return userExist;
	}
	
	@Override
	public UserDTO activateUser(UserDTO userDTO) {

		User user = userRepository.findByUsername(userDTO.getUsername().toLowerCase());
		user.setEnabled(true);
		User storedUser = userRepository.save(user);
		if (storedUser == null) {
			userDTO = null;
		}

		logger.info("USER ACTIVATED: " + userDTO.toString());

		return userDTO;
		
	}

	@Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
	
	@Override
    public UserDTO getUserByUsername(String username) {
		
		User user = userRepository.findByUsername(username);
		UserDTO userDTO = UserTransformer.entityToDTO(user);
		
        return userDTO;
    }

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public Boolean restorePassword(UserDTO userDTO) {
		
		Boolean result = false;
		
		try {
			User user = userRepository.findByUsername(userDTO.getUsername().toLowerCase());
			
			if(user != null) {
				String newPassword = SecretKeyUtils.generateRandomString(8); 
				user.setPassword(bCryptPasswordEncoder.encode(newPassword));
				userRepository.save(user);
				
				HashMap<String, String> parametersValues = new HashMap<String, String>();
				if(UserRole.INDIVIDUAL.equals(user.getUserType())) {
					parametersValues.put("name", user.getUsername());
				} else if (UserRole.SUPPLIER.equals(user.getUserType())) {
					parametersValues.put("name", user.getCompany());
				} else {
					parametersValues.put("name", user.getUsername());
				}
				parametersValues.put("email", user.getUsername());
				parametersValues.put("pass", newPassword);	
				mailService.sendEmail(102, parametersValues);
				
				result = true;
				logger.info("RESTORE PASSWORD FOR USER: "+ user.toString());
			} 

		} catch (IOException | SQLException e) {
			logger.error("ERROR RESTORING PASSWORD: " + e.getMessage());
		}
			
		return result;
	}

	@Override
	public Boolean requestRestorePassword(UserDTO userDTO, HttpServletRequest request) {

		Boolean userExist = false;

		try {
			User user = userRepository.findByUsername(userDTO.getUsername().toLowerCase());
			userDTO = UserTransformer.entityToDTO(user);

			if (user != null) {

				// Building the activation link
				String sKey = SecretKeyUtils.generateHashedRandomString();
				String link = LinkGeneratorUtils.linkRestorePassword(userDTO.getUsername(), sKey, request);

				HashMap<String, String> parametersValues = new HashMap<String, String>();

				if (UserRole.INDIVIDUAL.equals(userDTO.getUserType())) {
					parametersValues.put("name", userDTO.getUsername());
				} else if (UserRole.SUPPLIER.equals(userDTO.getUserType())) {
					parametersValues.put("name", userDTO.getCompany());
				} else {
					parametersValues.put("name", userDTO.getUsername());
				}

				parametersValues.put("link", link);
				parametersValues.put("email", userDTO.getUsername());

				Boolean sendEmail = mailService.sendEmail(103, parametersValues);

				if (sendEmail) {
					// STORE REGISTER IN USER TEMP TABLE
					UserTempDTO userTempDTO = new UserTempDTO();
					userTempDTO.setUsername(userDTO.getUsername());
					userTempDTO.setsKey(sKey);
					userTempService.addTempUser(userTempDTO);
				}
				
				userExist = true;
				logger.info("NEW PASSWORD REQUESTED: "+userDTO.toString());
			} 

		} catch (IOException | SQLException e) {
			logger.error("ERROR REQUESTING NEW PASSWORD: " + e.getMessage());
		}

		return userExist;
	}

}
