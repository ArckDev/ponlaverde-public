package com.upc.arcktech.ponlaverde.service.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.sun.mail.smtp.SMTPMessage;
import com.upc.arcktech.ponlaverde.dto.HtmlMessagesDTO;
import com.upc.arcktech.ponlaverde.service.HtmlMessagesService;
import com.upc.arcktech.ponlaverde.service.MailService;



@Service
public class MailServiceImpl implements MailService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String IMAGE = "image";
	private static final String FIELD = "field";
	private static final String I18N = "i18n";
	private static final String EXT = ".png";
	
	@Value("${email.account}")
	private String emailAccount;
	@Value("${email.password}")
	private String emailPassword;
	
	@Autowired
	HtmlMessagesService htmlMessagesService;
	@Autowired
	Environment env;

	@Override
	public boolean sendEmail(int serviceId, HashMap<String, String> parametersValues) throws IOException, SQLException {

		boolean emailOK = false;

		try {
			
			Session session = initiateEmailService();
			
			Message message = buildSimpleMessage(session, serviceId, parametersValues);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(parametersValues.get("email")));

			Transport.send(message);

			logger.info("Sent email for service " + serviceId + " to " + parametersValues.get("email"));

			emailOK = true;

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		return emailOK;
	}

	
	private Session initiateEmailService() {
		

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailAccount, emailPassword);
			}
		});
		
		return session;
	}
	
	
	private Message buildSimpleMessage(Session session, int serviceId, HashMap<String, String> parametersValues)
			throws MessagingException, IOException, SQLException {

		SMTPMessage m = new SMTPMessage(session);
		MimeMultipart content = new MimeMultipart("related");
		MimeBodyPart textPart = new MimeBodyPart();
		
		//GET MESSAGE AND SUBJECT FROM DB
		HtmlMessagesDTO htmlMessagesDTO = htmlMessagesService.getMessage(serviceId);
		String htmlTemplate = htmlMessagesDTO.getMessage().toString();

		
		// SUBSTITUTIONS

		// Get i18n params in html string
		ArrayList<String> parametersI18NList = new ArrayList<>();
		parametersI18NList = getTextInPattern(htmlTemplate, I18N);
		
		
		String html = "";
		
		// Replace I18N Pattern by properties text in html
		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle("i18n/email", locale);
		
		for(String parameterI18N: parametersI18NList) {
			html = replaceText(htmlTemplate, "%%" + parameterI18N + "%%", messageBundle.getString(parameterI18N));
			
			htmlTemplate = html;	
		}

		
		// Get imageName in html string
		ArrayList<String> imagesNameList = new ArrayList<>();
		imagesNameList = getTextInPattern(htmlTemplate, IMAGE);

		// Get parameters in html string
		ArrayList<String> parametersNameList = new ArrayList<>();
		parametersNameList = getTextInPattern(htmlTemplate, FIELD);

		ArrayList<String> cids = new ArrayList<>();
		cids = generateCID(imagesNameList.size());

		ArrayList<MimeBodyPart> imagesList = new ArrayList<>();


		// Replace Field Pattern by text in html
		for(String parameterName: parametersNameList) {
			html = replaceText(htmlTemplate, "&&" + parameterName + "&&", parametersValues.get(parameterName));
			htmlTemplate = html;	
		}


		// Replace Image Pattern by CID in html
		for (int i = 0; i < imagesNameList.size(); i++) {
			html = replaceText(htmlTemplate, "&&" + imagesNameList.get(i) + "&&", cids.get(i));
			htmlTemplate = html;
		}
		
		
		textPart.setText(html, "UTF-8", "html");

		content.addBodyPart(textPart);

		// Create image resources
		ClassLoader classLoader = getClass().getClassLoader();

		for (int i = 0; i < imagesNameList.size(); i++) {
			String image = classLoader.getResource("static/images/email/"+imagesNameList.get(i)+EXT).getPath();
			imagesList.add(createImageResource(imagesNameList.get(i), cids.get(i), image));
			content.addBodyPart(imagesList.get(i));
		}

		
		m.setContent(content);
		m.setSubject(htmlMessagesDTO.getSubject());

		return m;
	}

	
	private ArrayList<String> getTextInPattern(String htmlPrevious, String parameter) {

		ArrayList<String> matches = new ArrayList<>();

		switch (parameter) {
		
			case (IMAGE):
				matches = getImageNameInPattern(htmlPrevious);
				break;
			case (FIELD):
				matches = getParameterNameInPattern(htmlPrevious);
				break;
			case (I18N):
				matches = getParameterI18NInPattern(htmlPrevious);
				break;
		}

		return matches;
	}

	
	
	private ArrayList<String> getImageNameInPattern(String htmlPrevious) {

		ArrayList<String> matches = new ArrayList<>();
		Pattern pattern = Pattern.compile("&&image(.*?)&&");
		Matcher matcher = pattern.matcher(htmlPrevious);
		while (matcher.find()) {
			matches.add("image" + matcher.group(1));
		}

		return matches;
	}

	
	
	private ArrayList<String> getParameterNameInPattern(String htmlPrevious) {
		
		ArrayList<String> matches = new ArrayList<>();
		Pattern pattern = Pattern.compile("&&(.*?)&&");
		Matcher matcher = pattern.matcher(htmlPrevious);
		while (matcher.find()) {
			if (!matcher.group(1).contains("image")) {
				matches.add(matcher.group(1));
			}
		}

		return matches;
	}
	
	
	
	private ArrayList<String> getParameterI18NInPattern(String htmlPrevious) {
		
		ArrayList<String> matches = new ArrayList<>();
		Pattern pattern = Pattern.compile("%%(.*?)%%");
		Matcher matcher = pattern.matcher(htmlPrevious);
		while (matcher.find()) {
			matches.add(matcher.group(1));
		}

		return matches;
	}

	
	
	private ArrayList<String> generateCID(Integer size) {
		
		ArrayList<String> cids = new ArrayList<>();
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

		for (int i = 0; i < size; i++) {
			StringBuilder salt = new StringBuilder();
			Random rnd = new Random();
			while (salt.length() < 18) { // length of the random string.
				int index = (int) (rnd.nextFloat() * SALTCHARS.length());
				salt.append(SALTCHARS.charAt(index));
			}
			String saltStr = salt.toString();
			cids.add(saltStr);
		}

		return cids;
	}

	

	private MimeBodyPart createImageResource(String imageName, String cid, String filepath)
			throws MessagingException, IOException {

		MimeBodyPart imagePart = new MimeBodyPart();
		imagePart.attachFile(filepath);
		imagePart.setContentID("<" + cid + ">");
		imagePart.setDisposition(MimeBodyPart.INLINE);

		return imagePart;
	}

	
	
	private String replaceText(String htmlPrevious, String original, String replace) {

		String html = htmlPrevious.replace(original, replace);

		return html;
	}

}
