package com.upc.arcktech.ponlaverde.service;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.upc.arcktech.ponlaverde.constants.RoofStatus;
import com.upc.arcktech.ponlaverde.dto.PagedResultsDTO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.RoofLimitValuesDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;


public interface RoofService {

	// MANAGEMENT
	Boolean addRoof(RoofDTO roofDTO, RoofStatus status) throws IOException;
	Roof updateRoof(RoofDTO roofDTO, RoofStatus status);
	Boolean enableDisableRoof(Long idRoof);
	void deleteRoof(Long idRoof);
	void moveRoofToGlobal(Long idRoof);
	void restoreRoofToOwner(Long idRoof);
	void validateRoof(Long idRoof);
	void rejectRoof(RoofDTO roofDTO);
	void reportError(RoofDTO roofDTO);
	void discardError(RoofDTO roofDTO);
	void saveTempFiles(MultipartFile[] uploadingFiles, List<String> uploadedFilesList);
	void getLinkedResources(List<RoofDTO> roofDTOList);
	void getLinkedResources(RoofDTO roofDTO);
	
	// LIMIT VALUES
	RoofLimitValuesDTO getRoofLimitValuesAdmin();
	RoofLimitValuesDTO getRoofLimitValuesIndividual(UserDTO userDTO);
	RoofLimitValuesDTO getRoofLimitValuesSupplier(UserDTO userDTO);

	// STATUS
	List<String> getAdminAvailableStatus();
	List<String> getSupplierAvailableStatus();

	// FILTERS
	PagedResultsDTO filterValidatedAndAllIndividualRoofs(RoofDTO roofDTO, Pageable pageable);
	PagedResultsDTO findValidatedEnabledAndOwnedRoofs(UserDTO userDTO, Pageable pageable);
	PagedResultsDTO filterValidatedEnabledAndOwnedRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable);
	PagedResultsDTO findOwnedRoofsValidated(UserDTO userDTO, Pageable pageable);
	PagedResultsDTO filterOwnedValidatedRoofs(RoofDTO roofDTO, UserDTO userDTO, Pageable pageable);
	List<RoofDTO> filterValidatedEnabledAndOwnedEnabledRoofs(RoofDTO roofDTO, UserDTO userDTO);
	
	// FINDINGS
	PagedResultsDTO findRoofsWithErrors(Pageable pageable);
	PagedResultsDTO findOwnedRoofsWithErrors(UserDTO userDTO, Pageable pageable);
	PagedResultsDTO findAllByStatus(RoofStatus status, Pageable pageable);
	PagedResultsDTO findOwnedRoofsRejectedOrPending(UserDTO userDTO, Pageable pageable);
	PagedResultsDTO findAllByStatus(List<String> status, Pageable pageable);
	RoofDTO findRoofById(Long idRoof);
	Boolean checkIfModelExist(Long idUser, String model);
	
}
