package com.upc.arcktech.ponlaverde.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class RoofClimate {
    
    @Id
    @GeneratedValue
    private Long idClimate;
    private String code;

    
	public Long getIdClimate() {
		return idClimate;
	}
	public void setIdClimate(Long idClimate) {
		this.idClimate = idClimate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}




}
