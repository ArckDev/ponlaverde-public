package com.upc.arcktech.ponlaverde.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
public class UsedFeature {
    
    @Id
    @GeneratedValue
    private Long idUsedServ;
    private String serviceName;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date accessDate;
    private String session;
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;
    
    
    
	public Long getIdUsedServ() {
		return idUsedServ;
	}
	public void setIdUsedServ(Long idUsedServ) {
		this.idUsedServ = idUsedServ;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Date getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
    
    
	
    

}
