package com.upc.arcktech.ponlaverde.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


@Entity
public class UserSavedRoof {
    
    @Id
    @GeneratedValue
    private Long idUserSavedRoof;
    private String savedName;
    private Integer results;
    private String climate;
    private String slope;
    private Integer aesthetics;
    private Integer implantation;
    private Integer maintenance;
    private Integer price;
    private Integer regulation;
    private Integer storage;
    private Integer thermal;
    private Integer traffic;
    private Integer wall;
    private Integer weight;
    private Integer winds;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date savedDate;
    
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "id_user_saved_roof")
    @OrderBy("finalRate DESC")
    private List<UserSavedRoofDetails> userSavedRoofDetails = new ArrayList<>();  
	

	public String getSavedName() {
		return savedName;
	}
	public void setSavedName(String savedName) {
		this.savedName = savedName;
	}
	public Date getSavedDate() {
		return savedDate;
	}
	public void setSavedDate(Date savedDate) {
		this.savedDate = savedDate;
	}
	public Long getIdUserSavedRoof() {
		return idUserSavedRoof;
	}
	public void setIdUserSavedRoof(Long idUserSaved) {
		this.idUserSavedRoof = idUserSaved;
	}
	public List<UserSavedRoofDetails> getUserSavedRoofDetails() {
		return userSavedRoofDetails;
	}
	public void setUserSavedRoofDetails(List<UserSavedRoofDetails> roofSaved) {
		this.userSavedRoofDetails = roofSaved;
	}
	public Integer getResults() {
		return results;
	}
	public void setResults(Integer results) {
		this.results = results;
	}
	public String getClimate() {
		return climate;
	}
	public void setClimate(String climate) {
		this.climate = climate;
	}
	public String getSlope() {
		return slope;
	}
	public void setSlope(String slope) {
		this.slope = slope;
	}
	public Integer getAesthetics() {
		return aesthetics;
	}
	public void setAesthetics(Integer aesthetics) {
		this.aesthetics = aesthetics;
	}
	public Integer getImplantation() {
		return implantation;
	}
	public void setImplantation(Integer implantation) {
		this.implantation = implantation;
	}
	public Integer getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(Integer maintenance) {
		this.maintenance = maintenance;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getRegulation() {
		return regulation;
	}
	public void setRegulation(Integer regulation) {
		this.regulation = regulation;
	}
	public Integer getStorage() {
		return storage;
	}
	public void setStorage(Integer storage) {
		this.storage = storage;
	}
	public Integer getThermal() {
		return thermal;
	}
	public void setThermal(Integer thermal) {
		this.thermal = thermal;
	}
	public Integer getTraffic() {
		return traffic;
	}
	public void setTraffic(Integer traffic) {
		this.traffic = traffic;
	}
	public Integer getWall() {
		return wall;
	}
	public void setWall(Integer wall) {
		this.wall = wall;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Integer getWinds() {
		return winds;
	}
	public void setWinds(Integer winds) {
		this.winds = winds;
	}

	
 
}
