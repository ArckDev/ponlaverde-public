package com.upc.arcktech.ponlaverde.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserTemp {
	
	@Id
	@GeneratedValue
	private Long idUserPend;
	private String username;
	private String sKey;
	private Date registerDate;
	private Date lastEmail;
	
	
	public Long getIdUserPend() {
		return idUserPend;
	}
	public void setIdUserPend(Long idUserPend) {
		this.idUserPend = idUserPend;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getsKey() {
		return sKey;
	}
	public void setsKey(String sKey) {
		this.sKey = sKey;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public Date getLastEmail() {
		return lastEmail;
	}
	public void setLastEmail(Date lastEmail) {
		this.lastEmail = lastEmail;
	}
	
	


}
