package com.upc.arcktech.ponlaverde.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
public class Connection {
    
    @Id
    @GeneratedValue
    private Long idConnection;
    private String ipAccess;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date accessDate;
    private String city;
    private String country;
    private String deviceModel;
    private String deviceType;
    private String os;
    
    
	public Long getIdConnection() {
		return idConnection;
	}
	public void setIdConnection(Long id) {
		this.idConnection = id;
	}
	public String getIpAccess() {
		return ipAccess;
	}
	public void setIpAccess(String ipAccess) {
		this.ipAccess = ipAccess;
	}
	public Date getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDeviceModel() {
		return deviceModel;
	}
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}

    

}
