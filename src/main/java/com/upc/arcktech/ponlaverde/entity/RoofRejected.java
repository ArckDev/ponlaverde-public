package com.upc.arcktech.ponlaverde.entity;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


@Entity
public class RoofRejected {
    
    @Id
    @GeneratedValue
    private Long idRejectedRoof;
    private String rejectionReason;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date rejectionDate;
    
    @OneToOne
    @JoinColumn(name = "id_roof")
    private Roof roof;

	public Long getIdRejectedRoof() {
		return idRejectedRoof;
	}

	public void setIdRejectedRoof(Long idRejectedRoof) {
		this.idRejectedRoof = idRejectedRoof;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public Date getRejectionDate() {
		return rejectionDate;
	}

	public void setRejectionDate(Date rejectionDate) {
		this.rejectionDate = rejectionDate;
	}

	public Roof getRoof() {
		return roof;
	}

	public void setRoof(Roof roof) {
		this.roof = roof;
	}

    





}
