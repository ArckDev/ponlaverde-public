package com.upc.arcktech.ponlaverde.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author jose.rodriguez
 *
 */

@Entity
public class HtmlMessages {

	@Id
    @GeneratedValue
    private Long idHtml;
	@Column(columnDefinition="TEXT")
	private String message;
	private String subject;
	@Column(unique=true)
	private Integer serviceId;

	
	public HtmlMessages() {
		super();
	}

	public HtmlMessages(String message, String subject) {
		super();
		this.message = message;
		this.subject = subject;
	}
	
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public Integer getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	

	public Long getIdHtml() {
		return idHtml;
	}

	public void setIdHtml(Long idHtml) {
		this.idHtml = idHtml;
	}



}
