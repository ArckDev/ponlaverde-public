package com.upc.arcktech.ponlaverde.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;



@Entity
public class Roof {
    
    @Id
    @GeneratedValue
	private Long idRoof;
    
	private String maker;
	private String model;
	private Double price;
	private Integer weight;
	private Integer wall;
	private String type;
	private String slope;
	private String storage;
	private String implantation;
	private String maintenance;
	private String winds;
	private String thermal;
	private String regulation;
	private String traffic;
	private String aesthetics;
    private String status;
    private Boolean enabled;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date creationDate;
    
    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "id_roof")
    private List<RoofClimate> climate = new ArrayList<>();   
    
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "roof_user", joinColumns = @JoinColumn(name = "id_roof"), inverseJoinColumns = @JoinColumn(name = "id_user"))
    private Collection<User> user;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "id_roof")
    private List<RoofRejected> rejectedRoof = new ArrayList<>();  

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "id_roof")
    private List<RoofReportedError> errorReported = new ArrayList<>();  
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id_roof")
    private List<UserSavedRoofDetails> userSavedRoofDetails = new ArrayList<>();  
        
    
	public Long getIdRoof() {
		return idRoof;
	}
	public void setIdRoof(Long idRoof) {
		this.idRoof = idRoof;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Integer getWall() {
		return wall;
	}
	public void setWall(Integer wall) {
		this.wall = wall;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSlope() {
		return slope;
	}
	public void setSlope(String slope) {
		this.slope = slope;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public String getImplantation() {
		return implantation;
	}
	public void setImplantation(String implantation) {
		this.implantation = implantation;
	}
	public String getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(String maintenance) {
		this.maintenance = maintenance;
	}
	public String getWinds() {
		return winds;
	}
	public void setWinds(String winds) {
		this.winds = winds;
	}
	public String getThermal() {
		return thermal;
	}
	public void setThermal(String thermal) {
		this.thermal = thermal;
	}
	public String getRegulation() {
		return regulation;
	}
	public void setRegulation(String regulation) {
		this.regulation = regulation;
	}
	public String getTraffic() {
		return traffic;
	}
	public void setTraffic(String traffic) {
		this.traffic = traffic;
	}
	public String getAesthetics() {
		return aesthetics;
	}
	public void setAesthetics(String aesthetics) {
		this.aesthetics = aesthetics;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<RoofClimate> getClimate() {
		return climate;
	}
	public void setClimate(List<RoofClimate> climate) {
		this.climate = climate;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Collection<User> getUser() {
		return user;
	}
	public void setUser(Collection<User> user) {
		this.user = user;
	}
	public List<RoofReportedError> getErrorReported() {
		return errorReported;
	}
	public void setErrorReported(List<RoofReportedError> errorReported) {
		this.errorReported = errorReported;
	}
	public List<UserSavedRoofDetails> getUserSavedRoofDetails() {
		return userSavedRoofDetails;
	}
	public void setUserSavedRoofDetails(List<UserSavedRoofDetails> userSavedRoofDetails) {
		this.userSavedRoofDetails = userSavedRoofDetails;
	}
	public List<RoofRejected> getRejectedRoof() {
		return rejectedRoof;
	}
	public void setRejectedRoof(List<RoofRejected> rejectedRoof) {
		this.rejectedRoof = rejectedRoof;
	}


}
