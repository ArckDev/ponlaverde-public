package com.upc.arcktech.ponlaverde.entity;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


@Entity
public class RoofReportedError {
    
    @Id
    @GeneratedValue
    private Long idReportedErrorRoof;
    private String errorReported;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date reportDate;
    
    @OneToOne
    @JoinColumn(name = "id_roof")
    private Roof roof;
    
    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;
    
    

	public Long getIdReportedErrorRoof() {
		return idReportedErrorRoof;
	}

	public void setIdReportedErrorRoof(Long idReportedErrorRoof) {
		this.idReportedErrorRoof = idReportedErrorRoof;
	}

	public String getErrorReported() {
		return errorReported;
	}

	public void setErrorReported(String errorReported) {
		this.errorReported = errorReported;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public Roof getRoof() {
		return roof;
	}

	public void setRoof(Roof roof) {
		this.roof = roof;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

    





}
