package com.upc.arcktech.ponlaverde.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
public class UserSavedRoofDetails {
    
    @Id
    @GeneratedValue
    private Long idUserSavedRoofDetails;
    private Integer finalRate;
    private Integer aesthetics;
    private Integer implantation;
    private Integer maintenance;
    private Integer price;
    private Integer regulation;
    private Integer storage;
    private Integer thermal;
    private Integer traffic;
    private Integer wall;
    private Integer weight;
    private Integer winds;
    private Boolean selected = false;
    @OneToOne
    @JoinColumn(name = "id_roof")
    private Roof roof;

    
	public Long getIdUserSavedRoofDetails() {
		return idUserSavedRoofDetails;
	}
	public void setIdUserSavedRoofDetails(Long idRoofSaved) {
		this.idUserSavedRoofDetails = idRoofSaved;
	}
	public Integer getFinalRate() {
		return finalRate;
	}
	public void setFinalRate(Integer finalRate) {
		this.finalRate = finalRate;
	}
	public Roof getRoof() {
		return roof;
	}
	public void setRoof(Roof roof) {
		this.roof = roof;
	}
	public Integer getAesthetics() {
		return aesthetics;
	}
	public void setAesthetics(Integer aesthetics) {
		this.aesthetics = aesthetics;
	}
	public Integer getImplantation() {
		return implantation;
	}
	public void setImplantation(Integer implantation) {
		this.implantation = implantation;
	}
	public Integer getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(Integer maintenance) {
		this.maintenance = maintenance;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getRegulation() {
		return regulation;
	}
	public void setRegulation(Integer regulation) {
		this.regulation = regulation;
	}
	public Integer getStorage() {
		return storage;
	}
	public void setStorage(Integer storage) {
		this.storage = storage;
	}
	public Integer getThermal() {
		return thermal;
	}
	public void setThermal(Integer thermal) {
		this.thermal = thermal;
	}
	public Integer getTraffic() {
		return traffic;
	}
	public void setTraffic(Integer traffic) {
		this.traffic = traffic;
	}
	public Integer getWall() {
		return wall;
	}
	public void setWall(Integer wall) {
		this.wall = wall;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Integer getWinds() {
		return winds;
	}
	public void setWinds(Integer winds) {
		this.winds = winds;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

}
