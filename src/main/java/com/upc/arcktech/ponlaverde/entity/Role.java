package com.upc.arcktech.ponlaverde.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Role {
    
    @Id
    @GeneratedValue
    private Long idRole;
    private String name;
    @ManyToMany(mappedBy = "roles")
    private Collection<User> users;
    
    
    public Long getIdRole() {
        return idRole;
    }
    public void setIdRole(Long id) {
        this.idRole = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public Collection<User> getUsers() {
		return users;
	}
	public void setUsers(Collection<User> users) {
		this.users = users;
	}
    
}
