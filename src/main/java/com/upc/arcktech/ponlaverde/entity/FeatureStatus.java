package com.upc.arcktech.ponlaverde.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FeatureStatus {

	@Id
    private String name;
    private Boolean active;
    
    
    public FeatureStatus() {
		super();
	}

	public FeatureStatus(String name, Boolean active) {
		super();
		this.name = name;
		this.active = active;
	}
    
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean status) {
		this.active = status;
	}
	
	
	@Override
	public String toString() {
		return "FeatureStatus [name=" + name + ", active=" + active + "]";
	}


}
