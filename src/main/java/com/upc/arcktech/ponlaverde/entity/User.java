package com.upc.arcktech.ponlaverde.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;



@Entity
public class User {
    
    @Id
    @GeneratedValue
    private Long idUser;
    private String userType;
    private String name;
    private String surnames;
    
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(iso = ISO.DATE)
    private Date dateOfBirth;
    
    private String occupation;
    private String company;
    private String city;
    private String country;
    private String password;
   
    @Column(unique=true)
    @NotEmpty
    private String username;
    
    private Boolean enabled;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date registerDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date lastUpdateDate;
    
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "id_user"), inverseJoinColumns = @JoinColumn(name = "id_role"))
    private Collection<Role> roles;

    @ManyToMany(mappedBy = "user")
    private Collection<Roof> roofs;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user")
    private List<RoofReportedError> errorReported = new ArrayList<>(); 

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id_user")
    @OrderBy("savedDate DESC")
    private List<UserSavedRoof> userSavedRoof = new ArrayList<>(); 
    
    public Long getIdUser() {
        return idUser;
    }
    public void setIdUser(Long id) {
        this.idUser = id;
    }
    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getSurnames() {
        return surnames;
    }
    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }
    public Date getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    public String getOccupation() {
        return occupation;
    }
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public Collection<Role> getRoles() {
		return roles;
	}
	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Collection<Roof> getRoofs() {
		return roofs;
	}
	public void setRoofs(Collection<Roof> roofs) {
		this.roofs = roofs;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public List<RoofReportedError> getErrorReported() {
		return errorReported;
	}
	public void setErrorReported(List<RoofReportedError> errorReported) {
		this.errorReported = errorReported;
	}
	public List<UserSavedRoof> getUserSavedRoof() {
		return userSavedRoof;
	}
	public void setUserSavedRoof(List<UserSavedRoof> saved) {
		this.userSavedRoof = saved;
	}
	

	
}
