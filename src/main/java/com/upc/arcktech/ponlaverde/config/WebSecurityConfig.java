package com.upc.arcktech.ponlaverde.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
	@Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler(){
        return new CustomLoginSuccessHandler();
    }
    
    @Bean
    public LogoutSuccessHandler logoutSuccessHandler(){
        return new CustomLogoutSuccessHandler();
    }
    
    @Bean
    public SessionRegistry sessionRegistry() { 
        return new SessionRegistryImpl(); 
    }
    
    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
    	http.sessionManagement()
        	.sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
        	.maximumSessions(1)
        	.expiredUrl("/login")
        	.sessionRegistry(sessionRegistry());

    	http.csrf().disable();
    	http.headers().frameOptions().disable();
    	
    	http.authorizeRequests()
    	
    		.antMatchers("/h2-console/**").permitAll()
    		.antMatchers("/js/**", "/images/**", "/stylesheet/**").permitAll()
		    .antMatchers("/", "/accountVerification", "/api/**", "/logout-success" ,"/restore-password", "/restore-password-confirm", "/version").permitAll()
		    
		    .antMatchers("/error").hasAnyRole("AD", "IN", "SU")
		    .antMatchers("/roof-resources/**").hasAnyRole("AD", "IN", "SU")
		    .antMatchers("/admin/**").hasRole("AD")
		    .antMatchers("/individual/**").hasRole("IN")
		    .antMatchers("/supplier/**").hasRole("SU")
		    .anyRequest().authenticated()    
		    .and()
		    
		.formLogin()
		    .loginPage("/login")
		    .successHandler(authenticationSuccessHandler())
		    .permitAll()
		    .and()
		    
		.logout()
			.logoutUrl("/logout")
			.logoutSuccessHandler(logoutSuccessHandler())
	        .invalidateHttpSession(true);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }
}