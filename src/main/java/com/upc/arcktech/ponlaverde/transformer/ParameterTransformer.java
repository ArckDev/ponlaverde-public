package com.upc.arcktech.ponlaverde.transformer;


import java.util.ArrayList;
import java.util.List;

import com.upc.arcktech.ponlaverde.dto.ParameterDTO;
import com.upc.arcktech.ponlaverde.entity.Parameter;

public class ParameterTransformer {
	
	
	public static Parameter DTOtoEntity (ParameterDTO parameterDTO) {
		
		Parameter parameter = new Parameter();
		
		if (parameterDTO != null) {
			parameter.setActive(parameterDTO.getActive());
			parameter.setIdParameter(parameterDTO.getIdParameter());
			parameter.setParameter(parameterDTO.getParameter());
			parameter.setFeature(parameterDTO.getFeature());
		}
		
		return parameter;	
	}

	public static ParameterDTO entityToDTO (Parameter parameter) {
		
		ParameterDTO parameterDTO = new ParameterDTO();
		
		if (parameter != null) {
			parameterDTO.setActive(parameter.getActive());
			parameterDTO.setIdParameter(parameter.getIdParameter());
			parameterDTO.setParameter(parameter.getParameter());
			parameterDTO.setFeature(parameter.getFeature());
		}
		
		return parameterDTO;		
	}
	
	public static List<ParameterDTO> entityToDTO (List<Parameter> parameterList) {
		
		List<ParameterDTO> paramaterDTOList = new ArrayList<>();
		
		if(!parameterList.isEmpty()) {
			
			for(Parameter parameter: parameterList) {
				ParameterDTO parameterDTO = new ParameterDTO();
				parameterDTO = entityToDTO(parameter);
				
				paramaterDTOList.add(parameterDTO);
			}

		}
		
		return paramaterDTOList;		
	}

	
	
}
