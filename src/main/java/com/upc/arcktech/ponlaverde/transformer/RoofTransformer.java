package com.upc.arcktech.ponlaverde.transformer;

import java.util.ArrayList;
import java.util.List;

import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;
import com.upc.arcktech.ponlaverde.entity.RoofClimate;
import com.upc.arcktech.ponlaverde.entity.User;



public class RoofTransformer {
	
	public static Roof DTOtoEntity (RoofDTO roofDTO) {
		
		Roof roof = new Roof();
		List<RoofClimate> roofClimateList = new ArrayList<>();
		List<User> roofUserList = new ArrayList<>();
		
		if (roofDTO != null) {
			roof.setAesthetics(roofDTO.getAesthetics());
			roof.setCreationDate(roofDTO.getCreationDate());
			roof.setIdRoof(roofDTO.getIdRoof());
			roof.setImplantation(roofDTO.getImplantation());
			roof.setMaintenance(roofDTO.getMaintenance());
			roof.setMaker(roofDTO.getMaker());
			roof.setModel(roofDTO.getModel());
			roof.setPrice(roofDTO.getPrice());
			roof.setRegulation(roofDTO.getRegulation());
			roof.setSlope(roofDTO.getSlope());
			roof.setStatus(roofDTO.getStatus());
			roof.setStorage(roofDTO.getStorage());
			roof.setThermal(roofDTO.getThermal());
			roof.setTraffic(roofDTO.getTraffic());
			roof.setType(roofDTO.getType());
			roof.setWall(roofDTO.getWall());
			roof.setWeight(roofDTO.getWeight());
			roof.setWinds(roofDTO.getWinds());
			roof.setEnabled(roofDTO.getEnabled());
			roof.setRejectedRoof(RoofRejectedTransformer.DTOtoEntity(roofDTO.getRejectedRoofDTO()));
			roof.setErrorReported(RoofReportedErrorTransformer.DTOtoEntity(roofDTO.getErrorReportedListDTO()));
			
			for(String climate: roofDTO.getClimate()) {
				RoofClimate roofClimate = new RoofClimate();
				roofClimate.setCode(climate);
				roofClimateList.add(roofClimate);
			}
			roof.setClimate(roofClimateList);
			
			if(roofDTO.getUserDTO() != null) {
				User user = UserTransformer.DTOtoEntity(roofDTO.getUserDTO());
				roofUserList.add(user);
				roof.setUser(roofUserList);
			}
			
		}
		
		return roof;
	}
	
	public static RoofDTO entityToDTO (Roof roof) {
		
		RoofDTO roofDTO = new RoofDTO();
		List<String> roofClimateList = new ArrayList<>();
		
		if (roof != null) {
			
			roofDTO.setAesthetics(roof.getAesthetics());
			roofDTO.setCreationDate(roof.getCreationDate());
			roofDTO.setIdRoof(roof.getIdRoof());
			roofDTO.setImplantation(roof.getImplantation());
			roofDTO.setMaintenance(roof.getMaintenance());
			roofDTO.setMaker(roof.getMaker());
			roofDTO.setModel(roof.getModel());
			roofDTO.setPrice(roof.getPrice());
			roofDTO.setRegulation(roof.getRegulation());
			roofDTO.setSlope(roof.getSlope());
			roofDTO.setStatus(roof.getStatus());
			roofDTO.setStorage(roof.getStorage());
			roofDTO.setThermal(roof.getThermal());
			roofDTO.setTraffic(roof.getTraffic());
			roofDTO.setType(roof.getType());
			roofDTO.setWall(roof.getWall());
			roofDTO.setWeight(roof.getWeight());
			roofDTO.setWinds(roof.getWinds());
			roofDTO.setEnabled(roof.getEnabled());
			
			if(!roof.getRejectedRoof().isEmpty()) {
				roofDTO.setRejectedRoofDTO(RoofRejectedTransformer.entityToDTO(roof.getRejectedRoof()));
				roofDTO.setRejectionReason(RoofRejectedTransformer.buildRejectedReason(roof.getRejectedRoof().get(0)));
			}
			
			if(!roof.getErrorReported().isEmpty()) {
				roofDTO.setErrorReportedListDTO(RoofReportedErrorTransformer.entityToDTO(roof.getErrorReported()));
			}

			for(RoofClimate climate: roof.getClimate()) {
				roofClimateList.add(climate.getCode());
			}
			roofDTO.setClimate(roofClimateList);
			
			if(roof.getUser() != null) {
				UserDTO userDTO = UserTransformer.entityToDTO(roof.getUser().iterator().next());
				roofDTO.setUserDTO(userDTO);
			}
			
		}
		
		return roofDTO;
	}
	
	public static RoofDTO entityToDTOReduced (Roof roof) {
		
		RoofDTO roofDTO = new RoofDTO();
		List<String> roofClimateList = new ArrayList<>();
		
		if (roof != null) {
			
			roofDTO.setAesthetics(roof.getAesthetics());
			roofDTO.setCreationDate(roof.getCreationDate());
			roofDTO.setIdRoof(roof.getIdRoof());
			roofDTO.setImplantation(roof.getImplantation());
			roofDTO.setMaintenance(roof.getMaintenance());
			roofDTO.setMaker(roof.getMaker());
			roofDTO.setModel(roof.getModel());
			roofDTO.setPrice(roof.getPrice());
			roofDTO.setRegulation(roof.getRegulation());
			roofDTO.setSlope(roof.getSlope());
			roofDTO.setStatus(roof.getStatus());
			roofDTO.setStorage(roof.getStorage());
			roofDTO.setThermal(roof.getThermal());
			roofDTO.setTraffic(roof.getTraffic());
			roofDTO.setType(roof.getType());
			roofDTO.setWall(roof.getWall());
			roofDTO.setWeight(roof.getWeight());
			roofDTO.setWinds(roof.getWinds());
			roofDTO.setEnabled(roof.getEnabled());


			for(RoofClimate climate: roof.getClimate()) {
				roofClimateList.add(climate.getCode());
			}
			roofDTO.setClimate(roofClimateList);
		}
		
		return roofDTO;
	}
	
	
	public static List<RoofDTO> entityToDTO (List<Roof> roofList) {
		
		List<RoofDTO> roofDTOList = new ArrayList<>();
		
		if(!roofList.isEmpty()) {
			
			for(Roof roof: roofList) {
				RoofDTO roofDTO = new RoofDTO();
				roofDTO = entityToDTO(roof);
				
				roofDTOList.add(roofDTO);
			}

		}
		
		return roofDTOList;
	}
	
	
	public static List<RoofDTO> entityToDTOReduced (List<Roof> roofList) {
		
		List<RoofDTO> roofDTOList = new ArrayList<>();
		
		if(!roofList.isEmpty()) {
			
			for(Roof roof: roofList) {
				RoofDTO roofDTO = new RoofDTO();
				roofDTO = entityToDTOReduced(roof);
				
				roofDTOList.add(roofDTO);
			}

		}
		
		return roofDTOList;
	}
	
	
	public static List<Roof> DTOtoEntity (List<RoofDTO> roofDTOList) {
		
		List<Roof> roofList = new ArrayList<>();
		
		if(!roofDTOList.isEmpty()) {
			
			for(RoofDTO roofDTO: roofDTOList) {
				Roof roof = new Roof();
				roof = DTOtoEntity(roofDTO);
				
				roofList.add(roof);
			}

		}
		
		return roofList;
	}

}
