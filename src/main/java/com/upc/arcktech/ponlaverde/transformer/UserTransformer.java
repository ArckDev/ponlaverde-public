package com.upc.arcktech.ponlaverde.transformer;

import org.apache.commons.lang3.StringUtils;

import com.upc.arcktech.ponlaverde.dto.RoleDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.Role;
import com.upc.arcktech.ponlaverde.entity.User;



public class UserTransformer {
	
	public static User DTOtoEntity (UserDTO userDTO) {
		
		User user = new User();
		
		if (userDTO != null) {
			
			user.setIdUser(userDTO.getIdUser());
			user.setUsername(StringUtils.lowerCase(userDTO.getUsername()));
			user.setName(StringUtils.capitalize(userDTO.getName()));
			user.setSurnames(StringUtils.capitalize(userDTO.getSurnames()));
			user.setDateOfBirth(userDTO.getDateOfBirth());
			user.setUserType(userDTO.getUserType());
			user.setCity(StringUtils.capitalize(userDTO.getCity()));
			user.setCountry(userDTO.getCountry());
			user.setCompany(StringUtils.capitalize(userDTO.getCompany()));
			user.setOccupation(StringUtils.capitalize(userDTO.getOccupation()));
			user.setPassword(userDTO.getPassword());
			user.setEnabled(userDTO.getActive());
			user.setRegisterDate(userDTO.getRegisterDate());
			user.setLastUpdateDate(userDTO.getLastUpdateDate());
			user.setUserSavedRoof(UserSavedRoofTransformer.DTOtoEntity(userDTO.getUserSavedRoofDTO()));
		}
		
		return user;
	}
	
	public static UserDTO entityToDTO (User user) {
		
		UserDTO userDTO = new UserDTO();
		
		if (user != null) {
			
			userDTO.setIdUser(user.getIdUser());
			userDTO.setUsername(user.getUsername());
			userDTO.setName(user.getName());
			userDTO.setSurnames(user.getSurnames());
			userDTO.setDateOfBirth(user.getDateOfBirth());
			userDTO.setUserType(user.getUserType());
			userDTO.setCity(user.getCity());
			userDTO.setCountry(user.getCountry());
			userDTO.setCompany(user.getCompany());
			userDTO.setOccupation(user.getOccupation());
			userDTO.setPassword(user.getPassword());
			userDTO.setRegisterDate(user.getRegisterDate());
			userDTO.setLastUpdateDate(user.getLastUpdateDate());
			
			if(user.getUserSavedRoof() != null) {
				userDTO.setUserSavedRoofDTO(UserSavedRoofTransformer.entityToDTO(user.getUserSavedRoof()));
			}

			if(user.getRoles() != null) {
				Role role = user.getRoles().iterator().next();
				
				RoleDTO roleDTO = new RoleDTO();
				roleDTO.setName(role.getName());
				
				userDTO.setRoleDTO(roleDTO);
			}
			

		}
		
		return userDTO;
	}

}
