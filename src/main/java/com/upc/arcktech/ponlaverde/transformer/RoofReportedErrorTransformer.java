package com.upc.arcktech.ponlaverde.transformer;

import java.util.ArrayList;
import java.util.List;

import com.upc.arcktech.ponlaverde.dto.RoofReportedErrorDTO;
import com.upc.arcktech.ponlaverde.entity.RoofReportedError;


public class RoofReportedErrorTransformer {
	
	public static RoofReportedError DTOtoEntity (RoofReportedErrorDTO reportedErrorRoofDTO) {
		
		RoofReportedError reportedErrorRoof = new RoofReportedError();
		
		if (reportedErrorRoofDTO != null) {

			reportedErrorRoof.setErrorReported(reportedErrorRoofDTO.getErrorReported());
			reportedErrorRoof.setIdReportedErrorRoof(reportedErrorRoofDTO.getIdReportedErrorRoof());
			reportedErrorRoof.setReportDate(reportedErrorRoofDTO.getReportDate());
		}
		
		return reportedErrorRoof;
	}
	
	public static RoofReportedErrorDTO entityToDTO (RoofReportedError reportedErrorRoof) {
		
		RoofReportedErrorDTO reportedErrorRoofDTO = new RoofReportedErrorDTO();
		
		if (reportedErrorRoof != null) {

			reportedErrorRoofDTO.setErrorReported(reportedErrorRoof.getErrorReported());
			reportedErrorRoofDTO.setIdReportedErrorRoof(reportedErrorRoof.getIdReportedErrorRoof());
			reportedErrorRoofDTO.setReportDate(reportedErrorRoof.getReportDate());
		}
		
		return reportedErrorRoofDTO;
	}

	public static List<RoofReportedError> DTOtoEntity (List<RoofReportedErrorDTO> reportedErrorRoofDTOList) {
		
		List<RoofReportedError> reportedErrorRoofList = new ArrayList<>();
		
		if(!reportedErrorRoofDTOList.isEmpty()) {
			
			for(RoofReportedErrorDTO reportedErrorRoofDTO: reportedErrorRoofDTOList) {
				RoofReportedError reportedErrorRoof = new RoofReportedError();
				reportedErrorRoof = DTOtoEntity(reportedErrorRoofDTO);
				
				reportedErrorRoofList.add(reportedErrorRoof);
			}

		}
		
		return reportedErrorRoofList;		
	}
	
	public static List<RoofReportedErrorDTO> entityToDTO (List<RoofReportedError> reportedErrorRoofList) {
		
		List<RoofReportedErrorDTO> reportedErrorRoofDTOList = new ArrayList<>();
		
		if(!reportedErrorRoofList.isEmpty()) {
			
			for(RoofReportedError reportedErrorRoof: reportedErrorRoofList) {
				RoofReportedErrorDTO reportedErrorRoofDTO = new RoofReportedErrorDTO();
				reportedErrorRoofDTO = entityToDTO(reportedErrorRoof);
				
				reportedErrorRoofDTOList.add(reportedErrorRoofDTO);
			}

		}
		
		return reportedErrorRoofDTOList;		
	}
	
}
