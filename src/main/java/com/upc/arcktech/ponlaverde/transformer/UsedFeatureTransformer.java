package com.upc.arcktech.ponlaverde.transformer;

import com.upc.arcktech.ponlaverde.dto.UsedFeatureDTO;
import com.upc.arcktech.ponlaverde.entity.UsedFeature;


public class UsedFeatureTransformer {
	
	
	public static UsedFeature DTOtoEntity (UsedFeatureDTO usedFeatureDTO) {
		
		UsedFeature usedFeature = new UsedFeature();
		
		if (usedFeatureDTO != null) {
			usedFeature.setAccessDate(usedFeatureDTO.getAccessDate());
			usedFeature.setIdUsedServ(usedFeatureDTO.getIdUsedServ());
			usedFeature.setServiceName(usedFeatureDTO.getServiceName());
			usedFeature.setSession(usedFeatureDTO.getSession());
			usedFeature.setUser(UserTransformer.DTOtoEntity(usedFeatureDTO.getUserDTO()));
		}
		
		return usedFeature;	
	}

	public static UsedFeatureDTO entityToDTO (UsedFeature usedFeature) {
		
		UsedFeatureDTO usedFeatureDTO = new UsedFeatureDTO();
		
		if (usedFeature != null) {
			usedFeatureDTO.setAccessDate(usedFeature.getAccessDate());
			usedFeatureDTO.setIdUsedServ(usedFeature.getIdUsedServ());
			usedFeatureDTO.setServiceName(usedFeature.getServiceName());
			usedFeatureDTO.setSession(usedFeature.getSession());
			usedFeatureDTO.setUserDTO(UserTransformer.entityToDTO(usedFeature.getUser()));
		}
		
		return usedFeatureDTO;	
	}
	
}
