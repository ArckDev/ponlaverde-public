package com.upc.arcktech.ponlaverde.transformer;

import java.util.Arrays;

import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserRoofRatingDTO;


public class UserRoofRatingTransformer {
	
	public static RoofDTO userRoofRatingToRoof (UserRoofRatingDTO userRoofRatingDTO) {
		
		RoofDTO roofDTO = new RoofDTO();
		
		if (userRoofRatingDTO != null) {
			
			roofDTO.setClimate(Arrays.asList(userRoofRatingDTO.getClimate()));
			roofDTO.setSlope(userRoofRatingDTO.getSlope());

		}
		
		return roofDTO;
	}

}
