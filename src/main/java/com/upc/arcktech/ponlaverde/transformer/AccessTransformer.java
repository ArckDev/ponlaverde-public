package com.upc.arcktech.ponlaverde.transformer;

import com.upc.arcktech.ponlaverde.dto.AccessDTO;
import com.upc.arcktech.ponlaverde.entity.Access;


public class AccessTransformer {
	
	
	public static Access DTOtoEntity (AccessDTO accessDTO) {
		
		Access access = new Access();
		
		if (accessDTO != null) {
			access.setAccessDate(accessDTO.getAccessDate());
			access.setIdConnection(accessDTO.getIdConnection());
			access.setIpAccess(accessDTO.getIpAccess());
			access.setCity(accessDTO.getCity());
			access.setCountry(accessDTO.getCountry());
			access.setDeviceModel(accessDTO.getDeviceModel());
			access.setDeviceType(accessDTO.getDeviceType());
			access.setOs(accessDTO.getOs());
			access.setSession(accessDTO.getSession());
			access.setUser(UserTransformer.DTOtoEntity(accessDTO.getUserDTO()));
		}
		
		return access;	
	}

	public static AccessDTO entityToDTO (Access access) {
		
		AccessDTO accessDTO = new AccessDTO();
		
		if (access != null) {
			accessDTO.setAccessDate(access.getAccessDate());
			accessDTO.setIdConnection(access.getIdConnection());
			accessDTO.setIpAccess(access.getIpAccess());
			accessDTO.setCity(access.getCity());
			accessDTO.setCountry(access.getCountry());
			accessDTO.setDeviceModel(access.getDeviceModel());
			accessDTO.setDeviceType(access.getDeviceType());
			accessDTO.setOs(access.getOs());
			accessDTO.setSession(access.getSession());
			accessDTO.setUserDTO(UserTransformer.entityToDTO(access.getUser()));
		}
		
		return accessDTO;	
	}
	
}
