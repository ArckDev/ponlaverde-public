package com.upc.arcktech.ponlaverde.transformer;

import com.upc.arcktech.ponlaverde.dto.UserTempDTO;
import com.upc.arcktech.ponlaverde.entity.UserTemp;


public class UserTempTransformer {
	
	
	public static UserTemp DTOtoEntity (UserTempDTO userPendingVerificationDTO) {
		
		UserTemp userPendingVerification = new UserTemp();
		
		if (userPendingVerificationDTO != null) {
			userPendingVerification.setUsername(userPendingVerificationDTO.getUsername());
			userPendingVerification.setIdUserPend(userPendingVerificationDTO.getIdUserPend());
			userPendingVerification.setLastEmail(userPendingVerificationDTO.getLastEmail());
			userPendingVerification.setRegisterDate(userPendingVerificationDTO.getRegisterDate());
			userPendingVerification.setsKey(userPendingVerificationDTO.getsKey());
		}
		
		return userPendingVerification;	
	}

	public static UserTempDTO entityToDTO (UserTemp userPendingVerification) {
		
		UserTempDTO userPendingVerificationDTO = new UserTempDTO();
		
		if (userPendingVerification != null) {
			userPendingVerificationDTO.setUsername(userPendingVerification.getUsername());
			userPendingVerificationDTO.setIdUserPend(userPendingVerification.getIdUserPend());
			userPendingVerificationDTO.setLastEmail(userPendingVerification.getLastEmail());
			userPendingVerificationDTO.setRegisterDate(userPendingVerification.getRegisterDate());
			userPendingVerificationDTO.setsKey(userPendingVerification.getsKey());
		}
		
		return userPendingVerificationDTO;	
	}
	
}
