package com.upc.arcktech.ponlaverde.transformer;


import com.upc.arcktech.ponlaverde.dto.HtmlMessagesDTO;
import com.upc.arcktech.ponlaverde.entity.HtmlMessages;

public class HtmlMessagesTransformer {
	
	
	public static HtmlMessages DTOtoEntity(HtmlMessagesDTO htmlMessagesDTO) {

		HtmlMessages htmlMessages = new HtmlMessages();

		if (htmlMessagesDTO != null) {
			htmlMessages.setIdHtml(htmlMessagesDTO.getIdHtml());
			htmlMessages.setMessage(htmlMessagesDTO.getMessage());
			htmlMessages.setServiceId(htmlMessagesDTO.getServiceId());
			htmlMessages.setSubject(htmlMessagesDTO.getSubject());
		}

		return htmlMessages;
	}

	public static HtmlMessagesDTO entityToDTO(HtmlMessages htmlMessages) {

		HtmlMessagesDTO htmlMessagesDTO = new HtmlMessagesDTO();

		if (htmlMessages != null) {
			htmlMessagesDTO.setIdHtml(htmlMessages.getIdHtml());
			htmlMessagesDTO.setMessage(htmlMessages.getMessage());
			htmlMessagesDTO.setServiceId(htmlMessages.getServiceId());
			htmlMessagesDTO.setSubject(htmlMessages.getSubject());
		}
		return htmlMessagesDTO;
	}

}
