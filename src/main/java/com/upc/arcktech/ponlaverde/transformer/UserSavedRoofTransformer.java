package com.upc.arcktech.ponlaverde.transformer;

import java.util.ArrayList;
import java.util.List;

import com.upc.arcktech.ponlaverde.dto.UserSavedRoofDTO;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoof;


public class UserSavedRoofTransformer {
	
	public static UserSavedRoof DTOtoEntity (UserSavedRoofDTO userSavedRoofDTO) {
		
		UserSavedRoof userSavedRoof = new UserSavedRoof();
		
		if (userSavedRoofDTO != null) {

			userSavedRoof.setIdUserSavedRoof(userSavedRoofDTO.getIdUserSavedRoof());
			userSavedRoof.setSavedDate(userSavedRoofDTO.getSavedDate());
			userSavedRoof.setSavedName(userSavedRoofDTO.getSavedName());
			userSavedRoof.setAesthetics(userSavedRoofDTO.getAesthetics());
			userSavedRoof.setClimate(userSavedRoofDTO.getClimate());
			userSavedRoof.setImplantation(userSavedRoofDTO.getImplantation());
			userSavedRoof.setMaintenance(userSavedRoofDTO.getMaintenance());
			userSavedRoof.setPrice(userSavedRoofDTO.getPrice());
			userSavedRoof.setRegulation(userSavedRoofDTO.getRegulation());
			userSavedRoof.setResults(userSavedRoofDTO.getResults());
			userSavedRoof.setSlope(userSavedRoofDTO.getSlope());
			userSavedRoof.setStorage(userSavedRoofDTO.getStorage());
			userSavedRoof.setThermal(userSavedRoofDTO.getThermal());
			userSavedRoof.setTraffic(userSavedRoofDTO.getTraffic());
			userSavedRoof.setWall(userSavedRoofDTO.getWall());
			userSavedRoof.setWeight(userSavedRoofDTO.getWeight());
			userSavedRoof.setWinds(userSavedRoofDTO.getWinds());
			userSavedRoof.setUserSavedRoofDetails(
					UserSavedRoofDetailsTransformer.DTOtoEntity(userSavedRoofDTO.getUserSavedRoofDetailsDTO()));
		}
		
		return userSavedRoof;
	}
	
	public static UserSavedRoofDTO entityToDTO (UserSavedRoof userSavedRoof) {
		
		UserSavedRoofDTO userSavedRoofDTO = new UserSavedRoofDTO();
		
		if (userSavedRoof != null) {

			userSavedRoofDTO.setIdUserSavedRoof(userSavedRoof.getIdUserSavedRoof());
			userSavedRoofDTO.setSavedDate(userSavedRoof.getSavedDate());
			userSavedRoofDTO.setSavedName(userSavedRoof.getSavedName());		
			userSavedRoofDTO.setAesthetics(userSavedRoof.getAesthetics());
			userSavedRoofDTO.setClimate(userSavedRoof.getClimate());
			userSavedRoofDTO.setImplantation(userSavedRoof.getImplantation());
			userSavedRoofDTO.setMaintenance(userSavedRoof.getMaintenance());
			userSavedRoofDTO.setPrice(userSavedRoof.getPrice());
			userSavedRoofDTO.setRegulation(userSavedRoof.getRegulation());
			userSavedRoofDTO.setResults(userSavedRoof.getResults());
			userSavedRoofDTO.setSlope(userSavedRoof.getSlope());
			userSavedRoofDTO.setStorage(userSavedRoof.getStorage());
			userSavedRoofDTO.setThermal(userSavedRoof.getThermal());
			userSavedRoofDTO.setTraffic(userSavedRoof.getTraffic());
			userSavedRoofDTO.setWall(userSavedRoof.getWall());
			userSavedRoofDTO.setWeight(userSavedRoof.getWeight());
			userSavedRoofDTO.setWinds(userSavedRoof.getWinds());	
			
			if(userSavedRoof.getUserSavedRoofDetails() != null) {
				userSavedRoofDTO.setUserSavedRoofDetailsDTO(
						UserSavedRoofDetailsTransformer.entityToDTO(userSavedRoof.getUserSavedRoofDetails()));
			}
			
		}
		
		return userSavedRoofDTO;
	}


	public static List<UserSavedRoofDTO> entityToDTO (List<UserSavedRoof> userSavedRoofList) {
		
		List<UserSavedRoofDTO> userSavedRoofDTOList = new ArrayList<>();
		
		if(!userSavedRoofList.isEmpty()) {
			
			for(UserSavedRoof userSaved: userSavedRoofList) {
				UserSavedRoofDTO userSavedDTO = new UserSavedRoofDTO();
				userSavedDTO = entityToDTO(userSaved);
				
				userSavedRoofDTOList.add(userSavedDTO);
			}

		}
		
		return userSavedRoofDTOList;
	}
	
	
	public static List<UserSavedRoof> DTOtoEntity (List<UserSavedRoofDTO> userSavedRoofDTOList) {
		
		List<UserSavedRoof> userSavedRoofList = new ArrayList<>();
		
		if(!userSavedRoofDTOList.isEmpty()) {
			
			for(UserSavedRoofDTO userSavedDTO: userSavedRoofDTOList) {
				UserSavedRoof userSaved = new UserSavedRoof();
				userSaved = DTOtoEntity(userSavedDTO);
				
				userSavedRoofList.add(userSaved);
			}

		}
		
		return userSavedRoofList;
	}
	
}
