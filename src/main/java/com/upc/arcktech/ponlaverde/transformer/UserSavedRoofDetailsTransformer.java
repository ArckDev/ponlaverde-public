package com.upc.arcktech.ponlaverde.transformer;

import java.util.ArrayList;
import java.util.List;

import com.upc.arcktech.ponlaverde.dto.BestRoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserSavedRoofDetailsDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoofDetails;


public class UserSavedRoofDetailsTransformer {
	
	public static UserSavedRoofDetails DTOtoEntity (UserSavedRoofDetailsDTO userSavedRoofDetailsDTO) {
		
		UserSavedRoofDetails userSavedRoofDetails = new UserSavedRoofDetails();
		
		if (userSavedRoofDetailsDTO != null) {
			
			userSavedRoofDetails.setIdUserSavedRoofDetails(userSavedRoofDetailsDTO.getIdRoofSaved());
			userSavedRoofDetails.setFinalRate(userSavedRoofDetailsDTO.getFinalRate());
			userSavedRoofDetails.setRoof(RoofTransformer.DTOtoEntity(userSavedRoofDetailsDTO.getRoofDTO()));
			userSavedRoofDetails.setAesthetics(userSavedRoofDetailsDTO.getAesthetics());
			userSavedRoofDetails.setImplantation(userSavedRoofDetailsDTO.getImplantation());
			userSavedRoofDetails.setMaintenance(userSavedRoofDetailsDTO.getMaintenance());
			userSavedRoofDetails.setPrice(userSavedRoofDetailsDTO.getPrice());
			userSavedRoofDetails.setRegulation(userSavedRoofDetailsDTO.getRegulation());
			userSavedRoofDetails.setStorage(userSavedRoofDetailsDTO.getStorage());
			userSavedRoofDetails.setThermal(userSavedRoofDetailsDTO.getThermal());
			userSavedRoofDetails.setTraffic(userSavedRoofDetailsDTO.getTraffic());
			userSavedRoofDetails.setWall(userSavedRoofDetailsDTO.getWall());
			userSavedRoofDetails.setWeight(userSavedRoofDetailsDTO.getWeight());
			userSavedRoofDetails.setWinds(userSavedRoofDetailsDTO.getWinds());	
			userSavedRoofDetails.setSelected(userSavedRoofDetailsDTO.getSelected());
		}
		
		return userSavedRoofDetails;
	}
	
	public static UserSavedRoofDetailsDTO entityToDTO (UserSavedRoofDetails userSavedRoofDetails) {
		
		UserSavedRoofDetailsDTO userSavedRoofDetailsDTO = new UserSavedRoofDetailsDTO();
		
		if (userSavedRoofDetails != null) {
			
			userSavedRoofDetailsDTO.setIdRoofSaved(userSavedRoofDetails.getIdUserSavedRoofDetails());
			userSavedRoofDetailsDTO.setFinalRate(userSavedRoofDetails.getFinalRate());
			userSavedRoofDetailsDTO.setAesthetics(userSavedRoofDetails.getAesthetics());
			userSavedRoofDetailsDTO.setImplantation(userSavedRoofDetails.getImplantation());
			userSavedRoofDetailsDTO.setMaintenance(userSavedRoofDetails.getMaintenance());
			userSavedRoofDetailsDTO.setPrice(userSavedRoofDetails.getPrice());
			userSavedRoofDetailsDTO.setRegulation(userSavedRoofDetails.getRegulation());
			userSavedRoofDetailsDTO.setStorage(userSavedRoofDetails.getStorage());
			userSavedRoofDetailsDTO.setThermal(userSavedRoofDetails.getThermal());
			userSavedRoofDetailsDTO.setTraffic(userSavedRoofDetails.getTraffic());
			userSavedRoofDetailsDTO.setWall(userSavedRoofDetails.getWall());
			userSavedRoofDetailsDTO.setWeight(userSavedRoofDetails.getWeight());
			userSavedRoofDetailsDTO.setWinds(userSavedRoofDetails.getWinds());
			userSavedRoofDetailsDTO.setSelected(userSavedRoofDetails.getSelected());
			userSavedRoofDetailsDTO.setRoofDTO(RoofTransformer.entityToDTOReduced(userSavedRoofDetails.getRoof()));
		}
		
		return userSavedRoofDetailsDTO;
	}


	public static List<UserSavedRoofDetailsDTO> entityToDTO (List<UserSavedRoofDetails> userSavedRoofDetailsList) {
		
		List<UserSavedRoofDetailsDTO> userSavedRoofDetailsDTOList = new ArrayList<>();
		
		if(!userSavedRoofDetailsList.isEmpty()) {
			
			for(UserSavedRoofDetails userSavedRoofDetails: userSavedRoofDetailsList) {
				UserSavedRoofDetailsDTO userSavedRoofDetailsDTO = new UserSavedRoofDetailsDTO();
				userSavedRoofDetailsDTO = entityToDTO(userSavedRoofDetails);
				
				userSavedRoofDetailsDTOList.add(userSavedRoofDetailsDTO);
			}
		}
		
		return userSavedRoofDetailsDTOList;
	}
	
	
	public static List<UserSavedRoofDetails> DTOtoEntity (List<UserSavedRoofDetailsDTO> userSavedRoofDetailsDTOList) {
		
		List<UserSavedRoofDetails> userSavedRoofDetailsList = new ArrayList<>();
		
		if(!userSavedRoofDetailsDTOList.isEmpty()) {
			
			for(UserSavedRoofDetailsDTO userSavedRoofDetailsDTO: userSavedRoofDetailsDTOList) {
				UserSavedRoofDetails userSavedRoofDetails = new UserSavedRoofDetails();
				userSavedRoofDetails = DTOtoEntity(userSavedRoofDetailsDTO);
			
				userSavedRoofDetailsList.add(userSavedRoofDetails);
			}
		}
		
		return userSavedRoofDetailsList;
	}
	

	public static List<UserSavedRoofDetails> bestRoofDTOtoUserSavedRoofDetails (List<BestRoofDTO> bestRoofDTOList) {
		
		List<UserSavedRoofDetails> userSavedRoofDetailsList = new ArrayList<>();

		if(!bestRoofDTOList.isEmpty()) {
			
			for(BestRoofDTO bestRoofDTO: bestRoofDTOList) {

				UserSavedRoofDetails userSavedRoofDetails = new UserSavedRoofDetails();
				
				Roof roof = new Roof();
				roof = RoofTransformer.DTOtoEntity(bestRoofDTO.getRoofDTO());
				
				userSavedRoofDetails.setFinalRate(bestRoofDTO.getRoofDTO().getRate());
				userSavedRoofDetails.setRoof(roof);
				userSavedRoofDetails.setAesthetics(bestRoofDTO.getRoofRatingDTO().getAesthetics());
				userSavedRoofDetails.setImplantation(bestRoofDTO.getRoofRatingDTO().getImplantation());
				userSavedRoofDetails.setMaintenance(bestRoofDTO.getRoofRatingDTO().getMaintenance());
				userSavedRoofDetails.setPrice(bestRoofDTO.getRoofRatingDTO().getPrice());
				userSavedRoofDetails.setRegulation(bestRoofDTO.getRoofRatingDTO().getRegulation());
				userSavedRoofDetails.setStorage(bestRoofDTO.getRoofRatingDTO().getStorage());
				userSavedRoofDetails.setThermal(bestRoofDTO.getRoofRatingDTO().getThermal());
				userSavedRoofDetails.setTraffic(bestRoofDTO.getRoofRatingDTO().getTraffic());
				userSavedRoofDetails.setWall(bestRoofDTO.getRoofRatingDTO().getWall());
				userSavedRoofDetails.setWeight(bestRoofDTO.getRoofRatingDTO().getWeight());
				userSavedRoofDetails.setWinds(bestRoofDTO.getRoofRatingDTO().getWinds());
				userSavedRoofDetails.setSelected(false);

				userSavedRoofDetailsList.add(userSavedRoofDetails);
			}
		}
		
		return userSavedRoofDetailsList;
	}
	
}
