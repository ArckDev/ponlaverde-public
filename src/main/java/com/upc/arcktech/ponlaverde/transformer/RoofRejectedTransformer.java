package com.upc.arcktech.ponlaverde.transformer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.upc.arcktech.ponlaverde.dto.RoofRejectedDTO;
import com.upc.arcktech.ponlaverde.entity.RoofRejected;


public class RoofRejectedTransformer {
	
	public static RoofRejected DTOtoEntity (RoofRejectedDTO rejectedRoofDTO) {
		
		RoofRejected rejectedRoof = new RoofRejected();
		
		if (rejectedRoofDTO != null) {

			rejectedRoof.setIdRejectedRoof(rejectedRoofDTO.getIdRejectedRoof());
			rejectedRoof.setRejectionDate(rejectedRoofDTO.getRejectionDate());
			rejectedRoof.setRejectionReason(rejectedRoofDTO.getRejectionReason());
		}
		
		return rejectedRoof;
	}
	
	public static RoofRejectedDTO entityToDTO (RoofRejected rejectedRoof) {
		
		RoofRejectedDTO rejectedRoofDTO = new RoofRejectedDTO();
		
		if (rejectedRoof != null) {

			rejectedRoofDTO.setIdRejectedRoof(rejectedRoof.getIdRejectedRoof());
			rejectedRoofDTO.setRejectionDate(rejectedRoof.getRejectionDate());
			rejectedRoofDTO.setRejectionReason(rejectedRoof.getRejectionReason());
		}
		
		return rejectedRoofDTO;
	}
	
	
	public static List<RoofRejectedDTO> entityToDTO (List<RoofRejected> roofRejectedList) {
		
		List<RoofRejectedDTO> roofRejectedDTOList = new ArrayList<>();
		
		if(!roofRejectedList.isEmpty()) {
			
			for(RoofRejected roofRejected: roofRejectedList) {
				RoofRejectedDTO roofRejectedDTO = new RoofRejectedDTO();
				roofRejectedDTO = entityToDTO(roofRejected);
				
				roofRejectedDTOList.add(roofRejectedDTO);
			}

		}
		
		return roofRejectedDTOList;
	}
	
	
	public static List<RoofRejected> DTOtoEntity (List<RoofRejectedDTO> roofRejectedDTOList) {
		
		List<RoofRejected> roofRejectedList = new ArrayList<>();
		
		if(!roofRejectedDTOList.isEmpty()) {
			
			for(RoofRejectedDTO roofRejectedDTO: roofRejectedDTOList) {
				RoofRejected roofRejected = new RoofRejected();
				roofRejected = DTOtoEntity(roofRejectedDTO);
				
				roofRejectedList.add(roofRejected);
			}

		}
		
		return roofRejectedList;
	}
	

	public static String buildRejectedReason(RoofRejected rejectedRoof) {
		
		String rejectedReason = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy (HH:mm)");
		
		if(rejectedRoof != null) {
			StringBuilder builder = new StringBuilder();
			builder.append(sdf.format(rejectedRoof.getRejectionDate()));
			builder.append(" - ");
			builder.append(rejectedRoof.getRejectionReason());
			
			rejectedReason = builder.toString();
		}
		
		return rejectedReason;
	}
}
