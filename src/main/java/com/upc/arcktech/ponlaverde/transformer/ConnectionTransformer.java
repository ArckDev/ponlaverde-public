package com.upc.arcktech.ponlaverde.transformer;

import com.upc.arcktech.ponlaverde.dto.ConnectionDTO;
import com.upc.arcktech.ponlaverde.entity.Connection;


public class ConnectionTransformer {
	
	
	public static Connection DTOtoEntity (ConnectionDTO connectionDTO) {
		
		Connection connection = new Connection();
		
		if (connectionDTO != null) {
			connection.setAccessDate(connectionDTO.getAccessDate());
			connection.setIdConnection(connectionDTO.getIdConnection());
			connection.setIpAccess(connectionDTO.getIpAccess());
			connection.setCity(connectionDTO.getCity());
			connection.setCountry(connectionDTO.getCountry());
			connection.setDeviceModel(connectionDTO.getDeviceModel());
			connection.setDeviceType(connectionDTO.getDeviceType());
			connection.setOs(connectionDTO.getOs());
		}
		
		return connection;	
	}

	public static ConnectionDTO entityToDTO (Connection connection) {
		
		ConnectionDTO connectionDTO = new ConnectionDTO();
		
		if (connection != null) {
			connectionDTO.setAccessDate(connection.getAccessDate());
			connectionDTO.setIdConnection(connection.getIdConnection());
			connectionDTO.setIpAccess(connection.getIpAccess());
			connectionDTO.setCity(connection.getCity());
			connectionDTO.setCountry(connection.getCountry());
			connectionDTO.setDeviceModel(connection.getDeviceModel());
			connectionDTO.setDeviceType(connection.getDeviceType());
			connectionDTO.setOs(connection.getOs());
		}
		
		return connectionDTO;	
	}
	
}
