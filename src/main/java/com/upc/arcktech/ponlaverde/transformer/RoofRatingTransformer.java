package com.upc.arcktech.ponlaverde.transformer;


import java.util.ResourceBundle;

import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.RoofRatingDTO;


public class RoofRatingTransformer {
	
	private static ResourceBundle roofMappingBundle = ResourceBundle.getBundle("parameters/roofMapping");
	
	public static RoofRatingDTO roofToRoofRatingMapping (RoofDTO roofDTO) {

		RoofRatingDTO roofRatingDTO = new RoofRatingDTO();
		
		if (roofDTO != null) {
			
			roofRatingDTO.setIdRoof(roofDTO.getIdRoof());
			roofRatingDTO.setAesthetics(Integer.valueOf(roofMappingBundle.getString(roofDTO.getAesthetics())));
			roofRatingDTO.setImplantation(Integer.valueOf(roofMappingBundle.getString(roofDTO.getImplantation())));
			roofRatingDTO.setMaintenance(Integer.valueOf(roofMappingBundle.getString(roofDTO.getMaintenance())));
			roofRatingDTO.setRegulation(Integer.valueOf(roofMappingBundle.getString(roofDTO.getRegulation())));
			roofRatingDTO.setStorage(Integer.valueOf(roofMappingBundle.getString(roofDTO.getStorage())));
			roofRatingDTO.setThermal(Integer.valueOf(roofMappingBundle.getString(roofDTO.getThermal())));
			roofRatingDTO.setTraffic(Integer.valueOf(roofMappingBundle.getString(roofDTO.getTraffic())));
			roofRatingDTO.setWinds(Integer.valueOf(roofMappingBundle.getString(roofDTO.getWinds())));

		}
		
		return roofRatingDTO;
	}

}
