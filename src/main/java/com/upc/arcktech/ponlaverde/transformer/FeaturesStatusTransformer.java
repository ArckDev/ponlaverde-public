package com.upc.arcktech.ponlaverde.transformer;

import java.util.ArrayList;
import java.util.List;

import com.upc.arcktech.ponlaverde.dto.FeatureStatusDTO;
import com.upc.arcktech.ponlaverde.entity.FeatureStatus;


public class FeaturesStatusTransformer {
	
	
	public static FeatureStatus DTOtoEntity (FeatureStatusDTO featuresStatusDTO) {
		
		FeatureStatus featuresStatus = new FeatureStatus();
		
		if (featuresStatusDTO != null) {
			featuresStatus.setName(featuresStatusDTO.getName());
			featuresStatus.setActive(featuresStatusDTO.getActive());
		}
		
		return featuresStatus;	
	}

	public static FeatureStatusDTO entityToDTO (FeatureStatus featuresStatus) {
		
		FeatureStatusDTO featuresStatusDTO = new FeatureStatusDTO();
		
		if (featuresStatus != null) {
			featuresStatusDTO.setName(featuresStatus.getName());
			featuresStatusDTO.setActive(featuresStatus.getActive());
		}
		
		return featuresStatusDTO;	
	}
	
	public static List<FeatureStatusDTO> entityToDTO (List<FeatureStatus> featuresStatusList) {
		
		List<FeatureStatusDTO> featuresStatusDTOList = new ArrayList<>();
		
		if(!featuresStatusList.isEmpty()) {
			for(FeatureStatus featuresStatus: featuresStatusList) {
				FeatureStatusDTO featuresStatusDTO = new FeatureStatusDTO();
				featuresStatusDTO = entityToDTO(featuresStatus);
				
				featuresStatusDTOList.add(featuresStatusDTO);
			}
		}		
		return featuresStatusDTOList;
	}
	
	public static List<FeatureStatus> DTOtoEntity (List<FeatureStatusDTO> featuresStatusDTOList) {
		
		List<FeatureStatus> featuresStatusList = new ArrayList<>();
		
		if(!featuresStatusDTOList.isEmpty()) {
			for(FeatureStatusDTO featuresStatusDTO: featuresStatusDTOList) {
				FeatureStatus featuresStatus = new FeatureStatus();
				featuresStatus = DTOtoEntity(featuresStatusDTO);
				
				featuresStatusList.add(featuresStatus);
			}
		}		
		return featuresStatusList;
	}
	
}
