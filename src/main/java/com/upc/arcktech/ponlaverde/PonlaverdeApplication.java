package com.upc.arcktech.ponlaverde;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.upc.arcktech.ponlaverde.dao.RoofDAO;
import com.upc.arcktech.ponlaverde.dao.impl.RoofDAOImpl;
import com.upc.arcktech.ponlaverde.service.FileFolderService;
import com.upc.arcktech.ponlaverde.service.HtmlMessagesService;
import com.upc.arcktech.ponlaverde.service.MailService;
import com.upc.arcktech.ponlaverde.service.RecommenderService;
import com.upc.arcktech.ponlaverde.service.RoofService;
import com.upc.arcktech.ponlaverde.service.UserTempService;
import com.upc.arcktech.ponlaverde.service.UserService;
import com.upc.arcktech.ponlaverde.service.impl.FileFolderServiceImpl;
import com.upc.arcktech.ponlaverde.service.impl.HtmlMessagesServiceImpl;
import com.upc.arcktech.ponlaverde.service.impl.MailServiceImpl;
import com.upc.arcktech.ponlaverde.service.impl.RecommenderServiceImpl;
import com.upc.arcktech.ponlaverde.service.impl.RoofServiceImpl;
import com.upc.arcktech.ponlaverde.service.impl.UserTempServiceImpl;
import com.upc.arcktech.ponlaverde.service.impl.UserServiceImpl;



@SpringBootApplication
public class PonlaverdeApplication extends SpringBootServletInitializer {

	@Bean
	static UserService userService() {
		return new UserServiceImpl();
	}
	
	@Bean
	static MailService mailService() {
		return new MailServiceImpl();
	}
	
	@Bean
	static HtmlMessagesService htmlMessagesService() {
		return new HtmlMessagesServiceImpl();
	}
	
	@Bean
	static UserTempService userTempService() {
		return new UserTempServiceImpl();
	}
	
	@Bean
	static BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	static RecommenderService recommenderService() {
		return new RecommenderServiceImpl();
	}
	
	@Bean
	static RoofService roofService() {
		return new RoofServiceImpl();
	}
	
	@SuppressWarnings("rawtypes")
	@Bean
	static RoofDAO roofDAO() {
		return new RoofDAOImpl();
	}
	
	@Bean
	static FileFolderService fileFolderService() {
		return new FileFolderServiceImpl();
	}
	

	@PostConstruct
	public void init() {
		// Setting the default time zone for all the app
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PonlaverdeApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(PonlaverdeApplication.class, args);
	}
	
}