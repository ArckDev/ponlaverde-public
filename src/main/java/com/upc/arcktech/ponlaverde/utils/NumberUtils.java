package com.upc.arcktech.ponlaverde.utils;


public class NumberUtils {
	
	public static Integer getBiggerNumber(Integer value1, Integer value2) {
		
		Integer bigger = null;
		
		if(value1 != null && value2 != null) {
			
			if(value1 > value2) {
				bigger = value1;
			} else {
				bigger = value2;
			}
			
		} else if (value1 == null) {
			
			bigger = value2;
			
		} else if (value2 == null) {
			
			bigger = value1;
		}
		

		return bigger;
	}
	
	public static Integer getLowerNumber(Integer value1, Integer value2) {
		
		Integer lower = null;
		
		if(value1 != null && value2 != null) {
			
			if(value1 < value2) {
				lower = value1;
			} else {
				lower = value2;
			}
			
		} else if (value1 == null) {
			
			lower = value2;
			
		} else if (value2 == null) {
			
			lower = value1;
		}


		return lower;
	}
	
	
	public static Integer getBiggerNumberAndRoundUp(Double value1, Double value2) {
		
		Integer bigger = null;
		
		if(value1 != null && value2 != null) {
			
			if(value1 > value2) {
				bigger = (int) Math.ceil(value1);
			} else {
				bigger = (int) Math.ceil(value2);
			}
			
		} else if (value1 == null) {
			
			bigger = (int) Math.ceil(value2);
			
		} else if (value2 == null) {
			
			bigger = (int) Math.ceil(value1);
		}

		
		return bigger;
	}
	
	public static Integer getLowerNumberAndRoundUp(Double value1, Double value2) {
		
		Integer lower = null;
		
		
		if(value1 != null && value2 != null) {
			
			if(value1 < value2) {
				lower = (int) Math.ceil(value1);
			} else {
				lower = (int) Math.ceil(value2);
			}
			
		} else if (value1 == null) {
			
			lower = (int) Math.ceil(value2);
			
		} else if (value2 == null) {
			
			lower = (int) Math.ceil(value1);
		}


		return lower;
	}

}
