package com.upc.arcktech.ponlaverde.utils;


import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;

public class LocaleUtils {
	
	public static PropertiesFactoryBean setLocaleList(String listName) {
		
		PropertiesFactoryBean bean = new PropertiesFactoryBean();
		
		String lang  = LocaleContextHolder.getLocale().getLanguage();
        
        if ("es".equals(lang) || "ca".equals(lang) || "en".equals(lang) ) {
        	 bean.setLocation(new ClassPathResource(listName+"_"+lang+".properties"));
        } else {
        	bean.setLocation(new ClassPathResource(listName+"_en.properties"));
        }
        
		return bean;
	}

}
