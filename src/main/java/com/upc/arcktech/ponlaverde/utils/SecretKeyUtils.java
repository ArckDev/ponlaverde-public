/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.upc.arcktech.ponlaverde.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 *
 * @author Jose
 */
public class SecretKeyUtils {
	
	private static final String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    
    public static String generateHashedRandomString() {
        
    	StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        
        String md5Random = passwordToMd5(saltStr);
        
        return md5Random;
    }
    
    public static String generateRandomString(Integer length) {
        
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        
        return saltStr;
    }

    private static String passwordToMd5 (String password){
    	
    	String md5Password = "";
    	
        if (password != null){
            try {
             MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
             digest.update(password.toString().getBytes());
             byte messageDigest[] = digest.digest();

             // Create Hex String
             StringBuffer hexString = new StringBuffer();
             for (int i=0; i<messageDigest.length; i++)
                 hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
                 md5Password = hexString.toString();
                 
         } catch (NoSuchAlgorithmException e) {
             e.printStackTrace();
         }
       }

        return md5Password;
    }
    
}
