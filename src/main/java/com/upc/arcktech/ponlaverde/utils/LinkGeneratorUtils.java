package com.upc.arcktech.ponlaverde.utils;

import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author Jose
 */
public class LinkGeneratorUtils {
    

    public static String linkActivateAccount (String username, String sKey, HttpServletRequest request){
        
    	String scheme = request.getScheme();
    	String host = request.getHeader("Host");        // includes server name and server port
    	String contextPath = request.getContextPath();  // includes leading forward slash

    	String resultPath = scheme + "://" + host + contextPath;
    	
    	String link = resultPath+"/"+"accountVerification?email=" + username + "&sKey="+sKey;

        return link;
    }
    
    public static String linkRestorePassword (String username, String sKey, HttpServletRequest request){
        
    	String scheme = request.getScheme();
    	String host = request.getHeader("Host");        // includes server name and server port
    	String contextPath = request.getContextPath();  // includes leading forward slash

    	String resultPath = scheme + "://" + host + contextPath;
    	
    	String link = resultPath+"/"+"restore-password-confirm?email=" + username + "&sKey="+sKey;

        return link;
    }
    
    
    
    
}

