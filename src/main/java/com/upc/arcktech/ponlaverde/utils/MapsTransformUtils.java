package com.upc.arcktech.ponlaverde.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.context.i18n.LocaleContextHolder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MapsTransformUtils {
	
	public static Map<String, String> sortByValue(Map<String, String> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, String>> list = new LinkedList<Map.Entry<String, String>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, String>>() {
            public int compare(Map.Entry<String, String> o1,
                               Map.Entry<String, String> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, String> sortedMap = new LinkedHashMap<String, String>();
        for (Map.Entry<String, String> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
	
	public static Map<Long, Integer> sortByValueDescWithLimit(Map<Long, Integer> unsortMap, Integer limit) {

        // 1. Convert Map to List of Map
        List<Map.Entry<Long, Integer>> list = new LinkedList<Map.Entry<Long, Integer>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<Long, Integer>>() {
            public int compare(Map.Entry<Long, Integer> o1,
                               Map.Entry<Long, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<Long, Integer> sortedMap = new LinkedHashMap<Long, Integer>();
        
		if (limit != null) {
			for (int i = 0; i < limit; i++) {
				Map.Entry<Long, Integer> entry = list.get(i);
				sortedMap.put(entry.getKey(), entry.getValue());
			}
		} else {
			for (Map.Entry<Long, Integer> entry: list) {
				sortedMap.put(entry.getKey(), entry.getValue());
			}
        }

        return sortedMap;
    }
	
	public static Map<String, Double> sortByValueDoubleDescWithLimit(Map<String, Double> unsortMap, Integer limit) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> o1,
                               Map.Entry<String, Double> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        
		if (limit != null) {
			for (int i = 0; i < limit; i++) {
				Map.Entry<String, Double> entry = list.get(i);
				sortedMap.put(entry.getKey(), entry.getValue());
			}
		} else {
			for (Map.Entry<String, Double> entry: list) {
				sortedMap.put(entry.getKey(), entry.getValue());
			}
        }

        return sortedMap;
    }
	
	
	public static Map<String, String> sortByKey(Map<String, String> unsortMap) {
	
		Map<String, String> treeMap = new TreeMap<String, String>(unsortMap);
		
		return treeMap;
	}
	
	
	public static SortedMap<String, String> localeRecourceToMap(String list) {
		
		Locale locale = LocaleContextHolder.getLocale();		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(list, locale);	
		SortedMap<String, String> map = mapping(resourceBundle);

		return map;
	}
	
	public static SortedMap<String, String> recourceToMap(String list) {

		ResourceBundle resourceBundle = ResourceBundle.getBundle(list);
		SortedMap<String, String> map = mapping(resourceBundle);

		return map;
	}

	public static SortedMap<String, Integer> recourceToIntegerMap(String list) {
		
		SortedMap<String, Integer> map = new TreeMap<String, Integer>();		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(list);
		Enumeration<String> keys = resourceBundle.getKeys();
		
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			Integer value = Integer.parseInt(resourceBundle.getString(key));
			
			map.put(key, value);
		}

		return map;
	}
	
	public static SortedMap<String, Double> recourceToDoubleMap(String list) {
		
		SortedMap<String, Double> map = new TreeMap<String, Double>();		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(list);
		Enumeration<String> keys = resourceBundle.getKeys();
		
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			Double value = Double.parseDouble(resourceBundle.getString(key));
			
			map.put(key, value);
		}

		return map;
	}
	
	@SuppressWarnings("unchecked")
	public static SortedMap<String, Object> fromObjectToSortedMap(Object object){
		
        ObjectMapper oMapper = new ObjectMapper(); 
		SortedMap<String, Object> sortedMap = oMapper.convertValue(object, SortedMap.class);	
		
		return sortedMap;
		
	}
	
	@SuppressWarnings("unchecked")
	public static SortedMap<String, Integer> convertObjectToIntegerSortedMap(Object object){
		
		ObjectMapper oMapper = new ObjectMapper(); 
		SortedMap<String, Integer> sortedMap = oMapper.convertValue(object, SortedMap.class);

		return sortedMap;
	}
	
	@SuppressWarnings("unchecked")
	public static SortedMap<String, Double> convertObjectToDoubleSortedMap(Object object){
		
		ObjectMapper oMapper = new ObjectMapper();      
		SortedMap<String, Double> sortedMap = oMapper.convertValue(object, SortedMap.class);
		
		return sortedMap;
	}
	
	
	private static SortedMap<String, String> mapping(ResourceBundle resourceBundle) {
		
		SortedMap<String, String> map = new TreeMap<String, String>();
		
		Enumeration<String> keys = resourceBundle.getKeys();
		
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			String value = resourceBundle.getString(key);
			
			map.put(key, value);
		}
		return map;
	}
	

}
