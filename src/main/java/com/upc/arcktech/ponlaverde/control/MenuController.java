package com.upc.arcktech.ponlaverde.control;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.upc.arcktech.ponlaverde.service.AccessService;


/**
 * It paths to specific menu of each kind of user
 * 
 * @author jose.rodriguez
 *
 */
@Controller
@Scope("session")
public class MenuController extends BaseController {
	
	@Autowired
	AccessService accessService;
	
	/**
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/admin/menu")
	public String adminMenu(HttpServletRequest request) throws IOException {
		
		accessService.addAccess(request);

		return "admin/menu/menu";
	}
	
	
	/**
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/individual/menu")
	public String individualMenu(HttpServletRequest request) throws IOException {
		
		accessService.addAccess(request);

		return "individual/menu/menu";
	}
	
	/**
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/supplier/menu")
	public String supplierMenu(HttpServletRequest request) throws IOException {
		
		accessService.addAccess(request);

		return "supplier/menu/menu";
	}


	
}