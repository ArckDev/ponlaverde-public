package com.upc.arcktech.ponlaverde.control;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.Modals;
import com.upc.arcktech.ponlaverde.constants.Parameters;
import com.upc.arcktech.ponlaverde.constants.RoofStatus;
import com.upc.arcktech.ponlaverde.dto.PagedResultsDTO;
import com.upc.arcktech.ponlaverde.dto.ParameterDTO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.service.ParameterService;
import com.upc.arcktech.ponlaverde.service.RoofService;

@Controller
@Scope("session")
@PreAuthorize("@featureStatus.isActive('ERR')")
public class ReportedErrorController extends BaseController {
	
	@Autowired
	RoofService roofService;
	@Autowired
	ParameterService parameterService;
	
	
	
	/* ################################################################################################################## */
	/* #############################################        ADMIN        ################################################ */
	/* ################################################################################################################## */
	
	@RequestMapping("/admin/error/")
	public String adminErrorStoreAccess(HttpServletRequest request) {

		storeUsedFeature(request, FeatureName.ERROR);
		
		return "redirect:/admin/error/1";
	}
	
	
	@RequestMapping("/admin/error/{page}")
	public ModelAndView adminErrorList(@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required=false) String message,
			@PathVariable("page") Integer page) {

		Pageable pageable = paginationLstHandling(page);
		ModelAndView reported = new ModelAndView("admin/error/pending");
		this.adminErrorInit(reported, pageable);
		
		setupModal(modalType, message, reported);
		setNavigationParams(page, null);

		return reported;
	}

	@RequestMapping("/admin/error/details")
	public ModelAndView adminErrorDetails(RoofDTO roofDTO) {
		
		ModelAndView reported = new ModelAndView("admin/error/pending-details");

		this.errorDetails(roofDTO, reported);
	
		return reported;
	}

	@RequestMapping("/admin/error/discard")
	public String adminErrorDiscard(RoofDTO roofDTO, RedirectAttributes ra) {
		
		roofService.discardError(roofDTO);
		this.errorDiscard(ra);
		
		return "redirect:/admin/error/"+lastPageSel;
	}
	
	@RequestMapping("/admin/error/edit")
	public ModelAndView adminErrorRoofEdit(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("admin/error/pending-edit");
		roofDetails(roofDTO, roofDetails);
		roofDefaultValues(roofDetails);
		
		return roofDetails;
	}
	
	@RequestMapping("/admin/error/update")
	public String adminErrorRoofUpdate(RoofDTO roofDTO, RedirectAttributes ra)  {

		//TODO Edit IMAGES Update
		
		// SET THE LIST WITH IMAGES AND SHEETS
//		roofDTO.setUploadedImagesList(uploadedImagesList);
//		roofDTO.setUploadedDetailsList(uploadedDetailsList);
//		roofDTO.setUploadedSheetsList(uploadedSheetsList);

		updateRoofValidatedAndDiscardErrors(roofDTO, ra);
		
		return "redirect:/admin/error/"+lastPageSel;
	}


	/* ################################################################################################################## */
	/* #############################################       SUPPLIER      ################################################ */
	/* ################################################################################################################## */

	@RequestMapping("/supplier/error/")
	public String supplierErrorStoreAccess(HttpServletRequest request) {

		storeUsedFeature(request, FeatureName.ERROR);
		
		return "redirect:/supplier/error/1";
	}
	
	@RequestMapping("/supplier/error/{page}")
	public ModelAndView supplierErrorList(@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required=false) String message,
			@PathVariable("page") Integer page) {
		
		Pageable pageable = paginationLstHandling(page);
		ModelAndView reported = new ModelAndView("supplier/error/pending");
		this.supplierErrorInit(reported, pageable);
		
		setupModal(modalType, message, reported);
		setNavigationParams(page, null);

		return reported;
	}

	@RequestMapping("/supplier/error/details")
	public ModelAndView supplierErrorDetails(RoofDTO roofDTO) {
		
		ModelAndView reported = new ModelAndView("supplier/error/pending-details");
		this.errorDetails(roofDTO, reported);
		
		return reported;
	}
	
	@RequestMapping("/supplier/error/discard")
	public String supplierErrorDiscard(RoofDTO roofDTO, RedirectAttributes ra) {
		
		roofService.discardError(roofDTO);
		this.errorDiscard(ra);
		
		return "redirect:/supplier/error/"+lastPageSel;
	}
	
	@RequestMapping("/supplier/error/edit")
	public ModelAndView supplierErrorRoofEdit(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("supplier/error/pending-edit");
		roofDetails(roofDTO, roofDetails);
		roofDefaultValues(roofDetails);
		
		return roofDetails;
	}
	
	@RequestMapping("/supplier/error/update")
	public String supplierErrorRoofUpdate(RoofDTO roofDTO, RedirectAttributes ra)  {

		ParameterDTO requiredValidation = parameterService.findByParameter(Parameters.REQUIRED_VALIDATION);
		roofDTO.setMaker(this.loggedUserInfo().getName());
		
		//TODO Edit IMAGES Update
		
		// SET THE LIST WITH IMAGES AND SHEETS
//		roofDTO.setUploadedImagesList(uploadedImagesList);
//		roofDTO.setUploadedDetailsList(uploadedDetailsList);
//		roofDTO.setUploadedSheetsList(uploadedSheetsList);

		if (!requiredValidation.getActive()) {
			updateRoofValidatedAndDiscardErrors(roofDTO, ra);
		} else {
			updateRoofPendingAndDiscardErrors(roofDTO, ra);
		}
		
		return "redirect:/supplier/error/"+lastPageSel;
	}


	
	/* ################################################################################################################## */
	/* #############################################       INDIVIDUAL      ############################################## */
	/* ################################################################################################################## */

	@RequestMapping("/individual/error/report-error")
	public String reportAnError(RoofDTO roofDTO, RedirectAttributes ra) {

		roofService.reportError(roofDTO);

		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "error.reported");
		
		return "redirect:/individual/catalogue/1";
	}
	
	
	
	/* ################################################################################################################## */
	/* #############################################          AUX        ################################################ */
	/* ################################################################################################################## */

	private void adminErrorInit(ModelAndView reported, Pageable pageable) {
		
		PagedResultsDTO roofList = roofService.findRoofsWithErrors(pageable);
		
		reported.addObject("roofList", roofList);
	}
	
	
	private void supplierErrorInit(ModelAndView reported, Pageable pageable) {
		
		PagedResultsDTO roofList = roofService.findOwnedRoofsWithErrors(this.loggedUserInfo(), pageable);
		
		reported.addObject("roofList", roofList);
	}

	
	private void errorDetails(RoofDTO roofDTO, ModelAndView reported) {
		
		RoofDTO roof = roofService.findRoofById(roofDTO.getIdRoof());
		roofService.getLinkedResources(roof);

		reported.addObject("userDTO", loggedUserInfo());
		reported.addObject("roofDTO", roof);
	}
	
	
	private void errorDiscard(RedirectAttributes ra) {
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "error.discarded");
	}


	private void updateRoofValidatedAndDiscardErrors(RoofDTO roofDTO, RedirectAttributes ra) {
		
		roofService.updateRoof(roofDTO, RoofStatus.VALIDATED);
		roofService.discardError(roofDTO);
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.updated.discarded");
	}
	
	private void updateRoofPendingAndDiscardErrors(RoofDTO roofDTO, RedirectAttributes ra) {
		
		roofService.updateRoof(roofDTO, RoofStatus.PENDING);
		roofService.discardError(roofDTO);
		ra.addAttribute("modalType", Modals.WARNING_MODAL.toString());
		ra.addAttribute("message", "roof.pending-updated.discarded");
	}
	
}
