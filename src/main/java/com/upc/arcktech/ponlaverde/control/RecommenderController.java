package com.upc.arcktech.ponlaverde.control;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.Modals;
import com.upc.arcktech.ponlaverde.dto.BestRoofDTO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserRoofRatingDTO;
import com.upc.arcktech.ponlaverde.dto.UserSavedRoofDTO;
import com.upc.arcktech.ponlaverde.service.ParameterService;
import com.upc.arcktech.ponlaverde.service.RecommenderService;
import com.upc.arcktech.ponlaverde.service.RoofService;
import com.upc.arcktech.ponlaverde.utils.MapsTransformUtils;

@Controller
@Scope("session")
@PreAuthorize("@featureStatus.isActive('REC')")
public class RecommenderController extends BaseController {
	
	@Autowired
	RoofService roofService;
	@Autowired
	ParameterService parameterService;
	@Autowired
	RecommenderService recommenderService;
	
	private List<BestRoofDTO> bestRoofs = new ArrayList<>();
	private String json = "";
	private UserRoofRatingDTO userRoofRatingNav = new UserRoofRatingDTO();
	private static final ObjectMapper mapper = new ObjectMapper();
	
	
	/* ################################################################################################################## */
	/* #############################################       INDIVIDUAL      ############################################## */
	/* ################################################################################################################## */

	@RequestMapping("/individual/recommender")
	public ModelAndView recommender(HttpServletRequest request) {
		
		storeUsedFeature(request, FeatureName.RECOMMENDER);

		ModelAndView recommender = new ModelAndView("individual/recommender/recommender");
		roofDefaultValues(recommender);
		
		recommender.addObject("results", MapsTransformUtils.recourceToMap("parameters/recommenderListNumber"));
		recommender.addObject("visibility", "hidden");

		return recommender;
	}
	
	@RequestMapping("/individual/recommender-success")
	public ModelAndView recommenderModal(@RequestParam("modalType") String modalType, @RequestParam("message") String message) {

		ModelAndView recommender = new ModelAndView("individual/recommender/recommender");
		roofDefaultValues(recommender);
		
		recommender.addObject("results", MapsTransformUtils.recourceToMap("parameters/recommenderListNumber"));
		recommender.addObject("visibility", "hidden");
		
		modalWithMessage(modalType, message, recommender);
		
		return recommender;
	}
	
	@RequestMapping("/individual/recommender/start")
	public ModelAndView startRecommender(UserRoofRatingDTO userRoofRatingDTO) throws JsonProcessingException {

		ModelAndView recommender = new ModelAndView("individual/recommender/recommender");
		
		if(userRoofRatingDTO.getAesthetics() != null) {
			userRoofRatingNav = userRoofRatingDTO;
		}
		
		bestRoofs = recommenderService.recommenderProcess(loggedUserInfo(), userRoofRatingNav);
		
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		json = mapper.writeValueAsString(bestRoofs);

		roofDefaultValues(recommender);
		recommender.addObject("results", MapsTransformUtils.recourceToMap("parameters/recommenderListNumber"));
		recommender.addObject("bestRoofs", bestRoofs);
		recommender.addObject("userRoofRating", userRoofRatingNav);
		recommender.addObject("visibility", "visible");
		recommender.addObject("chartData", json);

		return recommender;
	}
	
	@RequestMapping("/individual/recommender/details")
	public ModelAndView individualRecommenderDetails(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("individual/recommender/roof-details");

		roofDetails(roofDTO, roofDetails);
		roofDetails.addObject("userDTO", loggedUserInfo());
		roofDetails.addObject("returnPath", "/individual/recommender/return");
		
		return roofDetails;
	}
	
	@RequestMapping("/individual/recommender/return")
	public ModelAndView individualRecommenderReturn() {

		ModelAndView recommender = new ModelAndView("individual/recommender/recommender");
		roofDefaultValues(recommender);

		recommender.addObject("results", MapsTransformUtils.recourceToMap("parameters/recommenderListNumber"));
		recommender.addObject("bestRoofs", bestRoofs);
		recommender.addObject("userRoofRating", userRoofRatingNav);
		recommender.addObject("visibility", "visible");
		recommender.addObject("chartData", json);

		return recommender;
	}
	
	@RequestMapping("/individual/recommender/save")
	public String saveRecommendation(UserSavedRoofDTO userSavedRoofDTO, @RequestParam String inputText,
			RedirectAttributes ra) {

		userSavedRoofDTO.setSavedName(inputText);
		recommenderService.saveRecommendation(bestRoofs, userSavedRoofDTO, loggedUserInfo());

		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "rec.saved-files");
		
		return "redirect:/individual/recommender-success";
	}

}
