package com.upc.arcktech.ponlaverde.control;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.Modals;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UsedFeatureDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.service.RoofService;
import com.upc.arcktech.ponlaverde.service.UsedFeatureService;
import com.upc.arcktech.ponlaverde.service.UserService;
import com.upc.arcktech.ponlaverde.utils.MapsTransformUtils;

@Controller
@Scope("session")
public class BaseController {
	
	@Autowired
	UserService userService;
	@Autowired
	RoofService roofService;
	@Autowired
	UsedFeatureService usedFeatureService;
	
	@Value("${max.records.page.squares}")
	private Integer maxRecordsPageSquares;
	@Value("${max.records.page.list}")
	private Integer maxRecordsPageList;
	
	protected Integer lastPageSel;
	protected String lastPath;

	
	protected String errorsBundle = "i18n/errors";
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	
    /**
	 * @param request
	 * @param modelAndView
	 */
	protected void modalWithMessage(String modalType, String message, ModelAndView modelAndView) {

		if(modalType != null) {
			modelAndView.addObject("onLoadModal", modalType);
		} else {
			modelAndView.addObject("onLoadModal", Modals.SUCCESS_MODAL.toString());
		}

		if(message != null) {
			modelAndView.addObject("key", message);
		} else {
			modelAndView.addObject("key", "action.completed");
		}
		
	}
	
	protected void setupModal(String modalType, String message, ModelAndView catalogue) {
		if(modalType != null) {
			modalWithMessage(modalType, message, catalogue);
		}
	}
	
	
	/**
	 * @return
	 */
	protected UserDTO loggedUserInfo() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDTO userDTO = userService.getUserByUsername(auth.getName());
		
		return userDTO;
	}
	
	/**
	 * @param roof
	 */
	protected void roofDefaultValues(ModelAndView roof) {

		roof.addObject("climate", MapsTransformUtils.localeRecourceToMap("list/climate"));
		roof.addObject("type", MapsTransformUtils.localeRecourceToMap("list/roofType"));
		roof.addObject("maintenance", MapsTransformUtils.localeRecourceToMap("list/roofMaintenance"));
		roof.addObject("rating", MapsTransformUtils.localeRecourceToMap("list/roofRating"));
		roof.addObject("regulation", MapsTransformUtils.localeRecourceToMap("list/roofRegulation"));
		roof.addObject("slope", MapsTransformUtils.localeRecourceToMap("list/roofSlope"));
		roof.addObject("storage", MapsTransformUtils.localeRecourceToMap("list/roofStorage"));
		roof.addObject("traffic", MapsTransformUtils.localeRecourceToMap("list/roofTrafficability"));
		roof.addObject("userDTO", loggedUserInfo());
	}
	
	
	protected Pageable paginationSqrHandling(Integer page) {
		
		Pageable pageable;
		
		if(page != null) {
			pageable = new PageRequest(page-1, maxRecordsPageSquares);
		} else {
			pageable = new PageRequest(0, maxRecordsPageSquares);
		}

		return pageable;
	}
	
	protected Pageable paginationLstHandling(Integer page) {
		
		Pageable pageable;
		
		if(page != null) {
			pageable = new PageRequest(page-1, maxRecordsPageList);
		} else {
			pageable = new PageRequest(0, maxRecordsPageList);
		}

		return pageable;
	}
	
	protected void setNavigationParams(Integer page, String type) {

		this.lastPageSel = page;
		this.lastPath = type;
	}
	
	protected void roofDetails(RoofDTO roofDTO, ModelAndView roofDetails) {
		
		RoofDTO roof = roofService.findRoofById(roofDTO.getIdRoof());
		this.roofService.getLinkedResources(roof);

		roofDetails.addObject("roofDTO", roof);
	}
	
	protected void storeUsedFeature(HttpServletRequest request, FeatureName featureName) {
		UsedFeatureDTO usedFeatureDTO = new UsedFeatureDTO();
		usedFeatureDTO.setServiceName(featureName.getCode());
		usedFeatureDTO.setSession(request.getRequestedSessionId());
		usedFeatureDTO.setUserDTO(loggedUserInfo());
		usedFeatureDTO.setAccessDate(new Date());
		
		usedFeatureService.addAccess(usedFeatureDTO);
	}

}
