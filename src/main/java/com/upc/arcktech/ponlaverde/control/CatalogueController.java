package com.upc.arcktech.ponlaverde.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.Modals;
import com.upc.arcktech.ponlaverde.constants.Parameters;
import com.upc.arcktech.ponlaverde.constants.ResponseCode;
import com.upc.arcktech.ponlaverde.constants.RoofStatus;
import com.upc.arcktech.ponlaverde.dto.AjaxResponseBodyDTO;
import com.upc.arcktech.ponlaverde.dto.PagedResultsDTO;
import com.upc.arcktech.ponlaverde.dto.ParameterDTO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.RoofLimitValuesDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.service.ParameterService;
import com.upc.arcktech.ponlaverde.service.RoofService;
import com.upc.arcktech.ponlaverde.service.UserService;

/**
 * It paths to specific menu of each kind of user
 * 
 * @author jose.rodriguez
 *
 */
@Controller
@Scope("session")
@PreAuthorize("@featureStatus.isActive('CAT')")
public class CatalogueController extends BaseController {

	@Autowired
	UserService userService;
	@Autowired
	RoofService roofService;
	@Autowired
	ParameterService parameterService;


	@Value("${root.file-path}")
	private String rootPath;
	@Value("${max.records.page.squares}")
	private Integer maxRecordsPageSquares;

	private List<String> uploadedImagesList = new ArrayList<>();
	private List<String> uploadedDetailsList = new ArrayList<>();
	private List<String> uploadedSheetsList = new ArrayList<>();
	
	private RoofDTO navRoofDTO;


	/* ################################################################################################################## */
	/* #############################################        ADMIN        ################################################ */
	/* ################################################################################################################## */

	
	@RequestMapping("/admin/catalogue/")
	public String adminCatalogueStoreAccess(HttpServletRequest request) {

		storeUsedFeature(request, FeatureName.CATALOGUE);
		
		return "redirect:/admin/catalogue/1";
	}

	@RequestMapping("/admin/catalogue/{page}")
	public ModelAndView adminCatalogue(@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required=false) String message,
			@PathVariable("page") Integer page) {

		Pageable pageable = paginationSqrHandling(page);
		ModelAndView catalogue = adminCatalogueInit(pageable);
		
		setupModal(modalType, message, catalogue);
		setNavigationParams(page, "/admin/catalogue/");
		
		return catalogue;
	}

	
	@RequestMapping("/admin/catalogue/filter/{page}")
	public ModelAndView adminFilter(RoofDTO roofDTO,
			@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required = false) String message, @PathVariable("page") Integer page) {

		Pageable pageable = paginationSqrHandling(page);
		ModelAndView catalogue = new ModelAndView("admin/catalogue/catalogue");
		
		updateFilterData(roofDTO);
		setupModal(modalType, message, catalogue);
		
		PagedResultsDTO roofList = roofService.filterValidatedAndAllIndividualRoofs(navRoofDTO, pageable);
		adminFilterInit(catalogue, roofList, navRoofDTO);

		setNavigationParams(page, "/admin/catalogue/filter/");
		
		return catalogue;
	}

	/**
	 * @return
	 */
	@RequestMapping("/admin/catalogue/filter-reset")
	public String adminResetFilter() {

		navRoofDTO = new RoofDTO();

		return "redirect:/admin/catalogue/1";
	}
	
	/**
	 * @param roofDTO
	 * @param request
	 * @return
	 */
	@RequestMapping("/admin/catalogue/details")
	public ModelAndView adminRoofDetails(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("admin/catalogue/roof-details");

		roofDetails(roofDTO, roofDetails);
		roofDetails.addObject("userDTO", loggedUserInfo());
		roofDetails.addObject("returnPath", lastPath+lastPageSel);

		return roofDetails;
	}
	
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/admin/catalogue/disable-enable")
	public String adminRoofEnableDisable(RoofDTO roofDTO, RedirectAttributes ra) {

		Boolean isActive = roofService.enableDisableRoof(roofDTO.getIdRoof());

		if(isActive) {
			ra.addAttribute("message", "roof.enabled");
		} else {
			ra.addAttribute("message", "roof.disabled");
		}
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());

		return "redirect:"+lastPath+lastPageSel;
	}
		
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/admin/catalogue/delete")
	public String adminRoofDelete(RoofDTO roofDTO, RedirectAttributes ra) {

		roofService.deleteRoof(roofDTO.getIdRoof());
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.deleted");
		
		return "redirect:"+lastPath+lastPageSel;
	}
		
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/admin/catalogue/move-global")
	public String roofMoveToGlobal(RoofDTO roofDTO, RedirectAttributes ra) {

		roofService.moveRoofToGlobal(roofDTO.getIdRoof());
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.moved");
		
		return "redirect:"+lastPath+lastPageSel;
	}
	
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/admin/catalogue/restore")
	public String restoreRoofOwner(RoofDTO roofDTO, RedirectAttributes ra) {

		roofService.restoreRoofToOwner(roofDTO.getIdRoof());
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.restored");

		return "redirect:"+lastPath+lastPageSel;
	}
	
	/**
	 * @param roofDTO
	 * @param request
	 * @return
	 */
	@RequestMapping("/admin/catalogue/edit-roof")
	public ModelAndView adminCatalogueRoofsEdit(RoofDTO roofDTO) {

		ModelAndView editRoof = new ModelAndView("admin/catalogue/update-roof");
		
		RoofDTO roof = roofService.findRoofById(roofDTO.getIdRoof());
		roofService.getLinkedResources(roof);

		editRoof.addObject("roofDTO", roof);

		roofDefaultValues(editRoof);
		
		return editRoof;
	}
	
	/**
	 * @param roofDTO
	 * @param request
	 * @return
	 */
	@RequestMapping("/admin/catalogue/edit-roof/update")
	public String admincatalogueRoofsEditUpdate(@Valid RoofDTO roofDTO) {
		
		// TODO BORRAR IMÁGENES ANTIGUAS

		
		// SET THE LIST WITH IMAGES AND SHEETS
		roofDTO.setUploadedImagesList(uploadedImagesList);
		roofDTO.setUploadedDetailsList(uploadedDetailsList);
		roofDTO.setUploadedSheetsList(uploadedSheetsList);

		// TODO CREAR NUEVO MÉTODO PARA ACTUALIZAR UN REGISTRO
		
//		roofService.addRoof(roofDTO, RoofStatus.VALIDATED);
		
		return "redirect:"+lastPath+lastPageSel;
	}
	
	/**
	 * @param request
	 * @return
	 */
	@RequestMapping("/admin/catalogue/new-roof")
	public ModelAndView adminCatalogueNewRoof() {
		
		ModelAndView newRoof = new ModelAndView("admin/catalogue/new-roof");
		roofDefaultValues(newRoof);
	
		return newRoof;
	}

	/**
	 * @param roofDTO
	 * @param bindingResult
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/admin/catalogue/new-roof/register")
	public String adminCatalogueNewRoofRegister(@Valid RoofDTO roofDTO, BindingResult bindingResult,
			RedirectAttributes ra) throws IOException {

		// SET THE LIST WITH IMAGES AND SHEETS
		roofDTO.setUploadedImagesList(uploadedImagesList);
		roofDTO.setUploadedDetailsList(uploadedDetailsList);
		roofDTO.setUploadedSheetsList(uploadedSheetsList);

		roofService.addRoof(roofDTO, RoofStatus.VALIDATED);
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.auto-validated");

		return "redirect:"+lastPath+lastPageSel;
	}
	
	

	/* ################################################################################################################## */
	/* #############################################      INDIVIDUAL     ################################################ */
	/* ################################################################################################################## */

	@RequestMapping("/individual/catalogue/")
	public String individualCatalogueStoreAccess(HttpServletRequest request) {

		storeUsedFeature(request, FeatureName.CATALOGUE);
		
		return "redirect:/individual/catalogue/1";
	}
	
	/**
	 * @param request
	 * @return
	 */
	@RequestMapping("/individual/catalogue/{page}")
	public ModelAndView individualCatalogue(@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required=false) String message,
			@PathVariable("page") Integer page) {
		
		Pageable pageable = paginationSqrHandling(page);
		ModelAndView catalogue = individualCatalogueInit(pageable);

		setupModal(modalType, message, catalogue);	
		setNavigationParams(page, "/individual/catalogue/");
		
		return catalogue;
	}
	
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/individual/catalogue/filter/{page}")
	public ModelAndView individualFilter(RoofDTO roofDTO,
			@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required = false) String message, @PathVariable("page") Integer page) {

		Pageable pageable = paginationSqrHandling(page);
		ModelAndView catalogue = new ModelAndView("individual/catalogue/catalogue");
		
		updateFilterData(roofDTO);
		setupModal(modalType, message, catalogue);
		
		UserDTO userDTO = loggedUserInfo();
		PagedResultsDTO roofList = roofService.filterValidatedEnabledAndOwnedRoofs(navRoofDTO, userDTO, pageable);
		individualFilterInit(catalogue, userDTO, roofList, navRoofDTO);

		setNavigationParams(page, "/individual/catalogue/filter/");
		
		return catalogue;
	}

	/**
	 * @return
	 */
	@RequestMapping("/individual/catalogue/filter-reset")
	public String individualResetFilter() {

		navRoofDTO = new RoofDTO();

		return "redirect:/individual/catalogue/1";
	}	
	
	/**
	 * @param roofDTO
	 * @param request
	 * @return
	 */
	@RequestMapping("/individual/catalogue/details")
	public ModelAndView individualRoofDetails(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("individual/catalogue/roof-details");

		roofDetails(roofDTO, roofDetails);
		roofDetails.addObject("userDTO", loggedUserInfo());
		roofDetails.addObject("returnPath", lastPath+lastPageSel);
		
		return roofDetails;
	}
	
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/individual/catalogue/disable-enable")
	public String individualRoofEnableDisable(RoofDTO roofDTO, RedirectAttributes ra) {

		Boolean isActive = roofService.enableDisableRoof(roofDTO.getIdRoof());

		if(isActive) {
			ra.addAttribute("message", "roof.enabled");
		} else {
			ra.addAttribute("message", "roof.disabled");
		}
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		
		return "redirect:"+lastPath+lastPageSel;
	}
	
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/individual/catalogue/delete")
	public String individualRoofDelete(RoofDTO roofDTO, RedirectAttributes ra) {

		roofService.deleteRoof(roofDTO.getIdRoof());
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.deleted");
		
		return "redirect:"+lastPath+lastPageSel;
	}

	/**
	 * @param request
	 * @return
	 */
	@RequestMapping("/individual/catalogue/new-roof")
	public ModelAndView individualCatalogueNewRoof() {
		
		ModelAndView newRoof = new ModelAndView("individual/catalogue/new-roof");
		roofDefaultValues(newRoof);
		
		return newRoof;
	}
	
	/**
	 * @param roofDTO
	 * @param bindingResult
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/individual/catalogue/new-roof/register")
	public String individualCatalogueNewRoofRegister(@Valid RoofDTO roofDTO, BindingResult bindingResult,
			RedirectAttributes ra) throws IOException {

		// SET THE LIST WITH IMAGES AND SHEETS
		roofDTO.setUploadedImagesList(uploadedImagesList);
		roofDTO.setUploadedDetailsList(uploadedDetailsList);
		roofDTO.setUploadedSheetsList(uploadedSheetsList);

		roofService.addRoof(roofDTO, RoofStatus.INDIVIDUAL);
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.auto-validated");		
		
		return "redirect:"+lastPath+lastPageSel;
	}
	
	@RequestMapping("/individual/catalogue/edit-roof")
	public ModelAndView individualCatalogueRoofsEdit(RoofDTO roofDTO) {

		ModelAndView editRoof = new ModelAndView("individual/catalogue/update-roof");
		
		RoofDTO roof = roofService.findRoofById(roofDTO.getIdRoof());
		roofService.getLinkedResources(roof);

		editRoof.addObject("roofDTO", roof);

		roofDefaultValues(editRoof);
		
		return editRoof;
	}

	
	
	/* ################################################################################################################## */
	/* #############################################       SUPPLIER      ################################################ */
	/* ################################################################################################################## */

	@RequestMapping("/supplier/catalogue/")
	public String supplierCatalogueStoreAccess(HttpServletRequest request) {

		storeUsedFeature(request, FeatureName.CATALOGUE);
		
		return "redirect:/supplier/catalogue/1";
	}
	
	/**
	 * @param request
	 * @return
	 */
	@RequestMapping("/supplier/catalogue/{page}")
	public ModelAndView supplierCatalogue(@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required=false) String message,
			@PathVariable("page") Integer page) {
		
		Pageable pageable = paginationSqrHandling(page);
		ModelAndView catalogue = supplierCatalogueInit(pageable);

		setupModal(modalType, message, catalogue);	
		setNavigationParams(page, "/supplier/catalogue/");

		return catalogue;
	}

	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/supplier/catalogue/filter/{page}")
	public ModelAndView supplierFilter(RoofDTO roofDTO,
			@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required = false) String message, @PathVariable("page") Integer page) {

		Pageable pageable = paginationSqrHandling(page);
		ModelAndView catalogue = new ModelAndView("supplier/catalogue/catalogue");
		
		updateFilterData(roofDTO);
		
		setupModal(modalType, message, catalogue);
		
		UserDTO userDTO = loggedUserInfo();
		PagedResultsDTO roofList = roofService.filterOwnedValidatedRoofs(navRoofDTO, userDTO, pageable);
		supplierFilterInit(catalogue, userDTO, roofList, navRoofDTO);
		
		setNavigationParams(page, "/supplier/catalogue/filter/");

		return catalogue;
	}

	/**
	 * @return
	 */
	@RequestMapping("/supplier/catalogue/filter-reset")
	public String supplierResetFilter() {

		navRoofDTO = new RoofDTO();

		return "redirect:/supplier/catalogue/1";
	}	
	
	/**
	 * @param roofDTO
	 * @param request
	 * @return
	 */
	@RequestMapping("/supplier/catalogue/details")
	public ModelAndView supplierRoofDetails(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("supplier/catalogue/roof-details");

		roofDetails(roofDTO, roofDetails);
		roofDetails.addObject("userDTO", loggedUserInfo());
		roofDetails.addObject("returnPath", lastPath+lastPageSel);
		
		return roofDetails;
	}
	
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/supplier/catalogue/disable-enable")
	public String supplierRoofEnableDisable(RoofDTO roofDTO, RedirectAttributes ra) {

		Boolean isActive = roofService.enableDisableRoof(roofDTO.getIdRoof());

		if(isActive) {
			ra.addAttribute("message", "roof.enabled");
		} else {
			ra.addAttribute("message", "roof.disabled");
		}
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());

		return "redirect:"+lastPath+lastPageSel;
	}
	
	/**
	 * @param roofDTO
	 * @return
	 */
	@RequestMapping("/supplier/catalogue/delete")
	public String supplierRoofDelete(RoofDTO roofDTO, RedirectAttributes ra) {

		roofService.deleteRoof(roofDTO.getIdRoof());
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.deleted");
		
		return "redirect:"+lastPath+lastPageSel;
	}	
	
	/**
	 * @param request
	 * @return
	 */
	@RequestMapping("/supplier/catalogue/new-roof")
	public ModelAndView supplierCatalogueNewRoof(HttpServletRequest request) {
		
		ModelAndView newRoof = new ModelAndView("supplier/catalogue/new-roof");
		roofDefaultValues(newRoof);	
		
		return newRoof;
	}
	
	/**
	 * @param roofDTO
	 * @param bindingResult
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/supplier/catalogue/new-roof/register")
	public String supplierCatalogueNewRoofRegister(@Valid RoofDTO roofDTO, BindingResult bindingResult,
			HttpServletRequest request, RedirectAttributes ra) throws IOException {

		ParameterDTO requiredValidation = parameterService.findByParameter(Parameters.REQUIRED_VALIDATION);
		
		roofDTO.setMaker(this.loggedUserInfo().getCompany());
		
		// SET THE LIST WITH IMAGES AND SHEETS
		roofDTO.setUploadedImagesList(uploadedImagesList);
		roofDTO.setUploadedDetailsList(uploadedDetailsList);
		roofDTO.setUploadedSheetsList(uploadedSheetsList);

		if (!requiredValidation.getActive()) {
			roofService.addRoof(roofDTO, RoofStatus.VALIDATED);
			ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
			ra.addAttribute("message", "roof.auto-validated");
			
		} else {
			roofService.addRoof(roofDTO, RoofStatus.PENDING);
			ra.addAttribute("modalType", Modals.WARNING_MODAL.toString());
			ra.addAttribute("message", "roof.pending");
		}
		
		return "redirect:"+lastPath+lastPageSel;
	}

	@RequestMapping("/supplier/catalogue/edit-roof")
	public ModelAndView supplierCatalogueRoofsEdit(RoofDTO roofDTO) {

		ModelAndView editRoof = new ModelAndView("supplier/catalogue/update-roof");
		
		RoofDTO roof = roofService.findRoofById(roofDTO.getIdRoof());
		roofService.getLinkedResources(roof);

		editRoof.addObject("roofDTO", roof);

		roofDefaultValues(editRoof);
		
		return editRoof;
	}
	
	
	/***************************************************************************************
	 ************************************* ASYNC *******************************************
	 ***************************************************************************************/

	/**
	 * @param uploadingFiles
	 * @param uploadingDetails
	 * @param uploadingSheets
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/api/save-image", method = RequestMethod.POST)
	public ResponseEntity<?> uploadingImages(
			@RequestParam("images") MultipartFile[] uploadingFiles,
			@RequestParam("details") MultipartFile[] uploadingDetails,
			@RequestParam("sheets") MultipartFile[] uploadingSheets) throws IOException {

		AjaxResponseBodyDTO result = new AjaxResponseBodyDTO();

		roofService.saveTempFiles(uploadingFiles, uploadedImagesList);
		roofService.saveTempFiles(uploadingDetails, uploadedDetailsList);
		roofService.saveTempFiles(uploadingSheets, uploadedSheetsList);

		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/api/search-model")
	public ResponseEntity<?> checkModelExists(@Valid @RequestBody RoofDTO roofDTO, Errors errors) {

		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle(errorsBundle, locale);
		
		AjaxResponseBodyDTO result = new AjaxResponseBodyDTO();
		List<String> errorList = new ArrayList<String>();
		
		if(roofService.checkIfModelExist(this.loggedUserInfo().getIdUser(), roofDTO.getModel())) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.model.exist"));
			result.setResult(errorList);	
		} else {
			result.setCode(ResponseCode.OK.toString());
		}

		return ResponseEntity.ok(result);
	}

	
	
	/***************************************************************************************
	 ************************************* PRIVATE *****************************************
	 ***************************************************************************************/
	
	/**
	 * @return
	 */
	private ModelAndView adminCatalogueInit(Pageable pageable) {
		
		ModelAndView catalogue = new ModelAndView("admin/catalogue/catalogue");

		PagedResultsDTO roofList = roofService.findAllByStatus(roofService.getAdminAvailableStatus(), pageable);
		roofService.getLinkedResources(roofList.getElements());
		RoofLimitValuesDTO roofLimitValuesDTO = roofService.getRoofLimitValuesAdmin();

		catalogue.addObject("roofList", roofList);
		catalogue.addObject("roofLimitValuesDTO", roofLimitValuesDTO);
		roofDefaultValues(catalogue);
		
		
		return catalogue;
	}
	
	/**
	 * @param catalogue
	 * @param roofList
	 */
	private void adminFilterInit(ModelAndView catalogue, PagedResultsDTO roofList, RoofDTO roofDTO) {
		
		roofService.getLinkedResources(roofList.getElements());
		RoofLimitValuesDTO roofLimitValuesDTO = roofService.getRoofLimitValuesAdmin();

		catalogue.addObject("roofList", roofList);
		catalogue.addObject("onLoadToggle", "#collapseOne");
		catalogue.addObject("roofLimitValuesDTO", roofLimitValuesDTO);
		catalogue.addObject("roofDTO", roofDTO);
		roofDefaultValues(catalogue);
	}	
	
	/**
	 * @return
	 */
	private ModelAndView individualCatalogueInit(Pageable pageable) {
		
		ModelAndView catalogue = new ModelAndView("individual/catalogue/catalogue");
		UserDTO userDTO = loggedUserInfo();
		
		PagedResultsDTO roofList = roofService.findValidatedEnabledAndOwnedRoofs(userDTO, pageable);
		roofService.getLinkedResources(roofList.getElements());
		RoofLimitValuesDTO roofLimitValuesDTO = roofService.getRoofLimitValuesIndividual(userDTO);

		catalogue.addObject("roofList", roofList);
		catalogue.addObject("roofLimitValuesDTO", roofLimitValuesDTO);
		roofDefaultValues(catalogue);
		
		return catalogue;
	}
	
	/**
	 * @param catalogue
	 * @param userDTO
	 * @param roofList
	 */
	private void individualFilterInit(ModelAndView catalogue, UserDTO userDTO, PagedResultsDTO roofList, RoofDTO roofDTO) {
		
		roofService.getLinkedResources(roofList.getElements());
		RoofLimitValuesDTO roofLimitValuesDTO = roofService.getRoofLimitValuesIndividual(userDTO);

		catalogue.addObject("roofList", roofList);
		catalogue.addObject("onLoadToggle", "#collapseOne");
		catalogue.addObject("roofLimitValuesDTO", roofLimitValuesDTO);
		catalogue.addObject("roofDTO", roofDTO);
		roofDefaultValues(catalogue);
	}
	
	/**
	 * @return
	 */
	private ModelAndView supplierCatalogueInit(Pageable pageable) {
		
		ModelAndView catalogue = new ModelAndView("supplier/catalogue/catalogue");

		UserDTO userDTO = loggedUserInfo();
		
		PagedResultsDTO roofList = roofService.findOwnedRoofsValidated(userDTO, pageable);
		roofService.getLinkedResources(roofList.getElements());
		RoofLimitValuesDTO roofLimitValuesDTO = roofService.getRoofLimitValuesSupplier(userDTO);

		catalogue.addObject("roofList", roofList);
		catalogue.addObject("roofLimitValuesDTO", roofLimitValuesDTO);
		roofDefaultValues(catalogue);
		
		return catalogue;
	}
	
	/**
	 * @param catalogue
	 * @param userDTO
	 * @param roofList
	 */
	private void supplierFilterInit(ModelAndView catalogue, UserDTO userDTO, PagedResultsDTO roofList, RoofDTO roofDTO) {
		
		roofService.getLinkedResources(roofList.getElements());
		RoofLimitValuesDTO roofLimitValuesDTO = roofService.getRoofLimitValuesSupplier(userDTO);

		catalogue.addObject("roofList", roofList);
		catalogue.addObject("onLoadToggle", "#collapseOne");
		catalogue.addObject("roofLimitValuesDTO", roofLimitValuesDTO);
		catalogue.addObject("roofDTO", roofDTO);
		roofDefaultValues(catalogue);
	}

	
	private void updateFilterData(RoofDTO roofDTO) {
		if(roofDTO.getPrice() != null) {
			navRoofDTO = roofDTO;
		}
	}
	
	
	
	

}