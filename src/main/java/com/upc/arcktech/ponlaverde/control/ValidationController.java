package com.upc.arcktech.ponlaverde.control;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.Modals;
import com.upc.arcktech.ponlaverde.constants.Parameters;
import com.upc.arcktech.ponlaverde.constants.RoofStatus;
import com.upc.arcktech.ponlaverde.dto.PagedResultsDTO;
import com.upc.arcktech.ponlaverde.dto.ParameterDTO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.service.ParameterService;
import com.upc.arcktech.ponlaverde.service.RoofService;

@Controller
@Scope("session")
@PreAuthorize("@featureStatus.isActive('VAL')")
public class ValidationController extends BaseController {
	
	@Autowired
	RoofService roofService;
	@Autowired
	ParameterService parameterService;
	
	
	
	/* ################################################################################################################## */
	/* #############################################        ADMIN        ################################################ */
	/* ################################################################################################################## */
	
	@RequestMapping("/admin/validation/")
	public String adminValidationStoreAccess(HttpServletRequest request) {

		storeUsedFeature(request, FeatureName.VALIDATION);
		
		return "redirect:/admin/validation/1";
	}
	
	@RequestMapping("/admin/validation/{page}")
	public ModelAndView adminValidationPendingList(@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required=false) String message,
			@PathVariable(value = "page", required=false) Integer page) {

		Pageable pageable = paginationSqrHandling(page);
		ModelAndView validationPending = adminValidationInit(pageable);
		
		setupModal(modalType, message, validationPending);
		setNavigationParams(page, null);
		
		return validationPending;
	}


	@RequestMapping("/admin/validation/details")
	public ModelAndView adminValidationRoofDetails(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("admin/validation/pending-details");
		roofDetails(roofDTO, roofDetails);
		
		return roofDetails;
	}

	@RequestMapping("/admin/validation/validate")
	public String adminValidationRoofValidate(RoofDTO roofDTO, RedirectAttributes ra) {
		
		this.roofService.validateRoof(roofDTO.getIdRoof());

		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.validated");
		
		return "redirect:/admin/validation/"+lastPageSel;
	}
	
	@RequestMapping("/admin/validation/reject")
	public String adminValidationRoofReject(RoofDTO roofDTO, RedirectAttributes ra) {
		
		this.roofService.rejectRoof(roofDTO);

		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.rejected");
		
		return "redirect:/admin/validation/"+lastPageSel;
	}

	
	/* ################################################################################################################## */
	/* #############################################       SUPPLIER      ################################################ */
	/* ################################################################################################################## */

	@RequestMapping("/supplier/validation/")
	public String supplierValidationStoreAccess(HttpServletRequest request) {

		storeUsedFeature(request, FeatureName.VALIDATION);
		
		return "redirect:/supplier/validation/1";
	}
	
	@RequestMapping("/supplier/validation/{page}")
	public ModelAndView supplierValidationPendingList(@RequestParam(value = "modalType", required = false) String modalType,
			@RequestParam(value = "message", required=false) String message,
			@PathVariable(value = "page", required=false) Integer page) {

		Pageable pageable = paginationSqrHandling(page);
		ModelAndView validationPending = supplierValidationInit(pageable);
		
		setupModal(modalType, message, validationPending);
		setNavigationParams(page, null);
		
		return validationPending;
	}
	
	@RequestMapping("/supplier/validation/details")
	public ModelAndView supplierValidationRoofDetails(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("supplier/validation/pending-details");
		roofDetails(roofDTO, roofDetails);
		
		return roofDetails;
	}
	
	@RequestMapping("/supplier/validation/edit")
	public ModelAndView supplierValidationRoofEdit(RoofDTO roofDTO) {

		ModelAndView roofDetails = new ModelAndView("supplier/validation/pending-edit");
		roofDetails(roofDTO, roofDetails);
		roofDefaultValues(roofDetails);
		
		return roofDetails;
	}
	
	@RequestMapping("/supplier/validation/update")
	public String supplierValidationRoofUpdate(RoofDTO roofDTO, RedirectAttributes ra) throws IOException  {

		//TODO Edit IMAGES Update
		
		// SET THE LIST WITH IMAGES AND SHEETS
//		roofDTO.setUploadedImagesList(uploadedImagesList);
//		roofDTO.setUploadedDetailsList(uploadedDetailsList);
//		roofDTO.setUploadedSheetsList(uploadedSheetsList);

		ParameterDTO requiredValidation = parameterService.findByParameter(Parameters.REQUIRED_VALIDATION);
		
		roofDTO.setMaker(this.loggedUserInfo().getCompany());

		if (!requiredValidation.getActive()) {
			roofService.addRoof(roofDTO, RoofStatus.VALIDATED);
			ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
			ra.addAttribute("message", "roof.auto-validated");
			
		} else {
			roofService.addRoof(roofDTO, RoofStatus.PENDING);
			ra.addAttribute("modalType", Modals.WARNING_MODAL.toString());
			ra.addAttribute("message", "roof.pending-updated");
		}

		
		return "redirect:/supplier/validation/"+lastPageSel;
	}
	
	@RequestMapping("/supplier/validation/delete")
	public String supplierRoofDelete(RoofDTO roofDTO, RedirectAttributes ra) {

		roofService.deleteRoof(roofDTO.getIdRoof());
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "roof.deleted");
		
		return "redirect:/supplier/validation/"+lastPageSel;
	}
	
	
	
	
	/* ################################################################################################################## */
	/* #############################################          AUX        ################################################ */
	/* ################################################################################################################## */
	
	private ModelAndView adminValidationInit(Pageable pageable) {
		ModelAndView validationPending = new ModelAndView("admin/validation/pending");

		PagedResultsDTO roofList = roofService.findAllByStatus(RoofStatus.PENDING, pageable);
		this.roofService.getLinkedResources(roofList.getElements());

		validationPending.addObject("roofList", roofList);
		
		return validationPending;
	}
	
	private ModelAndView supplierValidationInit(Pageable pageable) {
		ModelAndView validationPending = new ModelAndView("supplier/validation/pending");

		PagedResultsDTO roofList = roofService.findOwnedRoofsRejectedOrPending(this.loggedUserInfo(), pageable);
		this.roofService.getLinkedResources(roofList.getElements());

		validationPending.addObject("roofList", roofList);
		
		return validationPending;
	}
	
	
}
