package com.upc.arcktech.ponlaverde.control;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.Modals;
import com.upc.arcktech.ponlaverde.dto.UserSavedRoofDTO;
import com.upc.arcktech.ponlaverde.service.ParameterService;
import com.upc.arcktech.ponlaverde.service.RecommenderService;
import com.upc.arcktech.ponlaverde.service.RoofService;

@Controller
@Scope("session")
@PreAuthorize("@featureStatus.isActive('SAV')")
public class SavedRecommendationController extends BaseController {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	RoofService roofService;
	@Autowired
	ParameterService parameterService;
	@Autowired
	RecommenderService recommenderService;

	
	/* ################################################################################################################## */
	/* #############################################       INDIVIDUAL      ############################################## */
	/* ################################################################################################################## */

	
	@RequestMapping("/individual/saved-list")
	public ModelAndView getSavedList(HttpServletRequest request) {
		
		storeUsedFeature(request, FeatureName.RECOMMENDER_SAVE);

		ModelAndView savedList = new ModelAndView("individual/saved-recommendation/saved-list");

		savedList.addObject("savedRoofs", loggedUserInfo().getUserSavedRoofDTO());

		return savedList;
	}
	
	@RequestMapping("/individual/saved-list-success")
	public ModelAndView getSavedListModal(@RequestParam("modalType") String modalType, @RequestParam("message") String message) {

		ModelAndView savedList = new ModelAndView("individual/saved-recommendation/saved-list");

		savedList.addObject("savedRoofs", loggedUserInfo().getUserSavedRoofDTO());
		
		modalWithMessage(modalType, message, savedList);

		return savedList;
	}
	
	@RequestMapping("/individual/saved-list/saved-recommendation")
	public ModelAndView getSavedRecommendation(UserSavedRoofDTO userSavedRoofDTO) throws JsonProcessingException {

		ModelAndView savedRecommendation = new ModelAndView("individual/saved-recommendation/saved-recommendation");

		for(UserSavedRoofDTO savedRoofs: loggedUserInfo().getUserSavedRoofDTO()) {
			
			if(savedRoofs.getIdUserSavedRoof().equals(userSavedRoofDTO.getIdUserSavedRoof())){

				mapper.enable(SerializationFeature.INDENT_OUTPUT);
				String json = mapper.writeValueAsString(savedRoofs);
				
				savedRecommendation.addObject("savedRoofDetails", savedRoofs);
				savedRecommendation.addObject("chartData", json);
			}
			
		}

		return savedRecommendation;
	}
	
	@RequestMapping("/individual/saved-list/delete-recommendation")
	public String deleteSavedRecommendation(UserSavedRoofDTO userSavedRoofDTO, RedirectAttributes ra) {

		recommenderService.deleteRecommendation(userSavedRoofDTO, loggedUserInfo().getIdUser());
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "rec.deleted");
	
		return "redirect:/individual/saved-list-success";
	}
	

}
