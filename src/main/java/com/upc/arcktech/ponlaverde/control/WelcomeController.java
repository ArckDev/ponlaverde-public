package com.upc.arcktech.ponlaverde.control;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.upc.arcktech.ponlaverde.constants.ResponseCode;
import com.upc.arcktech.ponlaverde.dto.AjaxResponseBodyDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.dto.UserTempDTO;
import com.upc.arcktech.ponlaverde.dto.WelcomeMetricsDTO;
import com.upc.arcktech.ponlaverde.service.ConnectionService;
import com.upc.arcktech.ponlaverde.service.UserService;
import com.upc.arcktech.ponlaverde.service.UserTempService;
import com.upc.arcktech.ponlaverde.service.WelcomeMetricsService;
import com.upc.arcktech.ponlaverde.utils.MapsTransformUtils;

@Controller
@Scope("session")
public class WelcomeController extends BaseController {

	@Autowired
	UserService userService;
	@Autowired
	WelcomeMetricsService welcomeMetricsService;
	@Autowired
	ConnectionService connectionService;
	@Autowired
	UserTempService userTempService;

	/**
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/")
	public ModelAndView welcome(HttpServletRequest request) throws IOException  {

		ModelAndView welcome = new ModelAndView("welcome");

		storeConnection(request);
		initWelcomeData(welcome);
		welcome.addObject("onLoadModal", "#cookiesModal");

		return welcome;
	}
	
	/**
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/version")
	public ModelAndView version() throws IOException {

		ModelAndView version = new ModelAndView("version");
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("build.properties");
		Properties props = new Properties();
		
		try {
			props.load(inputStream);
			version.addObject("applicationName", props.get("build.name"));
			version.addObject("buildVersion", props.get("build.version"));
			version.addObject("buildTimestamp", props.get("build.date"));
			
		} catch (IOException e) {
			logger.error("ERROR READING VERSION FILE");
		} finally {
			inputStream.close();
		}

		return version;
	}
	
	/**
	 * @param model
	 * @param error
	 * @param logout
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/login")
    public ModelAndView login(Model model, String error, String logout, HttpServletRequest request) {
        
		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle(errorsBundle, locale);
		
		if (error != null)
            model.addAttribute("error", messageBundle.getString("error.login"));
        	logger.error("USER FAIL TO LOGIN: "+ "[SESSION ID: "+ request.getRequestedSessionId() + ", IP: " + request.getRemoteAddr() +"] ");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        ModelAndView welcome = new ModelAndView("welcome");
        initWelcomeData(welcome);
        welcome.addObject("onLoadModal", "#loginModal");

        return welcome;
    }

	/**
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/logout-success")
	public ModelAndView logoutSuccess() {

		ModelAndView welcome = new ModelAndView("welcome");
		
		initWelcomeData(welcome);

		return welcome;
	}
	
	
	/**
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/accountVerification")
	public ModelAndView accountVerification(HttpServletRequest request) throws IOException  {

		ModelAndView welcome = new ModelAndView("welcome");
		welcome = initWelcomeData(welcome);

		String username = request.getParameter("email");
		String sKey = request.getParameter("sKey");
		
		UserTempDTO userPendingVerificationDTO = new UserTempDTO();
		userPendingVerificationDTO.setUsername(username);
		userPendingVerificationDTO.setsKey(sKey);
		
		UserTempDTO userVerified = userTempService.verifyAccount(userPendingVerificationDTO);
		
		UserDTO userDTO = new UserDTO();
		
		if(userVerified != null) {
			userDTO.setUsername(username);
			userDTO = userService.activateUser(userDTO);			
			userTempService.removeTempUser(userVerified);			
			welcome.addObject("onLoadModal", "#successGenericModal");
			welcome.addObject("key", "activation.completed");			
			logger.info("ACCOUNT VERIFIED: "+userDTO.toString());
			
		} else {
			welcome.addObject("onLoadModal", "#errorGenericModal");
			welcome.addObject("key", "activation.error");			
			logger.info("ACCOUNT NO VERIFIED: "+userDTO.toString());
		}

		return welcome;
	}
	
	@RequestMapping("/restore-password")
	public ModelAndView restorePasswordRequest(UserDTO userDTO, Model model, String error, HttpServletRequest request) {

		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle(errorsBundle, locale);
		ModelAndView welcome = new ModelAndView("welcome");
        initWelcomeData(welcome);  
		
        //TODO Change messages!
		if(!"".equals(userDTO.getUsername())) {
			Boolean result = userService.requestRestorePassword(userDTO, request);
			if(result) {
				welcome.addObject("onLoadModal", "#successGenericModal");
				welcome.addObject("key", "restore.request");
				
			} else {
				model.addAttribute("error", messageBundle.getString("error.password-restore.user"));
				welcome.addObject("onLoadModal", "#restorePassModal");
			}
		} else {
			model.addAttribute("error", messageBundle.getString("error.password-restore"));
			welcome.addObject("onLoadModal", "#restorePassModal");
		}

		return welcome;
	}
	
	@RequestMapping("/restore-password-confirm")
	public ModelAndView restorePasswordConfirm(HttpServletRequest request) {

		ModelAndView welcome = new ModelAndView("welcome");
		welcome = initWelcomeData(welcome);

		String username = request.getParameter("email");
		String sKey = request.getParameter("sKey");

		UserTempDTO userTempDTO = new UserTempDTO();
		userTempDTO.setUsername(username);
		userTempDTO.setsKey(sKey);
		
		UserTempDTO userVerified = userTempService.verifyAccount(userTempDTO);
		
		UserDTO userDTO = new UserDTO();
		
		if(userVerified != null) {
			userDTO.setUsername(username);
			Boolean result = userService.restorePassword(userDTO);
			
			if(result) {
				userTempService.removeTempUser(userVerified);
				welcome.addObject("onLoadModal", "#successGenericModal");
				welcome.addObject("key", "restore.completed");	
				logger.info("ACCOUNT PASSWORD RESTORED: "+userDTO.toString());
				
			} else {
				welcome.addObject("onLoadModal", "#errorGenericModal");
				welcome.addObject("key", "restore.error"); 
				logger.info("BUSINESS ERROR RESTORING PASSWORD: "+userDTO.toString());
			}

		} else {
			welcome.addObject("onLoadModal", "#errorGenericModal");
			welcome.addObject("key", "restore.error.link");
			logger.info("ERROR RESTORING PASSWORD, THE LINK IS NO LONGER ACTIVE: "+userDTO.toString());
		}
		
		return welcome;
	}
	
	
	/**
	 * @param userDTO
	 * @param errors
	 * @return
	 */
	@PostMapping("/api/search-username")
	public ResponseEntity<?> checkUsernameExist(@Valid @RequestBody UserDTO userDTO, Errors errors) {
		
		AjaxResponseBodyDTO result = new AjaxResponseBodyDTO();
		result.setCode(ResponseCode.OK.toString());
		List<String> errorList = new ArrayList<String>();
		
		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle(errorsBundle, locale);
		EmailValidator validator = EmailValidator.getInstance();

		checkUsername(userDTO, result, errorList, messageBundle, validator);
		
		if(!errorList.isEmpty()) {
			result.setResult(errorList);
		}

		return ResponseEntity.ok(result);

	}

	
	/**
	 * @param userDTO
	 * @param errors
	 * @param request
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws SQLException
	 */
	@PostMapping("/api/validate-form-ind")
	public ResponseEntity<?> validateFormAndInitRegisterInd(@Valid @RequestBody UserDTO userDTO, Errors errors, HttpServletRequest request)
			throws NoSuchAlgorithmException, IOException, SQLException {
		
		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle(errorsBundle, locale);

		AjaxResponseBodyDTO result = new AjaxResponseBodyDTO();
		EmailValidator validator = EmailValidator.getInstance();

		List<String> errorList = new ArrayList<String>();

		result.setCode(ResponseCode.OK.toString());

		//CHECKS
		checkUsername(userDTO, result, errorList, messageBundle, validator);

		if (userDTO.getName() == "") {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.name"));
		}
		if (userDTO.getSurnames() == "") {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.surnames"));
		}
		if (userDTO.getCity() == "") {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.city"));
		}
		if (userDTO.getDateOfBirth() == null) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.dob"));
		}
		if (userDTO.getPassword() == "" || userDTO.getPasswordRepeat() == "" 
				|| !userDTO.getPassword().equals(userDTO.getPasswordRepeat())) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.password"));
		}
		if (userDTO.getPassword().length() < 8 || userDTO.getPasswordRepeat().length() < 8) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.password.short"));
		}
		if (!userDTO.getPrivacy()) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.privacy"));
		}
		if (!userDTO.getTerms()) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.terms"));
		}
		if(!errorList.isEmpty()) {
			result.setResult(errorList);
		} else {
			userService.addUser(userDTO, request);
			logger.info("USER REGISTERED: "+userDTO.toString());
		}

		return ResponseEntity.ok(result);
	}
	
	
	/**
	 * @param userDTO
	 * @param errors
	 * @param request
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws SQLException
	 */
	@PostMapping("/api/validate-form-sup")
	public ResponseEntity<?> validateFormAndInitRegisterSup(@Valid @RequestBody UserDTO userDTO, Errors errors, HttpServletRequest request)
			throws NoSuchAlgorithmException, IOException, SQLException {
		
		Locale locale = LocaleContextHolder.getLocale();
		ResourceBundle messageBundle = ResourceBundle.getBundle(errorsBundle, locale);

		AjaxResponseBodyDTO result = new AjaxResponseBodyDTO();
		EmailValidator validator = EmailValidator.getInstance();
		

		List<String> errorList = new ArrayList<String>();

		result.setCode(ResponseCode.OK.toString());

		//CHECKS
		checkUsername(userDTO, result, errorList, messageBundle, validator);
		
		if (userDTO.getCity() == "") {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.city"));
		}
		if (userDTO.getCompany() == "") {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.company"));
		}
		if (userDTO.getPassword() == "" || userDTO.getPasswordRepeat() == "" 
				|| !userDTO.getPassword().equals(userDTO.getPasswordRepeat())) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.password"));
		}	
		if (userDTO.getPassword().length() < 8 || userDTO.getPasswordRepeat().length() < 8) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.password.short"));
		}
		if (!userDTO.getPrivacy()) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.privacy"));
		}
		if (!userDTO.getTerms()) {
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.terms"));
		}
		if(!errorList.isEmpty()) {
			result.setResult(errorList);
		} else {
			userService.addUser(userDTO, request);
			logger.info("USER REGISTERED: "+userDTO.toString());
		}

		return ResponseEntity.ok(result);
	}
	
	
	/**
	 * @param request
	 * @throws IOException
	 */
	private void storeConnection(HttpServletRequest request) throws IOException {
	
		connectionService.addConnection(request);
	}

	
	/**
	 * @param welcome
	 * @return
	 */
	private ModelAndView initWelcomeData(ModelAndView welcome) {
		WelcomeMetricsDTO welcomeMetricsDTO = welcomeMetricsService.getWelcomeMetrics();
		
		SortedMap<String, String> countries = MapsTransformUtils.localeRecourceToMap("list/countries");
		SortedMap<String, String> spanishTowns = MapsTransformUtils.localeRecourceToMap("list/spanish_towns");

		welcome.addObject("welcomeMetricsDTO", welcomeMetricsDTO);
		welcome.addObject("countries", countries);
		welcome.addObject("spanishTowns", spanishTowns);
		welcome.addObject("key", "welcome");
		
		return welcome;
	}
	
	
	/**
	 * @param userDTO
	 * @param result
	 * @param errorList
	 * @param messageBundle
	 * @param validator
	 */
	private void checkUsername(UserDTO userDTO, AjaxResponseBodyDTO result, List<String> errorList,
			ResourceBundle messageBundle, EmailValidator validator) {
		
		if("".equals(userDTO.getUsername()) || userDTO.getUsername() == null) {
			
			result.setCode(ResponseCode.ERROR.toString());
			errorList.add(messageBundle.getString("error.email"));
		} else {
			
			// Validate specified String containing an email address
			if (validator.isValid(userDTO.getUsername())) {
				
				Boolean userExist = userService.findUser(userDTO);
				
				if(userExist){
					result.setCode(ResponseCode.ERROR.toString());
					errorList.add(messageBundle.getString("error.email.exist"));
					
				} else {
					result.setCode(ResponseCode.OK.toString());
				}
			} else {
				result.setCode(ResponseCode.ERROR.toString());
				errorList.add(messageBundle.getString("error.email.invalid"));
			}

		}
	}

}