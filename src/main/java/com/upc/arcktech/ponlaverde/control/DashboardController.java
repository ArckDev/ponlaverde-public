package com.upc.arcktech.ponlaverde.control;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.dto.DashboardChartsAdminDTO;
import com.upc.arcktech.ponlaverde.dto.DashboardChartsSupplierDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.service.DashboardService;

@Controller
@Scope("session")
@PreAuthorize("@featureStatus.isActive('DAS')")
public class DashboardController extends BaseController {
	
	@Autowired
	DashboardService dashboardService;
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	@GetMapping("/admin/dashboard")
	public ModelAndView adminDashboard(HttpServletRequest request) throws JsonProcessingException {

		ModelAndView dashboard = new ModelAndView("admin/dashboard/dashboard");
	
		DashboardChartsAdminDTO chartsDTO = new DashboardChartsAdminDTO();
		chartsDTO.setConnectedUsers(dashboardService.currentConnectedUsers());
		chartsDTO.setConnections(dashboardService.connectionsToSystem());
		chartsDTO.setAccess(dashboardService.loginsToSystem());
		chartsDTO.setDeviceOS(dashboardService.accessDeviceOS());
		chartsDTO.setDeviceType(dashboardService.accessDeviceType());
		chartsDTO.setIndUserRegister(dashboardService.individualUserRegistered());
		chartsDTO.setSupUserRegister(dashboardService.supplierUserRegistered());
		chartsDTO.setSystemTotalRoofsByStatus(dashboardService.systemTotalRoofsByStatus());
		chartsDTO.setMostValuedVariablesInRecommender(dashboardService.averageScoreVariablesInRecommender());
		chartsDTO.setRoofClimateSelectedInRecommender(dashboardService.roofClimateSelectedInRecommender());
		chartsDTO.setRoofSlopeSelectedInRecommender(dashboardService.roofSlopeSelectedInRecommender());
		chartsDTO.setMostPopularChosenRoofsInRecommender(dashboardService.mostPopularChosenRoofsInRecommender());
		chartsDTO.setMostSuggestedRoofsInRecommender(dashboardService.mostSuggestedRoofsInRecommender());
		chartsDTO.setUsersByCityAndCountry(dashboardService.usersByCityAndCountry());
		chartsDTO.setCommonCitiesConnection(dashboardService.commonCitiesConnection());
		chartsDTO.setCommonCountriesConnection(dashboardService.commonCountriesConnection());	
		chartsDTO.setUseOfFeatures(dashboardService.useOfFeatures());

		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		String chartsJson = mapper.writeValueAsString(chartsDTO);
		
		// Lists
		dashboard.addObject("popularChosenRoofs", chartsDTO.getMostPopularChosenRoofsInRecommender());
		dashboard.addObject("suggestedRoofs", chartsDTO.getMostSuggestedRoofsInRecommender());
		dashboard.addObject("successRate", dashboardService.successRateInRecommender());
		dashboard.addObject("usersByCityAndCountry", chartsDTO.getUsersByCityAndCountry());
		dashboard.addObject("commonCitiesConnection", chartsDTO.getCommonCitiesConnection());
		dashboard.addObject("commonCountriesConnection", chartsDTO.getCommonCountriesConnection());
		
		
		// Data linked to Charts
		dashboard.addObject("adminDashboardJson", chartsJson);
		dashboard.addObject("indUserRegisterTotal", chartsDTO.getIndUserRegister().getTotal());
		dashboard.addObject("supUserRegisterTotal", chartsDTO.getSupUserRegister().getTotal());
		dashboard.addObject("connections", chartsDTO.getConnections().getTotal());
		dashboard.addObject("access", chartsDTO.getAccess().getTotal());
		dashboard.addObject("connectedUsers", chartsDTO.getConnectedUsers().get(0).getCount());
		dashboard.addObject("deviceOS", chartsDTO.getDeviceOS().size());
		dashboard.addObject("deviceType", chartsDTO.getDeviceType().size());
		dashboard.addObject("systemTotalRoofsByStatus", chartsDTO.getSystemTotalRoofsByStatus().getTotal());

		storeUsedFeature(request, FeatureName.DASHBOARD);

		return dashboard;
	}
	
	
	@GetMapping("/supplier/dashboard")
	public ModelAndView supplierDashboard(HttpServletRequest request) throws JsonProcessingException {

		ModelAndView dashboard = new ModelAndView("supplier/dashboard/dashboard");
		
		UserDTO userDTO = loggedUserInfo();
		
		DashboardChartsSupplierDTO chartsDTO = new DashboardChartsSupplierDTO();
		chartsDTO.setSupplierMostSuggestedRoofs(dashboardService.supplierMostSuggestedRoofsInRecommender(userDTO)); 
		chartsDTO.setSupplierCommonUserCities(dashboardService.supplierCommonUserCitiesInRecommender(userDTO)); 
		chartsDTO.setSupplierMostChosenRoofs(dashboardService.supplierMostChosenRoofInRecommender(userDTO)); 
		chartsDTO.setSupplierTotalRoofsByStatus(dashboardService.supplierTotalRoofsByStatus(userDTO));

		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		String chartsJson = mapper.writeValueAsString(chartsDTO);
		
		// Lists
		dashboard.addObject("supplierMostSuggestedRoofs", chartsDTO.getSupplierMostSuggestedRoofs());
		dashboard.addObject("supplierMostChosenRoofs", chartsDTO.getSupplierMostChosenRoofs());
		dashboard.addObject("supplierCommonUserCities", chartsDTO.getSupplierCommonUserCities());
		
		
		// Data linked to Charts
		dashboard.addObject("supplierDashboardJson", chartsJson);
		dashboard.addObject("supplierTotalRoofsByStatus", chartsDTO.getSupplierTotalRoofsByStatus().getTotal());


		storeUsedFeature(request, FeatureName.DASHBOARD);

		return dashboard;
	}

}
