package com.upc.arcktech.ponlaverde.control;


import java.util.List;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.constants.Modals;
import com.upc.arcktech.ponlaverde.dto.FeatureStatusDTO;
import com.upc.arcktech.ponlaverde.dto.FeatureStatusListDTO;
import com.upc.arcktech.ponlaverde.dto.ParameterDTO;
import com.upc.arcktech.ponlaverde.dto.ParametersListDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.service.FeatureStatusService;
import com.upc.arcktech.ponlaverde.service.ParameterService;
import com.upc.arcktech.ponlaverde.service.UserService;
import com.upc.arcktech.ponlaverde.utils.MapsTransformUtils;



/**
 * It paths to specific settings menu of each kind of user
 * 
 * @author jose.rodriguez
 *
 */
@Controller
@Scope("session")
public class SettingsController extends BaseController {
	
	@Autowired
	UserService userService;
	@Autowired
	ParameterService parameterService;
	@Autowired
	FeatureStatusService featureStatusService;
	
	/* ################################################################################################################## */
	/* #############################################        ADMIN        ################################################ */
	/* ################################################################################################################## */

	
	
	@RequestMapping("/admin/settings")
	public ModelAndView adminSettings(HttpServletRequest request) {
		
		storeUsedFeature(request, FeatureName.SETTINGS);
		
		ModelAndView settings = new ModelAndView("admin/settings/settings");
		adminSettingsInit(settings);
		

		return settings;
	}
	
	@RequestMapping("/admin/settings-success")
	public ModelAndView adminSettingsModal(@RequestParam("modalType") String modalType, @RequestParam("message") String message) {
		
		ModelAndView settings = new ModelAndView("admin/settings/settings");
		adminSettingsInit(settings);
		
		modalWithMessage(modalType, message, settings);
		
		
		return settings;
	}
	
	
	@RequestMapping("/admin/settings/update-user")
	public String adminSettingsUpdateUser(UserDTO userDTO, RedirectAttributes ra) {

		this.userService.updateUser(userDTO);
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "settings.updated");

		
		return "redirect:/admin/settings-success";
	}
	
	@RequestMapping("/admin/settings/update-parameters")
	public String adminSettingsUpdateParameters(ParametersListDTO parametersListDTO,
			FeatureStatusListDTO featureStatusListDTO, RedirectAttributes ra) {

		this.parameterService.updateParameters(parametersListDTO);
		this.featureStatusService.updateFeaturesStatus(featureStatusListDTO);
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "settings.updated");

		
		return "redirect:/admin/settings-success";
	}


	
	/* ################################################################################################################## */
	/* ##########################################        INDIVIDUAL        ############################################## */
	/* ################################################################################################################## */

	
	
	@RequestMapping("/individual/settings")
	@PreAuthorize("@featureStatus.isActive('SET')")
	public ModelAndView individualSettings(HttpServletRequest request) {
		
		storeUsedFeature(request, FeatureName.SETTINGS);
		
		ModelAndView settings = new ModelAndView("individual/settings/settings");
		settingsInit(settings);
		

		return settings;
	}
	
	@RequestMapping("/individual/settings-success")
	@PreAuthorize("@featureStatus.isActive('SET')")
	public ModelAndView individualSettingsModal(@RequestParam("modalType") String modalType, @RequestParam("message") String message) {
		
		ModelAndView settings = new ModelAndView("individual/settings/settings");
		settingsInit(settings);
		
		modalWithMessage(modalType, message, settings);
		
		
		return settings;
	}
	
	
	@RequestMapping("/individual/settings/update-user")
	@PreAuthorize("@featureStatus.isActive('SET')")
	public String individualSettingsUpdateUser(UserDTO userDTO, RedirectAttributes ra) {

		this.userService.updateUser(userDTO);
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "settings.updated");

		
		return "redirect:/individual/settings-success";
	}
	
	
	
	/* ################################################################################################################## */
	/* ###########################################        SUPPLIER        ############################################### */
	/* ################################################################################################################## */

	
	
	@RequestMapping("/supplier/settings")
	@PreAuthorize("@featureStatus.isActive('SET')")
	public ModelAndView supplierSettings(HttpServletRequest request) {
		
		storeUsedFeature(request, FeatureName.SETTINGS);
		
		ModelAndView settings = new ModelAndView("supplier/settings/settings");
		settingsInit(settings);
		

		return settings;
	}
	
	@RequestMapping("/supplier/settings-success")
	@PreAuthorize("@featureStatus.isActive('SET')")
	public ModelAndView supplierSettingsModal(@RequestParam("modalType") String modalType, @RequestParam("message") String message) {
		
		ModelAndView settings = new ModelAndView("supplier/settings/settings");
		settingsInit(settings);
		
		modalWithMessage(modalType, message, settings);
		
		
		return settings;
	}
	
	
	@RequestMapping("/supplier/settings/update-user")
	@PreAuthorize("@featureStatus.isActive('SET')")
	public String supplierSettingsUpdateUser(UserDTO userDTO, RedirectAttributes ra) {

		this.userService.updateUser(userDTO);
		
		ra.addAttribute("modalType", Modals.SUCCESS_MODAL.toString());
		ra.addAttribute("message", "settings.updated");

		
		return "redirect:/supplier/settings-success";
	}


	
	private void adminSettingsInit(ModelAndView settings) {
		
		SortedMap<String, String> countries = MapsTransformUtils.localeRecourceToMap("list/countries");
		SortedMap<String, String> spanishTowns = MapsTransformUtils.localeRecourceToMap("list/spanish_towns");
		
		List<ParameterDTO> parameterList = this.parameterService.getAllParameters();
		List<FeatureStatusDTO> featureStatusList = this.featureStatusService.getAllFeaturesStatus();

		settings.addObject("countries", countries);
		settings.addObject("spanishTowns", spanishTowns);
		settings.addObject("userDTO", loggedUserInfo());
		settings.addObject("parameterList", parameterList);
		settings.addObject("featureStatusList", featureStatusList);
	}
	
	private void settingsInit(ModelAndView settings) {
		
		SortedMap<String, String> countries = MapsTransformUtils.localeRecourceToMap("list/countries");
		SortedMap<String, String> spanishTowns = MapsTransformUtils.localeRecourceToMap("list/spanish_towns");
		
		settings.addObject("countries", countries);
		settings.addObject("spanishTowns", spanishTowns);
		settings.addObject("userDTO", loggedUserInfo());

	}
	
}