package com.upc.arcktech.ponlaverde.dto;


import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


public class RoofReportedErrorDTO {
    
    private Long idReportedErrorRoof;
    private String errorReported;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date reportDate;


	public Long getIdReportedErrorRoof() {
		return idReportedErrorRoof;
	}

	public void setIdReportedErrorRoof(Long idReportedErrorRoof) {
		this.idReportedErrorRoof = idReportedErrorRoof;
	}

	public String getErrorReported() {
		return errorReported;
	}

	public void setErrorReported(String errorReported) {
		this.errorReported = errorReported;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}



}
