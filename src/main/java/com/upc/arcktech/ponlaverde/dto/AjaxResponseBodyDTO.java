package com.upc.arcktech.ponlaverde.dto;

import java.util.List;

public class AjaxResponseBodyDTO {

    String code;
    List<String> result;

    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }

	

}
