package com.upc.arcktech.ponlaverde.dto;

public class StatAverageRatedValuesDTO {
    

	private Double aesthetics;
    private Double implantation;
    private Double maintenance;
    private Double price;
    private Double regulation;
    private Double storage;
    private Double thermal;
    private Double traffic;
    private Double wall;
    private Double weight;
    private Double winds;
    

    public StatAverageRatedValuesDTO() {
		super();
	}

	public StatAverageRatedValuesDTO(Double aesthetics, Double implantation, Double maintenance, Double price,
			Double regulation, Double storage, Double thermal, Double traffic, Double wall, Double weight,
			Double winds) {
		super();
		this.aesthetics = aesthetics;
		this.implantation = implantation;
		this.maintenance = maintenance;
		this.price = price;
		this.regulation = regulation;
		this.storage = storage;
		this.thermal = thermal;
		this.traffic = traffic;
		this.wall = wall;
		this.weight = weight;
		this.winds = winds;
	}
	
	
	
	public Double getAesthetics() {
		return aesthetics;
	}
	public void setAesthetics(Double aesthetics) {
		this.aesthetics = aesthetics;
	}
	public Double getImplantation() {
		return implantation;
	}
	public void setImplantation(Double implantation) {
		this.implantation = implantation;
	}
	public Double getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(Double maintenance) {
		this.maintenance = maintenance;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getRegulation() {
		return regulation;
	}
	public void setRegulation(Double regulation) {
		this.regulation = regulation;
	}
	public Double getStorage() {
		return storage;
	}
	public void setStorage(Double storage) {
		this.storage = storage;
	}
	public Double getThermal() {
		return thermal;
	}
	public void setThermal(Double thermal) {
		this.thermal = thermal;
	}
	public Double getTraffic() {
		return traffic;
	}
	public void setTraffic(Double traffic) {
		this.traffic = traffic;
	}
	public Double getWall() {
		return wall;
	}
	public void setWall(Double wall) {
		this.wall = wall;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getWinds() {
		return winds;
	}
	public void setWinds(Double winds) {
		this.winds = winds;
	}


	
	

}
