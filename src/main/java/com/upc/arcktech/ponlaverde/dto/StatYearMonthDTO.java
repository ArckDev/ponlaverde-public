package com.upc.arcktech.ponlaverde.dto;

public class StatYearMonthDTO {
	
	private Integer year;
	private Integer month;
	

	public StatYearMonthDTO(Integer year, Integer month) {
		super();
		this.year = year;
		this.month = month;
	}
	
	
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}


	@Override
	public String toString() {
		return "StatYearMonthDTO [year=" + year + ", month=" + month + "]";
	}
	
	

}
