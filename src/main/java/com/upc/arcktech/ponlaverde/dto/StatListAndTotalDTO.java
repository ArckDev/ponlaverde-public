package com.upc.arcktech.ponlaverde.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class StatListAndTotalDTO<T> {
	
	List<T> list;
	@JsonIgnore
	Integer total;
	
	
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}

}
