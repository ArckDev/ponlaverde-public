package com.upc.arcktech.ponlaverde.dto;

import java.util.List;

public class DashboardChartsAdminDTO {
		
	private List<StatCountByFieldDTO> deviceOS;
	private List<StatCountByFieldDTO> deviceType;
	private StatListAndTotalDTO<?> connections;
	private StatListAndTotalDTO<?> access;
	private List<StatCountByFieldDTO> connectedUsers;
	private StatListAndTotalDTO<?> indUserRegister;
	private StatListAndTotalDTO<?> supUserRegister;
	private StatListAndTotalDTO<?> systemTotalRoofsByStatus;
	private List<StatCountByFieldDTO> mostValuedVariablesInRecommender;
	private List<StatCountByFieldDTO> roofSlopeSelectedInRecommender;
	private List<StatCountByFieldDTO> roofClimateSelectedInRecommender;
	private List<StatCountByMakerModelDTO> mostPopularChosenRoofsInRecommender;
	private List<StatCountByMakerModelDTO> mostSuggestedRoofsInRecommender;
	private List<StatCountByCityCountryDTO> usersByCityAndCountry;
	private List<StatCountByCityCountryDTO> commonCitiesConnection;
	private List<StatCountByFieldDTO> commonCountriesConnection;
	private StatUsedFeatureDTO useOfFeatures;
	
	
	
	
	public List<StatCountByFieldDTO> getDeviceOS() {
		return deviceOS;
	}
	public void setDeviceOS(List<StatCountByFieldDTO> deviceOS) {
		this.deviceOS = deviceOS;
	}
	public List<StatCountByFieldDTO> getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(List<StatCountByFieldDTO> deviceType) {
		this.deviceType = deviceType;
	}
	public StatListAndTotalDTO<?> getConnections() {
		return connections;
	}
	public void setConnections(StatListAndTotalDTO<?> connections) {
		this.connections = connections;
	}
	public StatListAndTotalDTO<?> getAccess() {
		return access;
	}
	public void setAccess(StatListAndTotalDTO<?> access) {
		this.access = access;
	}
	public List<StatCountByFieldDTO> getConnectedUsers() {
		return connectedUsers;
	}
	public void setConnectedUsers(List<StatCountByFieldDTO> connectedUsers) {
		this.connectedUsers = connectedUsers;
	}
	public StatListAndTotalDTO<?> getIndUserRegister() {
		return indUserRegister;
	}
	public void setIndUserRegister(StatListAndTotalDTO<?> indUserRegister) {
		this.indUserRegister = indUserRegister;
	}
	public StatListAndTotalDTO<?> getSupUserRegister() {
		return supUserRegister;
	}
	public void setSupUserRegister(StatListAndTotalDTO<?> supUserRegister) {
		this.supUserRegister = supUserRegister;
	}
	public StatListAndTotalDTO<?> getSystemTotalRoofsByStatus() {
		return systemTotalRoofsByStatus;
	}
	public void setSystemTotalRoofsByStatus(StatListAndTotalDTO<?> systemTotalRoofsByStatus) {
		this.systemTotalRoofsByStatus = systemTotalRoofsByStatus;
	}
	public List<StatCountByFieldDTO> getMostValuedVariablesInRecommender() {
		return mostValuedVariablesInRecommender;
	}
	public void setMostValuedVariablesInRecommender(List<StatCountByFieldDTO> mostValuedVariablesInRecommender) {
		this.mostValuedVariablesInRecommender = mostValuedVariablesInRecommender;
	}
	public List<StatCountByFieldDTO> getRoofSlopeSelectedInRecommender() {
		return roofSlopeSelectedInRecommender;
	}
	public void setRoofSlopeSelectedInRecommender(List<StatCountByFieldDTO> roofSlopeSelectedInRecommender) {
		this.roofSlopeSelectedInRecommender = roofSlopeSelectedInRecommender;
	}
	public List<StatCountByFieldDTO> getRoofClimateSelectedInRecommender() {
		return roofClimateSelectedInRecommender;
	}
	public void setRoofClimateSelectedInRecommender(List<StatCountByFieldDTO> roofClimateSelectedInRecommender) {
		this.roofClimateSelectedInRecommender = roofClimateSelectedInRecommender;
	}
	public List<StatCountByMakerModelDTO> getMostPopularChosenRoofsInRecommender() {
		return mostPopularChosenRoofsInRecommender;
	}
	public void setMostPopularChosenRoofsInRecommender(List<StatCountByMakerModelDTO> mostPopularChosenRoofsInRecommender) {
		this.mostPopularChosenRoofsInRecommender = mostPopularChosenRoofsInRecommender;
	}
	public List<StatCountByMakerModelDTO> getMostSuggestedRoofsInRecommender() {
		return mostSuggestedRoofsInRecommender;
	}
	public void setMostSuggestedRoofsInRecommender(List<StatCountByMakerModelDTO> mostSuggestedRoofsInRecommender) {
		this.mostSuggestedRoofsInRecommender = mostSuggestedRoofsInRecommender;
	}
	public List<StatCountByCityCountryDTO> getUsersByCityAndCountry() {
		return usersByCityAndCountry;
	}
	public void setUsersByCityAndCountry(List<StatCountByCityCountryDTO> usersByCityAndCountry) {
		this.usersByCityAndCountry = usersByCityAndCountry;
	}
	public List<StatCountByCityCountryDTO> getCommonCitiesConnection() {
		return commonCitiesConnection;
	}
	public void setCommonCitiesConnection(List<StatCountByCityCountryDTO> commonCitiesConnection) {
		this.commonCitiesConnection = commonCitiesConnection;
	}
	public StatUsedFeatureDTO getUseOfFeatures() {
		return useOfFeatures;
	}
	public void setUseOfFeatures(StatUsedFeatureDTO useOfFeatures) {
		this.useOfFeatures = useOfFeatures;
	}
	public List<StatCountByFieldDTO> getCommonCountriesConnection() {
		return commonCountriesConnection;
	}
	public void setCommonCountriesConnection(List<StatCountByFieldDTO> commonCountriesConnection) {
		this.commonCountriesConnection = commonCountriesConnection;
	}


	
	
	
}
