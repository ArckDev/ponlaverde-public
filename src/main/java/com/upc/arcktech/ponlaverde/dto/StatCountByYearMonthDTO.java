package com.upc.arcktech.ponlaverde.dto;

public class StatCountByYearMonthDTO {
	
	private Long count;
	private Integer year;
	private Integer month;	
	private String monthName;
	
	public StatCountByYearMonthDTO() {
		super();
	}
	
	public StatCountByYearMonthDTO(Long count, Integer year, Integer month) {
		super();
		this.count = count;
		this.year = year;
		this.month = month;
	}

	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public String getDate() {
		return this.monthName.substring(0, 3) + "-" + this.year.toString().substring(2, 4);
	}

	@Override
	public String toString() {
		return "StatCountByYearMonthDTO [count=" + count + ", year=" + year + ", month=" + month + ", monthName="
				+ monthName + "]";
	}
	

}
