package com.upc.arcktech.ponlaverde.dto;


/**
 * 	Object with the rate for each parameter entered by the user
 *
 */
public class UserRoofRatingDTO {
    

	private String climate;
	private String slope;

    private Integer price;
    private Integer weight;
    private Integer wall;
    private Integer storage;
    private Integer implantation;
    private Integer maintenance;
    private Integer winds;
    private Integer thermal;
    private Integer regulation;
    private Integer traffic;
    private Integer aesthetics;
    private String results;

    
	public String getClimate() {
		return climate;
	}
	public void setClimate(String climate) {
		this.climate = climate;
	}
	public String getSlope() {
		return slope;
	}
	public void setSlope(String slope) {
		this.slope = slope;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Integer getWall() {
		return wall;
	}
	public void setWall(Integer wall) {
		this.wall = wall;
	}
	public Integer getStorage() {
		return storage;
	}
	public void setStorage(Integer storage) {
		this.storage = storage;
	}
	public Integer getImplantation() {
		return implantation;
	}
	public void setImplantation(Integer implantation) {
		this.implantation = implantation;
	}
	public Integer getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(Integer maintenance) {
		this.maintenance = maintenance;
	}
	public Integer getWinds() {
		return winds;
	}
	public void setWinds(Integer winds) {
		this.winds = winds;
	}
	public Integer getThermal() {
		return thermal;
	}
	public void setThermal(Integer thermal) {
		this.thermal = thermal;
	}
	public Integer getRegulation() {
		return regulation;
	}
	public void setRegulation(Integer regulation) {
		this.regulation = regulation;
	}
	public Integer getTraffic() {
		return traffic;
	}
	public void setTraffic(Integer traffic) {
		this.traffic = traffic;
	}
	public Integer getAesthetics() {
		return aesthetics;
	}
	public void setAesthetics(Integer aesthetics) {
		this.aesthetics = aesthetics;
	}
	public String getResults() {
		return results;
	}
	public void setResults(String results) {
		this.results = results;
	}
    
    
	
    
    
	


}
