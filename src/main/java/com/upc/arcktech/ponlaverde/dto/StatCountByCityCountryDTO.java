package com.upc.arcktech.ponlaverde.dto;

public class StatCountByCityCountryDTO {
	
	private Long count;
	private String city;
	private String country;

	
	public StatCountByCityCountryDTO(Long count, String city, String country) {
		super();
		this.count = count;
		this.city = city;
		this.country = country;
	}
	
	public StatCountByCityCountryDTO(Long count, String city) {
		super();
		this.count = count;
		this.city = city;
	}

	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	


}
