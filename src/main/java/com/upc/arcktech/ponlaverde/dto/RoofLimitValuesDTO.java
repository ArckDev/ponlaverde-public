package com.upc.arcktech.ponlaverde.dto;

import java.util.ArrayList;
import java.util.List;

public class RoofLimitValuesDTO {

	private List<String> makerList = new ArrayList<String>();
	private Integer priceMax;
	private Integer priceMin;
	private Double priceMaxD;
	private Double priceMinD;	
	private Integer weightMax;
	private Integer weightMin;
	private Integer wallMax;
	private Integer wallMin;

	
	public List<String> getMakerList() {
		return makerList;
	}
	public void setMakerList(List<String> makerList) {
		this.makerList = makerList;
	}
	public Integer getPriceMax() {
		return priceMax;
	}
	public void setPriceMax(Integer priceMax) {
		this.priceMax = priceMax;
	}
	public Integer getPriceMin() {
		return priceMin;
	}
	public void setPriceMin(Integer priceMin) {
		this.priceMin = priceMin;
	}
	public Double getPriceMinD() {
		return priceMinD;
	}
	public void setPriceMinD(Double priceMinD) {
		this.priceMinD = priceMinD;
	}
	public Double getPriceMaxD() {
		return priceMaxD;
	}
	public void setPriceMaxD(Double priceMaxD) {
		this.priceMaxD = priceMaxD;
	}
	public Integer getWeightMax() {
		return weightMax;
	}
	public void setWeightMax(Integer weightMax) {
		this.weightMax = weightMax;
	}
	public Integer getWeightMin() {
		return weightMin;
	}
	public void setWeightMin(Integer weightMin) {
		this.weightMin = weightMin;
	}
	public Integer getWallMax() {
		return wallMax;
	}
	public void setWallMax(Integer wallMax) {
		this.wallMax = wallMax;
	}
	public Integer getWallMin() {
		return wallMin;
	}
	public void setWallMin(Integer wallMin) {
		this.wallMin = wallMin;
	}
	
	
	
	

}
