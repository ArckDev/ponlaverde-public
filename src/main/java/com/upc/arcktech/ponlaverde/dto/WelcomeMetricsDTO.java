package com.upc.arcktech.ponlaverde.dto;

public class WelcomeMetricsDTO {

	private Long idWelMet;
	private int visits;
	private int catalogue;
	private int users;
	private int suppliers;
	
	
	public Long getIdWelMet() {
		return idWelMet;
	}
	public void setIdWelMet(Long id) {
		this.idWelMet = id;
	}
	public int getVisits() {
		return visits;
	}
	public void setVisits(int visits) {
		this.visits = visits;
	}
	public int getCatalogue() {
		return catalogue;
	}
	public void setCatalogue(int catalogue) {
		this.catalogue = catalogue;
	}
	public int getUsers() {
		return users;
	}
	public void setUsers(int users) {
		this.users = users;
	}
	public int getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(int suppliers) {
		this.suppliers = suppliers;
	}

}
