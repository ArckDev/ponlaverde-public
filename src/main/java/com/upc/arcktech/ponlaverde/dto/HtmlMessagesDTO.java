package com.upc.arcktech.ponlaverde.dto;


public class HtmlMessagesDTO {

    private Long idHtml;
	private String message;
	private String subject;
	private Integer serviceId;
	
	
	public HtmlMessagesDTO() {
		super();
	}

	public HtmlMessagesDTO(String message, String subject) {
		super();
		this.message = message;
		this.subject = subject;
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Long getIdHtml() {
		return idHtml;
	}

	public void setIdHtml(Long id) {
		this.idHtml = id;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}




}
