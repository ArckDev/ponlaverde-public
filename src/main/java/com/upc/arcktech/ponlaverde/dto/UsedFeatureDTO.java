package com.upc.arcktech.ponlaverde.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


public class UsedFeatureDTO {
    
    private Long idUsedServ;
    private String serviceName;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date accessDate;
    private String session;
    private UserDTO userDTO;
    
    
	public Long getIdUsedServ() {
		return idUsedServ;
	}
	public void setIdUsedServ(Long idUsedServ) {
		this.idUsedServ = idUsedServ;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Date getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
	@Override
	public String toString() {
		return "UsedFeatureDTO [idUsedServ=" + idUsedServ + ", serviceName=" + serviceName + ", accessDate="
				+ accessDate + ", session=" + session + ", userDTO=" + userDTO + "]";
	}
    

}
