package com.upc.arcktech.ponlaverde.dto;

import java.util.List;

public class StatUsedFeatureDTO {
	
	List<StatCountByYearMonthDTO> cat;
	List<StatCountByYearMonthDTO> das;
	List<StatCountByYearMonthDTO> rec;
	List<StatCountByYearMonthDTO> err;
	List<StatCountByYearMonthDTO> set;
	List<StatCountByYearMonthDTO> val;
	List<StatCountByYearMonthDTO> sav;
	
	
	public List<StatCountByYearMonthDTO> getCat() {
		return cat;
	}
	public void setCat(List<StatCountByYearMonthDTO> cat) {
		this.cat = cat;
	}
	public List<StatCountByYearMonthDTO> getDas() {
		return das;
	}
	public void setDas(List<StatCountByYearMonthDTO> das) {
		this.das = das;
	}
	public List<StatCountByYearMonthDTO> getRec() {
		return rec;
	}
	public void setRec(List<StatCountByYearMonthDTO> rec) {
		this.rec = rec;
	}
	public List<StatCountByYearMonthDTO> getErr() {
		return err;
	}
	public void setErr(List<StatCountByYearMonthDTO> err) {
		this.err = err;
	}
	public List<StatCountByYearMonthDTO> getSet() {
		return set;
	}
	public void setSet(List<StatCountByYearMonthDTO> set) {
		this.set = set;
	}
	public List<StatCountByYearMonthDTO> getVal() {
		return val;
	}
	public void setVal(List<StatCountByYearMonthDTO> val) {
		this.val = val;
	}
	public List<StatCountByYearMonthDTO> getSav() {
		return sav;
	}
	public void setSav(List<StatCountByYearMonthDTO> sav) {
		this.sav = sav;
	}
	
	
	
	

}
