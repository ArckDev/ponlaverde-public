package com.upc.arcktech.ponlaverde.dto;

public class StatCountByFieldYearMonthDTO {
	
	private Long count;
	private String field;
	private Integer year;
	private Integer month;	
	private String monthName;
	
	
	public StatCountByFieldYearMonthDTO() {
		super();
	}
	
	public StatCountByFieldYearMonthDTO(Long count, String field, Integer year, Integer month) {
		super();
		this.count = count;
		this.field = field;
		this.year = year;
		this.month = month;
	}


	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public String getDate() {
		return this.monthName.substring(0, 3) + "-" + this.year.toString().substring(2, 4);
	}


	@Override
	public String toString() {
		return "StatCountByFieldYearMonthDTO [count=" + count + ", field=" + field + ", year=" + year + ", month="
				+ month + "]";
	}
	
	
	

}
