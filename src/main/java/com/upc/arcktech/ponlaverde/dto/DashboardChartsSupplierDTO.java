package com.upc.arcktech.ponlaverde.dto;

import java.util.List;

public class DashboardChartsSupplierDTO {

	private List<StatCountByMakerModelDTO> supplierMostChosenRoofs;
	private List<StatCountByMakerModelDTO> supplierMostSuggestedRoofs;
	private List<StatCountByCityCountryDTO> supplierCommonUserCities;
	private StatListAndTotalDTO<StatCountByFieldDTO> supplierTotalRoofsByStatus;
	
	
	public List<StatCountByMakerModelDTO> getSupplierMostChosenRoofs() {
		return supplierMostChosenRoofs;
	}
	public void setSupplierMostChosenRoofs(List<StatCountByMakerModelDTO> supplierMostChosenRoof) {
		this.supplierMostChosenRoofs = supplierMostChosenRoof;
	}
	public List<StatCountByMakerModelDTO> getSupplierMostSuggestedRoofs() {
		return supplierMostSuggestedRoofs;
	}
	public void setSupplierMostSuggestedRoofs(List<StatCountByMakerModelDTO> supplierMostSuggestedRoofs) {
		this.supplierMostSuggestedRoofs = supplierMostSuggestedRoofs;
	}
	public List<StatCountByCityCountryDTO> getSupplierCommonUserCities() {
		return supplierCommonUserCities;
	}
	public void setSupplierCommonUserCities(List<StatCountByCityCountryDTO> supplierCommonUserCities) {
		this.supplierCommonUserCities = supplierCommonUserCities;
	}
	public StatListAndTotalDTO<StatCountByFieldDTO> getSupplierTotalRoofsByStatus() {
		return supplierTotalRoofsByStatus;
	}
	public void setSupplierTotalRoofsByStatus(StatListAndTotalDTO<StatCountByFieldDTO> supplierTotalRoofsByStatus) {
		this.supplierTotalRoofsByStatus = supplierTotalRoofsByStatus;
	}
	
	
	
	
}
