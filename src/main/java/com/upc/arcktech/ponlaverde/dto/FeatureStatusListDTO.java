package com.upc.arcktech.ponlaverde.dto;

import java.util.List;

public class FeatureStatusListDTO {
	
	private List<String> featureStatus;

	public List<String> getFeatureStatus() {
		return featureStatus;
	}

	public void setFeatureStatus(List<String> featureStatus) {
		this.featureStatus = featureStatus;
	}

}
