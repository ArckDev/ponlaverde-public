package com.upc.arcktech.ponlaverde.dto;

public class StatCountByFieldDTO {
	
	private Long count;
	private String field;
	private Double countD;
	
	
	public StatCountByFieldDTO(String field, Double countD) {
		super();
		this.field = field;
		this.countD = countD;
	}


	public StatCountByFieldDTO(Long count, String field) {
		super();
		this.count = count;
		this.field = field;
	}
	
	
	public StatCountByFieldDTO() {
		super();
	}


	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public Double getCountD() {
		return countD;
	}
	public void setCountD(Double countD) {
		this.countD = countD;
	}


}
