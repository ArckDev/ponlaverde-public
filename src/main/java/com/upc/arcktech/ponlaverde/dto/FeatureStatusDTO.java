package com.upc.arcktech.ponlaverde.dto;


public class FeatureStatusDTO {
    
    private String name;
    private Boolean active;

    
	public FeatureStatusDTO() {
		super();
	}

	public FeatureStatusDTO(String name, Boolean active) {
		super();
		this.name = name;
		this.active = active;
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

	
	@Override
	public String toString() {
		return "FeatureStatusDTO [name=" + name + ", active=" + active + "]";
	}

}
