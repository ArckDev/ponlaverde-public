package com.upc.arcktech.ponlaverde.dto;


public class ParameterDTO {

	private Long idParameter;
	private String feature;
	private String parameter;
	private Boolean active;
	
	
	public Long getIdParameter() {
		return idParameter;
	}
	public void setIdParameter(Long idParameter) {
		this.idParameter = idParameter;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getFeature() {
		return feature;
	}
	public void setFeature(String feature) {
		this.feature = feature;
	}
	
	
	@Override
	public String toString() {
		return "ParameterDTO [idParameter=" + idParameter + ", feature=" + feature + ", parameter=" + parameter
				+ ", active=" + active + "]";
	}


}
