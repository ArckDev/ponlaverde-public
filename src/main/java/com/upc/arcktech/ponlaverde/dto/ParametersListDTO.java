package com.upc.arcktech.ponlaverde.dto;

import java.util.List;

public class ParametersListDTO {
	
	private List<String> parameter;

	public List<String> getParameter() {
		return parameter;
	}

	public void setParameter(List<String> parameter) {
		this.parameter = parameter;
	}



}
