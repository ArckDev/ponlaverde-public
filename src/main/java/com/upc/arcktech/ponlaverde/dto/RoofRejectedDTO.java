package com.upc.arcktech.ponlaverde.dto;


import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


public class RoofRejectedDTO {
    

    private Long idRejectedRoof;
    private String rejectionReason;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date rejectionDate;


	public Long getIdRejectedRoof() {
		return idRejectedRoof;
	}

	public void setIdRejectedRoof(Long idRejectedRoof) {
		this.idRejectedRoof = idRejectedRoof;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public Date getRejectionDate() {
		return rejectionDate;
	}

	public void setRejectionDate(Date rejectionDate) {
		this.rejectionDate = rejectionDate;
	}


    





}
