package com.upc.arcktech.ponlaverde.dto;

public class BestRoofDTO {
	
	private RoofDTO roofDTO;
	private RoofRatingDTO roofRatingDTO;
	
	
	public RoofDTO getRoofDTO() {
		return roofDTO;
	}
	public void setRoofDTO(RoofDTO roofDTO) {
		this.roofDTO = roofDTO;
	}
	public RoofRatingDTO getRoofRatingDTO() {
		return roofRatingDTO;
	}
	public void setRoofRatingDTO(RoofRatingDTO roofRatingDTO) {
		this.roofRatingDTO = roofRatingDTO;
	}

}
