package com.upc.arcktech.ponlaverde.dto;


import java.util.List;


public class PagedResultsDTO {
	
	private List<RoofDTO> elements;
	private Long totalElements;
	private Long totalOwnElements;
	private Long totalValElements;
	private Integer totalPages;
	private Integer number; //Page number
	private Integer size; //Total elements in this RQ
	private Integer ownSize; //Total owned elements in this RQ
	private Integer valSize; //Total validated elements in this RQ
	
	
	public List<RoofDTO> getElements() {
		return elements;
	}
	public void setElements(List<RoofDTO> elements) {
		this.elements = elements;
	}
	public Long getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}
	public Integer getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getOwnSize() {
		return ownSize;
	}
	public void setOwnSize(Integer ownSize) {
		this.ownSize = ownSize;
	}
	public Integer getValSize() {
		return valSize;
	}
	public void setValSize(Integer valSize) {
		this.valSize = valSize;
	}
	public Long getTotalOwnElements() {
		return totalOwnElements;
	}
	public void setTotalOwnElements(Long totalOwnElements) {
		this.totalOwnElements = totalOwnElements;
	}
	public Long getTotalValElements() {
		return totalValElements;
	}
	public void setTotalValElements(Long totalValElements) {
		this.totalValElements = totalValElements;
	}
	





	
	
}
