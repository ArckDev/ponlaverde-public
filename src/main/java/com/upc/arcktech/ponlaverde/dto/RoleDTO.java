package com.upc.arcktech.ponlaverde.dto;


public class RoleDTO {
    
    private Long idRole;
    private String name;
    
    
    public Long getIdRole() {
        return idRole;
    }
    public void setIdRole(Long id) {
        this.idRole = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
}
