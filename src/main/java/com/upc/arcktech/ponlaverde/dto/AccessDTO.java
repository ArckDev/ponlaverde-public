package com.upc.arcktech.ponlaverde.dto;


public class AccessDTO extends BaseAccessConnectionDTO{
	
    private String session;
    private UserDTO userDTO;
    

	public AccessDTO() {
		super();
	}
	
	
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	
	

}
