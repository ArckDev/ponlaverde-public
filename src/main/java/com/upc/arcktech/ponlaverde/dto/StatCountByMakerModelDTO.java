package com.upc.arcktech.ponlaverde.dto;

public class StatCountByMakerModelDTO {
	
	private String roofName;
	private String roofMaker;
	private String roofType;
	private Long count;
	

	public StatCountByMakerModelDTO() {
		super();
	}

	public StatCountByMakerModelDTO(Long count, String roofName, String roofMaker, String roofType) {
		super();
		this.roofName = roofName;
		this.roofMaker = roofMaker;
		this.roofType = roofType;
		this.count = count;
	}
	
	
	
	public String getRoofName() {
		return roofName;
	}
	public void setRoofName(String roofName) {
		this.roofName = roofName;
	}
	public String getRoofMaker() {
		return roofMaker;
	}
	public void setRoofMaker(String roofMaker) {
		this.roofMaker = roofMaker;
	}
	public String getRoofType() {
		return roofType;
	}
	public void setRoofType(String roofType) {
		this.roofType = roofType;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	
	


	

}
