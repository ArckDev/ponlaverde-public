package com.upc.arcktech.ponlaverde.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

public class RoofDTO {
    
    private Long idRoof;
    @NotNull
    @Size(min=2, max=30)
    private String maker;
    @NotNull
    private String model;
    @NotNull
    private Double price;
    @NotNull
    private Integer weight;
    @NotNull
    private Integer wall;
    @NotNull
    private String type;
    @NotNull
    private String slope;
    @NotNull
    private String storage;
    @NotNull
    private String implantation;
    @NotNull
    private String maintenance;
    @NotNull
    private String winds;
    @NotNull
    private String thermal;
    @NotNull
    private String regulation;
    @NotNull
    private String traffic;
    @NotNull
    private String aesthetics;
    private String status;
    private Boolean enabled;
    @NotNull
    private List<String> climate = new ArrayList<>();
    private Date creationDate;
    private MultipartFile[] images;
    private MultipartFile[] details;
    private MultipartFile[] sheets;
    private List<String> uploadedImagesList = new ArrayList<>();
    private List<String> uploadedDetailsList = new ArrayList<>();
    private List<String> uploadedSheetsList = new ArrayList<>();
    private List<RoofRejectedDTO> rejectedRoofDTO  = new ArrayList<>();
    private String rejectionReason;
    private List<RoofReportedErrorDTO> errorReportedListDTO = new ArrayList<>();
    private String errorReported;
    private UserDTO userDTO;
    private Integer rate;
    private Collection<UserSavedRoofDTO> userSavedDTO = new ArrayList<>();  
    
    
	public Long getIdRoof() {
		return idRoof;
	}
	public void setIdRoof(Long idRoof) {
		this.idRoof = idRoof;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getModel() {
		return model;
	}
	public String getModelTruncated() {
		int maxLength = 25;
		if(model.length() > maxLength) {
			return model.substring(0, maxLength).concat("...");
		}else {
			return model;
		}
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Integer getWall() {
		return wall;
	}
	public void setWall(Integer wall) {
		this.wall = wall;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSlope() {
		return slope;
	}
	public void setSlope(String slope) {
		this.slope = slope;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public String getImplantation() {
		return implantation;
	}
	public void setImplantation(String implantation) {
		this.implantation = implantation;
	}
	public String getMaintenance() {
		return maintenance;
	}
	public void setMaintenance(String maintenance) {
		this.maintenance = maintenance;
	}
	public String getWinds() {
		return winds;
	}
	public void setWinds(String winds) {
		this.winds = winds;
	}
	public String getThermal() {
		return thermal;
	}
	public void setThermal(String thermal) {
		this.thermal = thermal;
	}
	public String getRegulation() {
		return regulation;
	}
	public void setRegulation(String regulation) {
		this.regulation = regulation;
	}
	public String getTraffic() {
		return traffic;
	}
	public void setTraffic(String traffic) {
		this.traffic = traffic;
	}
	public String getAesthetics() {
		return aesthetics;
	}
	public void setAesthetics(String aesthetics) {
		this.aesthetics = aesthetics;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getClimate() {
		return climate;
	}
	public void setClimate(List<String> climate) {
		this.climate = climate;
	}
	public MultipartFile[] getImages() {
		return images;
	}
	public void setImages(MultipartFile[] images) {
		this.images = images;
	}
	public MultipartFile[] getDetails() {
		return details;
	}
	public void setDetails(MultipartFile[] details) {
		this.details = details;
	}
	public MultipartFile[] getSheets() {
		return sheets;
	}
	public void setSheets(MultipartFile[] sheets) {
		this.sheets = sheets;
	}
	public List<String> getUploadedImagesList() {
		return uploadedImagesList;
	}
	public void setUploadedImagesList(List<String> uploadedImagesList) {
		this.uploadedImagesList = uploadedImagesList;
	}
	public List<String> getUploadedDetailsList() {
		return uploadedDetailsList;
	}
	public void setUploadedDetailsList(List<String> uploadedDetailsList) {
		this.uploadedDetailsList = uploadedDetailsList;
	}
	public List<String> getUploadedSheetsList() {
		return uploadedSheetsList;
	}
	public void setUploadedSheetsList(List<String> uploadedSheetsList) {
		this.uploadedSheetsList = uploadedSheetsList;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
	public String getRejectionReason() {
		return rejectionReason;
	}
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}
	public List<RoofReportedErrorDTO> getErrorReportedListDTO() {
		return errorReportedListDTO;
	}
	public void setErrorReportedListDTO(List<RoofReportedErrorDTO> errorReportedListDTO) {
		this.errorReportedListDTO = errorReportedListDTO;
	}
	public String getErrorReported() {
		return errorReported;
	}
	public void setErrorReported(String errorReported) {
		this.errorReported = errorReported;
	}
	public Integer countErrorReported() {
		return this.errorReportedListDTO.size();
	}
	public Integer getRate() {
		return rate;
	}
	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public Collection<UserSavedRoofDTO> getUserSavedDTO() {
		return userSavedDTO;
	}
	public void setUserSavedDTO(Collection<UserSavedRoofDTO> userSavedDTO) {
		this.userSavedDTO = userSavedDTO;
	}
	public List<RoofRejectedDTO> getRejectedRoofDTO() {
		return rejectedRoofDTO;
	}
	public void setRejectedRoofDTO(List<RoofRejectedDTO> rejectedRoofDTO) {
		this.rejectedRoofDTO = rejectedRoofDTO;
	}
	
	
	@Override
	public String toString() {
		return "RoofDTO [maker=" + maker + ", model=" + model + ", price=" + price + ", weight=" + weight + ", wall="
				+ wall + ", type=" + type + ", slope=" + slope + ", storage=" + storage + ", implantation="
				+ implantation + ", maintenance=" + maintenance + ", winds=" + winds + ", thermal=" + thermal
				+ ", regulation=" + regulation + ", traffic=" + traffic + ", aesthetics=" + aesthetics + ", status="
				+ status + ", climate=" + climate + "]";
	}

}
