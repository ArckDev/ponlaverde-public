package com.upc.arcktech.ponlaverde.dto;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


public class UserDTO {
    

    private Long idUser;
    private String userType;
    @NotEmpty
    private String username;
    private String name;
    private String surnames;
    @DateTimeFormat(iso = ISO.DATE)
    private Date dateOfBirth;
    private String occupation;
    private String company;
    private String city;
    private String country;
    private String password;
    private String passwordRepeat;
    private Boolean privacy;
    private Boolean terms;
	private String salt;
    private Boolean active;
    private Date registerDate;
    private Date lastUpdateDate;
    private RoleDTO roleDTO;
    private List<UserSavedRoofDTO> userSavedRoofDTO = new ArrayList<>(); 
    
    
    public Long getIdUser() {
        return idUser;
    }
    public void setIdUser(Long id) {
        this.idUser = id;
    }
    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getSurnames() {
        return surnames;
    }
    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }
    public Date getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    public String getOccupation() {
        return occupation;
    }
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getPasswordRepeat() {
		return passwordRepeat;
	}
	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public RoleDTO getRoleDTO() {
		return roleDTO;
	}
	public void setRoleDTO(RoleDTO roleDTO) {
		this.roleDTO = roleDTO;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public List<UserSavedRoofDTO> getUserSavedRoofDTO() {
		return userSavedRoofDTO;
	}
	public void setUserSavedRoofDTO(List<UserSavedRoofDTO> userSavedRoofDTO) {
		this.userSavedRoofDTO = userSavedRoofDTO;
	}
	public Boolean getPrivacy() {
		return privacy;
	}
	public void setPrivacy(Boolean privacy) {
		this.privacy = privacy;
	}
	public Boolean getTerms() {
		return terms;
	}
	public void setTerms(Boolean terms) {
		this.terms = terms;
	}
	@Override
	public String toString() {
		return "UserDTO [idUser=" + idUser + ", userType=" + userType + ", username=" + username + ", name=" + name
				+ ", surnames=" + surnames + ", dateOfBirth=" + dateOfBirth + ", occupation=" + occupation
				+ ", company=" + company + ", city=" + city + ", country=" + country + ", active=" + active
				+ ", registerDate=" + registerDate + ", lastUpdateDate=" + lastUpdateDate + ", roleDTO=" + roleDTO
				+ "]";
	}

	
	

}
