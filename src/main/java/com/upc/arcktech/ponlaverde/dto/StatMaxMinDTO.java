package com.upc.arcktech.ponlaverde.dto;

public class StatMaxMinDTO {
	
	private Integer max;
	private Integer min;


	public StatMaxMinDTO() {
		super();
	}
	public StatMaxMinDTO(Integer max, Integer min) {
		super();
		this.max = max;
		this.min = min;
	}
	
	
	public Integer getMax() {
		return max;
	}
	public void setMax(Integer max) {
		this.max = max;
	}
	public Integer getMin() {
		return min;
	}
	public void setMin(Integer min) {
		this.min = min;
	}
	
	
	

	
}
