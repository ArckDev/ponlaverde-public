package com.upc.arcktech.ponlaverde.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.entity.Parameter;


@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Long>{
	
	Parameter findByParameter(String parameter);

	
}
