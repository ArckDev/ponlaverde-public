package com.upc.arcktech.ponlaverde.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;
import com.upc.arcktech.ponlaverde.entity.Connection;


@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long>{
	
	@Query("select count(c) from Connection c ")
	Integer getCountConnections();
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO"
			+ "(count(c), YEAR(c.accessDate), MONTH(c.accessDate)) " +
	           "FROM Connection c " +
	           "GROUP BY YEAR(c.accessDate), MONTH(c.accessDate) " +
	           "ORDER BY YEAR(c.accessDate) ASC, MONTH(c.accessDate) ASC")
	List<StatCountByYearMonthDTO> countConnectionsGroupByYearAndMonth();
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO"
			+ "(count(c), c.city, c.country) " +
	           "FROM Connection c " +
	           "WHERE (c.country IS NOT null OR c.country != '') AND (c.city IS NOT null OR c.city != '') " +
	           "GROUP BY c.city " +
	           "ORDER BY count(c) DESC")
	List<StatCountByCityCountryDTO> countConnectionsGroupByCity(Pageable pageable);
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO"
			+ "(count(c), c.country) " +
	           "FROM Connection c " +
	           "WHERE (c.country IS NOT null OR c.country != '')" +
	           "GROUP BY c.country " +
	           "ORDER BY count(c) DESC")
	List<StatCountByFieldDTO> countConnectionsGroupByCountry(Pageable pageable);
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO" 
			+ "(count(c), c.deviceType) "
			+ "FROM Connection c "
			+ "GROUP BY c.deviceType " 
			+ "ORDER BY count(c) DESC")
	List<StatCountByFieldDTO> countConnectionsGroupByDeviceType();
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO" 
			+ "(count(c), c.os) "
			+ "FROM Connection c "
			+ "GROUP BY c.os " 
			+ "ORDER BY count(c) DESC")
	List<StatCountByFieldDTO> countConnectionsGroupByOS();
	
	
	
}