package com.upc.arcktech.ponlaverde.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;
import com.upc.arcktech.ponlaverde.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
    
	User findByUsername(String username);
	
	@Query("select count(u) from User u where userType like 'IN'")
	Integer getCountIndividualUser();
	
	@Query("select count(u) from User u where userType like 'SU'")
	Integer getCountSupplierUser();

	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO"
			+ "(count(u), YEAR(u.registerDate), MONTH(u.registerDate)) " +
	           "FROM User u " +
	           "WHERE u.userType = ?1 " +
	           "GROUP BY YEAR(u.registerDate), MONTH(u.registerDate) " +
	           "ORDER BY YEAR(u.registerDate) ASC, MONTH(u.registerDate) ASC")
	List<StatCountByYearMonthDTO> countUsersRegisteredByUsertypeGroupByYearAndMonth(String userType);
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO"
			+ "(count(u), u.city, u.country) " +
	           "FROM User u " +
	           "WHERE (u.country IS NOT null OR u.country != '') AND (u.city IS NOT null OR u.city != '') " +
	           "GROUP BY u.city " +
	           "ORDER BY count(u) DESC")
	List<StatCountByCityCountryDTO> countUsersRegisteredByUsertypeGroupByCityAndCountry(Pageable pageable);
	
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO"
			+ "(count(u), u.city, u.country) " 
			+ "FROM User u "
			+ "INNER JOIN u.userSavedRoof s "
			+ "INNER JOIN s.userSavedRoofDetails d " 
			+ "INNER JOIN d.roof r "
			+ "INNER JOIN d.roof.user ru "
			+ "WHERE ru.idUser = ?1 AND d.selected = true "
			+ "GROUP BY u.city "
			+ "ORDER BY count(u) DESC ")
	List<StatCountByCityCountryDTO> countCityOfUserChosenRoofSupplierInRecommender(Long idUser, Pageable pageable);
	

}
