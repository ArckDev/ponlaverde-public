package com.upc.arcktech.ponlaverde.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.entity.HtmlMessages;

@Repository
public interface HtmlMessagesRepository extends JpaRepository<HtmlMessages, Long> {
	
	HtmlMessages findByServiceId(Integer serviceId);
    


}
