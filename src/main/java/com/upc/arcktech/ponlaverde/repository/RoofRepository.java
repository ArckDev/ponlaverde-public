package com.upc.arcktech.ponlaverde.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;
import java.lang.String;


@Repository
public interface RoofRepository extends JpaRepository<Roof, Long>{
	
	Page<Roof> findAllByStatusIn (List<String> status, Pageable pageable);
	List<Roof> findAllByStatus (String status);
	
	@Query("SELECT count(r) "
			+ "FROM Roof r INNER JOIN r.user u " 
			+ "WHERE u.idUser = ?1 AND lower(r.model) LIKE ?2 ")
	Integer findByModelOwned(Long idUser, String model);
	
	@Query("select distinct r from Roof r right join r.errorReported e where r.idRoof = e.roof.idRoof")
	Page<Roof> findAllRoofsWithErrors (Pageable pageable);	
	Page<Roof> findAllByStatus(String status, Pageable pageable);

	@Query("select count(r) from Roof r where status like 'VAL'")
	Integer getCountValidatedRoofs();
	
	@Query("select count(r) from Roof r")
	Integer countRoofs();
	
	@Query("SELECT count(r) "
			+ "FROM Roof r INNER JOIN r.user u " 
			+ "WHERE u.idUser = ?1 ")
	Integer countOwnedRoofs(Long idUser);
	
	
	@Query("select distinct maker from Roof where status like 'VAL' order by maker ")
	List<String> getAllMakersValidated();
	
	@Query("select distinct maker from Roof where status like 'IND' order by maker ")
	List<String> getAllMakersIndividual();
	
	@Query("select distinct maker from Roof where status not in ('PEN','REJ') order by maker ")
	List<String> getAllMakers();
	
	
	@Query("select max(price) from Roof where status not in ('PEN','REJ') ")
	Double getMaxPrice();
	
	@Query("select min(price) from Roof where status not in ('PEN','REJ') ")
	Double getMinPrice();
	
	
	@Query("select max(weight) from Roof where status not in ('PEN','REJ') ")
	Integer getMaxWeight();
	
	@Query("select min(weight) from Roof where status not in ('PEN','REJ') ")
	Integer getMinWeight();
	
	
	@Query("select max(wall) from Roof where status not in ('PEN','REJ') ")
	Integer getMaxWall();
	
	@Query("select min(wall) from Roof where status not in ('PEN','REJ') ")
	Integer getMinWall();
	
	
	
	
	@Query("select max(price) from Roof where status like 'VAL' ")
	Double getMaxPriceValidated();
	
	@Query("select min(price) from Roof where status like 'VAL' ")
	Double getMinPriceValidated();
	
	
	@Query("select max(weight) from Roof where status like 'VAL' ")
	Integer getMaxWeightValidated();
	
	@Query("select min(weight) from Roof where status like 'VAL' ")
	Integer getMinWeightValidated();
	
	
	@Query("select max(wall) from Roof where status like 'VAL' ")
	Integer getMaxWallValidated();
	
	@Query("select min(wall) from Roof where status like 'VAL' ")
	Integer getMinWallValidated();
	
	
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO"
			+ "(count(r), r.status) "
			+ "FROM Roof r " 
			+ "GROUP BY r.status " 
			+ "ORDER BY count(r) DESC")
	List<StatCountByFieldDTO> countRoofsGroupByStatus();
	
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO"
			+ "(count(r), r.status) "
			+ "FROM Roof r INNER JOIN r.user u " 
			+ "WHERE u.idUser = ?1 "
			+ "GROUP BY r.status " 
			+ "ORDER BY count(r) DESC")
	List<StatCountByFieldDTO> countOwnedRoofsGroupByStatus(Long idUser);

	
}
