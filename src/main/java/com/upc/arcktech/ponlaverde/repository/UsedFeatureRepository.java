package com.upc.arcktech.ponlaverde.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;
import com.upc.arcktech.ponlaverde.dto.StatMaxMinDTO;
import com.upc.arcktech.ponlaverde.entity.UsedFeature;


@Repository
public interface UsedFeatureRepository extends JpaRepository<UsedFeature, Long>{
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO"
			+ "(count(u), YEAR(u.accessDate), MONTH(u.accessDate)) " 
			+ "FROM UsedFeature u "
			+ "WHERE serviceName = ?1 "
			+ "GROUP BY serviceName, YEAR(u.accessDate), MONTH(u.accessDate) "
			+ "ORDER BY serviceName ASC, YEAR(u.accessDate) ASC, MONTH(u.accessDate) ASC")
	List<StatCountByYearMonthDTO> countUsedFeatureGroupByYearAndMonth(String serviceName);
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatMaxMinDTO"
			+ "(MAX(YEAR(u.accessDate)), MIN(YEAR(u.accessDate))) " 
			+ "FROM UsedFeature u ")
	StatMaxMinDTO maxAndMinYear();	
	
	@Query("SELECT "
			+ "(MAX(MONTH(u.accessDate))) " 
			+ "FROM UsedFeature u "
			+ "WHERE YEAR(u.accessDate) = ?1")
	Integer maxMonthForYear(Integer year);
	
	@Query("SELECT "
			+ "(MIN(MONTH(u.accessDate))) " 
			+ "FROM UsedFeature u "
			+ "WHERE YEAR(u.accessDate) = ?1")
	Integer minMonthForYear(Integer year);

	
}