package com.upc.arcktech.ponlaverde.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.dto.StatAverageRatedValuesDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoof;

@Repository
public interface UserSavedRoofRepository extends JpaRepository<UserSavedRoof, Long> {


	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO"
			+ "(count(u), r.model, r.maker, r.type) " 
			+ "FROM UserSavedRoofDetails u INNER JOIN u.roof r "
			+ "WHERE u.selected = true "
			+ "GROUP BY r.model " + "ORDER BY count(u) DESC")
	List<StatCountByMakerModelDTO> countChoosenRoofsInRecommender(Pageable pageable);
	
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO"
			+ "(count(u), r.model, r.maker, r.type) " 
			+ "FROM UserSavedRoofDetails u INNER JOIN u.roof r "
			+ "GROUP BY r.model " + "ORDER BY count(u) DESC")
	List<StatCountByMakerModelDTO> countSuggestedRoofsInRecommender(Pageable pageable);
	
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO" 
			+ "(count(d), d.climate) "
			+ "FROM UserSavedRoof d " 
			+ "GROUP BY d.climate " 
			+ "ORDER BY count(d) DESC")
	List<StatCountByFieldDTO> countClimatesSelectedInRecommender();
    
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO" 
			+ "(count(d), d.slope) "
			+ "FROM UserSavedRoof d " 
			+ "GROUP BY d.slope " 
			+ "ORDER BY count(d) DESC")
	List<StatCountByFieldDTO> countSlopeSelectedInRecommender();
    
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatAverageRatedValuesDTO"
			+ "(avg(u.aesthetics), avg(u.implantation), avg(u.maintenance), avg(u.price), avg(u.regulation), "
			+ "avg(u.storage), avg(u.thermal), avg(u.traffic), avg(u.wall), avg(u.weight), avg(u.winds)) " +
	           "FROM UserSavedRoof u")
	StatAverageRatedValuesDTO averageRatedVariablesInRecommender();
	
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO" 
			+ "(count(u), r.model, r.maker, r.type) "
			+ "FROM UserSavedRoofDetails d INNER JOIN d.roof r INNER JOIN d.roof.user u " 
			+ "WHERE u.idUser = ?1 AND d.selected = true "
			+ "GROUP BY r.model " 
			+ "ORDER BY count(d) DESC")
	List<StatCountByMakerModelDTO> countSupplierSelectedRoofsGroupByModelInRecommender(Long idUser, Pageable pageable);
	
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO" 
			+ "(count(u), r.model, r.maker, r.type) "
			+ "FROM UserSavedRoofDetails d INNER JOIN d.roof r INNER JOIN d.roof.user u " 
			+ "WHERE u.idUser = ?1 "
			+ "GROUP BY r.model " 
			+ "ORDER BY count(d) DESC")
	List<StatCountByMakerModelDTO> countSupplierRoofsGroupByModelInRecommender(Long idUser, Pageable pageable);
	



}