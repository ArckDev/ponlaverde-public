package com.upc.arcktech.ponlaverde.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.entity.FeatureStatus;


@Repository
public interface FeatureStatusRepository extends JpaRepository<FeatureStatus, Long>{
	
	FeatureStatus findByName(String name);

}