package com.upc.arcktech.ponlaverde.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.entity.UserTemp;

@Repository
public interface UserTempRepository extends JpaRepository<UserTemp, Long>{
    
	UserTemp findByUsernameAndSKey(String username, String sKey);

}
