package com.upc.arcktech.ponlaverde.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;
import com.upc.arcktech.ponlaverde.entity.Access;


@Repository
public interface AccessRepository extends JpaRepository<Access, Long>{
	
	@Query("select count(c) from Access c ")
	Integer getCountAccess();
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO"
			+ "(count(c), YEAR(c.accessDate), MONTH(c.accessDate)) " +
	           "FROM Access c " +
	           "GROUP BY YEAR(c.accessDate), MONTH(c.accessDate) " +
	           "ORDER BY YEAR(c.accessDate) ASC, MONTH(c.accessDate) ASC")
	List<StatCountByYearMonthDTO> countAccessGroupByYearAndMonth();
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO"
			+ "(count(c), c.city) " +
	           "FROM Access c " +
	           "WHERE (c.country IS NOT null OR c.country != '') AND (c.city IS NOT null OR c.city != '') " +
	           "GROUP BY c.city " +
	           "ORDER BY count(c) DESC")
	List<StatCountByCityCountryDTO> countAccessGroupByCity(Pageable pageable);
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO" 
			+ "(count(c), c.deviceType) "
			+ "FROM Access c "
			+ "GROUP BY c.deviceType " 
			+ "ORDER BY count(c) DESC")
	List<StatCountByFieldDTO> countAccessGroupByDeviceType();
	
	@Query("SELECT new com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO" 
			+ "(count(c), c.os) "
			+ "FROM Access c "
			+ "GROUP BY c.os " 
			+ "ORDER BY count(c) DESC")
	List<StatCountByFieldDTO> countAccessGroupByOS();
	
	
	
}