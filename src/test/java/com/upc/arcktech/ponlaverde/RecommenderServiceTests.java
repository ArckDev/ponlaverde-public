package com.upc.arcktech.ponlaverde;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upc.arcktech.ponlaverde.dto.BestRoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.dto.UserRoofRatingDTO;
import com.upc.arcktech.ponlaverde.dto.UserSavedRoofDTO;
import com.upc.arcktech.ponlaverde.entity.User;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoofDetails;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.repository.UserSavedRoofRepository;
import com.upc.arcktech.ponlaverde.service.RecommenderService;
import com.upc.arcktech.ponlaverde.transformer.UserSavedRoofDetailsTransformer;
import com.upc.arcktech.ponlaverde.transformer.UserSavedRoofTransformer;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RecommenderServiceTests {
	
	@Autowired
	RecommenderService recommenderService;
	@Autowired
	UserSavedRoofRepository userSavedRoofRepository;
	@Autowired
	UserRepository userRepository;

	@Test
	public void finalWeighingMapSumEquals100() {
		
		UserRoofRatingDTO userRoofRatingDTO = initUserRoofRatingDTO();

		Double checkValue = recommenderService.checkValue(userRoofRatingDTO);

		assertEquals(new Double(100), checkValue);
	}
	
	@Test
	public void recommenderProcessAllValidRate() {
		
		UserRoofRatingDTO userRoofRatingDTO = initUserRoofRatingDTO();
		UserDTO userDTO = initUserDTO();
		
		List<BestRoofDTO> roofToAnalizeList = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);

		Boolean rate = true;
		
		for(BestRoofDTO bestRoofDTO: roofToAnalizeList) {
			if(bestRoofDTO.getRoofDTO().getRate() == null) {
				rate = false;
				break;
			}
		}
		
		assumeTrue(rate);
		assertTrue(Integer.valueOf(userRoofRatingDTO.getResults()).equals(roofToAnalizeList.size()));
	}

	
	@Test
	public void testRoofDTOtoUserSavedRoofDetails() {
		
		UserRoofRatingDTO userRoofRatingDTO = initUserRoofRatingDTO();
		UserDTO userDTO = initUserDTO();
		
		List<BestRoofDTO> bestRoofs = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);
		
		List<UserSavedRoofDetails> userSavedRoofDetailsList = UserSavedRoofDetailsTransformer.bestRoofDTOtoUserSavedRoofDetails(bestRoofs);

		assertNotNull(userSavedRoofDetailsList);
	}
	
	@Test
	public void testSaveRecommendation() {
		
		UserRoofRatingDTO userRoofRatingDTO = initUserRoofRatingDTO();
		UserSavedRoofDTO userSavedRoofDTO = initUserSavedRoofDTO();
		UserDTO userDTO = initUserDTO();
		
		List<BestRoofDTO> bestRoofs = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);
		
		User user = recommenderService.saveRecommendation(bestRoofs, userSavedRoofDTO, userDTO);
		
		assertNotNull(user.getUserSavedRoof());
	}
	
	@Test
	public void testSaveBatchRecommendation() {
		
		UserRoofRatingDTO userRoofRatingDTO = initUserRoofRatingDTO();
		UserSavedRoofDTO userSavedRoofDTO = initUserSavedRoofDTO();
		UserDTO userDTO = initUserDTO();
		
		List<BestRoofDTO> bestRoofs = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);
		recommenderService.saveRecommendation(bestRoofs, userSavedRoofDTO, userDTO);
		
		List<BestRoofDTO> bestRoofs1 = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);
		recommenderService.saveRecommendation(bestRoofs1, userSavedRoofDTO, userDTO);
		
		List<BestRoofDTO> bestRoofs2 = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);
		recommenderService.saveRecommendation(bestRoofs2, userSavedRoofDTO, userDTO);
		
		List<BestRoofDTO> bestRoofs3 = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);
		recommenderService.saveRecommendation(bestRoofs3, userSavedRoofDTO, userDTO);
		
		List<BestRoofDTO> bestRoofs4 = recommenderService.recommenderProcess(userDTO, userRoofRatingDTO);
		User user4 = recommenderService.saveRecommendation(bestRoofs4, userSavedRoofDTO, userDTO);
		
		
		assertNotNull(user4.getUserSavedRoof());
	}
	
//	@Test
//	public void testUserSavedRoofs() {
//		
//		UserDTO userDTO = initUserDTO();
//		User userBefore = userRepository.findOne(userDTO.getIdUser());
//		
//		assertFalse(userBefore.getUserSavedRoof().isEmpty());
//
//	}
	

//	@Test
//	public void testDeleteRecommendation() {
//		
//		Boolean delete = false;
//		
//		UserDTO userDTO = initUserDTO();
//		User userBefore = userRepository.findOne(userDTO.getIdUser());
//		
//		if(userBefore.getUserSavedRoof() != null) {
//			
//			Integer roofsSavedBefore = userBefore.getUserSavedRoof().size();
//
//			recommenderService.deleteRecommendation(UserSavedRoofTransformer.entityToDTO(userBefore.getUserSavedRoof().get(0)), userDTO.getIdUser());
//			
//			User userAfter = userRepository.findOne(userDTO.getIdUser());
//			Integer roofsSavedAfter = userAfter.getUserSavedRoof().size();
//			
//			if(roofsSavedAfter < roofsSavedBefore) {
//				delete = true;
//			}
//			
//		} else {
//			delete = true;
//		}
//		
//		assumeTrue(delete);
//
//	}
	

	private UserRoofRatingDTO initUserRoofRatingDTO() {
	
		UserRoofRatingDTO userRoofRatingDTO = new UserRoofRatingDTO();
		userRoofRatingDTO.setClimate("MET");
		userRoofRatingDTO.setSlope("FLA");
		
		userRoofRatingDTO.setAesthetics(5);
		userRoofRatingDTO.setImplantation(4);
		userRoofRatingDTO.setMaintenance(8);
		userRoofRatingDTO.setPrice(10);
		userRoofRatingDTO.setRegulation(0);
		userRoofRatingDTO.setStorage(7);
		userRoofRatingDTO.setThermal(8);
		userRoofRatingDTO.setTraffic(4);
		userRoofRatingDTO.setWall(5);
		userRoofRatingDTO.setWeight(7);
		userRoofRatingDTO.setWinds(8);
		
		userRoofRatingDTO.setResults("5");
		
		return userRoofRatingDTO;
	}
	
	private UserSavedRoofDTO initUserSavedRoofDTO() {
		
		UserSavedRoofDTO userSavedRoofDTO = new UserSavedRoofDTO();
		userSavedRoofDTO.setClimate("MET");
		userSavedRoofDTO.setSlope("FLA");
		
		userSavedRoofDTO.setAesthetics(8);
		userSavedRoofDTO.setImplantation(4);
		userSavedRoofDTO.setMaintenance(8);
		userSavedRoofDTO.setPrice(10);
		userSavedRoofDTO.setRegulation(0);
		userSavedRoofDTO.setStorage(7);
		userSavedRoofDTO.setThermal(8);
		userSavedRoofDTO.setTraffic(4);
		userSavedRoofDTO.setWall(5);
		userSavedRoofDTO.setWeight(7);
		userSavedRoofDTO.setWinds(8);
		
		userSavedRoofDTO.setSavedName("Test");
		userSavedRoofDTO.setResults(7);
		
		return userSavedRoofDTO;
	}
	
	
	private UserDTO initUserDTO() {
		
		UserDTO userDTO = new UserDTO();
		userDTO.setIdUser(1L);
		
		return userDTO;
	}
	
	
}
