package com.upc.arcktech.ponlaverde.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.upc.arcktech.ponlaverde.BaseTest;
import com.upc.arcktech.ponlaverde.constants.UserRole;
import com.upc.arcktech.ponlaverde.dto.StatAverageRatedValuesDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;
import com.upc.arcktech.ponlaverde.dto.StatListAndTotalDTO;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoof;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoofDetails;
import com.upc.arcktech.ponlaverde.repository.AccessRepository;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.repository.UserSavedRoofRepository;
import com.upc.arcktech.ponlaverde.service.impl.DashboardServiceImpl;


public class DashboardServiceTest extends BaseTest{
	
	@Mock
	UserSavedRoofRepository userSavedRoofRepository;
	@Mock
	UserRepository userRepository;
	@Mock
	AccessRepository accessRepository;
	@InjectMocks
	DashboardServiceImpl dashboardService;
	

	@Test
	public void individualUserRegistered() {
		
		List<StatCountByYearMonthDTO> indUsersRegister = initStatCountByYearMonthDTO();

		when(userRepository
				.countUsersRegisteredByUsertypeGroupByYearAndMonth(UserRole.INDIVIDUAL.getCode())
		).thenReturn(indUsersRegister);
		
		when(userRepository.getCountIndividualUser()).thenReturn(23);
		
		@SuppressWarnings("rawtypes")
		StatListAndTotalDTO response = dashboardService.individualUserRegistered();
		
		assertNotNull(response);
	}


	@Test
	public void loginToSystem() {
		
		List<StatCountByYearMonthDTO> logins = initStatCountByYearMonthDTO();
		
		when(accessRepository.countAccessGroupByYearAndMonth()).thenReturn(logins);
		when(accessRepository.getCountAccess()).thenReturn(23);

		@SuppressWarnings("rawtypes")
		StatListAndTotalDTO response = dashboardService.loginsToSystem();
		
		assertNotNull(response);
		assertTrue(response.getTotal() == 23);
	}
	
	
	@Test
	public void roofSlopeSelectedInRecommender_shouldMapValues() {
		
		List<StatCountByFieldDTO> list = new ArrayList<>();
		StatCountByFieldDTO dto1 = new StatCountByFieldDTO();
		dto1.setCount(25L);
		dto1.setField("FLA");
		list.add(dto1);
		StatCountByFieldDTO dto2 = new StatCountByFieldDTO();
		dto2.setCount(5L);
		dto2.setField("SLO");
		list.add(dto2);
		
		when(userSavedRoofRepository.countSlopeSelectedInRecommender()).thenReturn(list);
		
		List<StatCountByFieldDTO> response = dashboardService.roofSlopeSelectedInRecommender();
		
		assertNotNull(response);
		assertTrue(response.get(0).getField().equals("Plana") || response.get(0).getField().equals("Inclinada") ||
				response.get(0).getField().equals("Flat") || response.get(0).getField().equals("Sloping"));
	}
	
	@Test
	public void supplierUserRegistered() {
		// TODO Auto-generated method stub

	}

	@Test
	public void connectionsToSystem() {
		// TODO Auto-generated method stub

	}

	@Test
	public void currentConnectedUsers() {
		// TODO Auto-generated method stub

	}

	@Test
	public void mostPopularChosenRoofsInRecommender() {
		// TODO Auto-generated method stub

	}

	@Test
	public void successRateInRecommender_shouldReturnValue() {

		when(userSavedRoofRepository.findAll()).thenReturn(createMockUserSavedRoof());
		
		Integer successRate = dashboardService.successRateInRecommender();
		
		assertNotNull(successRate);
		assertTrue(successRate >= 0);
		System.out.println("Success Rate: " + successRate);
	}

	@Test
	public void connectionDevice() {
		// TODO Auto-generated method stub

	}

	@Test
	public void commonCitiesConnection() {
		// TODO Auto-generated method stub

	}

	@Test
	public void usersByCityAndCountry() {
		// TODO Auto-generated method stub

	}

	@Test
	public void mostUsedFeature() {
		// TODO Auto-generated method stub

	}

	@Test
	public void mostValuedVariablesInRecommender() {
		
		StatAverageRatedValuesDTO values = new StatAverageRatedValuesDTO();
		values.setAesthetics(3d);
		values.setImplantation(6d);
		values.setMaintenance(7d);
		values.setPrice(9d);
		values.setRegulation(3d);
		values.setStorage(4d);
		values.setThermal(8d);
		values.setTraffic(6d);
		values.setWall(5d);
		values.setWeight(4d);
		values.setWinds(1d);

		when(userSavedRoofRepository.averageRatedVariablesInRecommender()).thenReturn(values);
		
		List<StatCountByFieldDTO> list = dashboardService.averageScoreVariablesInRecommender();
		
		assertNotNull(list);

	}

	@Test
	public void systemTotalRoofs() {
		// TODO Auto-generated method stub

	}

	@Test
	public void supplierPopularChosenRoofInRecommender() {
		// TODO Auto-generated method stub

	}

	@Test
	public void supplierCommonRoofsInRecommender() {
		// TODO Auto-generated method stub

	}

	@Test
	public void supplierCommonUserCitiesInRecommender() {
		// TODO Auto-generated method stub

	}

	@Test
	public void supplierTotalRoofs() {
		// TODO Auto-generated method stub

	}

	private List<StatCountByYearMonthDTO> initStatCountByYearMonthDTO() {
		List<StatCountByYearMonthDTO> indUsersRegister = new ArrayList<>();
		StatCountByYearMonthDTO dto1 = new StatCountByYearMonthDTO(5L, 2019, 2);
		StatCountByYearMonthDTO dto2 = new StatCountByYearMonthDTO(8L, 2019, 1);
		StatCountByYearMonthDTO dto3 = new StatCountByYearMonthDTO(2L, 2018, 12);
		StatCountByYearMonthDTO dto4 = new StatCountByYearMonthDTO(3L, 2018, 11);
		StatCountByYearMonthDTO dto5 = new StatCountByYearMonthDTO(5L, 2018, 10);
		indUsersRegister.add(dto5);
		indUsersRegister.add(dto4);
		indUsersRegister.add(dto3);
		indUsersRegister.add(dto2);
		indUsersRegister.add(dto1);
		return indUsersRegister;
	}
	
	private List<UserSavedRoof> createMockUserSavedRoof() {
		List<UserSavedRoof> userSavedRoofList = new ArrayList<>();
		
		for(int i = 0; i <= 10; i++) {
			
			UserSavedRoof userSavedRoof = new UserSavedRoof();
			List<UserSavedRoofDetails> detailsList = new ArrayList<>();
			
			for(int j = 0; j <= 10; j++) {

				UserSavedRoofDetails details = new UserSavedRoofDetails();

				if(j == 0) {
					int randomNum = ThreadLocalRandom.current().nextInt(80, 100);
					details.setFinalRate(randomNum);
					details.setSelected(true);
				} else {
					int randomNum = ThreadLocalRandom.current().nextInt(0, 90);
					details.setFinalRate(randomNum);
					details.setSelected(false);
				}
				
				detailsList.add(details);
			}
			userSavedRoof.setUserSavedRoofDetails(detailsList);
			userSavedRoofList.add(userSavedRoof);
		}
		return userSavedRoofList;
	}
}
