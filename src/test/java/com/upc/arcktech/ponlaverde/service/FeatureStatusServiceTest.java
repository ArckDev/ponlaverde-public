package com.upc.arcktech.ponlaverde.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.upc.arcktech.ponlaverde.BaseTest;
import com.upc.arcktech.ponlaverde.dto.FeatureStatusDTO;
import com.upc.arcktech.ponlaverde.dto.FeatureStatusListDTO;
import com.upc.arcktech.ponlaverde.entity.FeatureStatus;
import com.upc.arcktech.ponlaverde.repository.FeatureStatusRepository;
import com.upc.arcktech.ponlaverde.service.impl.FeatureStatusServiceImpl;

public class FeatureStatusServiceTest extends BaseTest {
	
	@Mock
	FeatureStatusRepository featureStatusRepository;
	@InjectMocks
	FeatureStatusServiceImpl featureStatusService;
	
	
	@Test
	public void getStatusList_shouldRetrieveAllTheStatus() {

		when(featureStatusRepository.findAll()).thenReturn(initFeaturesStatusList(true));
		
		List<FeatureStatusDTO> featuresStatusDTOList = featureStatusService.getAllFeaturesStatus();
		
		assertNotNull(featuresStatusDTOList);
		assertEquals(true, featuresStatusDTOList.get(0).getActive());
	}

	@Test
	public void updateFeaturesStatus_shouldUpdateEntities() {
		
		Boolean statusUpdatedTo = true;
		
		FeatureStatusListDTO featureStatusListDTO = initFeaturesStatusListDTO();
		List<FeatureStatus> featuresStatusList = initFeaturesStatusList(!statusUpdatedTo);
		
		when(featureStatusRepository.findAll()).thenReturn(featuresStatusList);
		when(featureStatusRepository.save(featuresStatusList)).thenReturn(initFeaturesStatusList(statusUpdatedTo));
		
		List<FeatureStatusDTO> featuresStatusListUpdated = featureStatusService.updateFeaturesStatus(featureStatusListDTO);
		
		assertNotNull(featuresStatusListUpdated);
		for(FeatureStatusDTO featuresStatusDTO: featuresStatusListUpdated) {
			assertEquals(statusUpdatedTo, featuresStatusDTO.getActive());
		}
		
	}
	
	@Test(expected = NullPointerException.class)
	public void updateFeaturesStatus_shouldThrowException() {
		
		List<FeatureStatus> featuresStatusList = initFeaturesStatusList(true);
		
		when(featureStatusRepository.findAll()).thenReturn(featuresStatusList);
		
		featureStatusService.updateFeaturesStatus(null);
	}
	
	
	
	private List<FeatureStatus> initFeaturesStatusList(Boolean status) {
		List<FeatureStatus> featuresStatusList = new ArrayList<>();
		featuresStatusList.add(new FeatureStatus("CAT", status));
		featuresStatusList.add(new FeatureStatus("DAS", status));
		featuresStatusList.add(new FeatureStatus("REC", status));
		featuresStatusList.add(new FeatureStatus("ERR", status));
		featuresStatusList.add(new FeatureStatus("SET", status));
		featuresStatusList.add(new FeatureStatus("VAL", status));
		featuresStatusList.add(new FeatureStatus("SAV", status));
		return featuresStatusList;
	}
	
	private List<FeatureStatusDTO> initFeaturesStatusDTOList(Boolean status) {
		List<FeatureStatusDTO> featuresStatusDTOList = new ArrayList<>();
		featuresStatusDTOList.add(new FeatureStatusDTO("CAT", status));
		featuresStatusDTOList.add(new FeatureStatusDTO("DAS", status));
		featuresStatusDTOList.add(new FeatureStatusDTO("REC", status));
		featuresStatusDTOList.add(new FeatureStatusDTO("ERR", status));
		featuresStatusDTOList.add(new FeatureStatusDTO("SET", status));
		featuresStatusDTOList.add(new FeatureStatusDTO("VAL", status));
		featuresStatusDTOList.add(new FeatureStatusDTO("SAV", status));
		return featuresStatusDTOList;
	}
	
	private FeatureStatusListDTO initFeaturesStatusListDTO() {
		
		FeatureStatusListDTO featureStatusListDTO = new FeatureStatusListDTO();
		
		List<String> featuresStatusDTOList = new ArrayList<>();
		featuresStatusDTOList.add("CAT");
		featuresStatusDTOList.add("DAS");
		featuresStatusDTOList.add("REC");
		featuresStatusDTOList.add("ERR");
		featuresStatusDTOList.add("SET");
		featuresStatusDTOList.add("VAL");
		featuresStatusDTOList.add("SAV");
		
		featureStatusListDTO.setFeatureStatus(featuresStatusDTOList);
		
		return featureStatusListDTO;
	}
	
}
