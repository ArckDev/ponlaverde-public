package com.upc.arcktech.ponlaverde.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.upc.arcktech.ponlaverde.BaseTest;
import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.dto.UsedFeatureDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;

public class UsedFeatureServiceTest extends BaseTest {
	
	@Autowired
	UsedFeatureService usedFeatureService;
	
	
	@Test
	public void addAccess() {
		
		UserDTO userDTO = new UserDTO();
		userDTO.setIdUser(3L);
		
		UsedFeatureDTO usedFeatureDTO = new UsedFeatureDTO();
		usedFeatureDTO.setServiceName(FeatureName.CATALOGUE.getCode());
		usedFeatureDTO.setSession("jsdsdjfbsdkfhyjkfsdf44sdf4sdfsdf");
		usedFeatureDTO.setUserDTO(userDTO);

		usedFeatureService.addAccess(usedFeatureDTO);
	}

}
