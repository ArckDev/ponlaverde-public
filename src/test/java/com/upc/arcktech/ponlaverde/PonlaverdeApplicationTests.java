package com.upc.arcktech.ponlaverde;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.User;
import com.upc.arcktech.ponlaverde.repository.RoofRepository;
import com.upc.arcktech.ponlaverde.repository.UserRepository;
import com.upc.arcktech.ponlaverde.service.RoofService;
import com.upc.arcktech.ponlaverde.service.UserService;
import com.upc.arcktech.ponlaverde.transformer.UserTransformer;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PonlaverdeApplicationTests {
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserService userService;
	@Autowired
	RoofService roofService;
	@Autowired
	RoofRepository roofRepository;

	
	@Test
	public void userRepositoryFindByExistingUsername() {
		
		User user = userRepository.findByUsername("a@a.com");
		
		assertNotNull(user);
	}
	
	@Test
	public void userRepositoryFindByNonExistingUsername() {
		
		User user = userRepository.findByUsername("d@a.com");
		
		assertNull(user);
	}
	
	@Test
	public void userServiceFindByUsernameNull() {
		
		UserDTO userDTO = userService.getUserByUsername("a@a.com");
		
		assertNotNull(userDTO);
	}
	

	
	@Test
	public void userSavedRoofDTONotNull() {
		
		UserDTO userDTO = userService.getUserByUsername("a@a.com");

		assertNotNull(userDTO.getUserSavedRoofDTO());
	}
	
	@Test
	public void roofNotNull() {
		
		RoofDTO roofDTO = roofService.findRoofById(1L);

		assertNotNull(roofDTO);
	}
	
	


}
