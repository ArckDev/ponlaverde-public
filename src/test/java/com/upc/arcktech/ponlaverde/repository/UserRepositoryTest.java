package com.upc.arcktech.ponlaverde.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upc.arcktech.ponlaverde.constants.UserRole;
import com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
	
	@Autowired
	UserRepository userRepository;
	
	@Test
	public void countUsersRegisteredByUsertypeGroupByYearAndMonth_shouldReturnAllResults() {
		
		List<StatCountByYearMonthDTO> registersList = userRepository.
				countUsersRegisteredByUsertypeGroupByYearAndMonth(UserRole.INDIVIDUAL.getCode());

		assertNotNull(registersList);
		assertTrue(registersList.size() >= 0);
	}
	
	@Test
	public void countUsersRegisteredByUsertypeGroupByCityAndCountry_shouldReturnAllResults() {
		
		List<StatCountByCityCountryDTO> usersByCityCountry = userRepository.countUsersRegisteredByUsertypeGroupByCityAndCountry(null);
	
		assertNotNull(usersByCityCountry);
		assertTrue(usersByCityCountry.size() > 0);
	
	}
	
	@Test
	public void countCityOfUserChosenRoofSupplierInRecommender_shouldReturnValues() {
		
		List<StatCountByCityCountryDTO> selectedRoofs = userRepository.countCityOfUserChosenRoofSupplierInRecommender(2L, null);
		
		assertNotNull(selectedRoofs);
		assertTrue(selectedRoofs.get(0).getCount() >= 0);
	}

}
