package com.upc.arcktech.ponlaverde.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upc.arcktech.ponlaverde.dto.StatCountByCityCountryDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConnectionRepositoryTest {

	@Autowired
	ConnectionRepository connectionRepository;
	
	@Test
	public void countConnectionsGroupByYearAndMonth_shouldReturnAllResults() {
		
		List<StatCountByYearMonthDTO> connectionsList = connectionRepository
				.countConnectionsGroupByYearAndMonth();
		
		assertNotNull(connectionsList);
		assertTrue(connectionsList.size() >= 0);
	}	
	
	@Test
	public void countConnectionsGroupByCity_shouldReturnAllCitiesOrderedDesc() {
		
		List<StatCountByCityCountryDTO> connectionsByCity = connectionRepository.countConnectionsGroupByCity(null);
		
		assertNotNull(connectionsByCity);
		assertTrue(connectionsByCity.get(0).getCount() > 0);
	}	
	
	@Test
	public void countConnectionsGroupByDeviceType_shouldReturnAllDeviceTypeOrderedCountDesc() {
		
		List<StatCountByFieldDTO> devices = connectionRepository.countConnectionsGroupByDeviceType();
		
		assertNotNull(devices);
		assertTrue(devices.get(0).getCount() >= 0);
	}
	
	@Test
	public void countConnectionsGroupByOS_shouldReturnAllOSOrderedCountDesc() {
		
		List<StatCountByFieldDTO> os = connectionRepository.countConnectionsGroupByOS();
		
		assertNotNull(os);
		assertTrue(os.get(0).getCount() >= 0);
	}
	
}
