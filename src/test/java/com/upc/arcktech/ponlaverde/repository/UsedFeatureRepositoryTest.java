package com.upc.arcktech.ponlaverde.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.upc.arcktech.ponlaverde.BaseTest;
import com.upc.arcktech.ponlaverde.constants.FeatureName;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldYearMonthDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByYearMonthDTO;
import com.upc.arcktech.ponlaverde.dto.StatMaxMinDTO;
import com.upc.arcktech.ponlaverde.entity.UsedFeature;
import com.upc.arcktech.ponlaverde.entity.User;

public class UsedFeatureRepositoryTest extends BaseTest {
	
	@Autowired
	UsedFeatureRepository usedFeatureRepository;
	@Autowired
	UserRepository userRepository;
	
	@Test
	public void addAccess() {
		
		User user = userRepository.findByUsername("c@c.com");
		
		UsedFeature usedFeature = new UsedFeature();
		usedFeature.setAccessDate(new Date());
		usedFeature.setServiceName(FeatureName.CATALOGUE.getCode());
		usedFeature.setSession("jsdsdjfbsdkfhyjkfsdf44sdf4sdfsdf");
		usedFeature.setUser(user);
		
		UsedFeature response = usedFeatureRepository.save(usedFeature);
		
		assertNotNull(response);
		assertSame("jsdsdjfbsdkfhyjkfsdf44sdf4sdfsdf", response.getSession());
		
		
	}
	
	@Test
	public void countUsedFeatureGroupByYearAndMonth() {
		
		String serviceName = "CAT";
		
		List<StatCountByYearMonthDTO> list = usedFeatureRepository.countUsedFeatureGroupByYearAndMonth(serviceName);
		
		assertNotNull(list);
		assertTrue(list.size() > 0);
	}
	
	@Test
	public void maxAndMinYear() {
		
		StatMaxMinDTO dto = usedFeatureRepository.maxAndMinYear();
		
		assertNotNull(dto);
	}
	
	@Test
	public void maxMonthForYear() {
		
		Integer month = usedFeatureRepository.maxMonthForYear(2019);
		
		assertNotNull(month);
	}
	
	@Test
	public void minMonthForYear() {
		
		Integer month = usedFeatureRepository.minMonthForYear(2019);
		
		assertNotNull(month);
	}
	
	
	

}
