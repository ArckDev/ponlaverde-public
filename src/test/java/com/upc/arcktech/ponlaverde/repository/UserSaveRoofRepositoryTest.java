package com.upc.arcktech.ponlaverde.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upc.arcktech.ponlaverde.dto.StatAverageRatedValuesDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;
import com.upc.arcktech.ponlaverde.dto.StatCountByMakerModelDTO;
import com.upc.arcktech.ponlaverde.entity.UserSavedRoof;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserSaveRoofRepositoryTest {

	@Autowired
	UserSavedRoofRepository userSavedRoofRepository;
	

	@Test
	public void countChoosenRoofs_shouldReturnMax5Results() {
		
		List<StatCountByMakerModelDTO> chosenList = userSavedRoofRepository.countChoosenRoofsInRecommender(new PageRequest(0, 5));
		
		assertNotNull(chosenList);
		assertTrue(chosenList.size() >= 0);
	}
	
	@Test
	public void countChoosenRoofs_shouldReturnAllResults() {
		
		List<StatCountByMakerModelDTO> chosenList = userSavedRoofRepository.countChoosenRoofsInRecommender(null);
		
		assertNotNull(chosenList);
		assertTrue(chosenList.size() >= 0);
	}
	
	@Test
	public void findAll_shouldReturnAllResults() {
		
		List<UserSavedRoof> userSavedRoofList = userSavedRoofRepository.findAll();
		
		assertNotNull(userSavedRoofList);
		assertTrue(userSavedRoofList.size() >= 0);
	}
	
	
	@Test
	public void getAverageRatedVariables_shouldReturnValues() {
		
		StatAverageRatedValuesDTO vars = userSavedRoofRepository.averageRatedVariablesInRecommender();
		
		assertNotNull(vars);
		assertTrue(vars.getAesthetics() >= 0);
	}
	
	
	@Test
	public void countSupplierSelectedRoofsGroupByModel_shouldReturnValues() {
		
		List<StatCountByMakerModelDTO> selectedRoofs = userSavedRoofRepository.countSupplierSelectedRoofsGroupByModelInRecommender(2L, new PageRequest(0, 1));
		
		assertNotNull(selectedRoofs);
		assertTrue(selectedRoofs.get(0).getCount() >= 0);
	}
	
	
	@Test
	public void countClimate_shouldReturnValues() {
		
		List<StatCountByFieldDTO> climates = userSavedRoofRepository.countClimatesSelectedInRecommender();
		
		assertNotNull(climates);
		assertTrue(climates.size() > 1);
	}
	
	
	@Test
	public void countSlope_shouldReturnValues() {
		
		List<StatCountByFieldDTO> slopes = userSavedRoofRepository.countSlopeSelectedInRecommender();
		
		assertNotNull(slopes);
		assertTrue(slopes.size() > 0);
	}

	
	
	

}
