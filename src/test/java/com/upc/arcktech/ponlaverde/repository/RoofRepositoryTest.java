package com.upc.arcktech.ponlaverde.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.upc.arcktech.ponlaverde.BaseTest;
import com.upc.arcktech.ponlaverde.dto.StatCountByFieldDTO;


public class RoofRepositoryTest extends BaseTest {
	
	@Autowired
	RoofRepository roofRepository;


	@Test
	public void countRoofsGroupByStatus_shouldReturnAllRoofs() {
		
		List<StatCountByFieldDTO> roofStatus = roofRepository.countRoofsGroupByStatus();
		
		assertNotNull(roofStatus);
		assertTrue(roofStatus.get(0).getCount() >= 0);

	}
	
	@Test
	public void countOwnedRoofsGroupByStatus_shouldReturnAllRoofs() {
		
		List<StatCountByFieldDTO> ownedRoofStatus = roofRepository.countOwnedRoofsGroupByStatus(2L);
		
		assertNotNull(ownedRoofStatus);
		assertTrue(ownedRoofStatus.get(0).getCount() >= 0);

	}
	
	
	


}
