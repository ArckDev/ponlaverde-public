package com.upc.arcktech.ponlaverde;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.upc.arcktech.ponlaverde.utils.SecretKeyUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SecretKeyUtilsTest {
	
	@Test
	public void generateRandomStringTest() {
		
		String string = SecretKeyUtils.generateRandomString(8);
		
		assertEquals(8, string.length());
	}

}
