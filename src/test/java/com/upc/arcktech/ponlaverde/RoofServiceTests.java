package com.upc.arcktech.ponlaverde;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.upc.arcktech.ponlaverde.dao.RoofDAO;
import com.upc.arcktech.ponlaverde.dto.RoofDTO;
import com.upc.arcktech.ponlaverde.dto.UserDTO;
import com.upc.arcktech.ponlaverde.entity.Roof;
import com.upc.arcktech.ponlaverde.repository.RoofRepository;
import com.upc.arcktech.ponlaverde.service.RoofService;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class RoofServiceTests {
	
	@Autowired
	RoofService roofService;
	@Autowired
	RoofRepository roofRepository;
	@Autowired
	RoofDAO roofDAO;
	
	
	@Test
	public void roofsPagedResultsTest() {
		
		Pageable pageable = new PageRequest(0, 10);
		
		Page<Roof> roofs = roofRepository.findAllByStatus("VAL", pageable);	

//		Page<Roof> roofs = roofRepository.findAll(pageable);
				
		assertEquals(10, roofs.getSize());
		
	}
	
	@Test
	public void roofsPagedResultsCriteriaTest() {
		
		Integer page = 1;
		Integer results = 10;
		Pageable pageable = new PageRequest((page-1), results);
		
		Page<Roof> roofs = roofDAO.filterAllIndividualRoofs(new RoofDTO(), pageable);

//		Page<Roof> roofs = roofRepository.findAll(pageable);
				
		assertEquals(10, roofs.getSize());
		
	}
	
	@Test
	public void findValidatedEnabledAndOwnedRoofsTest() {
		
		Integer page = 1;
		Integer results = 16;
		Pageable pageable = new PageRequest((page-1), results);
		
		UserDTO userDTO = new UserDTO();
		userDTO.setIdUser(5L);		
		
		roofService.findValidatedEnabledAndOwnedRoofs(userDTO, pageable);
		

	}
	

}
